<!DOCTYPE html>
<!--[if lte IE 7]> <html class="ie7 oldie" lang="fr"> <![endif]-->
<!--[if IE 8]> <html class="ie8 oldie" lang="fr"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="fr-FR"> <!--<![endif]-->

	<head>
		{$metas}
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
		
		{$statistiques}
		{$assets}
		{$rss}
	</head>
	
	<body class="home">
		<div id="my-wrapper"> <!-- Conteneur pour menu -->
			<header>
				<div id="top_header" class="text-center">	
					<span>Vous êtes étudiant ? C’est par <a href="http://etudiant.stadium.fr/">ici.</a></span>
				</div>
				<div id="bottom_header" class="inner">					
					<div id="zone-logo">
							<a href="/" id="logo" title="Retour à la page d'accueil">
								<svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 367.9 115.31">
								  <defs>
								    <style>
								      .cls-1 {
								        fill: #05453c;
								      }
								
								      .cls-2 {
								        fill: #c6785b;
								      }
								    </style>
								  </defs>
								  <g>
								    <path class="cls-1" d="m140.67,59.46l1.04-3.63c2.92,1.65,5.99,2.45,8.86,2.45,4.71,0,7.73-1.98,7.73-5.66,0-4.1-3.96-5.33-7.54-6.41-3.96-1.23-9.33-2.88-9.33-8.96,0-5.47,4.29-9.14,11.17-9.14,3.35,0,6.27.9,8.81,2.26l-1.51,3.54c-2.17-1.27-4.57-2.12-7.35-2.12-4.2,0-6.83,1.93-6.83,5.19,0,3.54,3.06,4.52,6.69,5.56,4.9,1.37,10.18,3.58,10.18,9.8,0,5.89-4.71,9.62-12.07,9.62-3.25,0-6.65-.8-9.85-2.5Z"/>
								    <path class="cls-1" d="m196.52,32.36h-11.64v29.18h-4.15v-29.18h-11.69v-3.82h27.48v3.82Z"/>
								    <path class="cls-1" d="m221.88,53.71h-15.55l-2.92,7.82h-4.52l13.24-32.99h4.15l13.06,32.99h-4.52l-2.92-7.82Zm-14.14-3.77h12.73l-6.32-16.64h-.09l-6.32,16.64Z"/>
								    <path class="cls-1" d="m238.94,28.54h11.36c9.38,0,15.04,6.27,15.04,16.5s-5.66,16.5-15.04,16.5h-11.36V28.54Zm11.36,29.18c6.79,0,10.56-4.76,10.56-12.68s-3.77-12.68-10.56-12.68h-7.16v25.36h7.16Z"/>
								    <path class="cls-1" d="m282.49,31.7v26.68h4.24v3.16h-12.63v-3.16h4.24v-26.68h-4.24v-3.16h12.63v3.16h-4.24Z"/>
								    <path class="cls-1" d="m297.62,48.39v-19.84h4.19v19.84c0,6.17,2.97,9.8,8.2,9.8s8.25-3.63,8.25-9.8v-19.84h4.15v19.84c0,8.48-4.67,13.57-12.4,13.57s-12.4-5.09-12.4-13.57Z"/>
								    <path class="cls-1" d="m335.14,28.54h3.91l12.44,21.02h.09l12.49-21.02h3.82v32.99h-4.19l.05-24.98h-.24l-10.94,18.43h-2.31l-10.84-18.43h-.19l.05,24.98h-4.15V28.54Z"/>
								  </g>
								  <path class="cls-1" d="m140.77,72.32h2.63l1.98,5.86h.05l1.91-5.86h2.61l-3.44,8.7h-2.32l-3.42-8.7Z"/>
								  <path class="cls-1" d="m151.05,76.67c0-2.77,1.72-4.52,4.38-4.52s4.39,1.75,4.39,4.52-1.7,4.52-4.39,4.52-4.38-1.75-4.38-4.52Zm6.24,0c0-1.48-.69-2.39-1.86-2.39s-1.84.91-1.84,2.39.67,2.39,1.84,2.39,1.86-.91,1.86-2.39Z"/>
								  <path class="cls-1" d="m161.44,80.46l.55-1.89c.79.45,1.74.71,2.65.71.86,0,1.29-.34,1.29-.81s-.33-.74-1.43-.98c-1.62-.36-2.89-1.03-2.89-2.63s1.29-2.7,3.52-2.7c1.05,0,2.05.21,2.92.67l-.72,1.88c-.64-.41-1.41-.62-2.2-.62-.84,0-1.15.31-1.15.67,0,.41.34.69,1.32.89,1.77.38,3.04,1.1,3.04,2.72,0,1.82-1.48,2.84-3.71,2.84-1.08,0-2.2-.22-3.2-.74Z"/>
								  <path class="cls-1" d="m175.18,72.32h1.91l.1,1.63.1.03c.5-1.13,1.58-1.82,2.87-1.82s2.15.72,2.55,1.84l.09.02c.48-1.15,1.58-1.86,2.94-1.86,1.82,0,2.94,1.29,2.94,3.46v5.4h-2.48v-5c0-1-.45-1.58-1.26-1.58-.91,0-1.79.72-1.79,2.42v4.16h-2.48v-5c0-.98-.43-1.58-1.26-1.58-.89,0-1.77.72-1.77,2.42v4.16h-2.48v-8.7Z"/>
								  <path class="cls-1" d="m190.86,76.67c0-2.77,1.72-4.52,4.38-4.52s4.39,1.75,4.39,4.52-1.7,4.52-4.39,4.52-4.38-1.75-4.38-4.52Zm6.24,0c0-1.48-.69-2.39-1.86-2.39s-1.84.91-1.84,2.39.67,2.39,1.84,2.39,1.86-.91,1.86-2.39Z"/>
								  <path class="cls-1" d="m201.83,72.32h1.91l.1,1.63.1.03c.5-1.13,1.58-1.82,2.87-1.82s2.15.72,2.55,1.84l.09.02c.48-1.15,1.58-1.86,2.94-1.86,1.82,0,2.94,1.29,2.94,3.46v5.4h-2.48v-5c0-1-.45-1.58-1.26-1.58-.91,0-1.79.72-1.79,2.42v4.16h-2.48v-5c0-.98-.43-1.58-1.26-1.58-.89,0-1.77.72-1.77,2.42v4.16h-2.48v-8.7Z"/>
								  <path class="cls-1" d="m217.51,76.62c0-2.77,1.75-4.47,4.35-4.47s3.87,1.74,3.87,4.18c0,.41-.03.77-.09,1.12h-5.57c.21,1.19,1.07,1.79,2.41,1.79.79,0,1.56-.19,2.32-.64l.67,1.75c-1.1.6-2.29.84-3.39.84-2.73,0-4.57-1.65-4.57-4.57Zm5.93-.67c0-1.29-.62-2.05-1.69-2.05s-1.7.88-1.7,2.05h3.39Z"/>
								  <path class="cls-1" d="m227.91,72.32h1.91l.1,1.63.1.03c.5-1.13,1.55-1.82,2.91-1.82,1.87,0,3.08,1.29,3.08,3.46v5.4h-2.48v-5c0-1.01-.46-1.58-1.32-1.58-.96,0-1.82.72-1.82,2.42v4.16h-2.48v-8.7Z"/>
								  <path class="cls-1" d="m244.69,80.61c-.65.4-1.38.58-2.18.58-2.03,0-3.11-1.27-3.11-3.32v-3.63h-1.53v-1.93h1.53l.36-2.32h2.12v2.32h2.51v1.93h-2.51v3.51c0,.83.4,1.29,1.19,1.29.38,0,.74-.1,1.05-.28l.58,1.84Z"/>
								  <path class="cls-1" d="m246.06,80.46l.55-1.89c.79.45,1.74.71,2.65.71.86,0,1.29-.34,1.29-.81s-.33-.74-1.43-.98c-1.62-.36-2.89-1.03-2.89-2.63s1.29-2.7,3.52-2.7c1.05,0,2.05.21,2.92.67l-.72,1.88c-.64-.41-1.41-.62-2.2-.62-.84,0-1.15.31-1.15.67,0,.41.34.69,1.32.89,1.77.38,3.04,1.1,3.04,2.72,0,1.82-1.48,2.84-3.71,2.84-1.08,0-2.2-.22-3.2-.74Z"/>
								  <path class="cls-1" d="m259.32,76.67c0-2.89,1.58-4.51,3.75-4.51,1.19,0,2,.46,2.39,1.07l.05-.02v-5.09h2.48v12.9h-1.93l-.09-1.36-.1-.03c-.48.88-1.38,1.56-2.79,1.56-2.22,0-3.77-1.63-3.77-4.52Zm6.24,0c0-1.5-.69-2.41-1.86-2.41s-1.84.91-1.84,2.41.67,2.39,1.84,2.39,1.86-.91,1.86-2.39Z"/>
								  <path class="cls-1" d="m270.94,68.99h2.7l-1.81,5.07h-1.58l.69-5.07Z"/>
								  <path class="cls-1" d="m274,76.62c0-2.77,1.75-4.47,4.35-4.47s3.87,1.74,3.87,4.18c0,.41-.03.77-.09,1.12h-5.57c.21,1.19,1.07,1.79,2.41,1.79.79,0,1.56-.19,2.32-.64l.67,1.75c-1.1.6-2.29.84-3.39.84-2.73,0-4.57-1.65-4.57-4.57Zm5.93-.67c0-1.29-.62-2.05-1.69-2.05s-1.7.88-1.7,2.05h3.39Z"/>
								  <path class="cls-1" d="m283.29,80.9l2.97-4.4-2.73-4.08.02-.1h2.75l1.65,2.73h.03l1.69-2.73h2.56l.02.1-2.79,4.08,2.98,4.4-.02.12h-2.75l-1.86-2.91h-.05l-1.77,2.91h-2.67l-.03-.12Z"/>
								  <path class="cls-1" d="m293.54,76.67c0-2.77,1.82-4.52,4.59-4.52.91,0,1.87.17,2.72.62l-.76,1.91c-.6-.33-1.22-.45-1.79-.45-1.36,0-2.22.83-2.22,2.44s.84,2.44,2.3,2.44c.67,0,1.31-.15,1.89-.46l.71,1.84c-.89.5-1.91.71-2.89.71-2.72,0-4.56-1.6-4.56-4.52Z"/>
								  <path class="cls-1" d="m302.39,76.62c0-2.77,1.75-4.47,4.35-4.47s3.87,1.74,3.87,4.18c0,.41-.03.77-.09,1.12h-5.57c.21,1.19,1.07,1.79,2.41,1.79.79,0,1.56-.19,2.32-.64l.67,1.75c-1.1.6-2.29.84-3.39.84-2.73,0-4.57-1.65-4.57-4.57Zm5.93-.67c0-1.29-.62-2.05-1.69-2.05s-1.7.88-1.7,2.05h3.39Z"/>
								  <path class="cls-1" d="m312.79,72.32h1.91l.1,1.48.09.03c.52-.98,1.55-1.68,2.96-1.68,2.13,0,3.59,1.65,3.59,4.52s-1.51,4.52-3.63,4.52c-1.15,0-1.98-.46-2.48-1.19l-.07.02v4.44h-2.48v-12.14Zm6.12,4.35c0-1.5-.69-2.41-1.86-2.41s-1.84.91-1.84,2.41.69,2.39,1.84,2.39,1.86-.91,1.86-2.39Z"/>
								  <path class="cls-1" d="m329.68,80.61c-.65.4-1.38.58-2.18.58-2.03,0-3.11-1.27-3.11-3.32v-3.63h-1.53v-1.93h1.53l.36-2.32h2.12v2.32h2.51v1.93h-2.51v3.51c0,.83.4,1.29,1.19,1.29.38,0,.74-.1,1.05-.28l.58,1.84Z"/>
								  <path class="cls-1" d="m331.41,69.74c0-.84.53-1.32,1.46-1.32s1.44.48,1.44,1.32-.55,1.32-1.44,1.32-1.46-.48-1.46-1.32Zm.22,2.58h2.48v8.7h-2.48v-8.7Z"/>
								  <path class="cls-1" d="m336.31,76.67c0-2.77,1.72-4.52,4.38-4.52s4.39,1.75,4.39,4.52-1.7,4.52-4.39,4.52-4.38-1.75-4.38-4.52Zm6.24,0c0-1.48-.69-2.39-1.86-2.39s-1.84.91-1.84,2.39.67,2.39,1.84,2.39,1.86-.91,1.86-2.39Z"/>
								  <path class="cls-1" d="m347.28,72.32h1.91l.1,1.63.1.03c.5-1.13,1.55-1.82,2.91-1.82,1.87,0,3.08,1.29,3.08,3.46v5.4h-2.48v-5c0-1.01-.46-1.58-1.32-1.58-.96,0-1.82.72-1.82,2.42v4.16h-2.48v-8.7Z"/>
								  <g>
								    <path class="cls-1" d="m67.8,19.88l-.69,4.29c14.6,4.13,25.34,17.57,25.34,33.48,0,19.19-15.61,34.8-34.8,34.8s-34.8-15.61-34.8-34.8c0-15.91,10.73-29.35,25.34-33.48,1.38-.39,2.79-.71,4.24-.93,1.71-.26,3.45-.39,5.23-.39s3.52.14,5.23.39l.68-4.26c-1.93-.29-3.9-.45-5.91-.45s-3.98.15-5.91.45c-1.44.22-2.86.52-4.24.9-16.66,4.48-28.96,19.71-28.96,37.77,0,21.57,17.55,39.11,39.11,39.11s39.11-17.55,39.11-39.11c0-18.06-12.3-33.29-28.96-37.77Z"/>
								    <path class="cls-2" d="m57.65,0C25.86,0,0,25.86,0,57.65s25.86,57.65,57.65,57.65,57.65-25.86,57.65-57.65S89.44,0,57.65,0Zm0,111.47c-29.67,0-53.82-24.14-53.82-53.82C3.83,32.28,21.5,10.96,45.17,5.31l2.33,14.57c1.39-.37,2.8-.68,4.24-.9l-2.33-14.52c2.69-.41,5.44-.63,8.24-.63s5.55.22,8.24.63l-2.33,14.52-.68,4.26-2.97,18.57c-.74-.1-1.49-.18-2.25-.18s-1.52.07-2.25.18l-2.97-18.57c-1.44.22-2.86.53-4.24.93l3.02,18.83c-5.63,2.49-9.57,8.11-9.57,14.65,0,8.83,7.18,16.02,16.02,16.02s16.02-7.18,16.02-16.02c0-6.54-3.94-12.16-9.57-14.65l3.02-18.83.69-4.29,2.33-14.57c23.67,5.65,41.33,26.96,41.33,52.34,0,29.68-24.14,53.82-53.82,53.82Z"/>
								  </g>
								</svg>
							</a>
					</div><!-- debug 
				-->{if isset($menus.menu_1)}
						<div id="bg-navigation">
							<button class="hamburger hamburger--elastic mobile-only" aria-label="Menu" id="ouvre-menu-burger" type="button">
							  <span class="hamburger-box">
							  <span class="hamburger-inner"></span>
							  </span>
						  		<!-- <span class="hamburger-label">Menu</span> -->
							</button>
							<div class="navigation">
								<nav id="mmenu">
									<div>						
										<div id="logo-burger" class="mobile-only text-center">
								      		<a href="/" id="logo" title="Retour à la page d'accueil">
												<svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 367.9 115.31">
												  <defs>
												    <style>
												      .cls-1 {
												        fill: #05453c;
												      }
												
												      .cls-2 {
												        fill: #c6785b;
												      }
												    </style>
												  </defs>
												  <g>
												    <path class="cls-1" d="m140.67,59.46l1.04-3.63c2.92,1.65,5.99,2.45,8.86,2.45,4.71,0,7.73-1.98,7.73-5.66,0-4.1-3.96-5.33-7.54-6.41-3.96-1.23-9.33-2.88-9.33-8.96,0-5.47,4.29-9.14,11.17-9.14,3.35,0,6.27.9,8.81,2.26l-1.51,3.54c-2.17-1.27-4.57-2.12-7.35-2.12-4.2,0-6.83,1.93-6.83,5.19,0,3.54,3.06,4.52,6.69,5.56,4.9,1.37,10.18,3.58,10.18,9.8,0,5.89-4.71,9.62-12.07,9.62-3.25,0-6.65-.8-9.85-2.5Z"/>
												    <path class="cls-1" d="m196.52,32.36h-11.64v29.18h-4.15v-29.18h-11.69v-3.82h27.48v3.82Z"/>
												    <path class="cls-1" d="m221.88,53.71h-15.55l-2.92,7.82h-4.52l13.24-32.99h4.15l13.06,32.99h-4.52l-2.92-7.82Zm-14.14-3.77h12.73l-6.32-16.64h-.09l-6.32,16.64Z"/>
												    <path class="cls-1" d="m238.94,28.54h11.36c9.38,0,15.04,6.27,15.04,16.5s-5.66,16.5-15.04,16.5h-11.36V28.54Zm11.36,29.18c6.79,0,10.56-4.76,10.56-12.68s-3.77-12.68-10.56-12.68h-7.16v25.36h7.16Z"/>
												    <path class="cls-1" d="m282.49,31.7v26.68h4.24v3.16h-12.63v-3.16h4.24v-26.68h-4.24v-3.16h12.63v3.16h-4.24Z"/>
												    <path class="cls-1" d="m297.62,48.39v-19.84h4.19v19.84c0,6.17,2.97,9.8,8.2,9.8s8.25-3.63,8.25-9.8v-19.84h4.15v19.84c0,8.48-4.67,13.57-12.4,13.57s-12.4-5.09-12.4-13.57Z"/>
												    <path class="cls-1" d="m335.14,28.54h3.91l12.44,21.02h.09l12.49-21.02h3.82v32.99h-4.19l.05-24.98h-.24l-10.94,18.43h-2.31l-10.84-18.43h-.19l.05,24.98h-4.15V28.54Z"/>
												  </g>
												  <path class="cls-1" d="m140.77,72.32h2.63l1.98,5.86h.05l1.91-5.86h2.61l-3.44,8.7h-2.32l-3.42-8.7Z"/>
												  <path class="cls-1" d="m151.05,76.67c0-2.77,1.72-4.52,4.38-4.52s4.39,1.75,4.39,4.52-1.7,4.52-4.39,4.52-4.38-1.75-4.38-4.52Zm6.24,0c0-1.48-.69-2.39-1.86-2.39s-1.84.91-1.84,2.39.67,2.39,1.84,2.39,1.86-.91,1.86-2.39Z"/>
												  <path class="cls-1" d="m161.44,80.46l.55-1.89c.79.45,1.74.71,2.65.71.86,0,1.29-.34,1.29-.81s-.33-.74-1.43-.98c-1.62-.36-2.89-1.03-2.89-2.63s1.29-2.7,3.52-2.7c1.05,0,2.05.21,2.92.67l-.72,1.88c-.64-.41-1.41-.62-2.2-.62-.84,0-1.15.31-1.15.67,0,.41.34.69,1.32.89,1.77.38,3.04,1.1,3.04,2.72,0,1.82-1.48,2.84-3.71,2.84-1.08,0-2.2-.22-3.2-.74Z"/>
												  <path class="cls-1" d="m175.18,72.32h1.91l.1,1.63.1.03c.5-1.13,1.58-1.82,2.87-1.82s2.15.72,2.55,1.84l.09.02c.48-1.15,1.58-1.86,2.94-1.86,1.82,0,2.94,1.29,2.94,3.46v5.4h-2.48v-5c0-1-.45-1.58-1.26-1.58-.91,0-1.79.72-1.79,2.42v4.16h-2.48v-5c0-.98-.43-1.58-1.26-1.58-.89,0-1.77.72-1.77,2.42v4.16h-2.48v-8.7Z"/>
												  <path class="cls-1" d="m190.86,76.67c0-2.77,1.72-4.52,4.38-4.52s4.39,1.75,4.39,4.52-1.7,4.52-4.39,4.52-4.38-1.75-4.38-4.52Zm6.24,0c0-1.48-.69-2.39-1.86-2.39s-1.84.91-1.84,2.39.67,2.39,1.84,2.39,1.86-.91,1.86-2.39Z"/>
												  <path class="cls-1" d="m201.83,72.32h1.91l.1,1.63.1.03c.5-1.13,1.58-1.82,2.87-1.82s2.15.72,2.55,1.84l.09.02c.48-1.15,1.58-1.86,2.94-1.86,1.82,0,2.94,1.29,2.94,3.46v5.4h-2.48v-5c0-1-.45-1.58-1.26-1.58-.91,0-1.79.72-1.79,2.42v4.16h-2.48v-5c0-.98-.43-1.58-1.26-1.58-.89,0-1.77.72-1.77,2.42v4.16h-2.48v-8.7Z"/>
												  <path class="cls-1" d="m217.51,76.62c0-2.77,1.75-4.47,4.35-4.47s3.87,1.74,3.87,4.18c0,.41-.03.77-.09,1.12h-5.57c.21,1.19,1.07,1.79,2.41,1.79.79,0,1.56-.19,2.32-.64l.67,1.75c-1.1.6-2.29.84-3.39.84-2.73,0-4.57-1.65-4.57-4.57Zm5.93-.67c0-1.29-.62-2.05-1.69-2.05s-1.7.88-1.7,2.05h3.39Z"/>
												  <path class="cls-1" d="m227.91,72.32h1.91l.1,1.63.1.03c.5-1.13,1.55-1.82,2.91-1.82,1.87,0,3.08,1.29,3.08,3.46v5.4h-2.48v-5c0-1.01-.46-1.58-1.32-1.58-.96,0-1.82.72-1.82,2.42v4.16h-2.48v-8.7Z"/>
												  <path class="cls-1" d="m244.69,80.61c-.65.4-1.38.58-2.18.58-2.03,0-3.11-1.27-3.11-3.32v-3.63h-1.53v-1.93h1.53l.36-2.32h2.12v2.32h2.51v1.93h-2.51v3.51c0,.83.4,1.29,1.19,1.29.38,0,.74-.1,1.05-.28l.58,1.84Z"/>
												  <path class="cls-1" d="m246.06,80.46l.55-1.89c.79.45,1.74.71,2.65.71.86,0,1.29-.34,1.29-.81s-.33-.74-1.43-.98c-1.62-.36-2.89-1.03-2.89-2.63s1.29-2.7,3.52-2.7c1.05,0,2.05.21,2.92.67l-.72,1.88c-.64-.41-1.41-.62-2.2-.62-.84,0-1.15.31-1.15.67,0,.41.34.69,1.32.89,1.77.38,3.04,1.1,3.04,2.72,0,1.82-1.48,2.84-3.71,2.84-1.08,0-2.2-.22-3.2-.74Z"/>
												  <path class="cls-1" d="m259.32,76.67c0-2.89,1.58-4.51,3.75-4.51,1.19,0,2,.46,2.39,1.07l.05-.02v-5.09h2.48v12.9h-1.93l-.09-1.36-.1-.03c-.48.88-1.38,1.56-2.79,1.56-2.22,0-3.77-1.63-3.77-4.52Zm6.24,0c0-1.5-.69-2.41-1.86-2.41s-1.84.91-1.84,2.41.67,2.39,1.84,2.39,1.86-.91,1.86-2.39Z"/>
												  <path class="cls-1" d="m270.94,68.99h2.7l-1.81,5.07h-1.58l.69-5.07Z"/>
												  <path class="cls-1" d="m274,76.62c0-2.77,1.75-4.47,4.35-4.47s3.87,1.74,3.87,4.18c0,.41-.03.77-.09,1.12h-5.57c.21,1.19,1.07,1.79,2.41,1.79.79,0,1.56-.19,2.32-.64l.67,1.75c-1.1.6-2.29.84-3.39.84-2.73,0-4.57-1.65-4.57-4.57Zm5.93-.67c0-1.29-.62-2.05-1.69-2.05s-1.7.88-1.7,2.05h3.39Z"/>
												  <path class="cls-1" d="m283.29,80.9l2.97-4.4-2.73-4.08.02-.1h2.75l1.65,2.73h.03l1.69-2.73h2.56l.02.1-2.79,4.08,2.98,4.4-.02.12h-2.75l-1.86-2.91h-.05l-1.77,2.91h-2.67l-.03-.12Z"/>
												  <path class="cls-1" d="m293.54,76.67c0-2.77,1.82-4.52,4.59-4.52.91,0,1.87.17,2.72.62l-.76,1.91c-.6-.33-1.22-.45-1.79-.45-1.36,0-2.22.83-2.22,2.44s.84,2.44,2.3,2.44c.67,0,1.31-.15,1.89-.46l.71,1.84c-.89.5-1.91.71-2.89.71-2.72,0-4.56-1.6-4.56-4.52Z"/>
												  <path class="cls-1" d="m302.39,76.62c0-2.77,1.75-4.47,4.35-4.47s3.87,1.74,3.87,4.18c0,.41-.03.77-.09,1.12h-5.57c.21,1.19,1.07,1.79,2.41,1.79.79,0,1.56-.19,2.32-.64l.67,1.75c-1.1.6-2.29.84-3.39.84-2.73,0-4.57-1.65-4.57-4.57Zm5.93-.67c0-1.29-.62-2.05-1.69-2.05s-1.7.88-1.7,2.05h3.39Z"/>
												  <path class="cls-1" d="m312.79,72.32h1.91l.1,1.48.09.03c.52-.98,1.55-1.68,2.96-1.68,2.13,0,3.59,1.65,3.59,4.52s-1.51,4.52-3.63,4.52c-1.15,0-1.98-.46-2.48-1.19l-.07.02v4.44h-2.48v-12.14Zm6.12,4.35c0-1.5-.69-2.41-1.86-2.41s-1.84.91-1.84,2.41.69,2.39,1.84,2.39,1.86-.91,1.86-2.39Z"/>
												  <path class="cls-1" d="m329.68,80.61c-.65.4-1.38.58-2.18.58-2.03,0-3.11-1.27-3.11-3.32v-3.63h-1.53v-1.93h1.53l.36-2.32h2.12v2.32h2.51v1.93h-2.51v3.51c0,.83.4,1.29,1.19,1.29.38,0,.74-.1,1.05-.28l.58,1.84Z"/>
												  <path class="cls-1" d="m331.41,69.74c0-.84.53-1.32,1.46-1.32s1.44.48,1.44,1.32-.55,1.32-1.44,1.32-1.46-.48-1.46-1.32Zm.22,2.58h2.48v8.7h-2.48v-8.7Z"/>
												  <path class="cls-1" d="m336.31,76.67c0-2.77,1.72-4.52,4.38-4.52s4.39,1.75,4.39,4.52-1.7,4.52-4.39,4.52-4.38-1.75-4.38-4.52Zm6.24,0c0-1.48-.69-2.39-1.86-2.39s-1.84.91-1.84,2.39.67,2.39,1.84,2.39,1.86-.91,1.86-2.39Z"/>
												  <path class="cls-1" d="m347.28,72.32h1.91l.1,1.63.1.03c.5-1.13,1.55-1.82,2.91-1.82,1.87,0,3.08,1.29,3.08,3.46v5.4h-2.48v-5c0-1.01-.46-1.58-1.32-1.58-.96,0-1.82.72-1.82,2.42v4.16h-2.48v-8.7Z"/>
												  <g>
												    <path class="cls-1" d="m67.8,19.88l-.69,4.29c14.6,4.13,25.34,17.57,25.34,33.48,0,19.19-15.61,34.8-34.8,34.8s-34.8-15.61-34.8-34.8c0-15.91,10.73-29.35,25.34-33.48,1.38-.39,2.79-.71,4.24-.93,1.71-.26,3.45-.39,5.23-.39s3.52.14,5.23.39l.68-4.26c-1.93-.29-3.9-.45-5.91-.45s-3.98.15-5.91.45c-1.44.22-2.86.52-4.24.9-16.66,4.48-28.96,19.71-28.96,37.77,0,21.57,17.55,39.11,39.11,39.11s39.11-17.55,39.11-39.11c0-18.06-12.3-33.29-28.96-37.77Z"/>
												    <path class="cls-2" d="m57.65,0C25.86,0,0,25.86,0,57.65s25.86,57.65,57.65,57.65,57.65-25.86,57.65-57.65S89.44,0,57.65,0Zm0,111.47c-29.67,0-53.82-24.14-53.82-53.82C3.83,32.28,21.5,10.96,45.17,5.31l2.33,14.57c1.39-.37,2.8-.68,4.24-.9l-2.33-14.52c2.69-.41,5.44-.63,8.24-.63s5.55.22,8.24.63l-2.33,14.52-.68,4.26-2.97,18.57c-.74-.1-1.49-.18-2.25-.18s-1.52.07-2.25.18l-2.97-18.57c-1.44.22-2.86.53-4.24.93l3.02,18.83c-5.63,2.49-9.57,8.11-9.57,14.65,0,8.83,7.18,16.02,16.02,16.02s16.02-7.18,16.02-16.02c0-6.54-3.94-12.16-9.57-14.65l3.02-18.83.69-4.29,2.33-14.57c23.67,5.65,41.33,26.96,41.33,52.34,0,29.68-24.14,53.82-53.82,53.82Z"/>
												  </g>
												</svg>
											</a>
									   	</div>
										{$menus.menu_1}	
									</div>									
								</nav>
							</div>
						</div>
					{/if}<!-- debug 
				--><div id="zone-widget">
						{$widgets.recherche}
						{$widgets.connexion}					
						{$widgets.panier}
					</div>					
				</div>
			</header>
				
			<div id="body">		
				{$widgets.diaporama}

				{if $widgets.zone_texte != ''}							
					<div id="presentation-stadium">
						{$widgets.zone_texte}
					</div>
				{/if}

				<div class="inner text-center zone-pdt-home">		
					<h2>Nos meilleures ventes</h2>			
					{$widgets.produits_accueil} 
				</div>
				
				<div id="zone-racourci">
					<div class="inner">				
						<div class="carre car-ent">
							<a href="/medaille-travail.html">
								<h3>Entreprises</h3>
								<span>Valorisez les années de travail de vos collaborateurs avec une médaille du travail. </span>	
							</a>				
						</div>
						<div class="carre car-collec">
							<a href="/collection-mairie.html">
								<h3>Collectivités et secteur public</h3>
								<span>Retrouvez tous nos produits dédiés au service public et aux mairies.</span>
							</a>
						</div>
						<div class="carre car-pomp">
							<a href="/medaille-des-sapeurs-pompiers.html">
								<h3>Sapeurs-Pompiers <small>(SDIS)</small></h3>
								<span>Récompensez leur courage avec une médaille !</span>
							</a>
						</div>
						<div class="carre car-etu">
							<a href="https://etudiant.stadium.fr">
								<h3>Étudiants</h3>
								<span>Découvrez notre offre dédiée de calots, faluches, insignes...</span>
							</a>
						</div>				
					</div>					
				</div>
				
				<div id="zone-temoignage">
					<div class="inner">	
						<h2>Nos clients vous en parlent</h2>			
						{$widgets.temoignage}	
					</div>
				</div>
				
				<div id="zone-histoire">
					<div class="inner">	
						<div class="col-demi text-center">
						
						
						
							<h2>La marque de vos moments d'exception</h2>
							<p class="text-left">Depuis 60 ans, STADIUM accompagne ses clients, publics ou privés, pour honorer les grands événements de leurs carrières. Cette mission ambitieuse est rendue possible grâce au savoir faire et l’expérience de notre équipe.</p>
							<a class="lien-histoire" href="/stadium/lequipe-stadium,7.html">Rencontrer l'équipe <img src="{$url.image}fleche-w.png" alt="" /></a>
						</div><!-- debug 
					--><div class="col-demi text-center">
							<img src="{$url.image}legion-honneur.webp" alt="" />
						</div>
					</div>
				</div>							
				
				
				
				
				
<!-- 				<div id="zone-racourci"> -->
				
<!-- 					<div class="parent"> -->
<!-- 					   <div class="child"> -->
<!-- 							<a href="#"> -->
<!-- 								<h3>Étudiants</h3> -->
<!-- 								<span>Découvrez notre offre dédiée aux étudiants et aux remises de diplômes ici</span> -->
<!-- 							</a> -->
<!-- 					   </div> -->
<!-- 					</div> -->
<!-- 					<div class="parent"> -->
<!-- 					   <div class="child"> -->
<!-- 							<a href="#"> -->
<!-- 								<h3>Étudiants</h3> -->
<!-- 								<span>Découvrez notre offre dédiée aux étudiants et aux remises de diplômes ici</span> -->
<!-- 							</a> -->
<!-- 					   </div> -->
<!-- 					</div> -->
<!-- 					<div class="parent"> -->
<!-- 					   <div class="child"> -->
<!-- 							<a href="#"> -->
<!-- 								<h3>Étudiants</h3> -->
<!-- 								<span>Découvrez notre offre dédiée aux étudiants et aux remises de diplômes ici</span> -->
<!-- 							</a> -->
<!-- 					   </div> -->
<!-- 					</div> -->
<!-- 					<div class="parent"> -->
<!-- 					   <div class="child"> -->
<!-- 							<a href="#"> -->
<!-- 								<h3>Étudiants</h3> -->
<!-- 								<span>Découvrez notre offre dédiée aux étudiants et aux remises de diplômes ici</span> -->
<!-- 							</a> -->
<!-- 					   </div> -->
<!-- 					</div> -->
				
				
<!-- 				</div> -->
				
				
				
				
<!-- 				<div id="zone-lien-info"> -->
<!-- 					<div class="inner"> -->
<!-- 						<div class="col-titre text-center"> -->
<!-- 							<h2>Vente de médailles, insignes et décorations officielles</h2> -->
<!-- 						</div>debug 
					<div class="col-deux-tier">-->
<!-- 							<div class="col-lien-info"> -->
<!-- 								<div class="home-info"> -->
<!-- 									<a href="/reglementation-de-la-medaille-du-travail,50.html"><img src="{$url.image}reglementation.jpg" alt="" /></a> -->
<!-- 									<a href="/reglementation-de-la-medaille-du-travail,50.html">Réglementation de la médaille du travail </a> -->
<!-- 								</div> -->
<!-- 								<div class="home-info"> -->
<!-- 									<a href="/contactez-nous,19.html"><img src="{$url.image}demande.jpg" alt="" /></a> -->
<!-- 									<a href="/contactez-nous,19.html">Demande de médaille d'honneur du travail </a> -->
<!-- 								</div> -->
<!-- 								<div class="home-info"> -->
<!-- 									<a href="/la-medaille-du-travail,49.html"><img src="{$url.image}med-travail.jpg" alt="" /></a> -->
<!-- 									<a href="/la-medaille-du-travail,49.html">Médailles du travail </a> -->
<!-- 								</div> -->
<!-- 								<div class="home-info"> -->
<!-- 									<a href="http://www.stadium.fr/medailles-militaires,51.html"><img src="{$url.image}med-militaire.jpg" alt="" /></a> -->
<!-- 									<a href="http://www.stadium.fr/medailles-militaires,51.html">Médailles militaires</a> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div>		 -->
				<div id="zone-text-ref">
					<div class="inner">
						<h3>Médailliste, spécialiste de la médaille et de la décoration civile et militaire, STADIUM perpétue un service de qualité depuis plus de 60 ans.</h3><br/>
<!-- 						<p>Médailliste, spécialiste de la médaille et décoration civile, STADIUM perpétue un service de qualité <strong>depuis plus de 60 ans</strong>.<br> -->
						<p>STADIUM, c’est :</p> 
						<ul>
							<li><strong>70 000 médailles par an</strong> : médaille du travail, médaille régionale, départementale et communale, médaille agricole, médaille des sapeurs-pompiers, médaille civile, médaille militaire, médaille de confrérie,…</li>
							<li><strong>6 000 clients</strong> : entreprises, collectivités, institutions et particuliers.</li>
						</ul>
						<p>Nous apportons services et conseils afin de faciliter le bon déroulement de votre cérémonie et réussir votre remise de médailles. Notre expérience et notre savoir-faire font de STADIUM le partenaire incontournable de vos actions de communication. <br/>
						Dans le cadre d’inauguration, commémoration ou célébration d’événement, nous vous proposons <strong>le produit qui vous correspond</strong>.
						Une <strong>édition particulière</strong> qui mettra en valeur votre image auprès de vos clients et collaborateurs. Nous vous accompagnons en vous proposant une gamme de produits personnalisables : médailles, pin’s, trophées, porte-clés, crayons, montres…<br/>
						Mais STADIUM, c’est aussi le spécialiste de la <strong>faluche</strong>, coiffe traditionnelle des étudiants, et du <strong>calot</strong> (ou khâlot), couvre-chef  militaire et civil, avec un large choix d’insignes.
						Vous pouvez consulter notre catalogue ou nous faire faire votre propre création.<br/>
						Confiez-nous votre projet, nous mettrons tout notre <strong>savoir-faire dans sa réalisation</strong> !</p>
					</div>
				</div>  				
			</div>
			
			<footer>
				<div class="inner">
					<div class="col-six">
							{if isset($menus.menu_6)}
								<h4>Boutique</h4>
								<div class="menu-footer">
									{$menus.menu_6}
								</div>
							{/if}
						</div><!-- debug 
					--><div class="col-six">
						{if isset($menus.menu_4)}
							<h4>Créations personnalisées</h4>
							<div class="menu-footer">
								{$menus.menu_4}
							</div>
						{/if}
						{if isset($menus.menu_3)}
							<h4>Revendeurs</h4>
							<div class="menu-footer">
								{$menus.menu_3}
							</div>
						{/if}
					</div><!-- debug 
				--><div class="col-six">
						<h4>Nous contacter</h4>
						<p>STADIUM</br>
						16 rue des Granits</br>
						44100 Nantes</br>
						Tél. : <a class="lien-tel" href="tel:+33240489184">02 40 48 60 61</a></br>
						<a href="/contact,19.html" class="lien-contact">Nous écrire</a></p>						
					</div><!-- debug 
				--><div class="col-six">
						{if isset($menus.menu_2)}
							<h4>Mentions</h4>
							<div class="menu-footer footer1">
								{$menus.menu_2}
							</div>
						{/if}
					</div><!-- debug 
				--><div class="col-six">
						{if isset($menus.menu_5)}
							<h4>Paiement sécurisé</h4>
							<div class="menu-footer menu-paiement">
								<a href="/paiement-securise,57.html"><img src="/assets/img/ico-paiement-b.png" alt="" /></a>
								<a href="/paiement-securise,57.html"><img src="/assets/img/ico-cb.png" alt="" /></a>
								<a href="/paiement-securise,57.html"><img src="/assets/img/ico-mastercarte.png" alt="" /></a><br />
								<a href="/paiement-securise,57.html"><img src="/assets/img/ico-visa.png" alt="" /></a>							
								{$menus.menu_5}
							</div>
							<a href="http://etudiant.stadium.fr/" class="_blank lien-site-faluche">Vous êtes étudiant ?</a>
						{/if}
					</div>
					<div class="text-center">
						<small><a href="http://www.aquilainformatique.com" class="_blank copyright">&copy;&nbsp;Création Aquila Informatique</a></small>
					</div>
				</div>
			</footer>
			
		</div>
		
		<div class="widgetContainer">
			<script type="text/javascript">
			{literal}
				window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
				d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
				_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
				$.src='//v2.zopim.com/?2nCUdT8fpARSzmepa7oFysPjcKZFKBa6';z.t=+new Date;$.
				type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
			{/literal}
			</script>
		</div>
		
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K76ZB5L" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		
		<!-- Cookie Consent by https://www.FreePrivacyPolicy.com -->
		<script type="text/javascript" src="//cms.ocea-manager.com/5/assets/application/js/cookie-consent.js"></script>
		<script type="text/javascript">
		{literal}document.addEventListener('DOMContentLoaded', function () {
			cookieconsent.run({"notice_banner_type":"simple","consent_type":"express","palette":"dark","language":"fr","change_preferences_selector":".section-47","cookies_policy_url":""});
		});{/literal}
		</script>
		<noscript>Cookie Consent by <a href="https://www.FreePrivacyPolicy.com/free-cookie-consent/" rel="nofollow noopener">FreePrivacyPolicy.com</a></noscript>
		<!-- End Cookie Consent -->
	</body>
</html>