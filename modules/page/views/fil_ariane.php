<?php
$html  = '<ul id="fil-ariane">';
$html .= '<li class="debut"></li>';

$i = 1;
$nb_fils = count($fil_ariane);

foreach ($fil_ariane as $fil)
{
	$html .= '<li>'.$fil.'</li>';
	
	if ($nb_fils > $i)
	{
		$html .= '<li class="separateur"><span>&rsaquo;</span></li>';
	}
	else
	{
		$html .= '<li class="fin"></li>';
	}
	
	$i++;
}
$html .= '</ul>';

echo $html;