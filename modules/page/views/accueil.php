<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

$assets = assets(array(
    'favicon.ico',
    'bootstrap/bootstrap.min.css',
    'jquery/jquery.mmenu.all.css',
    'jquery/colorbox.css',
    'hamburgers.min.css',
    'site.css',
    'jquery/jquery-core.min.js',
));
$assets .= assets(array(
    'bootstrap/bootstrap.min.js',
    //'jquery/simpleMobileMenu.js',
    'jquery/jquery.mmenu.all.min.js',
    'jquery/jquery.mmenu.debugger.js',
    'jquery/jquery.formsubmit.min.js',
    'jquery/jquery.placeholder.min.js',
    'jquery/jquery.colorbox-min.js',
    'jquery/jquery.cookie.js',
    'jquery/cookie-ie.js',
    'commun.js',
    'boutique.js',
    'catalogue.js',
    'site.js'
), FALSE, TRUE);

$data = array(
	'url' => array(
		'image'	=> base_url().'assets/img/'
	),
    'assets' => $assets,
	'rss' => rss_feed(array(array(
		'titre' => 'Actualités',
		'url'	=> '/rss'
	))),
	'metas' 		=> $metas,
	'statistiques' 	=> $statistiques,
	'menus' 		=> $menus,
	'widgets'		=> $widgets
);

$this->dwootemplate->output(tpl_path('page/accueil.tpl'), $data);
