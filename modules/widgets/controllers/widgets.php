<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

require_once APPPATH.'controllers/front.php';

/**
 * Gestions des widgets
 * 
 * @author 		Pauline Martin, Pauline MARTIN
 * @package 	application
 * @category 	Controllers
 * @version 	1.0
 */
class Widgets extends Front
{
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('catalogue/catalogue');
	}
	
	/**
     * BOUTIQUE
	 * Retourne l'affichage du panier
     * 
     * @return	VIEW
     */
    public function boutique_panier()
    {
    	//Actualiser() renvoi un echo donc utilisation du buffer
    	ob_start();
    	echo modules::run('boutique/paniers/actualiser');
    	$actualiser = ob_get_contents();
    	ob_end_clean();
    	
    	$panier = json_decode($actualiser);
    	
    	$data = array(
    			'panier' 	 => $panier->contenu,
    			'nb_article' => $panier->nb_article
    	);
    	
    	if ($this->input->is_ajax_request())
    	{
			$this->load->view('widgets/boutique_panier', $data);
    	}
    	else
    	{
    		$this->load->view('boutique_bouton_panier', $data);
    	}
    }

	/**
     * BOUTIQUE
	 * Retourne le formulaire de connexion d'un client
     * 
     * @return	VIEW
     */
    public function boutique_connexion()
    {
    	$this->lang->load('boutique/client');
    	
		if ($this->input->is_ajax_request())
		{
			//Connecté
			if ($client = modules::run('boutique/clients/est_connecte'))
			{
				$data = array('client' => $client);
				$this->load->view('boutique_connecte', $data);
			}
			//Non connecté
	    	else
	    	{
				$this->load->view('boutique_connexion');
	    	}
		}
		else
		{
			$this->load->view('boutique_bouton_connexion');
		}
    }
	
    /**
	 * CATALOGUE
	 * Retourne les produits de l'accueil
	 * 
	 * @param	integer	$module_id
	 * @param	integer	$nb_item
	 * @param	boolean	$random
	 * @return	VIEW
	 */
	public function produit_accueil($module_id = 1, $nb_item = 1, $random = FALSE)
	{
		$this->load->models(array(
			'catalogue/module_catalogue',
			'catalogue/produit'
		));
		$this->load->helper('catalogue/catalogue');
		
		$module = $this->module_catalogue->get_by_id($module_id);
		
		//Connecté
		$client = modules::run('boutique/clients/est_connecte');
		

		$conditions = array(
			'accueil' 	=> TRUE,
			'visible' 	=> TRUE,
			'random'  	=> $random,
			'module_id'	=> $module->id,
            'enfant'    => $module->afficher_produits_enfants_liste
		);
		
		$data = array(
			'module'	=> $module,
			'utilisateur'	=> $client,
			'produits'	=> $this->produit->lister($conditions, 0, $nb_item)
		);
		
		$this->load->view('widgets/produit_accueil', $data);
	}
	
	/**
	 * CATALOGUE
	 * Retourne les produits importants
	 * 
	 * @param	integer	$module_id
	 * @param	integer	$nb_item
	 * @param	boolean	$random
	 * @return	VIEW
	 */
	public function produit_important($module_id = 1, $nb_item = 1, $random = FALSE)
	{
		$this->load->models(array(
			'catalogue/module_catalogue',
			'catalogue/produit'
		));
		$this->load->helper('catalogue/catalogue');
		
		$module = $this->module_catalogue->get_by_id($module_id);
		
		$conditions = array(
			'important' => TRUE,
			'visible' 	=> TRUE,
			'random'  	=> $random,
			'module_id'	=> $module->id,
            'enfant'    => $module->afficher_produits_enfants_liste
		);
        
		$data = array(
			'module'	=> $module,
			'produits'	=> $this->produit->lister($conditions, 0, $nb_item)
		);
		$this->load->view('widgets/produit_important', $data);
	}
	
	/**
	 * CATALOGUE
	 * Retourne les produits d'une catégorie
	 * 
	 * @param	integer	$module_id
	 * @param	integer	$nb_item
	 * @param	integer	$categorie
	 * @param	boolean	$random
	 * @return	VIEW
	 */
	public function produit_categorie($module_id = 1, $nb_item = 1, $categorie = NULL, $random = FALSE)
	{
		$this->load->models(array(
			'catalogue/module_catalogue',
			'catalogue/produit'
		));
		
		$module = $this->module_catalogue->get_by_id($module_id);
		
		$conditions = array(
			'visible' 	=> TRUE,
			'random'  	=> $random,
			'module_id'	=> $module->id,
            'enfant'    => $module->afficher_produits_enfants_liste
		);
		
		if ( ! is_null($categorie))
		{
			$conditions['filtres']['categorie'] = $categorie;
		}
		
		$data = array(
			'module'	=> $module,
			'produits'	=> $this->produit->lister($conditions, 0, $nb_item)
		);
		$this->load->view('widgets/produit_categorie', $data);
	}	
	
	/**
	 * CATALOGUE
	 * Retourne les produits d'une catégorie
	 * 
	 * @param	integer	$module_id
	 * @param	integer	$nb_item
	 * @param	integer	$categorie
	 * @param	boolean	$random
	 * @return	VIEW
	 */
	public function produit_remise($module_id = 1, $nb_item = 1, $random = FALSE)
	{
		$this->load->models(array(
			'catalogue/module_catalogue',
			'catalogue/produit'
		));
		
		$module = $this->module_catalogue->get_by_id($module_id);
		
		$conditions = array(
			'visible' 	=> TRUE,
			'random'  	=> $random,
			'remise'	=> TRUE,
			'module_id'	=> $module->id,
			'enfant'    => $module->afficher_produits_enfants_liste
		);
		
		$data = array(
			'module'	=> $module,
			'produits'	=> $this->produit->lister($conditions, 0, $nb_item)
		);
		$this->load->view('widgets/produit_remise', $data);
	}	
	
	/**
	 * CATALOGUE 
	 * Recherche avec champ texte
	 * 
	 * @param	
	 * @return	VIEW
	 */
	public function recherche($actualites = TRUE, $contenu_libre = TRUE, $catalogue = FALSE)
	{
		
		$data = array(
			'actualites' 		=> $actualites,
			'contenus_libres'	=> $contenu_libre,
			'catalogue'			=> $catalogue
		);
		
		$this->load->view('widgets/recherche', $data);
	}
	
	/**
	 * ACTUALITES
	 * Retourne les actualités de l'accueil
	 * 
	 * @param 	integer	$module_id
	 * @param	integer	$nb_item
	 * @return	VIEW
	 */
	public function actualites_accueil($module_id = NULL, $nb_item = 1)
	{
	    $this->load->models(array(
	        'actualites/actualite',
	        'actualites/module_actualite'
	    ));
	    
	    $conditions = array(
	        'module_id' => $module_id,
	        'accueil' => TRUE,
	        'tri' =>  array(
	            'champ'   => 'id',
	            'ordre'   => 'random'
	        )	        
	    );
	    
	    $data = array(
	        'actualites' => $this->actualite->lister($conditions, 0, $nb_item)
	    );
	    
	    if ( ! is_null($module_id))
	    {
	        $module = $this->module_actualite->get_by_id($module_id);
	        $data['module']  = $module;
	        $data['section'] = $module->section->get();
	    }
	    
	    $this->load->view('widgets/actualites_accueil', $data);
	}
	/**
	 * ACTUALITES
	 * Retourne les actualités d'une catégorie
	 * 
	 * @param	integer	$module_id
	 * @param 	integer	$categorie_id
	 * @param	integer	$limit
	 * @return	VIEW
	 */
	public function actualites_categorie($module_id = NULL, $categorie_id, $nb_item = 1)
	{
		$this->load->models(array(
			'actualites/actualite', 
			'actualites/module_actualite'
		));
		
		$conditions = array(
			'module_id' => $module_id,
			'categories' => $categorie_id
		);
		
		$data = array(
			'actualites'=> $this->actualite->lister($conditions, 0, $nb_item)
		);
		
		if ( ! is_null($module_id))
		{
			$module = $this->module_actualite->get_by_id($module_id);
			
			$data['module']  = $module;
			$data['section'] = $module->section->get();
		}
		
		$this->load->view('widgets/actualites_categorie', $data);
	}
	
	/**
	 * CATEGORIES
	 * Retourne les catégories enfants d'une catégorie lié à une section
	 * 
	 * @param	integer	$section_id
	 * @param	boolean	$media
	 * @param	boolean	$contenu
	 * @return	VIEW
	 */
	public function categorie_arbre($section_id, $media = FALSE, $contenu = FALSE)
	{
		$this->load->model('categories/categorie');
		
		$section 			= $this->section->get_by_id($section_id);
		$type_module 		= $section->type_module->get();
		$categorie_racine 	= NULL;
		
		if ( ! empty($type_module->modele))
		{
			$modele = $type_module->modele;
			$this->load->model($type_module->dossier.'/'.$type_module->modele);
			
			$module 			= $this->$modele->get_by_section_id($section_id);
			$categorie_racine 	= $module->categorie_racine;
		}
		
		$liste_enfants = $this->categorie->arbre($categorie_racine, FALSE);
		$parent = $this->categorie->where('root_id', $categorie_racine)->where('left_id', 1)->get();
		
		if ($parent->exists())
		{
			$data = array(
				'arbres'	=> $liste_enfants,
				'parent'	=> $parent,
				'section'	=> $section,
				'affiche_media'		=> $media,
				'affiche_contenu'	=> $contenu
			);
			$this->load->view('widgets/categorie_arbre', $data);
		}
	}
    
    /**
     * Afficher les catégories enfants d'une catégorie
     * 
     * @param   integer $categorie_parent_id
     * @param   boolean $media
     * @return  VIEW
     */
    public function categorie_enfants($categorie_parent_id, $section_id, $media = FALSE)
    {
        $this->load->model('categories/categorie');
        
        $data = array(
            'categories'        => $this->categorie->enfants($categorie_parent_id),
            'section'           => $this->section->get_by_id($section_id),
            'afficher_media'    => $media
        );

        $this->load->view('widgets/categories_enfants', $data);
    }
	
	/**
	 * EDITEURS
	 * Retourne un extrait d'un contenu d'une page de contenu libre
	 * 
	 * @param 	integer $section_id
	 * @param 	integer $truncate
	 * @return	VIEW
	 */
	public function editeur_extrait($section_id, $truncate = NULL)
	{
		$this->load->model('editeurs/module_editeur');
		
		$section = $this->section->get_by_id($section_id);
		
		if ($section->exists())
		{
			$module = $section->module_editeur->get();
			
			$data = array(
				'section' => $section,
				'module'  => $module
			);
			$this->load->view('widgets/editeur_extrait', $data);
		}
	}
	
	
	/**
	 * DIAPORAMA
	 * Retourne diaporama administrable avec choix taille
	 *
	 * @param 	integer $section_id
	 * @param 	integer $truncate
	 * @return	VIEW
	 */
	public function diaporama($taille_photos)
	{
	    $this->load->library('user_agent');	
		$this->load->model('medias/media');
		$medias = $this->media->include_related('medias_widget')->order_by('ordre')->get_by_widget_id($this->_widget_id);
		
		//Si on est sur mobile on divise la taille des photos initiale par 3
		if($this->agent->is_mobile())
		{
		    $dimensions = explode('x', $taille_photos);
		    $new_taille_photos = floor($dimensions[0]/3.5).'x'.floor($dimensions[1]/3.5);
		}
		
		if ($medias->exists())
		{
			$data = array(
				'medias' => $medias,
			    'taille_photos'	=> ($this->agent->is_mobile()) ? $new_taille_photos : $taille_photos
			);
			echo $this->load->view('widgets/diaporama', $data);
		}
	}
	
	/**
	 * Retourne le plan du site (arborescence et liens)
	 * 
	 * @return	 VIEW
	 */
	public function plan_site()
	{
		$this->load->model('accueil');
		
		$data = array(
			'arbres' 	=> $this->section->arbre(),
			'visible'	=> $this->section->visible(),
			'selection'	=> array()
		);
		
		$this->load->view('widgets/plan_site', $data);
	}
	
	/**
	 * 
	 * Formulaire inscription à la newsletter
	 */
	public function newsletter()
	{
		$this->load->models(array('categories/categorie', 'newsletters/module_newsletter'));
		$categorie_root_id = $this->module_newsletter->get(1)->categorie_racine;
		$data = array(
			'categories'	=> ($categorie_root_id) ? $this->categorie->arbre($categorie_root_id, TRUE) : NULL
		);
		$this->load->view('widgets/newsletter', $data);	
	}
	
	/**
	 * Retourne l'ensemble des widgets disponibles
	 * 
	 * @return	array
	 */
	public function lister()
	{
		$this->config->load('widget');
		$this->load->model('widget');
		
		$tab_widgets = $this->config->item('tab_widgets');
		
		foreach ($this->widget->actifs() as $widget)
		{
			if (($widget->accueil && is_null($this->_section_id())) or ($widget->page_interne && ! is_null($this->_section_id())))
			{
				$this->_widget_id = $widget->id;
				$retour_widgets[$widget->identifiant] = modules::run($tab_widgets[$widget->type]['url'], $widget->param_1,$widget->param_2, $widget->param_3, $widget->param_4);
			}
			else 
			{
				$retour_widgets[$widget->identifiant] = NULL;
			}
		}
		return $retour_widgets;
	}
}

/* End of file widgets.php */
/* Location: ./application/controllers/widgets.php */