<div class="produits" id="produit-{$id}" data-index="{$index}">
	{if $nouveau}
		<span class="nouveau">{lang('catalogue_nouveau')}</span>
	{/if}
	
	<a href="{$url}" class="lien-image" title="{$libelle}">{$image}</a>

	<div class="zone-info-pdt">
		<h3><a href="{$url}" title="{$libelle}">{$libelle}</a></h3>	
		{if $commandable}
			<div class="tarification">
				{if $tarif.prix > 0}
					<span>&Agrave; partir de <span class="prix">{$tarif.prix}</span></span>
				{else}
					<span>Prix : <span class="prix gratuit">Gratuit</span></span>
				{/if}
				<br />
				{if $tarif.prix < $tarif.prix_sans_remise}
					<span class="prix-remise">Prix sans remise : {$tarif.prefixe}{$tarif.prix_sans_remise} {$tarif.taxe}</span><br />
					<span class="remise">Réduction : {$tarif.taux_remise}</span><br />
				{/if}
			</div>
		

		{else}
			<div class="tarification">
				<span class="prix indispo">{lang('stock_indisponible')}</span>		
			</div>
		{/if}
		<span class="libelle-2">{$libelle2}</span>	

	</div>
	

</div>