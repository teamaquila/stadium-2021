<?php
$form = assets(array(
	'bootstrap/bootstrap3-typeahead.min.js',
	'recherche/recherche.js'
));

$form .= assets('ajax_prix_liste.js', FALSE, TRUE);

	$form .= '<div id="widget_recherche">';
	$form .= form_open('/recherche.html', array(
		'method'=> 'post', 
		'class' => 'widget', 
		'id' 	=> 'recherche'
	));
	
	//Si seulement module catalogue, on mets le suggest sur le moteur de recherche
	$class = NULL;
	if ($catalogue && !$actualites && !$contenus_libres)
	{
		$class=" google_suggest";
	}
	//$form .= '<div id="zone_recherche">';
	$form .= '<div class="input-group">';
	$form .= form_input(array(
		'type' => 'text',
		'name' => 'recherche',
		'class' => 'moteur-recherche form-control'.$class,
		'data-ajax'	=> 'true',
		'autocomplete' => 'off',
		'placeholder' => lang('recherche')
	));
	$form .= '<span class="input-group-btn">';
	$form .= form_button(array(
		'type' => 'submit',
		'class'	=> 'button btn btn-default',
		'content' => 'Ok' 
			//'content' => '<span class="glyphicon glyphicon-search"></span>'
	));
	$form .= '</span>';
	$form .= '</div>';
	//$form .= '</div>';
	
	//$form .= '<div><a href="javascript:void(0)" id="toggle_recherche"><img src="/assets/img/ico-loupe.png" alt="" /></a></div>';
	//$form .= '<div><button name="" type="button" id="toggle_recherche" title=""><img src="/assets/img/ico-loupe.png" alt="" /></button></div>';
	$form .= form_close();
	$form .= '</div>';
	//$form .= '<button name="" type="button" id="toggle_recherche" class="btn btn-default widget" title=""><img src="/assets/img/ico-search.png" alt="" /></button>';
	$form .= '<button name="" type="button" id="toggle_recherche" class="btn btn-default widget" title=""><svg id="a" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 243.76 253.63"><path d="M242.38,244.26l-49.79-57.86c18.3-19.8,29.49-46.27,29.49-75.36C222.09,49.72,172.37,0,111.04,0S0,49.72,0,111.04s49.72,111.04,111.04,111.04c28.15,0,53.85-10.48,73.42-27.75l49.32,57.32c1.12,1.3,2.71,1.97,4.3,1.97,1.31,0,2.62-.45,3.7-1.37,2.37-2.04,2.64-5.62,.6-8ZM11.34,111.04C11.34,56.07,56.07,11.34,111.04,11.34s99.7,44.73,99.7,99.7-44.73,99.7-99.7,99.7S11.34,166.02,11.34,111.04Z"/></svg></button>';

echo $form;

