<?php
$liste = NULL;
//Liste des produits
if ( ! is_null($produits) && ($produits->result_count() > 0))
{
	$i = 1;

	foreach ($produits as $produit)
	{
		$module = new Module_catalogue();
		$module->get_by_id($produit->module_catalogue_id);
		
		$note = $produit->note();
		//Note
		$note_produit = NULL;
		if ( ! is_null($note))
		{
		    $note_produit .= '<div id="note">';
		    
		    for ($i = 1; $i <= 5; $i++)
		    {
		        if ($i <= $note)
		        {
		            $note_produit .= '<span class="glyphicon glyphicon-star"></span>';
		        }
		        else
		        {
		            $note_produit .= '<span class="glyphicon glyphicon-star-empty"></span>';
		        }
		    }
		    $note_produit .= '</div>';
		}
		
		//Produit
		$data = array(
			'id'			=> $produit->id,
			'libelle'		=> $produit->libelle,
		    'libelle2'		=> $produit->libelle2,
		    'description'	=> ($produit->resume) ? truncate($produit->resume, $module->longueur_texte_accueil) : truncate($produit->description, $module->longueur_texte_accueil),
			'stock'			=> statut_stock($produit->stock, $module->seuil_alerte_stock),
			'nouveau'		=> $produit->nouveau,
			'commandable'	=> !$produit->non_commandable,
			'url'			=> $produit->url(),
			'index'			=> $i,
		    'note'			=> $note_produit,
		    'chiffre_note'	=> $note,
		    'nb_note'       => $produit->nb_note()
		);
		
		//Image
		
		$medias = $produit->medias_associes();
		if ($medias->exists())
		{
			$data['image'] = html_media($produit->medias_associes(), array('taille' => $module->taille_image_accueil));
		}
		else
		{
			$data['image'] = '<img src="/assets/img/img-non-disponible-liste.jpg" alt="Photo indisponible '.$produit->libelle.'" />';
		}
		
		
		//Si les enfants du produits sont des articles collections composés d'autres articles on calcule le prix en fonction des enfants
		$trouve_compose = FALSE;
		$tab_tarifs_compose = array();
		foreach ($produit->produits_enfants() as $enfant)
		{
		    $produits_composes = $this->db->query("
				SELECT t_produits.id
				FROM t_produits
				INNER JOIN t_produits_produits ON t_produits.id = t_produits_produits.related_produit_id
				WHERE t_produits_produits.compose = 1
				AND t_produits_produits.produit_id = ".$enfant->id."
			");
		    if ($produits_composes->num_rows() != 0)
		    {
		        $trouve_compose = TRUE;
		        
		        $tab_tarifs_compose[$enfant->id]	= array(
		            'prix'	=> 0,
		            'prix_sans_remise'	=> 0
		        );
		        foreach ($produits_composes->result() as $row)
		        {
		            $prod_compo = new Produit();
		            $prod_compo->get_by_id($row->id);
		            $tab_tarifs_compose[$enfant->id]['prix'] +=  $prod_compo->tarif($utilisateur)['prix'];
		            $tab_tarifs_compose[$enfant->id]['prix_sans_remise'] +=  $prod_compo->tarif($utilisateur)['prix'];
		        }
		    }
		}
		
		
		if ($trouve_compose)
		{
		    $tarif_precedent = 0;		    
		    foreach ($tab_tarifs_compose as $enfant_id => $tarif_enfant)
		    {
		        if (($tarif_precedent == 0 ) or ($tarif_enfant['prix'] < $tarif_precedent))
		        {
		            $data['tarif']['prix']	= html_price($tarif_enfant['prix']);
		            $data['tarif']['prix_sans_remise']	= html_price($tarif_enfant['prix_sans_remise']);
		            $tarif_precedent = $tarif_enfant['prix'];
		        }
		    }
		    
		}
		else
		{
		    //Tarif
		    $tarif = $produit->tarif($utilisateur);
		    
		    if ($tarif !== FALSE)
		    {
		        //Tarif unique
		        $data['tarif'] = array(
		            'prefixe'		=> ($tarif['unique']) ? NULL : $this->lang->line('a_partir_de'),
		            'suffixe'		=> $this->lang->line('devise'),
		            'prix'			=> html_price($tarif['prix']),
		            'prix_ht'			=> html_price($tarif['prix']),
		            'prix_sans_remise' => html_price($tarif['prix_sans_remise']),
		            'taux_remise'	=> html_percentage($tarif['taux_remise'], 0),
		            'tva'			=> html_percentage($tarif['tva'], 2),
		            'taxe'			=> $this->lang->line('prix_taxe_'.$tarif['taxe'])
		        );
		        
		        //Grille tarifaire
		        if (is_array($tarif['grille']))
		        {
		            $tarifs = '<ul class="liste-tarifs">';
		            foreach ($tarif['grille'] as $grille)
		            {
		                $tarifs .= '<li>';
		                $tarifs .= '<span class="minimum">'.$grille['minimum'].'</span>';
		                $tarifs .= '<span class="maximum">'.$grille['maximum'].'</span>';
		                $tarifs .= '<span class="prix">'.html_price($grille['prix']).nbs();
		                $tarifs .= $this->lang->line('devise').nbs().$this->lang->line('prix_taxe_'.$tarif['taxe']).'</span>';
		                $tarifs .= '</li>';
		            }
		            $tarifs .= '</ul>';
		            
		            $data['tarif']['grille'] = $tarifs;
		        }
		    }
		}
		
		$liste .= $this->dwootemplate->get(tpl_path('widgets/produit_item.tpl'), $data);
		$i++;
	}

	$data = array(
		'liste' 		=> $liste,
		'identifiant'	=> 'produit-accueil-'.$module->id,
		'lien_liste_produits'	=> anchor($module->url(),lang('voir_produits'), 'class="btn btn-primary widget-toutes"')
	);
	$this->dwootemplate->output(tpl_path('widgets/produit.tpl'), $data);
}