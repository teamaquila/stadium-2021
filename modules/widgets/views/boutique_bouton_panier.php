<?php
$html = NULL;

$html .= form_button(array(
	'id' => 'bt-panier',
	'class' => 'btn btn-default widget',
	'data-url' => '/widgets/boutique_panier',
	//'content' => '<img src="/assets/img/ico-panier.png" alt="">',
    'content' => '<span id="nb-article" class="nb-article">'.$nb_article.'</span><svg id="a" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 182.49 253.4"><path d="M164.75,70.91h-22.3v-16.06C142.44,24.61,119.48,0,91.24,0S40.05,24.61,40.05,54.85v16.06H17.74c-9.8,0-17.74,7.94-17.74,17.74V235.65c0,9.8,7.94,17.74,17.74,17.74H164.75c9.8,0,17.74-7.94,17.74-17.74V88.65c0-9.8-7.94-17.74-17.74-17.74ZM51.38,54.85c0-23.99,17.88-43.51,39.86-43.51s39.86,19.52,39.86,43.51v16.06H51.38v-16.06Zm119.77,180.8c0,3.53-2.87,6.41-6.41,6.41H17.74c-3.53,0-6.41-2.87-6.41-6.41V88.65c0-3.53,2.87-6.41,6.41-6.41h22.3v26.96h11.34v-26.96h79.72v25.75h11.34v-25.75h22.3c3.53,0,6.41,2.87,6.41,6.41V235.65Z"/></svg>',
));

echo $html;