<?php
$html  = NULL;
$html .= '<div class="widget-panel connecte">';
$html .= anchor('/boutique/clients/commandes', lang('acceder_commandes'), 'class="btn bt-compte" rel="nofollow"');
$html .= anchor('/boutique/clients/lister_commandes_attente', lang('acceder_paniers'), 'class="btn bt-compte" rel="nofollow"');
$html .= anchor('/boutique/clients/compte', lang('acceder_coordonnees'), 'class="btn bt-compte" rel="nofollow"');

$html .= anchor('/boutique/clients/deconnecter', lang('se_deconnecter'), 'class="btn bt-deconnexion" rel="nofollow"');
$html .= '</div>';

echo $html;