<?php
$liste = NULL;
if ( ! is_null($actualites) && ($actualites->result_count() > 0))
{
	$i = 1;
	
	foreach ($actualites as $actualite)
	{
		//Catégorie
		$lien_categorie = NULL;
		$categories = $actualite->categorie->get();
		
		if ($categories->exists() && isset($section))
		{
			foreach ($categories as $categorie)
			{
				$lien_categorie .= anchor(construire_url(array(
					'section' 	=> $section->segment_url(),
					'categorie'	=> $categorie->url()
				)), $categorie->libelle, 'class="btn btn-primary"').nbs();
			}
		}
		
		$data = array(
			'titre'		=> $actualite->titre,
			'image' 	=> html_media($actualite->medias_associes(), array('taille' => $module->taille_image_accueil)),
			'contenu'	=> truncate($actualite->contenu, $module->longueur_texte_accueil),
			'date'		=> date_type($actualite->date_publication, $module->type_date_accueil),
			'id'		=> $actualite->id,
			'url'		=> $actualite->url(),
			'categorie'	=> $lien_categorie,
			'index'		=> $i
		);
		$liste .= $this->dwootemplate->get(tpl_path('widgets/actualite_item.tpl'), $data);
		$i++;
	}
	
	echo '<div id="actualites-accueil-'.$module->id.'" class="widget">'.
			$liste.
// 			anchor($section->url(), lang('actualite_voir_toutes'), 'class="btn btn-primary widget-toutes"').
		'</div>';
}