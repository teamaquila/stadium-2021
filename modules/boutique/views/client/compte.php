<?php
$formulaire = form_open(NULL, array('id' => 'adresse-compte', 'class' => 'adresses'));
$formulaire .= '<div class="bloc_titre_h1"><h1>Vos adresses</h1></div>';

$formulaire .= '<div class="inner_interne">';
$formulaire .= '<p class="welcome-client text-center">Bienvenue <span>'.$client->prenom.' '.$client->nom.'</span></p>';
$formulaire .= '<div class="row">';
	$formulaire .= '<div class="col-md-3 menu-compte">';
		$formulaire .= anchor('/boutique/clients/commandes', 'Afficher vos commandes', array('class' => 'bt_compte'));
		$formulaire .= anchor('/boutique/clients/paniers', 'Devis en attente', array('class' => 'bt_compte'));
		$formulaire .= anchor('/boutique/clients/compte', 'Modifier vos coordonnées', array('class' => 'bt_compte'));
		$formulaire .= anchor('/boutique/clients/formulairemotdepasse', 'Modifier mon mot de passe', array('class' => 'bt_compte'));
		//$formulaire .= '<a href="https://www.stadium.fr/medias/pdf/2021/bon-commande.pdf" class="bt_compte lien-telechargement-inverse">Bon de commande</a>';		
		$formulaire .= anchor('/boutique/clients/deconnecter', 'Se déconnecter', array('class' => 'bt_compte'));
	$formulaire .= '</div>';
	
	$formulaire .= '<div class="col-md-9">';

		if ( ! is_null($message))
		{
			
			$formulaire .= ($erreur) ? (form_fieldset(NULL, array('class' => 'alert alert-danger'))) : (form_fieldset(NULL, array('class' => 'alert alert-success')));
			$formulaire .= $message;
			$formulaire .= form_fieldset_close();
		}
		$formulaire .= form_hidden('facturation[id]', $client->id);
		
		$formulaire .= '<div class="row">';
		$formulaire .= '<div class="form-group col-xs-6">';
		
		$formulaire .= form_fieldset('Adresse de facturation', array('id' => 'facturation'));
		$formulaire .= '<div class="bloc-champs">';
		$attributs = array(
			'name'	=> 'facturation[nom]',
			'id'	=> 'facturation[nom]',
			'class'	=> 'form-control',
			'value'	=> isset_value($client, 'nom')
		);
		$formulaire .= form_label(lang('nom').'*', $attributs['id']);
		$formulaire .= form_input($attributs).br();
		
		$attributs = array(
			'name'	=> 'facturation[prenom]',
			'id'	=> 'facturation[prenom]',
			'class'	=> 'form-control',
			'value'	=> isset_value($client, 'prenom')
		);
		$formulaire .= form_label(lang('prenom').'*', $attributs['id']);
		$formulaire .= form_input($attributs).br();
		
		$attributs = array(
			'name'	=> 'facturation[societe]',
			'id'	=> 'facturation[societe]',
			'class'	=> 'form-control',
			'value'	=> isset_value($client, 'societe')
		);
		$formulaire .= form_label(lang('societe'), $attributs['id']);
		$formulaire .= form_input($attributs).br();
		
		$attributs = array(
			'name'	=> 'facturation[adresse]',
			'id'	=> 'facturation[adresse]',
			'class'	=> 'form-control',
			'cols'	=> 50,
			'rows'	=> 3,
			'value'	=> isset_value($client, 'adresse')
		);
		$formulaire .= form_label(lang('adresse').'*', $attributs['id']);
		$formulaire .= form_textarea($attributs).br();
		
		$formulaire .= '<p class="demi-colonne">';
		
		$attributs = array(
		    'name'  => 'facturation[code_postal]',
		    'id'    => 'facturation[code_postal]',
			'class'	=> 'form-control',
		    'value' => isset_value($client, 'code_postal')
		);
		$formulaire .= form_label(lang('code_postal').'*', $attributs['id']);
		$formulaire .= form_input($attributs);
		$formulaire .= '</p>';
		
		$formulaire .= '<p class="demi-colonne">';
		$attributs = array(
		    'name'  => 'facturation[ville]',
		    'id'    => 'facturation[ville]',
			'class'	=> 'form-control',
		    'value' => isset_value($client, 'ville')
		);
		$formulaire .= form_label(lang('ville').'*', $attributs['id']);
		$formulaire .= form_input($attributs);
		$formulaire .= '</p><br class="clear-both" />';
		
		$formulaire .= modules::run('boutique/clients/zone_livraison', $client->pays, 'facturation[pays]');
		
		
		$formulaire .= '<p class="demi-colonne">';
		$attributs = array(
			'name'	=> 'facturation[telephone]',
			'id'	=> 'facturation[telephone]',
			'class'	=> 'form-control',
			'value'	=> isset_value($client, 'telephone')
		);
		$formulaire .= form_label(lang('telephone').'*', $attributs['id']);
		$formulaire .= form_input($attributs);
		$formulaire .= '</p>';
		
		
		$formulaire .= '</div>';
		
		$formulaire .= form_fieldset_close();
		
		$formulaire .= '</div>';
		
		if ($module->option_livraison)
		{
			$formulaire .= '<div class="form-group col-xs-6">';
			
			foreach ($client->adresses(TRUE) as $adresse)
			{		
				$formulaire .= form_fieldset('Adresse de livraison', array('id' => 'livraison'));
				
				$formulaire .= form_hidden('livraison[id]', $adresse->adresses_utilisateur_id);
				$formulaire .= form_hidden('livraison[utilisateur]', $client->adresses_utilisateur_utilisateur_id);
				
				$formulaire .= '<div class="bloc-champs">';
				$attributs = array(
					'name'	=> 'livraison[nom]',
					'id'	=> 'livraison-nom',
					'class'	=> 'form-control',
					'value'	=> isset_value($adresse, 'adresses_utilisateur_nom')
				);
				$formulaire .= form_label('Nom*', $attributs['id']);
				$formulaire .= form_input($attributs).br();
				
				$attributs = array(
					'name'	=> 'livraison[prenom]',
					'id'	=> 'livraison-prenom',
					'class'	=> 'form-control',
					'value'	=> isset_value($adresse, 'adresses_utilisateur_prenom')
				);
				$formulaire .= form_label('Prénom*', $attributs['id']);
				$formulaire .= form_input($attributs).br();
				
				$attributs = array(
					'name'	=> 'livraison[societe]',
					'id'	=> 'livraison-societe',
					'class'	=> 'form-control',
					'value'	=> isset_value($adresse, 'adresses_utilisateur_societe')
				);
				$formulaire .= form_label('Société', $attributs['id']);
				$formulaire .= form_input($attributs).br();
				
				$attributs = array(
					'name'	=> 'livraison[adresse]',
					'id'	=> 'livraison-adresse',
					'class'	=> 'form-control',
					'cols'	=> 50,
					'rows'	=> 3,
					'value'	=> isset_value($adresse, 'adresses_utilisateur_adresse')
				);
				$formulaire .= form_label('Adresse*', $attributs['id']);
				$formulaire .= form_textarea($attributs).br();
				
				$formulaire .= '<p class="demi-colonne">';
				$attributs = array(
		            'name'  => 'livraison[code_postal]',
		            'id'    => 'livraison-code_postal',
					'class'	=> 'form-control',
		            'value' => isset_value($adresse, 'adresses_utilisateur_code_postal')
		        );
		        $formulaire .= form_label('Code postal*', $attributs['id']);
		        $formulaire .= form_input($attributs);
				$formulaire .= '</p>';
				
				$formulaire .= '<p class="demi-colonne">';
		        $attributs = array(
		            'name'  => 'livraison[ville]',
		            'id'    => 'livraison-ville',
					'class'	=> 'form-control',
		            'value' => isset_value($adresse, 'adresses_utilisateur_ville')
		        );
		        $formulaire .= form_label('Ville*', $attributs['id']);
		        $formulaire .= form_input($attributs);
				
				$formulaire .= '</p><br class="clear-both" />';
				
				$formulaire .= modules::run('boutique/clients/zone_livraison', $adresse->adresses_utilisateur_pays_id, 'livraison[pays_id]');
		
				
				$formulaire .= '<p class="demi-colonne">';
				$attributs = array(
					'name'	=> 'livraison[telephone]',
					'id'	=> 'livraison-telephone',
					'class'	=> 'form-control',
					'value'	=> isset_value($adresse, 'adresses_utilisateur_telephone')
				);
				$formulaire .= form_label('Téléphone*', $attributs['id']);
				$formulaire .= form_input($attributs);
				$formulaire .= '</p>';
				
				$formulaire .= form_fieldset_close();
				
			}
			$formulaire .= '</div>';
		
		}
		$formulaire .= '</div>';
		
		$formulaire .= '<div class="zone-btn-validation">';
		$formulaire .= form_fieldset(NULL, 'class="clear-both" id="valide_adresses"');
		$attributs = array(
			'type'		=> 'submit',
			'value'		=> 1,
			'class'		=> 'btn btn-primary',
			'content' 	=> lang('valider')
		);
		$formulaire .= form_button($attributs);
		$formulaire .= form_fieldset_close();
		$formulaire .= '</div>';
		
		
		$formulaire .= '<div class="row">';
		//Affichage des autres adresses de livraisons enregistrées
		$formulaire .= '<div id="adresses_supp_compte" class="form-group col-xs-12">';
		$formulaire .= '<h4>Vos autres adresses de livraison</h4>';
		
		//Création d'une nouvelle adresse de livraison => Bouton pour ouvrir popup formulaire
		$formulaire .= '<a href="javascript:void(0)" id="creer_livraison" class="btn btn-adresse" data-client_id="'.$client->id.'">Créer une nouvelle adresse</a>';
		$count = 0;
		foreach ($client->adresses(FALSE) as $adresse)
		{
			$formulaire .= '<div class="bloc_livraison_compte panel panel-default">';
				$formulaire .= '<div class="panel-heading">'.((!empty($adresse->adresses_utilisateur_libelle)) ? ('<span class="libelle">'.$adresse->adresses_utilisateur_libelle.'</span>') : NULL);
					$formulaire .= '<a href="javascript:void(0)" class="suppr_adresse_compte pull-right" data-livraison_id="'.$adresse->adresses_utilisateur_id.'" data-url="/boutique/clients/suppr_form_livraison/'.$adresse->adresses_utilisateur_id.'"><span class="glyphicon glyphicon-remove"></span></a>';
					$formulaire .= '<a href="javascript:void(0)" class="modif_adresse_compte pull-right" data-url="/boutique/clients/genere_form_livraison/0/'.$adresse->adresses_utilisateur_id.'"><span class="glyphicon glyphicon-pencil"></span></a>';
				$formulaire .= '</div>';
				$formulaire .= '<div class="panel-body">';
					$formulaire .= $adresse->adresses_utilisateur_nom.' '.$adresse->adresses_utilisateur_prenom.br();
					$formulaire .= (!empty($adresse->sadresses_utilisateur_ociete)) ? ($adresse->adresses_utilisateur_societe.br()) : NULL;
					$formulaire .= $adresse->adresses_utilisateur_adresse.br();
					$formulaire .= $adresse->adresses_utilisateur_code_postal.' '.$adresse->adresses_utilisateur_ville.br();
					$formulaire .= $this->pays->get_by_id($adresse->adresses_utilisateur_pays_id)->nom.br();
					$formulaire .= $adresse->adresses_utilisateur_email.br();
					$formulaire .= (!empty($adresse->adresses_utilisateur_telephone)) ? ($adresse->adresses_utilisateur_telephone.br()) : NULL;
					$formulaire .= (!empty($adresse->adresses_utilisateur_mobile)) ? ($adresse->adresses_utilisateur_mobile.br()) : NULL;
				
				$formulaire .= '</div>';
			$formulaire .= '</div>';
			$count++;
		
		}
		$formulaire .= '</div>';

		$formulaire .= '</div>';		
		
		$formulaire .= form_close();
		
		/*$formulaire .= '<div id="supression-compte">';
		$formulaire .= anchor('/boutique/clients/supprimer', 'Supprimer mon compte', array('class' => 'text-right btn'));
		$formulaire .= '</div>';*/
		
	$formulaire .= '</div>';
$formulaire .= '</div>';
$formulaire .= '</div>';
echo $formulaire;