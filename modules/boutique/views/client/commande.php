<?php
$etats = array(
		array('id' => 1, 'libelle' => lang('etat_enregistre')),
		array('id' => 2, 'libelle' => lang('etat_payee')),
		array('id' => 3, 'libelle' => lang('etat_preparee')),
		array('id' => 4, 'libelle' => lang('etat_expediee')),
		array('id' => 5, 'libelle' => lang('etat_annulee')),
		array('id' => 6, 'libelle' => lang('etat_retournee')),
);

//Liste des articles commandés
$liste_produits = '<table class="table" class="liste-produits">';
$liste_produits .= '<tr  class="entete-commande">';
	$liste_produits .= '<th class="c-th-1">Reference</th><th class="c-th-2">Libelle</th><th class="c-th-3">Tarif</th><th class="c-th-4">Quantité</th>';
	if ($module->afficher_commentaires)
	{
		$liste_produits .= '<th>Avis produit</th>';
	}
		
$liste_produits .= '</tr>';
foreach ($produits as $produit)
{
	$liste_produits .= '<tr>';
	$liste_produits .= '<td class="reference c-td-1">'.$produit->reference.'</td>';
	$liste_produits .= '<td class="libelle c-td-2">'.$produit->libelle.'</td>';
	$liste_produits .= '<td class="prix c-td-3">'.html_price($produit->prix).'</td>';
	$liste_produits .= '<td class="quantite c-td-4">'.$produit->quantite.'</td>';
	
	if ($module->afficher_commentaires)
	{
		$commentaire_produit = new Commentaire();
		$commentaire_produit->where('produit_reference', $produit->reference)->get();
		if ($commentaire_produit->exists())
		{
			//Si commentaire déjà écrit
			$liste_produits .= '<td class="comentaire"><a href="javascript:void(0)" class="voir_commentaire" data-contenu="'.$commentaire_produit->commentaire.'" data-note="'.$commentaire_produit->note.'">Voir mon avis </a></td>';
		}
		else
		{
			$obj_produit = new Produit();
			$obj_produit->get_by_reference($produit->reference);
			//Si pas encore de commentaire écrit pour cet article
			$liste_produits .= '<td class="comentaire"><a href="javascript:void(0)" class="saisir_commentaire btn btn-info" data-utilisateur_id="'.$commande->utilisateur_id.'" data-module_id="'.$obj_produit->module_catalogue_id.'" data-reference_id="'.$obj_produit->produit_parent()->reference.'">Ecrire un avis</td>';
		}
	}
	
	

	$liste_produits .= '</tr>';
	
}
$liste_produits .= '</table>';

$data = array(
	'commande' 	=> array(
		'reference'		=> $commande->reference,
		'date'			=> $commande->created,
		'montant_total'	=> html_price($commande->montant),
		'remise'		=> html_price($remise->montant),
		'etat'			=> $commande->etat,
	    'id'            => $commande->id
	),
    'prenom'        => $utilisateur->prenom,
    'nom'        => $utilisateur->nom,
    'etats'			=> $etats,
	'facturation' 	=> get_object_vars($facturation),
	'livraison' 	=> get_object_vars($livraison),
	'paiement'		=> get_object_vars($paiement),
	'transporteur'	=> get_object_vars($transporteur),
	'liste_produits'=> $liste_produits,
	'url' => array(
		'facture' => '/boutique/commandes/exporter/'.$commande->id,
	),
	'lien' 		=> array(
		'commandes'		=> anchor('/boutique/clients/commandes', 'Afficher vos commandes', array('class' => 'bt_compte')),
		'mot_de_passe'	=> anchor('/boutique/clients/formulairemotdepasse', 'Modifier mon mot de passe', array('class' => 'bt_compte')),
		'adresses'		=> anchor('/boutique/clients/compte', 'Modifier vos coordonnées', array('class' => 'bt_compte')),
		'paniers'		=> anchor('/boutique/clients/lister_commandes_attente', 'Devis en attente', array('class' => 'bt_compte')),
		'deconnexion'	=> anchor('/boutique/clients/deconnecter', 'Se déconnecter', array('class' => 'bt_compte')),
	)
);
$this->dwootemplate->output(tpl_path('boutique/client/commande.tpl'), $data);