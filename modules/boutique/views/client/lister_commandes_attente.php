<?php
$liste = NULL;

$liste_commandes = NULL;
$derniere_commande = NULL;
$i = 0;

$derniere_commande .= '<table class="table devis-attente">';
$derniere_commande .= '<tr><th class="c-th-5">Date</th><th class="c-th-1">Total</th><th class="c-th-6">Etat</th><th class="c-th-3">Voir</th></tr>';

foreach ($commandes as $commande)
{
	if (in_array($commande->etat, array(7)))
	{	
// 		$derniere_commande .= '<div class="ligne_liste_commandes row">';
// 		$derniere_commande .= '<a href="/boutique/clients/commande/'.$commande->id.'" class="date col-md-3">Devis du '.date_type($commande->created, 2).'</a>';
// 		$derniere_commande .= 'testttttttt <span class="montant_ligne_commande col-md-2">'.html_price($commande->montant).' €</span>';
// 		$derniere_commande .= '<span class="etat col-md-3">'.$etats[$commande->etat].'</span>';
// 		$derniere_commande .= anchor('/boutique/commandes/exporter/'.$commande->id.'/0/1', "<span class='jaune glyphicon glyphicon-save'></span> Télécharger votre devis", "class='col-md-4'").br();
// 		$derniere_commande .= '</div>';
		
		
		$derniere_commande .= '<tr class="ligne_liste_commandes">';
		$derniere_commande .= '<td class="c-td-5"><a href="/boutique/clients/commande/'.$commande->id.'" class="date">Devis du '.date_type($commande->created, 2).'</a></td>';
		$derniere_commande .= '<td class="c-td-1"><span class="montant_ligne_commande">'.html_price($commande->montant).'</span></td>';
		$derniere_commande .= '<td class="c-td-6"><span class="etat">'.$etats[$commande->etat].'</span></td>';
		$derniere_commande .= '<td class="c-td-3">'.anchor('/boutique/commandes/exporter/'.$commande->id.'/0/1', "Télécharger votre devis", array('class' => 'lien-telechargement')).'</td>';
		$derniere_commande .= '</tr>';
		
	
		$i++;
	}
}
$derniere_commande .= '</table>';

$data = array(
	'nb_commandes'		=> $i,
	'derniere_commande'	=> $derniere_commande,
	'liste_commandes'	=> $liste_commandes,
    'prenom'        => $client->prenom,
    'nom'        => $client->nom,
    'client_pro'    => $client->pro,
	'lien_retour' 		=> anchor('/boutique/clients/compte', 'Retourner sur votre compte', array('class' => 'bt_retour')),
    'lien' 		=> array(
        'commandes'		=> anchor('/boutique/clients/commandes', 'Afficher vos commandes', array('class' => 'bt_compte')),
        'adresses'		=> anchor('/boutique/clients/compte', 'Modifier vos coordonnées', array('class' => 'bt_compte')),
        'mot_de_passe'	=> anchor('/boutique/clients/formulairemotdepasse', 'Modifier mon mot de passe', array('class' => 'bt_compte')),
        'paniers'		=> anchor('/boutique/clients/lister_commandes_attente', 'Devis en attente', array('class' => 'bt_compte')),
        'deconnexion'	=> anchor('/boutique/clients/deconnecter', 'Se déconnecter', array('class' => 'bt_compte')),
    )
);
$this->dwootemplate->output(tpl_path('boutique/client/lister_commandes_attente.tpl'), $data);