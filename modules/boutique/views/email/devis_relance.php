<?php
//Présentation
$data1['presentation'] = array(
	'date_jour'	=> date_type(mdate('%Y-%m-%d'), 1),
	'prenom'	=> $client->prenom,
	'nom'		=> $client->nom,
	'reference'	=> $devis->reference,
	'date_devis'	=> datetime_type($devis->created),
);




if (file_exists(ROOTPATH.'assets/img/newsletter/logo.png'))
{
    $attributs['src'] = 'assets/img/newsletter/logo.png';
    $logo = img($attributs);
}
elseif (file_exists(ROOTPATH.'assets/img/newsletter/logo.jpg'))
{
    $attributs['src'] = 'assets/img/newsletter/logo.jpg';
    $logo = img($attributs);
}

$data = array(
	'contenu' => $this->dwootemplate->get(tpl_path('boutique/email/devis_relance.tpl'), $data1),
	'email_vendeur' => mailto($module->vendeur_email, $module->vendeur_email),
	'logo'     => $logo
);

$this->dwootemplate->output(tpl_path('boutique/client/email.tpl'), $data);