<?php
//Présentation
$data1['presentation'] = array(
	'date_jour'	=> date_type(mdate('%Y-%m-%d'), 1),
	'commandes_relancees'	=> $commandes_relancees,
	'nb_relances'		=> count($commandes_relancees)
);




if (file_exists(ROOTPATH.'assets/img/newsletter/logo.png'))
{
    $attributs['src'] = 'assets/img/newsletter/logo.png';
    $logo = img($attributs);
}
elseif (file_exists(ROOTPATH.'assets/img/newsletter/logo.jpg'))
{
    $attributs['src'] = 'assets/img/newsletter/logo.jpg';
    $logo = img($attributs);
}

$data = array(
	'contenu' => $this->dwootemplate->get(tpl_path('boutique/email/commande_relance_vendeur.tpl'), $data1),
	'email_vendeur' => mailto($module->vendeur_email, $module->vendeur_email),
	'logo'     => $logo
);

$this->dwootemplate->output(tpl_path('boutique/client/email.tpl'), $data);