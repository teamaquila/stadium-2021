<?php
$html = NULL;

if ( ! is_null($commandes) && ($commandes->result_count() > 0))
{
	foreach ($commandes as $commande)
	{		
		//$etat = form_dropdown('etat', $etats , $commande->etat, 'class="change_etat" id="'.$commande->id.'"');
		
		$label_etats = array(
			1 => 'label-info',
			2 => 'label-warning',
			3 => 'label-warning',
			4 => 'label-success',
			5 => 'label-danger',
			6 => 'label-danger',
			7 => 'label-primary',
			8 => 'label-primary',
		    9 => 'label-primary',
		    10 => 'label-primary',
		);
		
		$data = array(
			'id'		=> $commande->id,
			'reference'	=> $commande->reference,
			'montant'	=> html_price($commande->montant),
			'client'	=> $commande->utilisateur_prenom.' '.$commande->utilisateur_nom,
			'date'		=> datetime_type($commande->created),
			'etat'		=> '<span class="label '.$label_etats[$commande->etat].'">'.$etats[$commande->etat].'</span>',
			'etat_id'	=> $commande->etat,
			'relancee'	=> ($commande->etat == 1) ? ($commande->relancee) : NULL,
			'url' => array(
				'afficher'	=> '/boutique/admin/commandes/afficher/'.$commande->id
			)
		);
		$html .= $this->dwootemplate->get(tpl_path('boutique/admin/commandes/commande.tpl'), $data);
	}
}
else
{
	$html .= '<td colspan="6" class="liste-vide">'.lang('aucune_commande').'</td>';
}

echo $html;