<?php
//Liste déroulante des états
$etat = form_dropdown('etat', $etats , $commande->etat, 'class="change_etat" id="'.$commande->id.'"');

//Commande
$info_commande = array(
	'id'			=> $commande->id,
	'reference'		=> $commande->reference,
	'montant'		=> $commande->montant,
	'cadeau'		=> $commande->cadeau,
	'etat'			=> form_dropdown('change_etat', $etats, $commande->etat, 'class="change_etat" id="'.$commande->id.'"'),
	'etat_id'		=> $commande->etat,
	'remise'		=> NULL,
	'frais_port'	=> $livraison->frais_port,
	'date'			=> $commande->created,
	'suivi'			=> $transporteur->url_suivi.$livraison->code_suivi,
	'nom_transporteur'	=> $transporteur->nom,
	'etiquetage_colissimo'	=> ($commande->etat > 1) ? $transporteur->etiquetage_colissimo : NULL,
	'etiquetage_chronopost'	=> ($commande->etat > 1) ? $transporteur->etiquetage_chronopost : NULL,
	'nom' 			=> $client->nom,
	'prenom' 		=> $client->prenom,
	'email' 		=> $client->email,
	'telephone' 	=> $client->telephone,
	'hors_taxe' 	=> ($commande->hors_taxe) ? 'Oui' : 'Non'
);

//Remise sur la commande
if ($remise->exists())
{
	$info_commande['remise'] = get_object_vars($remise);
}

//Commentaire sur la commande

if ($module->option_commentaire_commande && ! empty($commande->commentaire))
{
	$info_commande['commentaire_commande'] = $commande->commentaire;
}
else
{
	$info_commande['commentaire_commande'] = 'Aucun';
}

//Affiche la liste des produits
$liste_produits  = NULL;

foreach ($commande->produits() as $produit_commande)
{
	$liste_produits .= '<table class="produits table">';
	$liste_produits .= '<tr><td class="quantite">'.$produit_commande->quantite.'x</td>';
	$liste_produits .= '<td class="libelle">['.$produit_commande->reference.'] '.$produit_commande->libelle;
	
	if (($produit_commande->options))
	{
	    $options = json_decode($produit_commande->options);
	    if (!empty($options))
	    {
	        $liste_produits .= '<p class="options">';
	        foreach ($options as $option => $value_option)
	        {
	            if (!empty($value_option))
	            {
	                if ($option == 'produit_referent_id')
	                {
	                    $produit_option_referent = new Produit();
	                    $produit_option_referent->get_by_id($value_option);
	                    $value_option = $produit_option_referent->reference;
	                }
	                $liste_produits .= $option.' : '.$value_option.br();
	            }
	            
	        }
	        $liste_produits .= '</p>';
	    }
	    
	}
	$liste_produits .= '</td>';
	$liste_produits .= '<td class="prix">'.html_price($produit_commande->prix*$produit_commande->quantite).'</td>';
	
	//Options
	if ($produit_commande->options != 'false')
	{
		$liste_produits .= '</tr><tr class="options"><td colspan="3">';
		
		foreach (json_decode($produit_commande->options) as $key => $value)
		{
			//Le libellé est obligatoire pour afficher l'option
			if (lang($key) !== FALSE)
			{
				$liste_produits .= '<p> + <span class="cle">'.lang($key).'</span>';
				
				if (is_uri($value))
				{
					$valeur = anchor($value, basename($value), array('target' => '_blank'));
				}
				elseif (is_bool($value))
				{
					$valeur = ($value) ? 'Oui' : 'Non';
				}
				else
				{
					$valeur = $value;
				}
				
				$liste_produits .= '<span class="valeur">'.$valeur.'</span></p>';
			}
		}
		$liste_produits .= '</td></tr>';
	}
	else
	{
		$liste_produits .= '</tr>';
	}
	
	$liste_produits .= '</table>';
}


$data = array(
	'liste_produits'=> $liste_produits,
	'info_commande'	=> $info_commande,
	'facturation'	=> get_object_vars($facturation),
	'livraison'		=> get_object_vars($livraison),
	'complements_livraison' => ( ! empty($livraison->complements)) ? get_object_vars(json_decode($livraison->complements)) : NULL,
	'info_paiement'	=> get_object_vars($paiement),
	'url' => array(
		'historique' => '/boutique/admin/commandes/historique/'.$commande->id,
		'icones' => INTERFACEPATH.'application/img/icones-commune/',
		'import_csv' => '/boutique/admin/commandes/exporter/csv/'.$commande->id,
		'import_pdf' => '/boutique/admin/commandes/exporter/pdf/'.$commande->id,
	),
);	

$this->dwootemplate->output(tpl_path('boutique/admin/commandes/affiche.tpl'), $data);