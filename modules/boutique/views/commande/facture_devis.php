<?php
$this->load->model('transporteur', 'client', 'code_remise');
$liste_produits = NULL;

$type_client = $this->client->get_by_id($commande->client_id)->type_client_id;
$montant_produits_ttc = 0;
$montant_produits_HT = 0;
$montant_commande_HT = 0;
$remise_ttc = 0;
$tableau_tva = array();
$existe_remise_pourcentage = FALSE;
$existe_remise_montant = FALSE;
$taux_tva_livraison = 1 + ($module->tva_livraison/100);

$count_lignes = 0;

$total_montant_ht = 0;
$total_remises_ht = 0;
$total_remises_ttc = 0;
$total_net_ht = 0;

//Option remise
if ($module->option_remise)
{
	$remise_ttc = $remise->montant;
	
	if (($remise_ttc > 0) && ($detail_remise->type_reduction == 1))
	{
		$existe_remise_pourcentage = TRUE;
	}
	elseif (($remise_ttc > 0) && ($detail_remise->type_reduction == 2))
	{
		$existe_remise_montant = TRUE;
	}
}

$existe_remise_produit = FALSE;
foreach ($produits as $produit)
{
    if ($produit->prix_promo != 0)
    {
        $existe_remise_produit = TRUE;
    }
}

//Produits commandés
foreach ($produits as $produit)
{
	$obj_produit = new Produit();
	$obj_produit->get_by_id($produit->produit_id);
	
	$count_lignes++;
	
	$prix_ht = $produit->prix_sans_remise/( 1 + $produit->tva/100 );
	$prix_ttc = $produit->prix;
	$montant_ht = ($produit->prix_sans_remise*$produit->quantite)/( 1 + $produit->tva/100 );
	$montant_ttc = ($produit->prix_sans_remise*$produit->quantite);
	
	$total_montant_ht += $montant_ht;
	
	$options = NULL;
	$decalage = NULL;
	if (is_object(json_decode($produit->options)))
	{
		foreach (json_decode($produit->options) as $index => $option)
		{
			if (!empty($option))
			{
				switch ($index) {
					case 'associe_gravure':
						$lien = base_url().'/echanges/export/fichiers_gravure/'.$option;
						$options .= '<br/><i>Fichier : <a href="'.$lien.'">'.$option.'</a></i>';
	
						break;
							
					case 'contenu_gravure':
						$options .= '<br/><i>Gravure : '.preg_replace('/\\n/', ' / ', $option).'</i>';
						break;
							
					case 'gravure_complementaire':
						$options .= '<br/><i>Gravure : '.preg_replace('/\\n/', ' / ', $option).'</i>';
						break;
						
					case 'pack_parent' :
						$decalage = '- ';
					default:
						;
						break;
				}
			}
	
		}
	}
	
	$liste_produits .= '<tr>';
		if (isset($remise_ttc) && $obj_produit->produit_remisable)
		{$liste_produits .= '<td class="td1 td-1_remise">'.$decalage.$produit->libelle.$options.'</td>';}
		else{$liste_produits .= '<td class="td1">'.$decalage.$produit->libelle.$options.'</td>';}
		
	$liste_produits .= '<td class="td2">'.$produit->quantite.'</td>
		<td class="td3">'.prix($prix_ht).'</td>
		<td class="td6">'.prix($montant_ht).'</td>
		<td class="td7">'.prix($produit->tva).'</td>
		<td class="td8">'.prix($montant_ttc).'</td>';
	
			
	if (($existe_remise_pourcentage) && ($obj_produit->produit_remisable) && ($produit->prix_sans_remise != $produit->prix))
	{
	    /* Vérification du type de client*/
	    if($type_client == 2){
	        $remise_produit = ($montant_ht*$detail_remise->reduction)/100;
	        $montant_ht = $montant_ttc/( 1 + $produit->tva/100 )-$remise_produit;
	        $montant_ttc = $montant_ttc-$remise_produit;
	        
	        $total_remises_ht += $remise_produit ;
	        $taxe_remise = 'HT';
	    }
	    else{
	        $remise_produit = ($montant_ttc*$detail_remise->reduction)/100;
	        $montant_ht = $montant_ttc/( 1 + $produit->tva/100 )-$remise_produit;
	        $montant_ttc = $montant_ttc-$remise_produit;
	        
	        $total_remises_ht += $remise_produit;
	        $taxe_remise = 'TTC';
	    }
	    
	    $total_net_ht += $montant_ht;
	    
	    $liste_produits .= '<td class="td9">'.prix($remise_produit).'</td>
		<td class="td10">'.prix($montant_ttc).'</td>';
	}
	elseif (($produit->prix_promo != 0))
	{
	    /* Vérification du type de client*/
	    if($type_client == 2){
	        $remise_produit = ($montant_ht*$detail_remise->reduction)/100;
	        $montant_ht = $montant_ttc/( 1 + $produit->tva/100 )-$remise_produit;
	        $montant_ttc = $montant_ttc-$remise_produit;
	        
	        $total_remises_ht += $remise_produit ;
	        $taxe_remise = 'HT';
	    }
	    else{
	        $remise_produit = ($montant_ttc*$detail_remise->reduction)/100;
	        $montant_ht = $montant_ttc/( 1 + $produit->tva/100 )-$remise_produit;
	        $montant_ttc = $montant_ttc-$remise_produit;
	        
	        $total_remises_ht += $remise_produit;
	        $taxe_remise = 'TTC';
	    }
	    
	    $total_net_ht += $montant_ht;
	    
	    $liste_produits .= '<td class="td9">'.prix($remise_produit).'</td>
		<td class="td10">'.prix($montant_ttc).'</td>';
	    
	}
	elseif ($existe_remise_pourcentage && $detail_remise->produits_associes()->result_count() == 0)
	{
	    /* Vérification du type de client*/
	    if($type_client == 2){
	        $remise_produit = ($montant_ht*$detail_remise->reduction)/100;
	        $montant_ht = $montant_ttc/( 1 + $produit->tva/100 )-$remise_produit;
	        $montant_ttc = $montant_ttc-$remise_produit;
	        
	        $total_remises_ht += $remise_produit ;
	        $taxe_remise = 'HT';
	    }
	    else{
	        $remise_produit = ($montant_ttc*$detail_remise->reduction)/100;
	        $montant_ht = $montant_ttc/( 1 + $produit->tva/100 )-$remise_produit;
	        $montant_ttc = $montant_ttc-$remise_produit;
	        
	        $total_remises_ht += $remise_produit;
	        $taxe_remise = 'TTC';
	    }
	    
	    $total_net_ht += $montant_ht;
	    
	    $liste_produits .= '<td class="td9">'.prix($remise_produit).'</td>
		<td class="td10">'.prix($montant_ttc).'</td>';
	    
	}
	else 
	{
		$total_net_ht += $montant_ht;
		
		//Si la remise ne s'applique pas à ce produit, mais sur un autre prosuit de la commande, mettre les colonnes
		if ($existe_remise_pourcentage || $existe_remise_produit)
		{
			$liste_produits .= '<td class="td9">'.prix(0).'</td>
			<td class="td10">'.prix($montant_ttc).'</td>';
		}
	}
		
	$liste_produits .='</tr>';
	
	$montant_produits_ttc += $montant_ttc;
	$montant_produits_HT += $montant_ht;
	if ( ! array_key_exists($produit->tva, $tableau_tva))
	{
		$tableau_tva[$produit->tva] = 0;
	}
	
	//Cumul des montant TVA
	$tableau_tva[$produit->tva] += ($montant_ttc-$montant_ht);
}

//Remise
$montant_remise = NULL;

if ($module->option_remise)
{
	$remise_ttc = $remise->montant;
	
	if ($existe_remise_pourcentage)
	{
	    $remise_ht = $total_remises_ht;
	    
	    if($remise_ht==0){
	        $remise_ht = $remise->montant-($remise->montant/$produit->tva);
	    }
		
		$montant_remise = '
		<tr>
			<td>Remise</td>
			<td> -'.prix($remise_ht).'</td>
		</tr>';
	}
	else
	{
		$montant_commande_HT += $montant_produits_HT;
	}
}
else
{
	$montant_commande_HT += $montant_produits_HT;
}

//Montant commande après remise
$montant_commande_ttc = $montant_produits_ttc-$remise_ttc;

//Frais de port
if ($module->option_livraison)
{
	$count_lignes++;
	
	$liste_produits .= '<tr>';
	if (isset($remise_ttc))
	{
		$liste_produits .= '<td class="td1 td-1_remise">Frais de port</td>';
	}
	else
	{
		$liste_produits .= '<td class="td1">Frais de port</td>';
	}
		
	$liste_produits .= '<td class="td2">1</td>
		<td class="td3">'.prix($commande->frais_port/$taux_tva_livraison).'</td>
		<td class="td6">'.prix($commande->frais_port/$taux_tva_livraison).'</td>
		<td class="td7">'.prix($module->tva_livraison).'</td>
		<td class="td8">'.prix($commande->frais_port).'</td>';
	
	if ($existe_remise_pourcentage)
	{
		$liste_produits .= '<td class="td9">'.prix(0).'</td>
		<td class="td10">'.prix($commande->frais_port).'</td>';
	}
		
	$liste_produits .= '</tr>';
	
	$montant_commande_HT += $commande->frais_port/$taux_tva_livraison;

	$total_montant_ht += $commande->frais_port/$taux_tva_livraison;
	$total_net_ht += $commande->frais_port/$taux_tva_livraison;
	
	if ( ! array_key_exists($module->tva_livraison, $tableau_tva))
	{
		$tableau_tva[$module->tva_livraison] = 0;
	}
	$tableau_tva[$module->tva_livraison] += $commande->frais_port-($commande->frais_port/$taux_tva_livraison);
}

//TVA
$ligne_tva = NULL;
$total_tva = 0;
	
foreach ($tableau_tva as $tauxTva => $montantTva)
{
	$ligne_tva .= '<tr><td>Total TVA '.prix($tauxTva).'</td><td>'.prix($montantTva).'</td></tr>';
	$total_tva += $montantTva;
}
	
$remise_montant_TTC = NULL;
$total_non_remise_ttc = NULL;
if ($existe_remise_montant)
{
	$remise_montant_TTC 	= ($detail_remise->reduction);
	$total_non_remise_ttc 	= ($commande->montant-$remise_montant_TTC);
}

//Pagination
$saut_page = FALSE;
$total_pages = 1;

if ($count_lignes > $item_par_page)
{
	$saut_page = TRUE;
	$total_pages = 2;
}

$data = array(
	'logo' => '<img src="'.root_path().'/assets/img/logo.png" id="logo" alt="logo" />',
	'commande' 	=> array(
		'reference'		=> $commande->reference,
		'date'			=> $commande->created,
		'montant_ht'	=> prix($total_montant_ht),
		'montant_net_ht'=> prix($total_net_ht),
		'montant_ttc'	=> prix($commande->montant),
		'montant_remise'=> $montant_remise,
		'existe_remise'	=> $existe_remise_pourcentage,
		'existe_remise_montant'	=> $existe_remise_montant,
		'total_non_remise_ttc' 	=> prix($total_non_remise_ttc),
		'remise_montant_ttc' 	=> prix($remise_montant_TTC),
		'commentaire'		=> $commande->commentaire,
		'transporteur'	=> $this->transporteur->get_by_id($commande->transporteur_id)->nom
	),
	'facturation' 	=> get_object_vars($facturation),
	'livraison' 	=> get_object_vars($livraison),
	'paiement'		=> get_object_vars($paiement),
	'vendeur' => array(
		'societe'		=> $module->vendeur_societe,
		'nom'			=> $module->vendeur_nom,
		'adresse'		=> $module->vendeur_adresse,
		'code_postal'	=> $module->vendeur_code_postal,
		'ville'			=> $module->vendeur_ville,
		'pays'			=> $module->vendeur_pays,
		'telephone'		=> $module->vendeur_telephone,
		'email'			=> $module->vendeur_email,
		'fax'			=> $module->vendeur_fax,
		'infos'			=> $module->vendeur_infos,
	),
	'liste_produits'	=> $liste_produits,
	'option_livraison'	=> $module->option_livraison,
	'ligne_tva'			=> $ligne_tva,
	'mentions_legales'	=> $module->mentions_facture,
	'saut_page'			=> $saut_page,
	'body_top'			=> $body_top,
	'total_page'		=> $total_pages
);
$this->dwootemplate->output(tpl_path('boutique/commande/facture_devis.tpl'), $data);