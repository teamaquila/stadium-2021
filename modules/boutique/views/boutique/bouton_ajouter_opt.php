<?php
$html = NULL;

$this->load->models(array('catalogue/critere', 'catalogue/criteres_valeurs_produit'));

$html .= '<div class="widget boutique" itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer">';
$html .= form_open('boutique/paniers/ajouter', array('class' => 'ajouter-panier', 'autocomplete' => 'off'));
//$html .= form_fieldset();

if ($formulaire_produit)
{
    $html .= '<div class="bloc_formulaire">
					<label for="">Gravure :</label>
					<textarea rows="" cols="" name="options[contenu_gravure]" placeholder=" Initiale du prénom + nom + promo "></textarea>
					<br/>
					<label for="">Fichier</label>
					<input type="file" name="options[associe_gravure]" />
					<input type="hidden" name="options[reference_gravure]" value="'.$formulaire_produit.'"/>
                    <span>(format .xls ou .xlsx)</span>
                    <a class="btn btn-primary fichier-exemple" href="/medias/fichier-gravure/matrice.xlsx" target="_blank">Fichier d\'exemple</a><br class="clear-both">
			 </div>';
}


//$html .= form_fieldset('', array('class' => 'test',));
$html .= form_fieldset();

//Options
if (is_array($options) && ! empty($options))
{
    foreach ($options as $key => $value)
    {
        $html .= form_hidden('options['.$key.']', $value);
    }
}

$html .= '<div class="zone-selection">';

$html .= '<div class="bloc_choix_produit">';
//Références
$references = $produit->produits_enfants(array(
    'stock' => ($module->gestion_stock),
    'visible' => TRUE
), 0, 1);
if ($references->exists() && ($references->result_count() > 0))
{
    if ($references->result_count() > 1)
    {
        //Critères
        $liste_criteres = NULL;
        foreach ($references as $reference)
        {
            $criteres_associes = new Criteres_valeurs_produit();
            //$criteres_associes->where('produit_id', $reference->id)->where('choix_enfant >', 0)->include_related('critere')->order_by('choix_enfant')->get();
            $criteres_associes->where('produit_id', $reference->id)->where('choix_enfant >', 0)->include_related('critere')->include_related('criteres_valeur')->order_by('choix_enfant')->get();
            //$criteres_associes->check_last_query();
            if ( ! is_null($criteres_associes) && ($criteres_associes->result_count() > 0))
            {
                foreach ($criteres_associes as $critere_associe)
                {
                    if ( ! empty($critere_associe->criteres_valeur_valeur))
                    {
                        $liste_criteres[$critere_associe->critere_choix_enfant][$critere_associe->critere_id][$critere_associe->criteres_valeur_valeur] = $critere_associe->id;
                        
                    }
                    else
                    {
                        //	$liste_criteres[$critere_associe->critere_choix_enfant][$critere_associe->critere_id][''] = $critere_associe->id;
                    }
                }
            }
        }
        
        if (is_array($liste_criteres))
        {
            //Ordonne le tableau avec l'ordre signalé dans choix enfant
            ksort($liste_criteres);
            
            
            $html .= '<input type="hidden" name="parent_id_criteres" value="'.$produit->id.'">';
            
            foreach ($liste_criteres[1] as $critere_id => $tab_valeurs)
            {
                
                
                if (isset($liste_criteres[2]))
                {
                    
                    $i = 0;
                    $deuxième_ligne_choix = NULL;
                    $html .= '<div data-ordre="1" class="zone-choix-critere" id="zone-choix-critere-principal" >';
                    $html .= '<span>'.$this->critere->get_by_id($critere_id)->libelle.'</span>';
                    foreach ($tab_valeurs as $valeur_critere => $id_crit)
                    {
                        $this->criteres_valeurs_produit->get_by_id($id_crit);
                        $affiche = (($valeur_critere == '')) ? 'TU' : $valeur_critere;
                        if ($this->criteres_valeurs_produit->criteres_valeur->media_associe())
                        {
                            $affiche = html_media($this->criteres_valeurs_produit->criteres_valeur->media_associe(), array('taille' => '50x50'));
                        }
                        
                        $selected = NULL;
                        $selected = ($i == 0) ? ('selected="selected"') : NULL;
                        $active = ($i == 0) ? ('class="active"') : NULL;
                        $html .= '<div class="item_critere critere_'.strtolower (urlencode($valeur_critere)).'">
								<input '.$selected.' value="'.$valeur_critere.'" type="radio" data-critere_id="'.$critere_id.'" data-parent_id="'.$produit->id.'" name="choix_critere['.$critere_id.']" class="select_critere_produit_enfant" id="critere_'.$valeur_critere.'">
								<label for="critere_'.$valeur_critere.'" '.$active.'>'.$affiche.'</label>
								</div>';
                        
                        if ($i == 0)
                        {
                            $deuxième_ligne_choix .= Modules::run('catalogue/get_deuxieme_choix_criteres', $valeur_critere, $critere_id, $produit->id, $liste_criteres);
                            
                        }
                        $i++;
                    }
                    
                    $html .= '</div>';
                    
                    $html .= '<span>'.$this->critere->get_by_id(key($liste_criteres[2]))->libelle.'</span>';
                    $html .= '<div data-ordre="2" class="zone-choix-critere" id="zone-choix-critere-secondaire" data-critere_id="'.key($liste_criteres[2]).'">';
                    $html .= $deuxième_ligne_choix;
                    $html .= '</div>';
                }
                
                else
                {
                    $i = 0;
                    $deuxième_ligne_choix = NULL;
                    $html .= '<div data-ordre="1" class="zone-choix-critere" id="zone-choix-critere-principal" >';
                    $html .= '<span class="libelle-condi">&Eacute;chelon : </span>';
                    $html .= '<select name="produit_id">';
                    $html .= '<option>Choisir une référence</option>';
                    
                    foreach ($tab_valeurs as $valeur_critere => $id_crit)
                    {
                        $this->criteres_valeurs_produit->get_by_id($id_crit);
                        $affiche = (($valeur_critere == '')) ? 'TU' : $valeur_critere;
                        if ($this->criteres_valeurs_produit->criteres_valeur->media_associe())
                        {
                            $affiche = html_media($this->criteres_valeurs_produit->criteres_valeur->media_associe(), array('taille' => '50x50'));
                        }
                        
                        // 				            $selected = ($i == 0) ? ('selected="selected"') : NULL;
                        // 				            $active = ($i == 0) ? ('class="active"') : NULL;
                        $selected = NULL;
                        $active = NULL;
                        // 							$html .= '<div class="item_critere critere_'.strtolower (urlencode($valeur_critere)).'">
                        // 								<input '.$selected.' value="'.$this->criteres_valeurs_produit->produit_id.'" type="radio" name="produit_id" id="critere_'.$valeur_critere.'">
                        // 								<label for="critere_'.$valeur_critere.'" '.$active.'>'.$affiche.'</label>
                        // 								</div>';
                        
                        $html .= '<option '.$selected.' value="'.$this->criteres_valeurs_produit->produit_id.'" data-critere_id="'.$critere_id.'" data-parent_id="'.$produit->id.'"  >'.$valeur_critere.'</option>';
                        
                        
                        $i++;
                    }
                    $html .= '</select>';
                    $html .= '</div>';
                    
                }
                
                
            }
        }
    }
    elseif ($references->result_count() == 1)
    {
        $html .= '<span class="reference-libelle">'.$references->libelle.'</span>';
        $html .= form_hidden('produit_id', $references->id);
    }
    
    //Tarifs
    $tarifs = $references->tarif($client);
    
    /**
     * Choix de quantité
     */
    $html .= '</div>';
    $html .= '<div class="zone-quantite">';
    $html .= '<div class="">';
    $html .= '<label>'.lang('quantite').' :</label>';
    
    //Gestion de stock
    if ($module->gestion_stock)
    {
        $stock = array_combine(range(0, $references->stock, $references->conditionnement), range(0, $references->stock, $references->conditionnement));
        
        unset($stock[0]);
        
        $html .= form_dropdown('quantite', $stock, NULL, 'class="quantite form-control"');
        
        //Info stock
        $html .= '<div class="statut-stock">'.statut_stock($references->stock, $module->seuil_alerte_stock).'</div>';
    }
    //Achat illimité
    else
    {
        $html .= '<div class="btn-saisie-quantite input-qte"><span class="bt-moins bt-qte">'.form_button(array(
            'class' => 'btn btn-default ',
            'content' => '-'
        )).'</span>';
        $html .= form_input(array(
            'type'  => 'text',
            'name'	=> 'quantite',
            'class' => 'quantite form-control',
            'value'	=> ($produit->conditionnement) ? $produit->conditionnement : 1
        ));
        $html .= '<span class="bt-plus bt-qte">'.form_button(array(
            'class' => 'btn btn-default ',
            'content' => '+'
        )).'</div></span>';
        
    }
    $html .= '</div>';
}
//Produit
else
{
    //Tarifs
    $tarifs = $produit->tarif($client);
    
    /**
     * Choix de quantité
     */
    $html .= '</div>';
    $html .= '<div class="zone-quantite">';
    $html .= '<div class="input-qte">';
    $html .= '<label>'.lang('quantite').' :</label>';
    
    //Gestion de stock
    if ($module->gestion_stock)
    {
        $stock = array_combine(range(0, $produit->stock, $produit->conditionnement), range(0, $produit->stock, $produit->conditionnement));
        
        unset($stock[0]);
        
        $html .= form_dropdown('quantite', $stock, NULL, 'class="quantite form-control"');
        
        //Info stock
        $html .= '<div class="statut-stock">'.statut_stock($produit->stock, $module->seuil_alerte_stock).'</div>';
    }
    //Achat illimité
    else
    {
        $html .= '<div class="btn-saisie-quantite  input-qte"><span class="bt-moins bt-qte">'.form_button(array(
            'class' => 'btn btn-default ',
            'content' => '-'
        )).'</span>';
        $html .= form_input(array(
            'type'  => 'text',
            'name'	=> 'quantite',
            'class' => 'quantite form-control',
            'value'	=> ($produit->conditionnement) ? $produit->conditionnement : 1
        ));
        $html .= '<span class="bt-plus bt-qte">'.form_button(array(
            'class' => 'btn btn-default ',
            'content' => '+'
        )).'</span></div>';
        
    }
    $html .= '</div>';
    
    $html .= form_hidden('produit_id', $produit->id);
}

$html .= '</div>';
$html .= '</div>';

$html .= '<div class="zone-btn-ajout">';
$html .= '<div class="col-demi">';
//Prix unitaire, prix remisé et taux de remise
$html .= $this->load->view('boutique/boutique/tarifs', array('tarifs' => $tarifs, 'client'  => $client), TRUE);

//Si les enfants du produits sont des articles collections composés d'autres articles on calcule le prix en fonction des enfants
$trouve_compose = FALSE;
foreach ($produit->produits_enfants() as $enfant)
{
    $produits_composes = $this->db->query("
				SELECT t_produits.id
				FROM t_produits
				INNER JOIN t_produits_produits ON t_produits.id = t_produits_produits.related_produit_id
				WHERE t_produits_produits.compose = 1
				AND t_produits_produits.produit_id = ".$enfant->id."
			");
    if ($produits_composes->num_rows() != 0)
    {
        $trouve_compose = TRUE;
    }
}
$html .= '</div>';
$html .= '<div class="col-demi">';
//Bouton panier
if (($tarifs['prix'] == 0) && !$module->autoriser_achat_gratuit && !$trouve_compose)
{
    $html .= '<div class="btn">'.lang('produit_indisponible_vente').'</div>';
}
else
{
    $html .= form_button(array(
        'type'		=> 'submit',
        'class'		=> 'btn btn-panier',
        'content'	=> ''.lang('boutique_ajouter_panier')
    ));
}
$html .= '</div>';
$html .= '</div>';

$html .= form_fieldset_close();

$html .= form_close();
$html .= '</div>';

echo $html;