<?php
$html = NULL;
$html .= '<div class="tarif">';


//Si les enfants du produits sont des articles collections composés d'autres articles on calcule le prix en fonction des enfants
$trouve_compose = FALSE;
$tab_tarifs_compose = array();
foreach ($produit->produits_enfants() as $enfant)
{
    $produits_composes = $this->db->query("
				SELECT t_produits.id
				FROM t_produits
				INNER JOIN t_produits_produits ON t_produits.id = t_produits_produits.related_produit_id
				WHERE t_produits_produits.compose = 1
				AND t_produits_produits.produit_id = ".$enfant->id."
			");
    if ($produits_composes->num_rows() != 0)
    {
        $trouve_compose = TRUE;
        
        $tab_tarifs_compose[$enfant->id]	= array(
            'prix'	=> 0,
            'prix_sans_remise'	=> 0
        );
        foreach ($produits_composes->result() as $row)
        {
            $prod_compo = new Produit();
            $prod_compo->get_by_id($row->id);
            $tab_tarifs_compose[$enfant->id]['prix'] +=  $prod_compo->tarif($client)['prix'];
            $tab_tarifs_compose[$enfant->id]['prix_sans_remise'] +=  $prod_compo->tarif($client)['prix'];
        }
    }
}

if ($trouve_compose)
{
    $tarif_precedent = 0;
    foreach ($tab_tarifs_compose as $enfant_id => $tarif_enfant)
    {
        if (($tarif_precedent == 0 ) or ($tarif_enfant['prix'] < $tarif_precedent))
        {
            $data['tarif']['prix']	= $tarif_enfant['prix'];
            $data['tarif']['prix_sans_remise']	= $tarif_enfant['prix_sans_remise'];
            $tarif_precedent = $tarif_enfant['prix'];
        }
    }

    if ($tarifs['taux_remise'] > 0)
    {
        $html .= '<span class="prix-non-remise">'.html_price($data['tarif']['prix']).' <span class="taxe">'.lang('prix_taxe_'.$tarifs['taxe']).'</span></span>';
    }
    $html .= '<span itemprop="price" class="prix-vente">'.html_price($data['tarif']['prix']).' <span class="taxe">'.lang('prix_taxe_'.$tarifs['taxe']).'</span></span>';
    
    
}
else
{    
    if ($tarifs['prix'] > 0)
    {
        if ($tarifs['taux_remise'] > 0)
        {
            $html .= '<span class="prix-non-remise">'.html_price($tarifs['prix']).' <span class="taxe">'.lang('prix_taxe_'.$tarifs['taxe']).'</span></span>';
            $html .= '<span class="taux-remise label label-info"> -'.html_percentage($tarifs['taux_remise']).'</span>';
        }
        $html .= '<span itemprop="price" class="prix-vente">'.html_price($tarifs['prix']).' <span class="taxe">'.lang('prix_taxe_'.$tarifs['taxe']).'</span></span>';
    }
    elseif($module->autoriser_achat_gratuit)
    {
        $html .= '<span class="prix-vente">'.lang('catalogue_gratuit').'</span>';
    }
    else{
        $html .= '<span class="prix-vente"></span>';
    }
    
    
    
}
//Prix unitaire et prix remisé
//$formulaire .= $this->load->view('catalogue/prix_unitaire', $prix_unitaire, TRUE);
$html .= '<span style="display : none" itemprop="availability" content="in_stock"></span>';
$html .= '<span style="display : none" itemprop="quantity">1</span>';
$html .= '<meta itemprop="currency" content="EUR" />';

$html .= '</div>';

echo $html;