<?php
if ($this->cart->total_items() > 0)
{
	$html = NULL;
	
	foreach ($panier as $item)
	{
		$produit = $this->produit->get_by_id($item['id']);
		
		//Image du produit
		$media = html_media($produit->medias_associes(), array(
			'max_item' 	=> 1,
			'taille'	=> '50x50'
		));
		
		$html .= '<tr id="'.$item['rowid'].'">';
		$html .= '<td class="td-2">';
		
		if (isset($item['options']['produit_impose']) or (isset($item['options']['pack_parent'])))
		{
		    $html .= '<img style="display: inline-block;float: left;margin-right: 20px;" src="/assets/img/fleche_categorie.png">';
		}
		
		$html .= '<span class="image">'.$media.'</span>';
		$html .= '<span class="description">'.$produit->libelle.'</span>'.br();
		$html .= '<span class="reference">Référence : '.$produit->reference.'</span>';
		
		if((isset($item['options']['gravure_complementaire']) || (isset($item['options']['associe_gravure']))) && isset($item['options']['produit_referent_id']))
		{
		    $produit_referent = new Produit();
		    $produit_referent->get_by_id($item['options']['produit_referent_id']);
		    if (isset($item['options']['gravure_complementaire']))
		    {
		        $html .= br().'<span class="gravure">Gravure : '.$item['options']['gravure_complementaire'].'</span>';
		    }
		    if (isset($item['options']['associe_gravure']))
		    {
		        $html .= br().'<span class="gravure">Fichier associe : '.$item['options']['associe_gravure'].'</span>';
		    }
		    $html .= br().'<span class="gravure">Produit référent : '.$produit_referent->reference.'</span>';
		}
		
		$html .= '</td>';
		
		//Produit payant
		if ($item['price'] > 0)
		{
			$html .= '<td class="td-3">'.html_price($item['price']).'</td>';
			$html .= '<td class="td-4">'.$item['qty'].'</td>';
            $html .= '<td class="td-5">'.html_price($item['price']*$item['qty']).'</td>';
		}
		//Produit gratuit
		else
		{
			if (isset($item['options']['is_pack']))
			{
			    $html .= '<td class="td-3" >----</td>';
			    $html .= '<td class="td-4">'.$item['qty'].'</td>';
			    $html .= '<td class="td-5" >----</td>';
			}
			else
			{
			    
			    $html .= '<td class="td-3">Gratuit</td>';
			    $html .= '<td class="td-4">'.$item['qty'].'</td>';
			    $html .= '<td class="td-5">Gratuit</td>';
			}
		}
       
		$html .= '</tr>';
	}
	
	$data = array(
		'sous_total' 	=> $this->cart->total(),
		'ligne_panier'	=> $html,
		'total'		=> $commande->montant,
		'suffixe'	=> ($commande->hors_taxe) ? ' HT' : ' TTC',
		'remise'	=> FALSE,
		'acompte'	=> FALSE,
	    'sur_devis'	=> (isset($this->session->userdata('livraison')['sur_devis']) && ($this->session->userdata('livraison')['sur_devis'] == TRUE)) ? TRUE : FALSE
	    
	);
	
	if ($commande->livraison()->exists())
	{
		$livraison = $commande->livraison();
		
		$data['livraison'] = array(
			'frais_port'	=> $livraison->frais_port,
			'transporteur'	=> $this->transporteur->get_by_id($livraison->transporteur_id)->nom,
		);
	}
	
	//Remise
	$remise = $commande->remise();
	
	if ($remise->exists())
	{
		$data['remise'] = get_object_vars($remise);
	}
	
	//Acompte (en option)
	if ($module->option_acompte && ($commande->acompte > 0))
	{
		$data['acompte']['montant'] = $commande->acompte;
		$data['acompte']['taux'] = $module->taux_acompte;
	}
	
	$this->dwootemplate->output(tpl_path('boutique/panier/etape4.tpl'), $data);
}
else
{
	echo lang('panier_vide');
}