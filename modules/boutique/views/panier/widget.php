<?php
$html = NULL;

if ($this->cart->total_items() > 0)
{
	$html .= '<table class="table">';
	$html .= '<tr>';
	$html .= '<th class="th-1"></th>';
	$html .= '<th class="th-2">'.lang('reference').'</th>';
	$html .= '<th class="th-3">'.lang('qte').'</th>';
	$html .= '<th class="th-4">'.lang('prix').'</th>';
	$html .= '</tr>';
	
	//Panier
	foreach( $panier as $item )
	{		
	    $produit = $this->produit->include_related('module_catalogue', array('gestion_stock'), FALSE)->get_by_id($item['id']);
	    
		$html .= '<tr id="ligne-'.$item['rowid'].'">';
		$html .= '<td class="td-1">';
		
		
		if (!isset($item['options']['produit_impose']) && (!isset($item['options']['pack_parent'])))
		{
		    
		    $html .= form_button(array(
		        'class' => 'btn-dfault btn-xs supprimer-produit',
		        'data-id' => $item['rowid'],
		        'value' => $item['rowid'],
		        'content' => '<span class="glyphicon glyphicon-remove"></span>'
		    ));
		}
		else
		{

		    $html .= '<img src="/assets/img/fleche_categorie.png">';
		}
		$html .= '</td>';
		$html .= '<td class="td-2">'.$produit->libelle.'</td>';
		$html .= '<td class="td-3">'.$item['qty'].'</td>';
		$html .= '<td class="td-4">'.html_price($item['subtotal']).'</td>';
		$html .= '</tr>';
	}
	$html .= '<tr id="ligne-total">';
	$html .= '<td colspan="3">'.lang('total').'</td>';
	$html .= '<td class="align-right">'.html_price($this->cart->total()).'</td>';
	$html .= '</tr>';
	$html .= '</table>';
	
	$html .= '<p class="text-center">';
	$html .= anchor('boutique/etape,1.html', lang('commander'), array('class' => 'btn-primary'));
	$html .= '</p>';
}
else
{
	$html .= '<div class="liste-vide text-center"><img src="'.assets('boutique/emptycart.png').'" class="img_panier_vide"> <br/><br/><span>'.lang('panier_vide').'</span></div>';
}
echo $html;