<?php
require_once APPPATH.'controllers/admin/back.php';

/**
 * Commandes
 * 
 * @author 		Pauline MARTIN, Pauline Martin
 * @category 	Controller
 * @version 	1.1
 */
class Commandes extends Back
{
	const ITEM_PAR_PAGE = 20;

	const ETAT_TOUS 		= "all";
	const ETAT_ENREGISTREE 	= 1;
	const ETAT_PAYEE 		= 2;
	const ETAT_PREPAREE 	= 3;
	const ETAT_EXPEDIEE 	= 4;
	const ETAT_ANNULEE 		= 5;
	const ETAT_RETOURNEE 	= 6;
	const ETAT_ATTENTE_PAIEMENT = 7;
	const ETAT_PAIEMENT_COMPTE = 8;
	const ETAT_SUR_DEVIS_FRAIS_PORT 	= 9;
	const ETAT_PAIEMENT_CHEQUE 	= 10;
	
	private $etats;
	private $_module;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->lang->load(array('boutique', 'client'));
		$this->load->models(array(
			'module_boutique', 'utilisateur', 'commande',
			'paiement', 'produits_commande', 'catalogue/produit'
		));
		
		$this->_module = $this->module_boutique->get(1);
		
		$this->etats = array(
			"all" => 'Tous',
			1 => lang('etat_enregistre'),
			2 => lang('etat_payee'),
			3 => lang('etat_preparee'),
			4 => lang('etat_expediee'),
			5 => lang('etat_annulee'),
			6 => lang('etat_retournee'),
			7 => lang('etat_attente_paiement'),
			8 => lang('etat_paiement_compte'),
		    9 => lang('etat_sur_devis_frais_port'),
		    10 => lang('etat_paiement_cheque'),
		);
	}
	
	/**
	 * Affiche la liste des commandes
	 * 
	 * @return	VIEW
	 */
	public function lister($offset = 0)
	{
		$this->load->library('pagination');
		
		if ($this->input->post('etat'))
		{
			$etat = $this->input->post('etat');
			$this->session->set_userdata('etat_commande',$etat);
		}
		elseif ($this->session->userdata('etat_commande'))
		{
			$etat = $this->session->userdata('etat_commande');
		}
		else
		{
			$etat = self::ETAT_TOUS;
		}

		if ($post = $this->input->post())
		{
		    $debut_stat = date_to_array($this->input->post('date_debut'));
		    $date_debut = new DateTime($debut_stat[3].'-'.$debut_stat[2].'-'.$debut_stat[1]);
		    
		    
		    $fin_stat = date_to_array($this->input->post('date_fin'));
		    $date_fin = new DateTime($fin_stat[3].'-'.$fin_stat[2].'-'.$fin_stat[1]);
		}
		else
		{
		    $date_fin = new DateTime();
		    
		    $date_debut = $date_fin->modify('- 1 year');
		    $date_fin = new DateTime();
		}
		
		$etat = ($etat == self::ETAT_TOUS) ? NULL : $etat;
		
		$conditions = $this->input->post();
		$conditions['etat'] = $etat;
		$conditions['date_debut'] =$date_debut;
		$conditions['date_fin'] = $date_fin;
		
		$total_item = $this->commande->lister($conditions);
		
		
		$this->pagination->initialize(array(
			'base_url'		=> '/boutique/admin/commandes/lister',
			'total_rows'	=> $total_item,
			'per_page'		=> self::ITEM_PAR_PAGE,
			'cur_page'		=> $offset
		));
		
		$data = array(
			'pagination' 	=> $this->pagination->create_links(),
			'commandes' 	=> $this->commande->lister($conditions, $offset, self::ITEM_PAR_PAGE),
			'total_item'	=> $total_item,
			'etats'			=> $this->etats,
		    'date_debut'    => $date_debut->format("d/m/Y"),
		    'date_fin'      => $date_fin->format("d/m/Y")
		);
		
		if ($this->input->post('reload'))
		{
			echo json_encode(array(
				'liste'	=> $this->load->view('admin/commandes/lister_ajax', $data, TRUE),
				'total_item' => $total_item,
				'pagination' => $data['pagination'],
				'date_debut' => $data['date_debut'],
				'date_fin'	 => $data['date_fin']
			));
		}
		else
		{
			$data['liste_ajax'] = $this->load->view('/admin/commandes/lister_ajax', $data, TRUE);
			return $this->load->view('/admin/commandes/lister', $data);
		}
	}
	
    /**
     * Modifie l'état de la commande
     * Envoi de mail au client
     * 
     * @param 	integer $commande_id
     * @param 	integer $etat_id
     * @return	boolean
     */
	public function changer_etat($commande_id, $etat_id)
	{
		$commande = $this->commande->get_by_id($commande_id);
		
		if ($commande->exists() && $commande->changer_etat($etat_id))
		{
			switch($commande->etat)
			{
				case self::ETAT_PREPAREE:
					modules::run('boutique/emails/commande_preparee', $commande);
					break;
					
				case self::ETAT_EXPEDIEE:
					modules::run('boutique/emails/commande_expediee', $commande);
					break;
					
				case self::ETAT_ANNULEE:
                    //Suppression de l'export XML
                    if (file_exists(root_path().'export/web-commande-'.$commande->reference.'.xml'))
                    {
                        unlink(root_path().'export/web-commande-'.$commande->reference.'.xml'); 
                    }
					modules::run('boutique/emails/commande_annulee', $commande);
					break;
					
				case self::ETAT_RETOURNEE:
					modules::run('boutique/emails/commande_retournee', $commande);
					break;
					
				default: break;
			}
		}
	}
	
	/**
	 * Formlaire pour le code suivi du colis
	 * 
	 * @param	integer	$commande_id
	 * @return	VIEW
	 */
	public function suivre_colis($commande_id = NULL)
	{
		$this->load->model('commandes_etat');
		
		if ($this->input->post('submit'))
		{
			if ($this->input->post('code_suivi'))
			{
				$commande = $this->commande->get_by_id($commande_id);
				$livraison = $commande->livraison();
				$livraison->code_suivi = $this->input->post('code_suivi');
				
				if ($livraison->save())
				{
					$this->changer_etat($commande->id, self::ETAT_EXPEDIEE);
					echo json_encode(array('erreur' => FALSE));
				}
			}
			else
			{
				$this->changer_etat($commande_id, self::ETAT_EXPEDIEE);
				echo json_encode(array('erreur' => FALSE));
			}
			return;
		}
		
		if (is_null($commande_id) && $this->input->post('commande_id'))
		{
			$commande_id = $this->input->post('commande_id');	
		}
		
		$data = array(
			'commande' => $this->commande->get_by_id($commande_id)
		);
		$this->load->view('boutique/admin/commandes/suivre_colis', $data);
	}

	/**
	 * Afficher la commande
	 * 
	 * @param	integer	$id
	 * @return 	VIEW
	 */
	public function afficher($id)
	{
		$this->load->models(array('facturation', 'livraison'));
		
		$commande = $this->commande->get_by_id($id);
		
		if ($commande->exists())
		{	
			$data = array(
				'module'		=> $this->_module,
				'etats' 		=> $this->etats,
				'commande' 		=> $commande, 
				'remise'		=> $commande->remise(),
				'paiement'		=> $commande->paiement(),
				'livraison'		=> $commande->livraison(),
				'facturation'	=> $commande->facturation(),
				'client'		=> $commande->client(),
				'transporteur'	=> $commande->transporteur()
			);
			$this->load->view('/admin/commandes/affiche', $data);
		}
	}
	
	/**
	 * Afficher l'historique de modification
	 * de la commande
	 * 
	 * @param	integer	$id
	 * @return	VIEW
	 */
	public function historique($id)
	{
		$commande = $this->commande->get_by_id($id);
		
		$data = array(
			'etats' => $this->etats,
			'commande_etats' => $commande->etats()
		);
		
		$this->load->view('/admin/commandes/historique', $data);
	}
	
	
	/**
	 * Exporte une ou toutes les commandes
	 * Format : csv et pdf
	 * 
	 * @param 	string 	$format
	 * @param 	integer $commande_id
	 * @return 	void
	 */
	public function exporter($format = 'csv', $commande_id = 0, $produits = TRUE)
	{
	    $date_fin_init = new DateTime();
	    $date_debut_init = $date_fin_init->modify('- 1 year');
	    $date_fin_init = new DateTime();
	    
	    switch($format)
 		{
 			case 'csv':
				$this->load->library('csv');
				if (($commande_id != 0))
				{
                    $this->commande->select('reference, montant, frais_port, etat, created');
                    $this->commande->include_related('utilisateur', array('nom', 'prenom', 'email'));
					$this->commande->include_related('livraison');
					$this->commande->include_related('facturation');
                    $this->commande->where('id',$commande_id);
                    $this->commande->include_related('code_remises_commande', array('libelle', 'code', 'montant'));
                    
                    if ($this->input->get('etat'))
                    {
                    	$this->commande->where('etat',$this->input->get('etat'));
                    }
                    
                    if ($this->input->get('date_debut') && $this->input->get('date_fin'))
                    {
                        //$date_debut = date_format($this->input->get('date_debut'),'Y-m-d 00:00:00'); $date_fin = date_format($this->input->get('date_fin'),'Y-m-d 23:59:59');
                        $date_debut = $this->input->get('date_debut').' 00:00:00'; $date_fin = $this->input->get('date_fin').' 23:59:59';
                        $this->commande->group_start();
                        $this->commande->where('created >= ', $date_debut,TRUE);
                        $this->commande->where('created <= ', $date_fin,TRUE);
                        $this->commande->group_end();
                    } 
                    else  // initialiser sur l'année en cours 
                    {
                        $date_debut = date_format($date_debut_init,'Y-m-d').' 00:00:00'; $date_fin = date_format($date_fin_init,'Y-m-d').' 23:59:59';
                        $this->commande->group_start();
                        $this->commande->where('created >= ', $date_debut,TRUE);
                        $this->commande->where('created <= ', $date_fin,TRUE);
                        $this->commande->group_end();
                    }
                    
                    if ($produits)
                    {
                    	$entete = array(
                    			'reference_commande', 'montant_commande', 'frais_port_commande', 'etat_commande', 'date_commande',
                    			'nom_client', 'prenom_client', 'email_client', 'reference_produit', 'libelle_produit',
                    			'prix_produit', 'quantite_produit', 'libelle_remise', 'montant_remise',
                    			'livraison_societe','livraison_nom', 'livraison_prenom','livraison_adresse', 'livraison_code_postal', 'livraison_ville', 'livraison_pays', 'livraison_email', 'livraison_telephone','livraison_fax', 'livraison_jour',
                    			'facturation_societe','facturation_nom', 'facturation_prenom','facturation_adresse', 'facturation_code_postal', 'facturation_ville', 'facturation_pays', 'facturation_email', 'facturation_telephone','facturation_mobile','facturation_fax'
                    	);
                    	
                  	  	$this->commande->include_related('produits_commande', array('libelle', 'reference', 'prix', 'quantite'));
                  	  	$commande = $this->commande->get()->all_to_array(array(
                  	  			'reference', 'montant', 'frais_port', 'etat', 'created',
                  	  			'utlisateur_nom', 'utlisateur_prenom', 'utlisateur_email',
                  	  			'produits_commande_reference', 'produits_commande_libelle',
                  	  			'produits_commande_prix', 'produits_commande_quantite',
                  	  			'code_remises_commande_libelle', 'code_remises_commande_montant',
                  	  			'livraison_societe','livraison_nom', 'livraison_prenom','livraison_adresse', 'livraison_code_postal', 'livraison_ville', 'livraison_pays', 'livraison_email', 'livraison_telephone','livraison_fax', 'livraison_jour',
                  	  			'facturation_societe','facturation_nom', 'facturation_prenom','facturation_adresse', 'facturation_code_postal', 'facturation_ville', 'facturation_pays', 'facturation_email', 'facturation_telephone','facturation_mobile','facturation_fax'
                  	  	));
                    }
                    else
                    {
                    	$entete = array(
                    			'reference_commande', 'montant_commande', 'frais_port_commande', 'etat_commande', 'date_commande',
                    			'nom_client', 'prenom_client', 'email_client', 'libelle_remise', 'montant_remise',
                    			'livraison_societe','livraison_nom', 'livraison_prenom','livraison_adresse', 'livraison_code_postal', 'livraison_ville', 'livraison_pays', 'livraison_email', 'livraison_telephone','livraison_fax', 'livraison_jour',
                    			'facturation_societe','facturation_nom', 'facturation_prenom','facturation_adresse', 'facturation_code_postal', 'facturation_ville', 'facturation_pays', 'facturation_email', 'facturation_telephone','facturation_mobile','facturation_fax'
                    	);
                    	$commande = $this->commande->group_by('reference')->get()->all_to_array(array(
                    			'reference', 'montant', 'frais_port', 'etat', 'created',
                    			'utlisateur_nom', 'utlisateur_prenom', 'utlisateur_email',
                    			'code_remises_commande_libelle', 'code_remises_commande_montant',
                    			'livraison_societe','livraison_nom', 'livraison_prenom','livraison_adresse', 'livraison_code_postal', 'livraison_ville', 'livraison_pays', 'livraison_email', 'livraison_telephone','livraison_fax', 'livraison_jour',
                    			'facturation_societe','facturation_nom', 'facturation_prenom','facturation_adresse', 'facturation_code_postal', 'facturation_ville', 'facturation_pays', 'facturation_email', 'facturation_telephone','facturation_mobile','facturation_fax'
                    	));
                    }
                    
					$this->csv->array_to_csv($commande, $commande[0]['reference'].'-commande.csv', $entete);
				}
				else
				{
					
					$this->commande->select('reference, montant, frais_port, etat, created');
					$this->commande->include_related('utilisateur', array('nom', 'prenom', 'email'));
					$this->commande->include_related('livraison');
					$this->commande->include_related('facturation');
					$this->commande->include_related('code_remises_commande', array('libelle', 'code', 'montant'));
					
					if ($this->input->get('etat'))
					{
						$this->commande->where('etat',$this->input->get('etat'));
					}
					
					if ($this->input->get('date_debut') && $this->input->get('date_fin'))
					{
					    //$date_debut = date_format($this->input->get('date_debut'),'Y-m-d 00:00:00'); $date_fin = date_format($this->input->get('date_fin'),'Y-m-d 23:59:59');
					    $date_debut = $this->input->get('date_debut').' 00:00:00'; $date_fin = $this->input->get('date_fin').' 23:59:59';
					    $this->commande->group_start();
					    $this->commande->where('created >= ', $date_debut,TRUE);
					    $this->commande->where('created <= ', $date_fin,TRUE);
					    $this->commande->group_end();
					}
					else  // initialiser sur l'année en cours
					{
					    $date_debut = date_format($date_debut_init,'Y-m-d').' 00:00:00'; $date_fin = date_format($date_fin_init,'Y-m-d').' 23:59:59';
					    $this->commande->group_start();
					    $this->commande->where('created >= ', $date_debut,TRUE);
					    $this->commande->where('created <= ', $date_fin,TRUE);
					    $this->commande->group_end();
					}
					
					if ($produits)
					{
						$entete = array(
							'reference_commande', 'montant_commande', 'frais_port_commande', 'etat_commande', 'date_commande', 
							'nom_client', 'prenom_client', 'email_client', 'reference_produit', 'libelle_produit', 
							'prix_produit', 'quantite_produit', 'libelle_remise', 'montant_remise',
	                        'livraison_societe','livraison_nom', 'livraison_prenom','livraison_adresse', 'livraison_code_postal', 'livraison_ville', 'livraison_pays', 'livraison_email', 'livraison_telephone','livraison_fax', 'livraison_jour',
	                        'facturation_societe','facturation_nom', 'facturation_prenom','facturation_adresse', 'facturation_code_postal', 'facturation_ville', 'facturation_pays', 'facturation_email', 'facturation_telephone','facturation_mobile','facturation_fax'
						);
						 
						$this->commande->include_related('produits_commande', array('libelle', 'reference', 'prix', 'quantite'));
						$commandes = $this->commande->get()->all_to_array(array(
							'reference', 'montant', 'frais_port', 'etat', 'created', 
							'client_nom', 'client_prenom', 'client_email',
							'produits_commande_reference', 'produits_commande_libelle', 
							'produits_commande_prix', 'produits_commande_quantite',
							'code_remises_commande_libelle', 'code_remises_commande_montant',
	                        'livraison_societe','livraison_nom', 'livraison_prenom','livraison_adresse', 'livraison_code_postal', 'livraison_ville', 'livraison_pays', 'livraison_email', 'livraison_telephone','livraison_fax', 'livraison_jour',
	                        'facturation_societe','facturation_nom', 'facturation_prenom','facturation_adresse', 'facturation_code_postal', 'facturation_ville', 'facturation_pays', 'facturation_email', 'facturation_telephone','facturation_mobile','facturation_fax'
						));
					}
					else
					{
						$entete = array(
							'reference_commande', 'montant_commande', 'frais_port_commande', 'etat_commande', 'date_commande', 
							'nom_client', 'prenom_client', 'email_client', 
							'libelle_remise', 'montant_remise',
	                        'livraison_societe','livraison_nom', 'livraison_prenom','livraison_adresse', 'livraison_code_postal', 'livraison_ville', 'livraison_pays', 'livraison_email', 'livraison_telephone','livraison_fax', 'livraison_jour',
	                        'facturation_societe','facturation_nom', 'facturation_prenom','facturation_adresse', 'facturation_code_postal', 'facturation_ville', 'facturation_pays', 'facturation_email', 'facturation_telephone','facturation_mobile','facturation_fax'
						);
						$commandes = $this->commande->group_by('reference')->get()->all_to_array(array(
							'reference', 'montant', 'frais_port', 'etat', 'created', 
							'utilisateur_nom', 'utilisateur_prenom', 'utilisateur_email',
							'code_remises_commande_libelle', 'code_remises_commande_montant',
	                        'livraison_societe','livraison_nom', 'livraison_prenom','livraison_adresse', 'livraison_code_postal', 'livraison_ville', 'livraison_pays', 'livraison_email', 'livraison_telephone','livraison_fax', 'livraison_jour',
	                        'facturation_societe','facturation_nom', 'facturation_prenom','facturation_adresse', 'facturation_code_postal', 'facturation_ville', 'facturation_pays', 'facturation_email', 'facturation_telephone','facturation_mobile','facturation_fax'
						));
					}
					$this->csv->array_to_csv($commandes, mdate('%Y-%m-%d').'-commandes.csv', $entete);
				}
 			break;
 			
 			case 'pdf':
				if ( ! is_null($commande_id))
				{
					$this->load->library('html2pdf');
					$this->load->model('code_remise');
					$this->config->load('config');
					
					$commande = $this->commande->get_by_id($commande_id);
					$config_facture = $this->config->item('facture');
					
					$data = array(
						'commande' 	=> $commande,
						'facturation' => $commande->facturation(),
						'livraison' => $commande->livraison(),
						'paiement'	=> $commande->paiement(),
						'remise'	=> $commande->remise(),
						'produits'	=> $commande->produits(),
						'module'	=> $this->_module,
						'detail_remise'	=> $this->code_remise->get_by_code($commande->remise()->code),
						'item_par_page'	=> $config_facture['item_par_page'],
						'body_top'		=> $config_facture['body_top']
					);
					
					if ($commande->hors_taxe)
					{
						$content = $this->load->view('boutique/commande/facture_ht', $data, TRUE);
					}
					else
					{
						$content = $this->load->view('boutique/commande/facture_ttc', $data, TRUE);
					}
					
			 		$this->html2pdf->folder(root_path().'echanges/export/');
					$this->html2pdf->filename($commande->reference.'.pdf');
					$this->html2pdf->paper('a4', 'portrait');
					$this->html2pdf->html($content);
					$this->html2pdf->create();
				}
	 		break;
 			
 			default: break;
 		}
    }
}

/* End of file commandes.php */
/* Location: ./modules/boutique/controllers/admin/commandes.php */