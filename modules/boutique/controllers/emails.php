<?php
require_once APPPATH.'controllers/front.php';

/**
 * Email
 * 
 * @author 		Pauline MARTIN
 * @package 	boutique
 * @category 	Controllers
 * @version 	1.1
 */
class Emails extends Front
{
	/**
	 * Options du module
	 * @var object
	 */
	private $_module;
	
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->load->models(array('module_boutique', 'transporteur', 'livraison', 'facturation'));
		$this->lang->load(array('boutique', 'client'));
		$this->load->library('email');
		
		$this->_module = $this->module_boutique->get(1);
	}
	
	/**
	 * Envoi mail confirmant l'inscription d'un client
	 * 
	 * @param 	object	$client
	 * @return	boolean
	 */
	public function inscription($client, $motdepasse)
	{
		$data = array(
			'parametres'	=> $this->_parametres,
			'module' 		=> $this->_module,
			'client' 		=> $client,
			'mot_de_passe'	=> $motdepasse
		);
		$template = $this->load->view('boutique/email/inscription', $data, TRUE);
		
		$this->email->clear();
		$this->email->from($this->_module->vendeur_email, $this->_module->vendeur_societe);
		$this->email->to($client->email);
		$this->email->subject($this->_module->sujet_inscription);
		$this->email->message($template);
		
		return $this->email->send();
	}
	
	/**
	 * Envoi mail avec le nouveau mot de passe du client suite à sa demande
	 * 
	 * @param 	object 	$client
	 * @return	boolean
	 */
	public function nouveau_mdp($client)
	{
		$data = array(
			'parametres'=> $this->_parametres,
			'module' 	=> $this->_module,
			'client' 	=> $client,
		);
		$template = $this->load->view('boutique/email/nouveau_mdp', $data, TRUE);
		
		$this->email->clear();
		$this->email->from($this->_module->vendeur_email, $this->_module->vendeur_societe);
		$this->email->to($client->email);
		$this->email->subject($this->_module->sujet_nouveau_mdp);
		$this->email->message($template);
		
		return $this->email->send();
	}
	
	/**
	 * Envoi mail avec le nouveau mot de passe du client suite à une modification en administration
	 *
	 * @param 	object 	$client
	 * @return	boolean
	 */
	public function nouveau_mdp_admin($client)
	{
		$data = array(
				'parametres'=> $this->_parametres,
				'module' 	=> $this->_module,
				'client' 	=> $client,
		);
		$template = $this->load->view('boutique/email/nouveau_mdp_admin', $data, TRUE);
	
		$this->email->clear();
		$this->email->from($this->_module->vendeur_email, $this->_module->vendeur_societe);
		$this->email->to($client->email);
		$this->email->subject($this->_module->sujet_nouveau_mdp);
		$this->email->message($template);
	
		return $this->email->send();
	}
	
	/**
	 * Envoi mail après paiement en banque
	 * 
	 * @param 	object	$commande
	 * @return	boolean
	 */
	public function commande_payee($commande)
	{
		/*$commande = new Commande();
		$commande->get_by_id($commande_id);*/
		$client = $commande->client();
		
		$data = array(
			'parametres'	=> $this->_parametres,
			'module' 		=> $this->_module,
			'commande' 		=> $commande,
			'produits' 		=> $commande->produits(),
			'transporteur'	=> $commande->transporteur(),
			'livraison'		=> $commande->livraison(),
			'facturation'	=> $commande->facturation(),
			'client'		=> $client
		);
		
		$template  	= $this->load->view('boutique/email/commande_payee', $data, TRUE);
		$templateb 	= $this->load->view('boutique/email/commande_vendeur', $data, TRUE);
		$log_file    = root_path().'logs/email_commande.log';
           
		//Client
		$this->email->clear();
		$this->email->from($this->_module->vendeur_email, $this->_module->vendeur_societe);
		$this->email->to($client->email);
		$this->email->subject($this->_module->sujet_commande_payee);
		$this->email->message($template);
		 
		$file = FALSE;
		modules::run('boutique/commandes/exporter', $commande->id, 1, $client->id);
		
		if (file_exists(root_path().'/echanges/export/'.$commande->reference.'.pdf'))
		{
		    $file = TRUE;
		    $this->email->attach(root_path().'/echanges/export/'.$commande->reference.'.pdf');
		}
		$this->email->send();
		
		/* $log_file    = root_path().'logs/email_commande.log';
		 $log_string  = date('Y-m-d H:i:s').' | ';
		 $log_string .= $this->email->print_debugger();
		 write_file($log_file, $log_string, FOPEN_WRITE_CREATE);*/
		
		//Vendeur
		$this->email->clear(TRUE);
		$this->email->from($this->_parametres->expediteur_email, $this->_parametres->expediteur_libelle);
		$this->email->to($this->_module->vendeur_email);
		//$this->email->cc('antoine.raitiere@aquilainfo.com');
		$this->email->subject($this->_module->sujet_commande_payee);
		$this->email->message($templateb);
		
		
		if ($file !== FALSE)
		{
		    $this->email->attach(root_path().'/echanges/export/'.$commande->reference.'.pdf');
		}
		
		if ($this->email->send())
		{
		    //unlink($file);
		    if($this->_module->vendeur_copie1 != NULL)
		    {
		        $this->email->to($this->_module->vendeur_copie1);
		        unlink(root_path().'/echanges/export/'.$commande->reference.'.pdf');
		        if ($this->email->send())
		        {
		            if($this->_module->vendeur_copie2 != NULL)
		            {
		                $this->email->to($this->_module->vendeur_copie2);
		                unlink($file);
		                if ($this->email->send())
		                {
		                    return TRUE;
		                }
		            }
		        }
		    }
		    return TRUE;
		}
		
		return FALSE;
	}
	
	/**
	 * Envoi mail après validation de la commande gratuite
	 * 
	 * @param 	object	$commande
	 * @return	boolean
	 */
	public function commande_gratuite($commande)
	{
		$this->load->module('boutique/commandes');
		$client = $commande->client();
		
		$data = array(
			'parametres'	=> $this->_parametres,
			'module' 		=> $this->_module,
			'commande' 		=> $commande,
			'produits' 		=> $commande->produits(),
			'transporteur'	=> $commande->transporteur(),
			'livraison'		=> $commande->livraison(),
			'facturation'	=> $commande->facturation(),
			'client'		=> $client
		);
		
		$template  	= $this->load->view('boutique/email/commande_gratuite', $data, TRUE);
		$templateb 	= $this->load->view('boutique/email/commande_vendeur', $data, TRUE);
		
		//Client
		$this->email->clear();
		$this->email->from($this->_module->vendeur_email, $this->_module->vendeur_societe);
		$this->email->to($client->email);
		$this->email->subject($this->_module->sujet_commande_payee);
		$this->email->message($template);

		if (($file = $this->commandes->exporter($commande->id, TRUE, $client->id)) !== FALSE)
		{
			$this->email->attach($file);
		}
		if ($this->email->send())
		{
			//Supprime la pièce jointe
			if ($file !== FALSE)
			{
				unlink($file);
			}
		}
		
		//Vendeur
		$this->email->clear();
		$this->email->from($this->_parametres->expediteur_email, $this->_parametres->expediteur_libelle);
		$this->email->to($this->_module->vendeur_email);
		$this->email->subject(lang('mail_sujet_nvl_commande'));
		$this->email->message($templateb);
		
		return $this->email->send();
	}

	/**
	 * Envoi mail quand état commande passe en expédition
	 * 
	 * @param 	object	$commande
	 * @return	boolean
	 */
	public function commande_expediee($commande)
	{
		$client = $commande->client();
		
		$data = array(
			'parametres'	=> $this->_parametres,
			'module' 		=> $this->_module,
			'commande' 		=> $commande,
			'produits' 		=> $commande->produits(),
			'transporteur'	=> $commande->transporteur(),
			'livraison'		=> $commande->livraison(),
			'facturation'	=> $commande->facturation(),
			'client'		=> $client
		);
		$template = $this->load->view('boutique/email/commande_expediee', $data, TRUE);
		
		$this->email->clear();
		$this->email->from($this->_module->vendeur_email, $this->_module->vendeur_societe);
		$this->email->to($client->email);
		$this->email->subject($this->_module->sujet_commande_expediee);
		$this->email->message($template);
		
		return $this->email->send();
	}
	
	/**
	 * Envoi mail quand état commande passe en préparation
	 * 
	 * @param 	object	$commande
	 * @return	boolean
	 */
	public function commande_preparee($commande)
	{
		$client = $commande->client();
		
		$data = array(
			'parametres'	=> $this->_parametres,
			'module' 		=> $this->_module,
			'commande' 		=> $commande,
			'produits' 		=> $commande->produits(),
			'transporteur'	=> $commande->transporteur(),
			'livraison'		=> $commande->livraison(),
			'facturation'	=> $commande->facturation(),
			'client'		=> $client
		);
		$template = $this->load->view('boutique/email/commande_preparee', $data, TRUE);
		
		$this->email->clear();
		$this->email->from($this->_module->vendeur_email, $this->_module->vendeur_societe);
		$this->email->to($client->email);
		$this->email->subject($this->_module->sujet_commande_preparee);
		$this->email->message($template);
		
		return $this->email->send();
	}
	
	/**
	 * Envoi mail quand état commande passe en annulée
	 * 
	 * @param 	object	$commande
	 * @return	boolean
	 */
	public function commande_annulee($commande)
	{		
		$client = $commande->client();
		
		$data = array(
			'parametres'	=> $this->_parametres,
			'module' 		=> $this->_module,
			'commande' 		=> $commande,
			'produits' 		=> $commande->produits(),
			'transporteur'	=> $commande->transporteur(),
			'livraison'		=> $commande->livraison(),
			'facturation'	=> $commande->facturation(),
			'client'		=> $client
		);
		$template = $this->load->view('boutique/email/commande_annulee', $data, TRUE);
		
		$this->email->clear();
		$this->email->from($this->_module->vendeur_email, $this->_module->vendeur_societe);
		$this->email->to($client->email);
		$this->email->subject($this->_module->sujet_commande_annulee);
		$this->email->message($template);
		
		return $this->email->send();
	}
	
	/**
	 * Envoi mail quand état commande passe en retournée
	 * 
	 * @param 	object	$commande
	 * @return	boolean
	 */
	public function commande_retournee($commande)
	{		
		$client = $commande->client();
		
		$data = array(
			'parametres'	=> $this->_parametres,
			'module' 		=> $this->_module,
			'commande' 		=> $commande,
			'produits' 		=> $commande->produits(),
			'transporteur'	=> $commande->transporteur(),
			'livraison'		=> $commande->livraison(),
			'facturation'	=> $commande->facturation(),
			'client'		=> $client
		);
		$template = $this->load->view('boutique/email/commande_retournee', $data, TRUE);
		
		$this->email->clear();
		$this->email->from($this->_module->vendeur_email, $this->_module->vendeur_societe);
		$this->email->to($client->email);
		$this->email->subject($this->_module->sujet_commande_retournee);
		$this->email->message($template);
		
		return $this->email->send();
	}
	
	/**
	 * Envoi mail après validation de la commande avec articles proforma : devis
	 *
	 * @param 	object	$commande
	 * @return	boolean
	 */
	public function commande_compte($commande)
	{
	    $this->load->module('boutique/commandes');
	    $client = $commande->client();
	    
	    $data = array(
	        'version'		=> $this->_parametres,
	        'module' 		=> $this->_module,
	        'commande' 		=> $commande,
	        'produits' 		=> $commande->produits(),
	        'transporteur'	=> $commande->transporteur(),
	        'livraison'		=> $commande->livraison(),
	        'facturation'	=> $commande->facturation(),
	        'client'		=> $client
	    );
	    
	    $template  	= $this->load->view('boutique/email/commande_compte', $data, TRUE);
	    $templateb 	= $this->load->view('boutique/email/commande_vendeur_compte', $data, TRUE);
	    
	    //Client
	    $this->email->clear();
	    $this->email->from($this->_module->vendeur_email, $this->_module->vendeur_societe);
	    $this->email->to($client->email);
	    $this->email->subject('Votre commande est acceptée sur votre compte');
	    $this->email->message($template);
	    
	    $file = FALSE;
	    modules::run('boutique/commandes/exporter', $commande->id, 1, $client->id);
	    
	    if (file_exists(root_path().'/echanges/export/'.$commande->reference.'.pdf'))
	    {
	        $file = TRUE;
	        $this->email->attach(root_path().'/echanges/export/'.$commande->reference.'.pdf');
	    }
	    $this->email->send();

	    //Vendeur
	    $this->email->clear(TRUE);
	    $this->email->from($this->_parametres->expediteur_email, $this->_parametres->expediteur_libelle);
	    $this->email->to($this->_module->vendeur_email);
	    //$this->email->cc('antoine.raitiere@aquilainfo.com');
	    $this->email->subject('Une nouvelle commande en paiement compte');
	    $this->email->message($templateb);

	    if ($file !== FALSE)
	    {
	        $this->email->attach(root_path().'/echanges/export/'.$commande->reference.'.pdf');
	    }
	        
        if(isset($client->email) && $client->email != '' && $client->email != NULL)
        {
            if ($this->email->send())
            {
                unlink($file);
                
                if($this->_module->vendeur_copie1 != NULL)
                {
                    $this->email->to($this->_module->vendeur_copie1);
                    
                    if ($this->email->send())
                    {
                        if($this->_module->vendeur_copie2 != NULL)
                        {
                            $this->email->to($this->_module->vendeur_copie2);
                            unlink($file);
                            if ($this->email->send())
                            {
                                return TRUE;
                            }
                        }
                    }
                }
                return TRUE;
            }
        }
        return FALSE;
    }
	
	
	/**
	 * Envoi mail lors de la relance d'une commande enregistrée
	 *
	 * @param 	object	$commande
	 * @return	boolean
	 */
	public function commande_relance($commande)
	{
		$this->load->module('boutique/commandes');
		$client = $commande->client();
		
		$lien_relance = base_url().'/boutique/commandes/recupere_commande/'.base64_encode($commande->reference);
	
		$data = array(
			'parametres'	=> $this->_parametres,
			'module' 		=> $this->_module,
			'commande' 		=> $commande,
			'produits' 		=> $commande->produits(),
			'client'		=> $client,
			'lien_relance'	=> $lien_relance	
		);
	
		$template  	= $this->load->view('boutique/email/commande_relance', $data, TRUE);
		$log_file    = root_path().'logs/email_commande_relance.log';
		 
		//Client
		$this->email->clear();
		$this->email->from($this->_module->vendeur_email, $this->_module->vendeur_societe);
		$this->email->to($client->email);
		$this->email->subject($client->prenom.', '.$this->_module->sujet_commande_relance);
		$this->email->message($template);

		if ($this->email->send())
		{
			unlink($file);
			return TRUE;
		}
		return FALSE;
	}
	
	public function commande_cheque($commande)
	{
	    $this->load->module('boutique/commandes');
	    $client = $commande->client();
	    
	    $data = array(
	        'version'		=> $this->_parametres,
	        'module' 		=> $this->_module,
	        'commande' 		=> $commande,
	        'produits' 		=> $commande->produits(),
	        'transporteur'	=> $commande->transporteur(),
	        'livraison'		=> $commande->livraison(),
	        'facturation'	=> $commande->facturation(),
	        'client'		=> $client
	    );
	    
	    $template  	= $this->load->view('boutique/email/commande_cheque', $data, TRUE);
	    $templateb 	= $this->load->view('boutique/email/commande_vendeur_cheque', $data, TRUE);
	    
	    //Client
	    $this->email->clear();
	    $this->email->from($this->_module->vendeur_email, $this->_module->vendeur_societe);
	    $this->email->to($client->email);
	    $this->email->subject('Votre commande est validée, nous attendons votre chèque');
	    $this->email->message($template);
	    
	    $file = FALSE;
	    modules::run('boutique/commandes/exporter', $commande->id, 1, $client->id);
	    
	    if (file_exists(root_path().'/echanges/export/'.$commande->reference.'.pdf'))
	    {
	        $file = TRUE;
	        $this->email->attach(root_path().'/echanges/export/'.$commande->reference.'.pdf');
	    }
	    $this->email->send();
	    
	    //Vendeur
	    $this->email->clear(TRUE);
	    $this->email->from($this->_parametres->expediteur_email, $this->_parametres->expediteur_libelle);
	    $this->email->to($this->_module->vendeur_email);
	    //$this->email->cc('antoine.raitiere@aquilainfo.com');
	    $this->email->subject('Une nouvelle commande en attente de paiement en chèque');
	    $this->email->message($templateb);
	    
	    if ($file !== FALSE)
	    {
	        $this->email->attach(root_path().'/echanges/export/'.$commande->reference.'.pdf');
	    }
	    
	    if(isset($client->email) && $client->email != '' && $client->email != NULL)
	    {
	        if ($this->email->send())
	        {
	            unlink($file);
	            
	            if($this->_module->vendeur_copie1 != NULL)
	            {
	                $this->email->to($this->_module->vendeur_copie1);
	                
	                if ($this->email->send())
	                {
	                    if($this->_module->vendeur_copie2 != NULL)
	                    {
	                        $this->email->to($this->_module->vendeur_copie2);
	                        unlink($file);
	                        if ($this->email->send())
	                        {
	                            return TRUE;
	                        }
	                    }
	                }
	            }
	            return TRUE;
	        }
	    }
	    return FALSE;
	}
}

/* End of file emails.php */
/* Location: ./modules/boutique/controllers/emails.php */