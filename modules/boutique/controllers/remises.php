<?php
require_once APPPATH.'controllers/front.php';

/**
 * Remises
 * Contrôleur permettant les actions sur les remises
 * 
 * @author 		Pauline MARTIN
 * @package 	boutique
 * @category 	Controllers
 * @version 	1.1
 */
class Remises extends Front
{
	/**
	 * Paramètres du module
	 * 
	 * @var object
	 */
	private $_module;
	
	/**
	 * Données du client
	 * 
	 * @var object
	 */
	private $_client;
	
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->load->libraries(array('cart', 'order'));
		$this->load->models(array(
			'module_boutique', 'code_remise', 'catalogue/produit',
			'code_remises_commande', 'commande'
		));
		$this->load->modules(array(
			'boutique/clients',
			'boutique/panier'
		));
		
		$this->_module = $this->module_boutique->get(1);
		$this->_client = $this->clients->est_connecte();
	}
	
	/**
	 * Affiche le formulaire ou la remise
	 * 
	 * @return 	VIEW
	 */
	public function afficher()
	{
		if ($remise = $this->order->discount())
		{
			$code_remise = $this->code_remise->get_by_code($remise['code']);
			
			if ($code_remise->exists() && ($this->cart->total() >= $code_remise->montant_mini))
			{
				$data = array(
					'code_remise' => $code_remise,
					'montant' => $remise['montant']
				);
					
				return $this->load->view('boutique/remises/valide', $data, TRUE);
			}
		}
		return $this->load->view('boutique/remises/formulaire', NULL, TRUE);
	}
	
	/**
	 * Retourne le sous-total du panier après remise
	 * 
	 * @return	decimal
	 */
	public function sous_total_remise()
	{
		if (($this->_sous_total_remisable() !== FALSE) && ($this->_sous_total_remisable() >= $this->_montant()))
		{
			return round($this->_sous_total_remisable()+$this->_sous_total_non_remisable()-$this->_montant(), 2);
		}
		return $this->cart->total();
	}
	
	/**
	 * Vérifie la validité du code promo :
	 * 1- si existant
	 * 2- dans la période de promo
	 * 3- si le quota n'est pas dépassé
	 * 4- si usage unique pour chaque client
     * 5- montant du panier > montant mini si renseigné
	 * 
	 * @return	mixed
	 */
	public function verifier_validite($retour_echo = TRUE)
	{
		if ( $this->input->post('submit-remise') or ($code = $this->order->discount()))
		{
			$code = ($this->input->post('code_remise') ? $this->input->post('code_remise') : $this->order->discount()['code']);
			
            if($code != NULL)
			{
                $code_remise = $this->code_remise->get_by_code($code);
            }
            
            if ($code != NULL && $code_remise->exists())
            {
				//Vérification de la date
				if ($code_remise->valide())
				{
					//Vérification du quota					
					if ($code_remise->quota == 0 or ($code_remise->utilises() < $code_remise->quota))
					{
					    //Vérification montant minimum du panier
					    if ($this->cart->total() >= $code_remise->montant_mini)
                        {
    						//Code à usage unique
    						if ($code_remise->usage_unique)
    						{
    							if ($this->_client)
    					    	{
    					    		if ($this->code_remises_commande->utilise($code, $this->_client->id))
    					    		{
    					    			echo json_encode(array(
    					    					'erreur' => 1,
    					    					'message'=> lang('code_unique_deja_utilise')
    					    			));
    					    			return  FALSE;
    					    		}
    					    	}
    					    	else
    					    	{
    					    		echo json_encode(array(
    					    			'erreur' => 1,
    					    			'message'=> lang('connexion_utilisation_code')
    					    		));
    					    		return  FALSE;
    					    	}
    						}
    						// Vérifier le type de remise : rattachée à un produit (par ligne) | non rattachée à un produit (globale) | frais de port gratuit
    						$produits_remises = NULL;
    						$_obj_produits_associes = $code_remise->produits_associes();

    						if ($_obj_produits_associes->result_count() > 0)
    						{ // Traitement de la remise par ligne
    							$montant_reduction = 0;
    							$produit_associe_trouve = FALSE;
    							//Si produits associés au code remise, alors vérifier si les articles du panier correspondent
    							//Si au moins un article correspond, faire la remise à la ligne du/des produits associés
    							foreach ($_obj_produits_associes as $produits_ass)
    							{
    								$tab_restriction[$produits_ass->id] = $produits_ass->id;
    							}
    							foreach ($this->cart->contents() as $item)
    							{
    								if (array_key_exists($item['id'], $tab_restriction))
    								{
    									$produit_associe_trouve = TRUE;
    									$produits_remises[$item['id']] = array(
    											'id'	=> $item['id']
    									);
    						
    									$produit_pour_remise = new Produit;
    									$produit_pour_remise->get_by_id($item['id']);
    									$tarif_produit = $produit_pour_remise->tarif($this->_client, $item['qty']);
    						
    									
    									if($code_remise->type_reduction == 1)
    									{
    									    $item['price'] = $tarif_produit['prix'] - ($tarif_produit['prix']*$item['qty']*($code_remise->reduction/100));
    									    //$montant_reduction += ($tarif_produit['prix']*$item['qty']) - $item['price'];
    									    $montant_reduction += ((($tarif_produit['prix']*$item['qty'])*($code_remise->reduction/100)));
    									}
    									else
    									{
    									    $item['price'] = $tarif_produit['prix']*$item['qty'] - $code_remise->reduction;
    									    if($item['price']<0)
    									    {
    									        $item['price'] = 0;
    									        $montant_reduction += $tarif_produit['prix']*$item['qty'];
    									    }
    									    else{
    									        $montant_reduction += $code_remise->reduction;
    									    }
    									}
                                    }
    								
    							}
    							$montant_applique = $montant_reduction ;
    								
    							if (!$produit_associe_trouve)
    							{
    								//Code promo non applicable
    								echo json_encode(array(
    										'erreur' => 1,
    										'message' => $this->lang->line('code_promo_non_applicable')
    								));
    								return FALSE;
    							}
    						}
    						else
    						{   // Traitement de la remise globale
    							$montant_reduction = $this->_calcul_reduction($code_remise);

    							if ($montant_reduction == 0 && $code_remise->type_reduction!=3)
    							{
    							    // vérifier qu'il s'agit d'une remise correspondant aux frais de port offert
    								//Code promo non applicable
    								echo json_encode(array(
    										'erreur' => 1,
    										'message' => $this->lang->line('code_promo_non_applicable')
    								));
    								return FALSE;
    							}
    							$montant_applique = $montant_reduction;
    								
    							//Si montant remise supérieur au montant de la commande
    							if ($montant_reduction > ($total = $this->cart->total()))
    							{
    								$montant_applique = $total;
    							}
    								
    						}
    						
    				    }
                        else 
                        {
                            echo json_encode(array(
                                'erreur' => 1,
                                'message'=> lang('montant_panier_insuffisant')
                            ));
                            return FALSE ;
                        }
                        
						;
						//Enregistre en session les données de la remise
						$this->order->insert_discount(array(
							'code'		    => $code_remise->code,
							'type' 		    => $code_remise->type_reduction,
							'reduction'     => $montant_reduction,
							'montant'		=> $montant_applique,
							'produit_associes'	=> $produits_remises
						));
						
						//Recalcul de la commande après remise
						//modules::run('boutique/calcul_commande');
						
						$data = array(
							'code_remise' => $code_remise,
							'montant' => $montant_applique
						);
						
						if($retour_echo)
						{
						    echo json_encode(array(
						        'erreur' => 0,
						        'vue' 	=> $this->load->view('boutique/remises/valide', $data, TRUE)
						    ));
						}
						
						return FALSE;
					}
					else
					{
						echo json_encode(array(
							'erreur' => 1,
							'message' => lang('erreur_quota_depasse')
						));
						return FALSE;
					}
				}
				else
				{
					echo json_encode(array(
						'erreur' => 1,
						'message' => lang('erreur_hors_periode')
					));
					return FALSE;
				}
			}
			else
			{
				echo json_encode(array(
					'erreur' => 1,
					'message' => lang('erreur_code_inexistant')
				));
				return FALSE;
			}
		}
		/*else
		{
			echo json_encode(array(
				'erreur' => 1,
				'message' => lang('erreur_code_inexistant')
			));
		}*/
		return FALSE;
	}
	
	/**
	 * Supprime la session de la remise
	 * 
	 * @param	string 	$code
	 * @return	JSON
	 */
	public function annuler($code)
	{
		$code_remise = $this->code_remise->get_by_code($code);
		
		if ($code_remise->exists() && ($remise = $this->order->discount()))
		{			
			//Supprime la session de la remise
			$this->order->delete_discount();

			echo json_encode(array(
				'erreur' => 0,
				'vue' 	 => $this->load->view('boutique/remises/formulaire', NULL, TRUE)
			));
			return;
		}
		
		echo json_encode(array(
			'erreur'  => 1,
			'message' => lang('erreur_code_vide')
		));
	}
	
	/**
	 * Retourne le montant de la réduction
	 * 
	 * @return	decimal
	 */
	private function _montant()
	{
		if ($remise = $this->order->discount())
		{
			$code_remise = $this->code_remise->get_by_code($remise['code']);
			
			if ($code_remise->exists())
			{
				$montant_reduction = 0;
				$sous_total_remisable = $this->_sous_total_remisable();
				
				if ($code_remise->type_reduction == 1) //pourcentage
				{
					$montant_reduction = ($sous_total_remisable*($code_remise->reduction/100));
				}
				elseif (($code_remise->type_reduction == 2) && ($code_remise->reduction <= $sous_total_remisable)) //valeur
				{
					$montant_reduction = $code_remise->reduction;
				}
                elseif (($code_remise->type_reduction == 2)) //valeur
                {
                    $montant_reduction = $sous_total_remisable;
                }
				
				return round($montant_reduction, 2);
			}
		}
		return 0;
	}
	
	/**
	 * Retourne le sous-total remisable du panier
	 * 
	 * @return	decimal OR FALSE
	 */
	private function _sous_total_remisable()
	{
		$sous_total = 0;
		
		$produits_remises = NULL;
		$tab_restriction = array();
		$code_remise = new Code_remise();
		$code_remise->get_by_code($this->order->discount()['code']);
		if ($code_remise->exists())
		{
			$_obj_produits_associes = $code_remise->produits_associes();
			if ($_obj_produits_associes->result_count() > 0)
			{
				$montant_reduction = 0;
				//Si produits associés au code remise, alors vérifier si les articles du panier correspondent
				//Si au moins un article correspond, faire la remise à la ligne du/des produits associés
				foreach ($_obj_produits_associes as $produits_ass)
				{
					$tab_restriction[$produits_ass->id] = $produits_ass->id;
				}
			}
		}
		
		
		//Parcours des produits remisables
		foreach ($this->cart->contents() as $item_panier)
		{
			$produit = $this->produit->get_by_id($item_panier['id']);
			
			if ($produit->remisable)
			{
			    if (empty($tab_restriction))
			    {
			        $tarif = $produit->tarif($this->_client, $item_panier['qty']);
			        
			        if($this->_client && $this->_client->chromo_payant() != FALSE && isset($item_panier['options']['chromos']) && $item_panier['options']['chromos'] == '1')
			        {
			            $tarif['prix'] = $tarif['prix'] + $this->_client->chromo_payant();
			        }
			        
			        $sous_total += $tarif['prix']*$item_panier['qty'];
			        
			    } elseif (array_key_exists($produit->id, $tab_restriction))
			    {
			        $tarif = $produit->tarif($this->_client, $item_panier['qty']);
			        
			        if($this->_client && $this->_client->chromo_payant() != FALSE && isset($item_panier['options']['chromos']) && $item_panier['options']['chromos'] == '1')
			        {
			            $tarif['prix'] = $tarif['prix'] + $this->_client->chromo_payant();
			        }
			        
			        $sous_total += $tarif['prix']*$item_panier['qty'];
			        
			    }
			}
			
			/*if (($produit->remisable && array_key_exists($produit->id, $tab_restriction)) or ($produit->remisable && is_null($tab_restriction)))
			{
				$tarif = $produit->tarif($this->_client, $item_panier['qty']);
				$sous_total += $tarif['prix']*$item_panier['qty'];
			}*/
		}
		
		return ($sous_total > 0) ? round($sous_total, 2) : FALSE;
	}
	/**
	 * Retourne le sous-total remisable du panier
	 *
	 * @return	decimal OR FALSE
	 */
	private function _sous_total_non_remisable()
	{
		$sous_total = 0;
	
		$produits_remises = NULL;
		$tab_restriction = NULL;
		$code_remise = new Code_remise();
		$code_remise->get_by_code($this->order->discount()['code']);
		$_obj_produits_associes = $code_remise->produits_associes();
		if ($_obj_produits_associes && $_obj_produits_associes->result_count() > 0)
		{
			$montant_reduction = 0;
			//Si produits associés au code remise, alors vérifier si les articles du panier correspondent
			//Si au moins un article correspond, faire la remise à la ligne du/des produits associés
			foreach ($_obj_produits_associes as $produits_ass)
			{
				$tab_restriction[$produits_ass->id] = $produits_ass->id;
			}
		}
	
		//Parcours des produits remisables
		foreach ($this->cart->contents() as $item_panier)
		{
			$produit = $this->produit->get_by_id($item_panier['id']);
				
			if (!$produit->remisable)
			{
			    $tarif = $produit->tarif($this->_client, $item_panier['qty']);
			    $sous_total += $tarif['prix']*$item_panier['qty'];
			} elseif (!empty($tab_restriction) && (!array_key_exists($produit->id, $tab_restriction)))
			{
			    $tarif = $produit->tarif($this->_client, $item_panier['qty']);
			    $sous_total += $tarif['prix']*$item_panier['qty'];
			    
			}
			
			/*if ((!array_key_exists($produit->id, $tab_restriction) && !is_null($tab_restriction)) or (!$produit->remisable && is_null($tab_restriction)))
			{
				$tarif = $produit->tarif($this->_client, $item_panier['qty']);
				$sous_total += $tarif['prix']*$item_panier['qty'];
			}*/
		}
	
		return ($sous_total > 0) ? round($sous_total, 2) : FALSE;
	}
	

	private function _calcul_reduction($code_remise = NULL)
	{
		$this->load->model('catalogue/module_catalogue');
	
		$montant_reduction = 0;
		//Si le client est connecté, on peut utiliser sont statut pour le tarif (ex : professionnel)
		$client = $this->_client;

		 
		$produits_remises = NULL;
		$tab_restriction = NULL;
		$_obj_produits_associes = $code_remise->produits_associes();
		if ($_obj_produits_associes->result_count() > 0)
		{
			$montant_reduction = 0;
			//Si produits associés au code remise, alors vérifier si les articles du panier correspondent
			//Si au moins un article correspond, faire la remise à la ligne du/des produits associés
			foreach ($_obj_produits_associes as $produits_ass)
			{
				$tab_restriction[$produits_ass->id] = $produits_ass->id;
			}
		}
		//Calcul de la remise sur chaque produit du panier hors produits non remisables(hors frais de port)
		foreach ($this->cart->contents() as $item_panier) {
			
			$produit = $this->produit->get_by_id($item_panier['id']);
	
			$remisable = $produit->remisable;
			
			if ($remisable)
			{
				if ($code_remise->type_reduction == 1) //pourcentage
				{
					if(isset($tab_restriction) && is_array($tab_restriction))
					{
						if (array_key_exists($item_panier['id'], $tab_restriction))
						{
								
							$produits_remises[$item_panier['id']] = array(
									'id'	=> $item_panier['id']
							);
								
							$produit_pour_remise = new Produit;
							$produit_pour_remise->get_by_id($item_panier['id']);
							$tarif_produit = $produit_pour_remise->tarif();

							$item_panier['price'] = $tarif_produit['prix'] - ($tarif_produit['prix']*$item_panier['qty']*($code_remise->reduction/100));
							$montant_reduction += ($tarif_produit['prix']*$item_panier['qty']*($code_remise->reduction/100));
								
						}
					}

					else
					{
						$tarif 	 = $produit->tarif($client, $item_panier['qty']);
						$montant_reduction += ($tarif['prix']*$item_panier['qty']*($code_remise->reduction/100));
						
					}

				}
				elseif ($code_remise->type_reduction == 2) //valeur
				{
					$montant_reduction = $code_remise->reduction;
					break;
				}
			}
				
		}
		return $montant_reduction;
	}
}

/* End of file remises.php */
/* Location: ./modules/boutique/controllers/remises.php */