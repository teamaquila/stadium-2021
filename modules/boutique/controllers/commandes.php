<?php
require_once APPPATH.'controllers/front.php';

/**
 * Commandes
 * Contrôleur permettant les actions sur les commandes
 * 
 * @author 		Pauline MARTIN
 * @package 	boutique
 * @category 	Controllers
 * @version 	1.2
 */
class Commandes extends Front
{
	/**
	 * Liste des états de commandes
	 * 
	 * @var integer
	 */
	const ENREGISTREE 	= 1;
	const PAYEE 		= 2;
	const ANNULEE 		= 5;
	const REFUSEE 		= 6;
	const ATTENTE_PAIEMENT 		= 7;
	const PAIEMENT_COMPTE 		= 8;
	const SUR_DEVIS_FRAIS_PORT 	= 9;
	const PAIEMENT_CHEQUE 		= 10;
	
	/**
	 * Données clients
	 * 
	 * @var object
	 */
	private $_client;
	
	/**
	 * Paramètres du module
	 * 
	 * @var object
	 */
	private $_module;
	
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('cookie');
		$this->load->libraries(array('cart', 'order'));
		$this->load->models(array(
			'commande', 'produits_commande', 'client', 'facturation',
			'livraison', 'frais_port', 'pays', 'paiement',
			'transporteur', 'module_boutique', 'catalogue/produit',
			'code_remise', 'code_remises_commande', 'commandes_etat'
		));
		$this->load->config('catalogue/catalogue');
		
		
		$this->_module = $this->module_boutique->get(1);
		$this->_client = modules::run('boutique/clients/est_connecte');
	}
	
	/**
	 * Enregistrement complet de la commande avant le paiement
	 * Vérifie si la commande a déjà été enregistré
	 * 
	 * @return	object OR FALSE
	 */
	public function enregistrer()
	{
		if ($this->cart->total_items() > 0)
		{
			//Calcul de la tarification TVA
			$montants = modules::run('boutique/calcul_commande');
			
			//Données de la commande
			$data = array(
				'id' 			=> $this->order->item('id'),
				'utilisateur_id'=> $this->_client->id,
			    'reference'		=> $this->_module->prefixe_reference.date('ymd').'-'.strtoupper(uniqid()),
				'sous_total'	=> $montants['sous_total'],
				'montant'		=> $montants['total'],
				'acompte'		=> $montants['acompte'],
				'hors_taxe'		=> $montants['hors_taxe'],
				'tva_livraison'	=> $this->_module->tva_livraison,
				'commentaire' 	=> $this->order->item('commentaire'),
			    'date_livraison'=> date_to_mysql($this->order->item('date_livraison')),
			);
			
			$sur_devis = FALSE;
			if (isset($livraison['sur_devis']) && ($livraison['sur_devis'] === TRUE))
			{
			    $data['frais_port'] = 0;
			    $data['montant']	= $data['sous_total'];
			    
			    $sur_devis = TRUE;
			}
			
			if ($commande = $this->commande->enregistrer($data))
			{
				//Etat de la commande
				$commande->changer_etat(self::ENREGISTREE);
				
				//Supprime les produits déja commandés
				$commande->produits()->delete_all();
				
				//Produits commandés
				foreach ($this->cart->contents() as $item)
				{
					
					$obj_produit = new Produit();
					$obj_produit->get_by_id($item['id']);
					$tarif_produit = $obj_produit->tarif();
					
					$this->produits_commande->clear();
					$this->produits_commande->enregistrer(array(
						'commande_id' 	=> $commande->id,
						'produit_id'	=> $item['id'],
						'libelle'		=> $obj_produit->libelle,
						'reference'		=> $obj_produit->reference,
						'prix'			=> $item['price'],
						'prix_sans_remise' => $tarif_produit['prix_sans_remise'],
					    'tva'			=> $this->config->item('taux_tva')[66][$obj_produit->tva_id],
						'quantite'		=> $item['qty'],
						'options'		=> json_encode($this->cart->product_options($item['rowid']))
					));
				}
				
				//Remise
				if ($this->_module->option_remise)
				{
					$this->_remise($commande);
				}
				
				//Adresse de facturation
				$this->order->insert_biling(array('commande_id' => $commande->id));
				
				$facturation = $this->order->biling();
				$facturation = array_map('strtoupper', $facturation);
				if ($this->facturation->enregistrer($facturation))
				{
					$this->order->insert_biling(array('id' => $this->facturation->id));
				}

				//Adresse de livraison (non obligatoire)
				if ($this->_module->option_livraison)
				{
					$this->order->insert_delivery(array('commande_id' => $commande->id));
					$this->order->insert_delivery(array('frais_port' => $this->order->transporter()['frais_port']));
					$this->order->insert_delivery(array('transporteur_id' => $this->order->transporter()['transporteur_id']));
					$this->order->insert_delivery(array('tva_frais_port' => $this->_module->tva_livraison));

					$transporteur = $this->transporteur->get_by_id($this->order->transporter()['transporteur_id']);

					$obj_livraison = $this->order->delivery();
					
					$id_sage = $this->_client->id_sage;
					$obj_livraison['id_sage'] = $id_sage;
										
					$obj_livraison = array_map('strtoupper', $obj_livraison);
						
					if ($transporteur->etiquetage_colissimo) //Si c'est colissimo, mais pas en relais, à domicile
					{
						$shipping_date = new DateTime();
						$shipping_date->modify('+ 2 Days');
						

						$obj_livraison['options']	= json_encode(array(
								"date"				=> $shipping_date->format('d/m/Y'),
								'typeDePoint'		=>'DOM'
						));
					
					
					}
					elseif ($transporteur->etiquetage_chronopost) //Si c'est chronopost, mais pas en relais, à domicile
					{
						$shipping_date = new DateTime();
						$shipping_date->modify('+ 2 Days');
							
							
						$adresses['livraison']['options']	= json_encode(array(
								"date"				=> $shipping_date->format('d/m/Y'),
								'typeDePoint'		=>'DOM'
						));
						
									
					}

					if ($this->session->userdata('livraison_relais') && $transporteur->relais_colissimo)
					{
						$liv = $this->session->userdata('livraison_relais');
						
						$shipping_date = new DateTime();
						$shipping_date->modify('+ 2 Days');
						$postfields = array(
								"accountNumber"		=> $this->_module->identifiant_colissimo,
								"password"			=> $this->_module->mdp_colissimo,
								"id"		=> $liv['relais_id'],
								"date"		=> $shipping_date->format('d/m/Y'),
						);
						ini_set('soap.wsdl_cache_enabled', '0');
						$wsdl="https://ws.colissimo.fr/pointretrait-ws-cxf/PointRetraitServiceWS/2.0/findPointRetraitAcheminementByID?wsdl";
						$service=new SoapClient($wsdl, $postfields);
						$result =  $service->findPointRetraitAcheminementByID($postfields);
						 
						if ($result->return->errorCode == 0)
						{
							//Mise à jour de la session
						
						
							$liv = array(
								'relais_id'		=> $liv['relais_id'],
								'nom'			=> $result->return->pointRetraitAcheminement->nom,
								 'adresse'		=> $result->return->pointRetraitAcheminement->adresse1.' '.$result->return->pointRetraitAcheminement->adresse2.' '.$result->return->pointRetraitAcheminement->adresse3,
								'code_postal'	=> $result->return->pointRetraitAcheminement->codePostal,
								'ville'			=> $result->return->pointRetraitAcheminement->localite,
								'pays'			=> $result->return->pointRetraitAcheminement->libellePays,
								'options'		=> json_encode(array(
										'RegateCode'		=> $liv['relais_id'],
										"date"				=> $shipping_date->format('d/m/Y'),
										'distributionSort'	=> $result->return->pointRetraitAcheminement->distributionSort,
										'lotAcheminement'	=> $result->return->pointRetraitAcheminement->lotAcheminement ,
										'versionPlanTri'	=> $result->return->pointRetraitAcheminement->versionPlanTri ,
										'typeDePoint'		=> $result->return->pointRetraitAcheminement->typeDePoint ,
										//'shipping_date'		=> $shipping_date
								))
						
							);
						}
						
						

						if (isset($this->order->delivery()['id']) && ($this->order->delivery()['id'] != FALSE))
						{
							$liv['id'] = $this->order->delivery()['id'];
						}
						
						$liv['commande_id']			= $commande->id;
						$liv['frais_port']			= $this->order->transporter()['frais_port'];
						$liv['transporteur_id']		= $transporteur->id;
						$liv['tva_frais_port']		= $this->_module->tva_livraison;
						$liv['telephone']			= $this->order->delivery()['telephone'];
						$liv['email']				= $this->_client->email;
						
						$this->livraison->enregistrer($liv);

						$this->order->insert_delivery(array('id' => $this->livraison->id));
					}
					elseif ($this->session->userdata('livraison_relais') && $transporteur->relais_chronopost)
					{
						$liv = $this->session->userdata('livraison_relais');
							
						$shipping_date = new DateTime();
						$shipping_date->modify('+ 2 Days');
						$postfields = array(
								"accountNumber"		=> intval($this->_module->identifiant_chronopost),
								"password"			=> $this->_module->mdp_chronopost,
								"identifiant"		=> $liv['relais_id'],
								"language"			=> "FR",
								"countryCode"		=> "FR",
								"version"			=> "2.0"
						);
						ini_set('soap.wsdl_cache_enabled', '0');
						$wsdl="https://ws.chronopost.fr/recherchebt-ws-cxf/PointRelaisServiceWS?wsdl";
						$service=new SoapClient($wsdl, $postfields);
						$result =  $service->rechercheDetailPointChronopostInter($postfields);
					
						if ($result->return->errorCode == 0)
						{
							//Mise à jour de la session
								
								
							$liv = array(
									'relais_id'		=> $liv['relais_id'],
									'societe'		=> $result->return->listePointRelais->nom,
									'nom'			=> $this->_client->nom,
									'prenom'		=> $this->_client->prenom,
									'adresse'		=> $result->return->listePointRelais->adresse1.' '.$result->return->listePointRelais->adresse2.' '.$result->return->listePointRelais->adresse3,
									'code_postal'	=> $result->return->listePointRelais->codePostal,
									'ville'			=> $result->return->listePointRelais->localite,
									'pays'			=> "France",
									'options'		=> json_encode(array(
											'RegateCode'		=> $liv['relais_id'],
											"date"				=> $shipping_date->format('d/m/Y'),
											/*'distributionSort'	=> $result->return->listePointRelais->distributionSort,
											 'lotAcheminement'	=> $result->return->listePointRelais->lotAcheminement ,
							'versionPlanTri'	=> $result->return->listePointRelais->versionPlanTri ,*/
											'typeDePoint'		=> $result->return->listePointRelais->typeDePoint ,
											//'shipping_date'		=> $shipping_date
									))
										
							);
						}
							
					
						if (isset($this->order->delivery()['id']) && ($this->order->delivery()['id'] != FALSE))
						{
						    $liv['id'] = $this->order->delivery()['id'];
						}
					
						$liv['commande_id']			= $commande->id;
						$liv['frais_port']			= $this->order->transporter()['frais_port'];
						$liv['transporteur_id']		= $transporteur->id;
						$liv['tva_frais_port']		= $this->_module->tva_livraison;
						$liv['mobile']				=$this->order->delivery()['telephone'];
						$liv['email']				= $this->_client->email;
					
						$adresses['livraison'] = $liv;
						
						$this->livraison->enregistrer($liv);
						$this->order->insert_delivery(array('id' => $this->livraison->id));
					}
					elseif ($this->livraison->enregistrer($obj_livraison))
					{
						$this->order->insert_delivery(array('id' => $this->livraison->id));
					}
				}

				//Met à jour la session
				$this->order->update(array(
					'id' 			=> $commande->id,
					'utilisateur_id'=> $this->_client->id,
					'reference' 	=> $commande->reference,
					'sous_total'	=> $montants['sous_total'],
					'montant'		=> $montants['total'],
					'acompte'		=> $montants['acompte'],
					'hors_taxe'		=> $montants['hors_taxe']
				));
				
				return $commande;
			}
		}
		return FALSE;
	}
	
	/**
	 * Récapitulatif de la commande après retour de la banque
	 * 
	 * @return	VIEW
	 */
	public function validation()
	{
	
		if ($commande_session = $this->order->contents())
		{
			$commande = $this->commande->get_by_id($commande_session['id']);

			if ($commande->exists())
			{
				//Commande gratuite
				if ($commande->montant == 0)
				{
					//Mise à jour de l'état
					$commande->changer_etat(self::PAYEE);
					modules::run('boutique/paiements/_changer_stock', $commande);
					
					//Envoi email au client
					modules::run('boutique/emails/commande_gratuite', $commande);
					
					$data = array(
						'module' 	=> $this->_module,
						'commande'	=> $commande
					);
					return $this->load->view('boutique/commande/validation', $data, TRUE);
				}
				elseif ($this->_module->option_acompte && ($commande->acompte == 0))
				{
					//Mise à jour de l'état
					$commande->changer_etat(self::ATTENTE_PAIEMENT);
					
					//Mise à jour du stock
					modules::run('boutique/paiements/_changer_stock', $commande);
					
					//Envoi email au client
					modules::run('boutique/emails/commande_gratuite', $commande);
					
					$data = array(
						'parametres'=> $this->_parametres,
						'module' 	=> $this->_module,
						'commande'	=> $commande
					);
					return $this->load->view('boutique/commande/validation', $data, TRUE);
				}
				//Commande payée par Paybox
				elseif ($commande->reference == $this->input->get('maref'))
				{

					//Enregistrement du paiement si test
					if ($this->_module->pbx_test && ($this->input->get('erreur') == '0000'))
					{
						modules::run('boutique/paiements/test', $commande);

// 						if ($this->_module->type_export_commande == 1)
// 						{
// 							modules::run('boutique/exporter/temps_reel', $commande->id);	
// 						}
					}
					
					//Paiement refusé
					elseif (preg_match('/^001(.*)/', $this->input->get('erreur')))
					{
					    $code_remise_commande = new Code_remises_commande();
                        $code_remise_commande->get_by_commande_id($commande->id);
                        $code_remise_commande->delete(); 
						$commande->changer_etat(self::REFUSEE);
					}
					
					//Commmande annulée
					elseif ($commande->etat == self::ENREGISTREE)
					{
					    $code_remise_commande = new Code_remises_commande();
                        $code_remise_commande->get_by_commande_id($commande->id);
                        $code_remise_commande->delete(); 
						$commande->changer_etat(self::ANNULEE);
					}
					$data = array(
						'module' 	=> $this->_module,
						'commande' 	=> $commande,
						'parametres'=> $this->_parametres,
					);
					return $this->load->view('boutique/commande/validation', $data, TRUE);
				}
				else
				{
					return lang('err_commande_inexistante');
				}
			}
			else
			{
				return lang('err_commande_inexistante');
			}
		}
		else
		{
			return lang('err_session_expiree');
		}
	}
	
	/**
	 * Exporter une commande en PDF
	 * 
	 * @param	integer	$commande_id
	 * @param	boolean	$email
	 * @return 	void
	 */
	public function exporter($commande_id, $email = FALSE, $client_id = NULL)
	{
		ini_set("memory_limit","512M");
		$commande = $this->commande->get_by_id($commande_id);

		if ($commande->exists() && (($this->_client && $this->_client->id == $commande->utilisateur_id) || (!is_null($client_id) && $client_id == $commande->utilisateur_id)))
		{
			$this->load->model('code_remise');
			$this->load->library('html2pdf');
			$this->config->load('config');
			
			$etats = array(
				0 => lang('etat_erreur'),
				1 => lang('etat_enregistre'),
				2 => lang('etat_payee'),
				3 => lang('etat_preparee'),
				4 => lang('etat_expediee'),
				5 => lang('etat_annulee'),
				6 => lang('etat_retournee')
			);
			
			$config_facture = $this->config->item('facture');

			$data = array(
				'commande' 		=> $commande,
				'facturation' 	=> $commande->facturation(),
				'livraison'	 	=> $commande->livraison(),
				'paiement'		=> $commande->paiement(),
				'remise'		=> $commande->remise(),
				'produits'		=> $commande->produits(),
				'module'		=> $this->_module,
				'detail_remise'	=> $this->code_remise->get_by_code($commande->remise()->code),
				'item_par_page'	=> $config_facture['item_par_page'],
				'body_top'		=> $config_facture['body_top'],
			);
			//Facture HT
			if ($commande->hors_taxe)
			{
				$content = $this->load->view('boutique/commande/facture_ht', $data, TRUE);
			}
			
			//Facture d'acompte
			elseif ($this->_module->option_acompte && ($commande->acompte > 0))
			{
				$content = $this->load->view('boutique/commande/facture_acompte', $data, TRUE);
			}
			
			//Facture TTC
			else
			{
				$content = $this->load->view('boutique/commande/facture_ttc', $data, TRUE);
			}
	 		
			$this->html2pdf->folder(root_path().'echanges/export/');
			$this->html2pdf->filename($commande->reference.'.pdf');
			$this->html2pdf->paper('a4', 'portrait');
			$this->html2pdf->html($content);
			
			if ($email)
			{
				$this->html2pdf->create('save');
				return TRUE;
			}
			else
		    {
				$this->html2pdf->create();
			}
		}
		else
		{
			show_404();
		}
	}
	
	//Permet l'impression du bordereau colissimo pour une commande colissimo ou so colissimo
	public function etiquetage_colissimo($commande_id)
	{
		$commande = $this->commande->include_related('livraison')->include_related('livraison/transporteur')->get_by_id($commande_id);
		
		$client = new Client();
		$client->get_by_id($commande->utilisateur_id);
		
		if ($this->_module->active_colissimo && $commande->livraison_transporteur_etiquetage_colissimo)
		{
			
			$options = json_decode($commande->livraison_options);
			
			$tab_date = explode('/', $options->date);
			$date = new DateTime();
			$date->setDate($tab_date[2],$tab_date[1],$tab_date[0]);
			//$date = $tab_date[2].'-'.$tab_date[1].'-'.$tab_date[0];
			
			$postfields = array(
				'letter'	=> array(
					"contractNumber"	=> $this->_module->identifiant_colissimo,
					"password"			=> $this->_module->mdp_colissimo,
					'service'	=> array(
						"dateDeposite"		=> $date->format('Y-m-d').'T'.$date->format('H:i:s'),
						'crbt'				=> 0,
						'serviceType'		=> 'SO',
						'returnType'		=> 'CreatePDFFile',
						'commercialName'	=> $this->_module->vendeur_societe
					),
					'parcel'	=> array(
						'weight'	=> 0.12,
						'horsGabarit'	=> 0,
						'DeliveryMode' 	=> $options->typeDePoint,
						//'contents'		=> array()
					),
					'dest'		=> array(
						'alert'	=> 'none',
						'codeBarForreference'	=> 0,
						'addressVO'	=> array(
							'companyName'	=> $commande->livraison_societe,
							'Name'			=> ($commande->livraison_transporteur_relais_colissimo) ? ($client->nom) : ($commande->livraison_nom),
							'Surname'		=> ($commande->livraison_prenom) ? ($commande->livraison_prenom) : ($client->prenom),
							'line2'			=> $commande->livraison_adresse,
							'countryCode'	=> 'FR',
							'city'			=> $commande->livraison_ville,
							'postalCode'	=> $commande->livraison_code_postal,
							'MobileNumber'	=> $commande->livraison_mobile_transporteur ? $commande->livraison_mobile_transporteur : $commande->livraison_telephone,
							'email'			=> $commande->livraison_email,
						)
					),
					'exp'		=> array(
						'alert'	=> 'none',
						'addressVO'	=> array(
								'companyName'	=> $this->_module->vendeur_societe,
								'line2'			=> $this->_module->vendeur_adresse,
								'countryCode'	=> 'FR',
								'city'			=> $this->_module->vendeur_ville,
								'postalCode'	=> $this->_module->vendeur_code_postal,
								'email'			=> $this->_module->vendeur_email,
						)
					)
				),
				
    		);
			
			if ($commande->livraison_transporteur_relais_colissimo)
			{
				$postfields['letter']['parcel']['RegateCode'] = $options->RegateCode;
			}
			
			ini_set('soap.wsdl_cache_enabled', '0');
			$wsdl="https://ws.colissimo.fr/soap.shippingclpV2/services/WSColiPosteLetterService?wsdl";
			$service=new SoapClient($wsdl, $postfields);
			$result =  $service->GetLetterColissimo($postfields);
			
			if ($result->getLetterColissimoReturn->errorID == '0')
			{
				$livraison = new Livraison();
				$livraison->get_by_id( $commande->livraison_id);
				$livraison->code_suivi = $result->getLetterColissimoReturn->parcelNumber;
				$livraison->lien = $result->getLetterColissimoReturn->PdfUrl;
				$livraison->save();
				
				redirect($result->getLetterColissimoReturn->PdfUrl);
				exit();
				
			
			}
			else
			{
				echo $result->getLetterColissimoReturn->error;
			}
		}
	}
	
	//Permet l'impression du bordereau colissimo pour une commande colissimo ou so colissimo
	public function etiquetage_chronopost($commande_id)
	{
		$commande = $this->commande->include_related('livraison')->include_related('livraison/transporteur')->get_by_id($commande_id);
	
		$client = new Client();
		$client->get_by_id($commande->utilisateur_id);

		if ($this->_module->active_chronopost && $commande->livraison_transporteur_etiquetage_chronopost)
		{
			if (is_null($commande->livraison_code_suivi) || empty($commande->livraison_code_suivi) )
			{
				$options = json_decode($commande->livraison_options);
	
				$tab_date = explode('/', $options->date);
				$date = new DateTime();
				$date->setDate($tab_date[2],$tab_date[1],$tab_date[0]);
				//$date = $tab_date[2].'-'.$tab_date[1].'-'.$tab_date[0];
	
				//calcul du poids total du colis
				$poids_total = 0;
				foreach ($commande->produits() as $produits_commande)
				{
					$produit = new Produit();
					$produit->get_by_id($produits_commande->produit_id);
						
					$poids_total += $produit->poids;
				}
				$tab_adresses_livraisons =  preg_split('/(\r\n|\n|\r)/', $commande->livraison_adresse, 2);
	
				$postfields = array(
						'version'			=> '2.0',
						'mode'				=> 'PDF',
						'password'			=> $this->_module->mdp_chronopost,
						'esdValue'			=> array(
								'height'			=> 14,
								'width'			=> 14,
								'length'			=> 14,
						),
						'headerValue'		=> array(
								'accountNumber'	=> intval($this->_module->identifiant_chronopost),
								'subAccount'	=> ''
						),
						'shipperValue'		=> array(
								'shipperCivility'	=> 1,
								'shipperName'		=> $this->_module->vendeur_societe,
								'shipperAdress1'	=> $this->_module->vendeur_adresse,
								'shipperCountry'	=> 'FR',
								'shipperCity'		=> $this->_module->vendeur_ville,
								'shipperZipCode'	=> $this->_module->vendeur_code_postal,
								'shipperEmail'		=> $this->_module->vendeur_email,
								'shipperPhone'		=> $this->_module->vendeur_telephone,
								'shipperPreAlert'	=> 0
						),
						'customerValue'		=> array(
								'customerCivility'		=> 1,
								'customerName'			=> $commande->livraison_societe,
								'customerName2'			=> ($commande->livraison_transporteur_relais_chronopost) ? ($client->nom) : ($commande->livraison_nom),
								'customerContactName'	=> ($commande->livraison_transporteur_relais_chronopost) ? ($client->nom.' '.$client->prenom) : ($commande->livraison_nom.' '.$commande->livraison_prenom),
								'customerAdress1'		=> $tab_adresses_livraisons[0],
								'customerAdress2'		=> (isset($tab_adresses_livraisons[1])) ? preg_replace("/(\r\n|\n|\r)/", " ", $tab_adresses_livraisons[1]) : NULL,
								'customerCountry'		=> $this->pays->get_by_nom($commande->livraison_pays)->extension,
								'customerCity'			=> $commande->livraison_ville,
								'customerZipCode'		=> $commande->livraison_code_postal,
								'customerPhone'			=> ($commande->livraison_mobile) ? ($commande->livraison_mobile) : ($commande->livraison_telephone),
								'customerMobilePhone'	=> ($commande->livraison_mobile) ? ($commande->livraison_mobile) : ($commande->livraison_telephone),
								'customerEmail'			=> $commande->livraison_email,
								'customerPreAlert'		=> 0
						),
						'recipientValue'	=> array(
								'recipientCivility'		=> 1,
								'recipientName'			=> $commande->livraison_societe,
								'recipientName2'		=> ($commande->livraison_transporteur_relais_chronopost) ? ($client->nom.' '.$client->prenom) : ($commande->livraison_nom.' '.$commande->livraison_prenom),
								'recipientContactName'	=> ($commande->livraison_transporteur_relais_chronopost) ? ($client->nom.' '.$client->prenom) : ($commande->livraison_nom.' '.$commande->livraison_prenom),
								'recipientAdress1'		=> $tab_adresses_livraisons[0],
								'recipientAdress2'		=> (isset($tab_adresses_livraisons[1])) ? preg_replace("/(\r\n|\n|\r)/", " ", $tab_adresses_livraisons[1]) : NULL,
								'recipientCountry'		=> $this->pays->get_by_nom($commande->livraison_pays)->extension,
								'recipientCity'			=> $commande->livraison_ville,
								'recipientZipCode'		=> $commande->livraison_code_postal,
								'recipientPhone'		=> ($commande->livraison_mobile) ? ($commande->livraison_mobile) : ($commande->livraison_telephone),
								'recipientMobilePhone'	=> ($commande->livraison_mobile) ? ($commande->livraison_mobile) : ($commande->livraison_telephone),
								'recipientEmail'		=> $commande->livraison_email,
								'recipientPreAlert'	 	=> 22
						),
						'refValue'			=> array(),
						'skybillValue'		=> array(
								'shipHour'		=> date('H'),
								'evtCode'		=> 'DC',
								'service'		=> 0,
								'productCode'	=> ($commande->livraison_transporteur_relais_chronopost) ? 86 : '1F',
								'objectType'	=> 'MAR',
								'weight'		=> round($poids_total/1000,3) //Le poids est renseigné en grammes par défaut donc / pour avoir en KG
						),
						'skybillParamsValue'=> array(),
				);
	
				if ($commande->livraison_transporteur_relais_colissimo)
				{
					$postfields['letter']['parcel']['RegateCode'] = $options->RegateCode;
				}
	
	
				$postfields["trace"]				= TRUE;
	
				ini_set('soap.wsdl_cache_enabled', '0');
				$wsdl="https://ws.chronopost.fr/shipping-cxf/ShippingServiceWS?wsdl";
				$service=new SoapClient($wsdl, $postfields);
				$result =  $service->ShippingV2($postfields);
				//$result =  $service->shippingWithReservationAndESDWithRefClient($postfields);
					
				if ($result->return->errorCode == '0')
				{
	
					//On mets le contenu du pdf dans un fichier
					$this->load->helper('file');
					if (file_put_contents(ROOTPATH.'/echanges/export/chronopost/'.$result->return->skybillNumber.'.pdf',  $result->return->skybill))
					{
						$commande->code_suivi = $result->return->skybillNumber;
						$commande->save();
							
						header('Pragma: public');
						header('Expires: 0');
						header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
						header('Cache-Control: public');
						header('Content-Type: application/pdf');
						header('Content-Disposition: attachment; filename='.$commande->code_suivi.'.pdf');
	
						@readfile(root_path().'/echanges/export/chronopost/'.$commande->code_suivi.'.pdf');
							
					}
					else
					{
						echo 'Erreur lors de la création de l\'étiquette.';
					}
	
	
				}
				else
				{
					echo $result->return->errorMessage;
				}
			}
			else
			{
				header('Pragma: public');
				header('Expires: 0');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Cache-Control: public');
				header('Content-Type: application/pdf');
				header('Content-Disposition: attachment; filename='.$commande->code_suivi.'.pdf');
	
				@readfile(root_path().'/export/chronopost/'.$commande->code_suivi.'.pdf');
			}
		}
	}
	
	/**
	 * Enregistre la remise lié à la commande
	 * 
	 * @param 	object	$commande
	 */
	private function _remise($commande)
	{
		//Supprime l'ancienne remise
		$commande_remise = $commande->remise();
		
		if ($commande_remise->exists())
		{
			$commande_remise->delete();
		}
		
		//Remise utilisée
		if ($remise = $this->order->discount())
		{
			$code_remise = $this->code_remise->get_by_code($remise['code']);
				
			if ($code_remise->valide())
			{
				$this->code_remises_commande->enregistrer(array(
						'commande_id' 	=> $commande->id,
						'libelle' 		=> $code_remise->libelle,
						'code' 			=> $code_remise->code,
						'montant'		=> $remise['montant']
				));
			}
		}
	}
	
	
	/**
	 * Relance des paniers enregistrés à l'étape 4, non finalisés
	 * @param 	int	$id identifiant de la commande si on veut relancer une seule
	 * 
	 */
	public function relance_commande($id = NULL)
	{
		$i = 0;
		$this->commande->where('etat', 1)->where('relancee' , 0);
		
		if (!is_null($id))
		{
			$this->commande->where('id', $id);
		}
		
		$commandes_enregistrées = $this->commande->get();
		foreach ($commandes_enregistrées as $commande)
		{
			modules::run('boutique/emails/commande_relance', $commande);
			$commande->relancee = TRUE;
			$commande->save();
			$i++;
		}
		echo $i.' commandes relancée(s)';
	}
	
	
	/***
	 * Transforme la commande enregistrée relancée en panier après clic lien du mail
	 * 
	 */
	public function recupere_commande($encode_reference_commande)
	{
		$commande = $this->commande->get_by_reference(base64_decode($encode_reference_commande));
		foreach ($commande->produits() as $produits_commande)
		{
			$produit = $this->produit->include_related('module_catalogue', array('gestion_stock'), 'gestion_stock')->get_by_reference($produits_commande->reference);
		
			if ($produit->exists())
			{
				//Stock
				if ($produit->gestion_stock && ($produit->stock <  $produits_commande->quantite))
				{
					return array(
						'erreur'	=> TRUE,
						'message'	=> lang('erreur_stock_insuffisant').' ('.$produit->stock.')'
					);
				}
				
				
				$client = Modules::run('boutique/clients/est_connecte');
				$tarif 	= $produit->tarif($client, $produits_commande->quantite);
				
				if (is_array($tarif) && (( ! $tarif['unique'] && isset($tarif['grille'])) or $tarif['unique']))
				{
					//Prix TTC si saisie en HT
					if ( ! $tarif['taxe'])
					{
						$tarif['prix'] = $tarif['prix']*(1+$tarif['tva']/100);
					}
					
					$cart = array(
						'id' 		=> $produit->id,
						'qty'		=> $produits_commande->quantite,
						'price'		=> $tarif['prix'],
						'name'		=> $produit->libelle,
					);
					
					if ($rowid = $this->cart->insert($cart))
					{
						$json = array(
							'erreur'	=> FALSE,
							'libelle'	=> $produit->libelle,
							'tarif'		=> $tarif['prix'],
							'id'		=> $cart['id'],
							'rowid'		=> $rowid
						);
					}
					else
					{
						$json = array(
							'erreur'	=> TRUE,
							'message'	=> lang('erreur_ajout_panier')
						);
					}
				}
				else
				{
					$json = array(
						'erreur'	=> TRUE,
						'message'	=> lang('erreur_prix_produit')
					);
				}
			}
			else
			{
				$json = array(
					'erreur'	=> TRUE,
					'message'	=> lang('erreur_produit_inexistant')
				);
			}
		}
		redirect('/boutique/etape,1.html');
		
	}
	
	/***
	 * Paiement compte si client pro
	 *
	 */
	public function paiement_compte()
	{
	    //Les CGV Doivent être validées
	    if ($this->_module->option_cgv && !$this->input->post('cgv_validee'))
	    {
	        redirect('/boutique/etape,4.html?cgv_validee=0');
	    }
	    
	    $commande_id = $this->input->post('commande_id');
	    $commande = $this->commande->get_by_id($commande_id);
	    
	    //Mise à jour de l'état
	    $commande->changer_etat(self::PAIEMENT_COMPTE);
	    
	    //Envoi email au client
	    modules::run('boutique/emails/commande_compte', $commande);
	    
	    //Mise à jour du stock
	    $this->_changer_stock($commande);
	    
	    modules::run('boutique/exporter/temps_reel', $commande->id);
	    
	    $data = array(
	        'parametres'	=> $this->_parametres,
	        'commande'	=> $commande,
	        'client'	=> $commande->client()
	    );
	    $validation = $this->load->view('boutique/commande/validation', $data, TRUE);
	    
	    $data = array(
	        'guide_etapes'=> NULL,
	        'commande'	=> $validation
	    );
	    
	    parent::_afficher('boutique/page', array(
	        'module'=> $this->load->view('boutique/boutique/etape5', $data, TRUE),
	        //'seo'	=> $this->_seo()
	    ));
	    
	    //Efface les sessions
	    $this->cart->destroy();
	    $this->order->reset();
	    $this->_efface_sessions();
	}
		
	/***
	 * Paiement compte si client pro
	 *
	 */
	public function paiement_cheque()
	{
	    //Les CGV Doivent être validées
	    if ($this->_module->option_cgv && !$this->input->post('cgv_validee'))
	    {
	        redirect('/boutique/etape,4.html?cgv_validee=0');
	    }

	    $commande_id = $this->input->post('commande_id');
	    $commande = $this->commande->get_by_id($commande_id);
	    
	    //Mise à jour de l'état
	    $commande->changer_etat(self::PAIEMENT_CHEQUE);
	    
	    //Envoi email au client
	    modules::run('boutique/emails/commande_cheque', $commande);
	    
	    //Mise à jour du stock
	    $this->_changer_stock($commande);
	    
	    modules::run('boutique/exporter/temps_reel', $commande->id);
	    
	    $data = array(
	        'parametres'	=> $this->_parametres,
	        'commande'	=> $commande,
	        'client'	=> $commande->client()
	    );
	    $validation = $this->load->view('boutique/commande/validation', $data, TRUE);
	    
	    $data = array(
	        'guide_etapes'=> NULL,
	        'commande'	=> $validation
	    );
	    
	    parent::_afficher('boutique/page', array(
	        'module'=> $this->load->view('boutique/boutique/etape5', $data, TRUE),
	        //'seo'	=> $this->_seo()
	    ));
	    
	    //Efface les sessions
	    $this->cart->destroy();
	    $this->order->reset();
	    $this->_efface_sessions();
	}
	
	/**
	 * Met à jour le stock du produit
	 *
	 * @param	object	$commande
	 * @return	TRUE
	 */
	public function _changer_stock($commande)
	{
	    $this->load->models(array('catalogue/produit', 'catalogue/module_catalogue'));
	    
	    foreach ($commande->produits() as $produit_commande)
	    {
	        $this->produit->clear();
	        
	        $produit = $this->produit->get_by_id($produit_commande->produit_id);
	        $module  = $produit->module();
	        
	        if ($module->gestion_stock)
	        {
	            $produit->stock -= $produit_commande->quantite;
	            $produit->save();
	            
	            //Si référence, on décrémente aussi le produit parent
	            if ($produit = $produit->produit_parent())
	            {
	                $produit->stock -= $produit_commande->quantite;
	                $produit->save();
	            }
	        }
	    }
	    return TRUE;
	}
	
	/**
	 * Supprime toutes les sessions liés à une commande
	 *
	 * @return	void
	 */
	private function _efface_sessions()
	{
	    $this->cart->destroy();
	    $this->session->unset_userdata('adresses');
	    $this->session->unset_userdata('livraison');
	    $this->session->unset_userdata('commande');
	    $this->session->unset_userdata('remise');
	    $this->session->unset_userdata('commande_commentaire');
	    $this->session->unset_userdata('commande_date_livraison');
	    $this->session->unset_userdata('cgv');
	}
	
	public function enregistrer_devis()
	{
	    if ($this->cart->total_items() > 0)
	    {
	        $session = array();
	        $commande_session = array();

	        if ($commande_session = $this->order->contents())
	        {
	            $commande = $this->commande->get_by_id($commande_session['id']);
	            
	            //Mise à jour de l'état
	            $commande->changer_etat(self::ATTENTE_PAIEMENT);
	            
	            $data = array(
	                'parametres'=> $this->_parametres,
	                'module' 	=> $this->_module,
	                'commande'	=> $commande,
	                'client'	=> $commande->client()
	            );
	            $validation = $this->load->view('boutique/commande/validation', $data, TRUE);
	            
	            $data = array(
	                'guide_etapes'	=> NULL,
	                'commande'		=> $validation
	            );
	            
	            parent::_afficher('boutique/page', array(
	                'module'=> $this->load->view('boutique/boutique/etape5', $data, TRUE),
	                //'seo'	=> $this->_seo()
	            ));
	            
	            $this->_efface_sessions();
	        }
	    }
	}
	
	public function envoyer_commande_sur_devis_frais_port()
	{
	    
	    if ($this->cart->total_items() > 0)
	    {
	        
	        $this->load->library('email');
	        $session = array();
	        $commande_session = array();
	        
	        if ($this->session->userdata('order'))
	        {
	            
	            $commande_session = $this->session->userdata('order');
	            $commande = $this->commande->get_by_id($commande_session['id']);
	            
	            //Mise à jour de l'état
	            $commande->changer_etat(self::SUR_DEVIS_FRAIS_PORT);
	            
	            $client = $commande->client();
	            
	            //Envoi d'un mail pour informer d'un nouveau devis frais de port en attente
	            $data = array(
	                'version'		=> $this->_parametres,
	                'parametres'=> $this->_parametres,
	                'module' 		=> $this->_module,
	                'client' 		=> $client,
	                'commande'		=> $commande,
	                'livraison'		=> $commande->livraison(),
	                'facturation'	=> $commande->facturation(),
	                'produits'      => $commande->produits(),
	            );
	            
	            $template = $this->load->view('boutique/email/commande_surdevis_fraisport', $data, TRUE);
	            $this->email->clear();
	            $this->email->from($this->_module->vendeur_email, $this->_module->vendeur_societe);
	            $this->email->to($client->email);
	            $this->email->subject("Une nouvelle commande sur devis pour frais de port");
	            $this->email->message($template);
	            $this->email->send();
	            
	            
	            $templateb = $this->load->view('boutique/email/commande_surdevis_fraisport_vendeur', $data, TRUE);
	            $this->email->clear();
	            $this->email->from($client->email, $client->nom.' '.$client->prenom);
	            $this->email->to($this->_module->vendeur_email);
	            $this->email->subject("Une nouvelle commande sur devis pour frais de port sur votre site");
	            $this->email->message($templateb);
	            $this->email->send();
	            
	            $data = array(
	                'version'	=> $this->_parametres,
	                'parametres'=> $this->_parametres,
	                'commande'	=> $commande,
	                'client'	=> $client
	            );
	            $validation = $this->load->view('boutique/commande/validation', $data, TRUE);
	            
	            $data = array(
	                'fil_ariane'=> NULL,
	                'commande'	=> $validation
	            );
	            
	            parent::_afficher('boutique/page', array(
	                'module'=> $this->load->view('boutique/boutique/etape5', $data, TRUE),
	                //'seo'	=> $this->_seo()
	            ));
	            
	            $this->_efface_sessions();
	        }
	    }
	}
	
	/**
	 * Relance client par mail pour toute commande effectuée il y a 10 mois, par mail pour indiquer que c'est le moment
	 *
	 * CRON
	 */
	public function relance_commande_client()
	{
	    $ip_autorise = array(
	        '81.252.124.194',
	        '46.18.195.156',
	        //'127.0.0.1'
	    );
	    
	    if (in_array($_SERVER['REMOTE_ADDR'], $ip_autorise))
	    {
    	    ini_set("memory_limit","512M");
    	    $this->load->helper('csv');
    	    $this->load->library('email');
    	    $commandes_relance = new Commande();
    	    
    	    $jour_test = new DateTime();
    	    $jour_test->modify('- 333 days');
    	    
    	    
    	    $commandes_relance->like('created', $jour_test->format('Y-m-d'))->where_in('etat', array(2,3,4,8,9))->get();
    
    	    if ($commandes_relance->exists())
    	    {
    	        foreach ($commandes_relance as $commande)
    	        {
    	            $client = new Client();
    	            $client->get_by_id($commande->utilisateur_id);
    	            if ($client->pro == 1) //On le relance que les pros
    	            {
    	                $data = array(
    	                    'version'		=> $this->_version,
    	                    'module' 		=> $this->_module,
    	                    'client' 		=> $client,
    	                    'commande'		=> $commande,
    	                );
    	                
    	                $template = $this->load->view('boutique/email/commande_relance', $data, TRUE);
    	                $this->email->clear();
    	                $this->email->from($this->_module->vendeur_email, $this->_module->vendeur_societe);
    	                $this->email->to($client->email);
    	                //$list_bcc = array($this->_module->vendeur_email,'support-web@aquilainfo.com');
    	                //$this->email->bcc($this->_module->vendeur_email);
    	                $this->email->subject("N'oubliez pas votre commande");
    	                $this->email->message($template);
    	                $this->email->send();
    	                
    	                $tab_commandes_relancees[$commande->id] = array(
    	                    'reference'	=> $commande->reference,
    	                    'nom'		=> $client->nom,
    	                    'code'		=> $client->code
    	                );
    	            }
    	            
    	        }
    	        
    	        $data['version'] =  $this->_version;
    	        $data['module'] =  $this->_module;
    	        $data['commandes_relancees'] = $tab_commandes_relancees;
    	        
    	        $templateb = $this->load->view('boutique/email/commande_relance_vendeur', $data, TRUE);
    	        $this->email->clear();
    	        $this->email->from('no-reply@stadium.fr', $this->_module->vendeur_societe);
    	        $this->email->to($this->_module->vendeur_email);
    	        $this->email->bcc('support-web@aquilainfo.com');
    	        $this->email->subject("Liste des clients relancés sur le site ce jour ci");
    	        $this->email->message($templateb);
    	        //var_dump($templateb);
    	        $this->email->send();
    	        
    	        // création d'un fichier .csv pour lister les clients relancés qui devra être déposé sur le ftp.
    	        write_file( root_path().'echanges/export/'.mdate('%Y-%m-%d').'-clients-relances.csv', array_to_csv($tab_commandes_relancees, ""), 'w+');
    	    }
	    }
	}
	
	
	
	/**
	 * Relance client par mail pour tout devis non transformé depuis 15 jours
	 * Lancé une fois par jour
	 * CRON
	 */
	public function relance_devis_client()
	{
	    $ip_autorise = array(
	        '81.252.124.194',
	        '46.18.195.156',
	       //'127.0.0.1'
	    );
	    
	    if (in_array($_SERVER['REMOTE_ADDR'], $ip_autorise))
	    {
    	    ini_set("memory_limit","512M");
    	    $this->load->library('email');
    	    $devis_relance = new Commande();
    	    
    	    $jour_test = new DateTime();
    	    $jour_test->modify('-15 days');
    	    
    	    /*$conditions = array(
    	     'created >' => $jour_test->format('Y-m-d').' 00:00:00',
    	     'created <' => $jour_test->format('Y-m-d').' 23:59:59',
    	     
    	     );*/
    	    
    	    $devis_relance->like('created', $jour_test->format('Y-m-d'))->where('etat', 7)->get();
    	    if ($devis_relance->exists())
    	    {
    	        $tab_devis_relances = array();
    	        foreach ($devis_relance as $devis)
    	        {
    	            $client = new Client();
    	            $client->get_by_id($devis->utilisateur_id);
    	            if ($client->pro == 1) //On le relance que les pros
    	            {
    	                $data = array(
    	                    'version'		=> $this->_version,
    	                    'module' 		=> $this->_module,
    	                    'client' 		=> $client,
    	                    'devis'			=> $devis,
    	                );
    	                
    	                $template = $this->load->view('boutique/email/devis_relance', $data, TRUE);
    	                $this->email->clear();
    	                $this->email->from($this->_module->vendeur_email, $this->_module->vendeur_societe);
    	                $this->email->to($client->email);
    	                //$this->email->bcc('support-web@aquilainfo.com');
    	                $this->email->subject("N'oubliez pas votre devis");
    	                $this->email->message($template);
    	                $this->email->send();
    	                
    	                if (!empty($client->mail_representant) && !is_null($client->mail_representant))
    	                {
    	                    $data_revendeur['devis_relances'] = array();
    	                    $data_revendeur['devis_relances'][$devis->id] = array(
    	                        'reference'	=> $devis->reference,
    	                        'nom'		=> $client->nom,
    	                        'code'		=> $client->code
    	                    );
    	                    $templateb = $this->load->view('boutique/email/devis_relance_vendeur', $data_revendeur, TRUE);
    	                    $this->email->clear();
    	                    $this->email->from('no-reply@stadium.fr', $this->_module->vendeur_societe);
    	                    $this->email->to($client->mail_representant);
    	                    $this->email->bcc('support-web@aquilainfo.com');
    	                    $this->email->subject("Liste des devis clients relancés sur le site ce jour ci");
    	                    $this->email->message($templateb);
    	                    $this->email->send();
    	                    
    	                    $tab_devis_relances[$devis->id] = array(
    	                        'reference'	=> $devis->reference,
    	                        'nom'		=> $client->nom,
    	                        'code'		=> $client->code
    	                    );
    	                }
    	                else
    	                {
    	                    
    	                    $tab_devis_relances[$devis->id] = array(
    	                        'reference'	=> $devis->reference,
    	                        'nom'		=> $client->nom,
    	                        'code'		=> $client->code
    	                    );
    	                }
    	                
    	            }
    	            
    	        }
    	        $data['devis_relances'] = $tab_devis_relances;
    	        
    	        $data['module'] = $this->_module;
    	        
    	        $templateb = $this->load->view('boutique/email/devis_relance_vendeur', $data, TRUE);
    	        $this->email->clear();
    	        $this->email->from('no-reply@stadium.fr', $this->_module->vendeur_societe);
    	        $this->email->to($this->_module->vendeur_email);
    	        $this->email->bcc('support-web@aquilainfo.com');
    	        $this->email->subject("Liste des devis clients relancés sur le site ce jour ci");
    	        $this->email->message($templateb);
    	        $this->email->send();
    	    }
	    }
	}
	
}
/* End of file commandes.php */
/* Location: ./modules/boutique/controllers/commandes.php */