<?php
require_once APPPATH.'controllers/front.php';

/**
 * Exportation des commandes
 * 
 * Types export commande
 * 0 => pas d'export
 * 1 => temps réel
 * 2 => journalier
 * 
 * @author 		Pauline Martin, Pauline MARTIN
 * @package		Boutique
 * @category	Controllers
 * @version 	1.0
 */
class Exporter extends Front
{
	/**
	 * Parametres du module boutique
	 * 
	 * @var object
	 */
	private $_module;
	
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->load->models(array('commande', 'paiement', 'module_boutique'));
		
		$this->_module = $this->module_boutique->get(1);
	}

	/**
	 * Export des commandes vers Abelia
	 * Format : XML
	 * 
	 * @param	integer	$commande_id
	 */
	public function temps_reel($commande_id = NULL)
	{
	    $this->load->helpers(array('xml', 'file'));
	    $this->load->helper('csv');
	    $this->load->models(array(
	        'boutique/commande',
	        'boutique/transporteur',
	        'boutique/facturation',
	        'boutique/livraison',
	        'boutique/paiement',
	        'catalogue/produit',
	        'boutique/produits_commande',
	        'boutique/code_remises_commande',
	        'boutique/client',
	        'pays'
	    ));
	    
	    $this->benchmark->mark('code_start');
	    $i = 0;
	    $fichier_log = root_path().'logs/export.log';
	    
// 	    $tva = $this->tva->get();
// 	    $tva_calcule = 1 + ($tva->taux / 100);
	    
	    if ( ! is_null($commande_id))
	    {
	        $this->commande->select('id,reference, utilisateur_id, montant, etat, created, tva_livraison');
	        $this->commande->include_related('livraison', array('frais_port', 'transporteur_id'));
	        $this->commande->include_related('produits_commande', array('id','produit_id','libelle', 'reference', 'prix', 'quantite', 'options', 'tva'));
	        $this->commande->include_related('utilisateur', array('id', 'code' ,'nom', 'prenom', 'email', 'code'));
	        $this->commande->where('id',$commande_id);
	        $this->commande->include_related('code_remises_commande', array('libelle', 'code', 'montant'));
	        $this->commande->order_by('produits_commande_id', 'asc');
	        $commande = $this->commande->get();
	        
	        
	        $commande_id_cours = 0;
	        foreach ($commande as $comm) {
	            $facturation = $comm->facturation();
	            $livraison = $comm->livraison();
	            
	            $pays_livraison = $this->pays->get_by_nom($livraison->pays)->extension;
	            
	            $options = NULL;
	            if (is_object(json_decode($comm->produits_commande_options)))
	            {
	                foreach (json_decode($comm->produits_commande_options) as $index => $option)
	                {
	                    if (!empty($option))
	                    {
	                        
	                        switch ($index) {
	                            case 'associe_gravure':
	                                $options .= 'Fichier:'.$option.',';
	                                break;
	                                
	                            case 'contenu_gravure':
	                                $options .= 'Gravure:'.preg_replace('/\\n/', ' / ', $option).',';
	                                break;
	                                
	                            case 'gravure_complementaire':
	                                $options .= 'Gravure:'.preg_replace('/\\n/', ' / ', $option).',';
	                                break;
	                            default:
	                                ;
	                                break;
	                        }
	                    }
	                    
	                }
	                $options = substr($options, 0, -1);
	            }
	            
	            
	            
	            //tarifs de base si pro et prix spéciaux, renvoyer aussi le prix de base
	            $prix_ht = 0;
	            $remise_produit = 0;
	            $client_produit = new Clients_produit();
	            $client_produit->where('client_id', $comm->utilisateur_id)->where('produit_id', $comm->produits_commande_produit_id)->get();
	            $client_produit->check_last_query();
	            if ($client_produit->exists())
	            {
	                $prix_ht = $client_produit->prix_brut;
	                $remise_produit = $client_produit->remise;
	            }
	            
	            $tab_commande[] = array(
	                $comm->utilisateur_id, $comm->utilisateur_code, $comm->created, $comm->reference,
	                $comm->produits_commande_reference, $comm->produits_commande_libelle,
	                $comm->produits_commande_quantite,$comm->produits_commande_prix,$comm->produits_commande_tva,
	                $options,
	                $comm->montant, round($comm->frais_port/$comm->tva_livraison, 10) ,
	                $livraison->id,str_replace("'", "\'",$livraison->nom), str_replace("'", "\'",$livraison->prenom),preg_replace("#(?<!\r)\n#", '', $livraison->adresse),preg_replace("#(?<!\r)\n#", '', $livraison->adresse2),str_replace("'", "\'",$livraison->ville),$livraison->code_postal,$pays_livraison , $livraison->id_sage,
	                $prix_ht, $remise_produit, $livraison->societe
	            );
	            
	            $i++;
	        }
	        write_file( root_path().'/echanges/export/commandes/'.$comm->reference.'-commande.csv', array_to_csv($tab_commande, ""), 'w+');
	        write_file( root_path().'/echanges/sauvegarde_export/commandes/'.$comm->reference.'-commande.csv', array_to_csv($tab_commande, ""), 'w+');
	        //Export du client associé en même temps
	        Modules::run('boutique/clients/exporter', $comm->utilisateur_id, FALSE, $comm->reference);
	        
	    }
	    
	    $this->benchmark->mark('code_end');
	    $temps_traitement = $this->benchmark->elapsed_time('code_start', 'code_end');
	    
	    //Log fichier
	    $log_string = mdate('%Y-%m-%d %H:%i:%s').' | '.$temps_traitement.'s | '.$i.' commandes '."\n";
	    
	    write_file($fichier_log, $log_string, FOPEN_WRITE_CREATE);
	    
	    return;
	}

	/**
	 * Export des commandes vers Abelia
	 * Format : XML
	 */
	public function journalier()
	{
		if ($this->_module->type_export_commande == 2)
		{
			$this->load->helpers(array('xml', 'file'));
			$this->load->models(array(
				'boutique/commande', 
				'boutique/transporteur', 
				'boutique/facturation', 
				'boutique/livraison', 
				'boutique/paiement', 
				'catalogue/produit', 
				'boutique/produits_commande', 
				'boutique/code_remises_commande', 
				'boutique/client'
			));
			
			$this->benchmark->mark('code_start');
			$i = 0;
			$fichier_log = root_path().'logs/export.log';
			
			$date = new DateTime();
			$date_now = $date->format('Y-m-d H:i:s');
			
			$date->sub(new DateInterval('P1D'));
			$date_hier = $date->format('Y-m-d H:i:s');
			
			$commandes = $this->commande->where('etat', 2)->where('created >=', $date_hier)->where('created <=', $date_now)->get();
			
			$oXml = new XMLWriter();
			$oXml->openUri(ROOTPATH.'echanges/export/web-commande-'.$date->format('Y-m-d').'.xml');
			$oXml->setIndent(true);
			$oXml->startDocument('1.0', 'UTF-8');
			$oXml->startElement('commandes');
			
			foreach ($commandes as $commande)
			{
				$facturation 	= $commande->facturation();
				$livraison	 	= $commande->livraison();
				$paiement		= $commande->paiement();
				$remise			= $commande->remise();
                
				$produits		= $commande->produits();
				$transporteur 	= $commande->transporteur();
				$client			= $commande->client();
                
                $code_remise = NULL;
                if ($code_remise->exists())
                {
                    $this->load->model('code_remise');
                    $code_remise = $this->code_remise->get_by_code($remise->code);    
                }
				
				if (($paiement->id_transaction != 0) && ($paiement->num_autorisation != ''))
				{
					$oXml->startElement('commande');
					$oXml->writeElement('id', $commande->id);
					$oXml->writeElement('reference', $commande->reference);
					$oXml->writeElement('date', $commande->created);
					$oXml->writeElement('montant', number_format($commande->montant, 2));
					$oXml->writeElement('mode_paiement', $paiement->type_carte);
					$oXml->writeElement('transporteur', $transporteur->nom);
					$oXml->writeElement('code_suivi', $commande->code_suivi);
					$oXml->writeElement('frais_port', number_format($commande->frais_port, 2));
					$oXml->writeElement('login_compte', $facturation->email);
					
					$oXml->writeElement('facturation_societe', $facturation->societe);
					$oXml->writeElement('facturation_nom', $facturation->nom);
					$oXml->writeElement('facturation_prenom', $facturation->prenom);
					$oXml->writeElement('facturation_adresse', $facturation->adresse);
					$oXml->writeElement('facturation_ville', $facturation->ville);
					$oXml->writeElement('facturation_code_postal', $facturation->code_postal);
					$oXml->writeElement('facturation_pays', $facturation->pays);
					$oXml->writeElement('facturation_email', $facturation->email);
					$oXml->writeElement('facturation_telephone', $facturation->telephone);
					$oXml->writeElement('facturation_mobile', $facturation->mobile);
					$oXml->writeElement('facturation_fax', $facturation->fax);
					
					$oXml->writeElement('livraison_societe', $livraison->societe);
					$oXml->writeElement('livraison_nom', $livraison->nom);
					$oXml->writeElement('livraison_prenom', $livraison->prenom);
					$oXml->writeElement('livraison_adresse', $livraison->adresse);
					$oXml->writeElement('livraison_ville', $livraison->ville);
					$oXml->writeElement('livraison_code_postal', $livraison->code_postal);
					$oXml->writeElement('livraison_pays', $livraison->pays);
					$oXml->writeElement('livraison_email', $livraison->email);
					$oXml->writeElement('livraison_telephone', $livraison->telephone);
					$oXml->writeElement('livraison_mobile', $livraison->mobile);
					$oXml->writeElement('livraison_fax', $livraison->fax);
					
					$oXml->startElement('articles');
					
					foreach ($produits as $produit)
					{
						$prix = $produit->prix;
                        if ( ! is_null ($code_remise) && $code_remise->exists())
                        {
                            if ($code_remise->type_reduction == 1)
                            {
                                $prix = $produit->prix*(1-$code_remise->reduction/100);
                            }
                            else
                            {
                         		$prix = $produit->prix-($code_remise->reduction/count($produits));
                            }    
                        }
                        
						$oXml->startElement('article');
						$oXml->writeElement('id', $produit->produit_id);
						$oXml->writeElement('quantite', $produit->quantite);
						$oXml->writeElement('prix', number_format($prix, 2));
						$oXml->endElement();
					}
					$oXml->endElement();
					$oXml->endElement();
					
					$i++;
				}
			}
			$oXml->endElement();
			$oXml->flush();
			
			$this->benchmark->mark('code_end');
			$temps_traitement = $this->benchmark->elapsed_time('code_start', 'code_end');
			
			//Log fichier
			$log_string = mdate('%Y-%m-%d %H:%i:%s').' | '.$temps_traitement.'s | '.$i.' commandes '."\n";
			
			write_file($fichier_log, $log_string, FOPEN_WRITE_CREATE);
			
			return;
		}
	}
}
/* End of file exporter.php */
/* Location: ./modules/boutique/controllers/exporter.php */