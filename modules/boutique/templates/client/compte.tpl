<div class="client compte">
	<div class="bloc_titre_h1"><h1>Mon compte</h1></div>
	<div class="inner_interne">
		<h2>{$client}, bienvenue sur votre espace client</h2>
		<br />
		{$lien.commandes}
	    {$lien.paniers}
	    {$lien.adresses}
	    {$lien.mot_de_passe}
	    {$lien.deconnexion}
	</div>
</div>