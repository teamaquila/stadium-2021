<div class="client compte commande">
	<div class="bloc_titre_h1"><h1>Ma commande {$commande.reference}</h1></div>
	<div class="inner_interne">
	<p class="welcome-client text-center">Bienvenue <span>{$prenom} {$nom}</span></p>
	
		<div class="row">
			<div class="col-md-3 menu-compte">
				{$lien.commandes}
			    {$lien.paniers}
			    {$lien.adresses}
			    {$lien.mot_de_passe}
			     <!-- <a href="https://www.stadium.fr/medias/pdf/2021/bon-commande.pdf" class="bt_compte lien-telechargement-inverse">Bon de commande</a> -->
			    {$lien.deconnexion}
	    	</div>
	    
		    <div class="col-md-9">
		    
		    	{if $commande.etat == 7}
					<a href="{$url.facture}" id="telechargement-facture" class="_blank btn lien-telechargement">Télécharger votre devis</a>
				{else}
					<a href="{$url.facture}" id="telechargement-facture" class="_blank btn lien-telechargement">Télécharger la facture</a>
				{/if}
				
				<div id="infos">
					<span class="date">Le {date_type($commande.date,1)}</span>
					<!--<span class="numero_commande">Commmande n°{$commande.reference}</span>-->
				</div>
				{if $commande.etat != 7}
					<div class="bloc_etats">
						{foreach $etats etat_default}
							{if $etat_default.id == $commande.etat}
								<div class="selected etat-{$etat_default.id}"> {$etat_default.id} {$etat_default.libelle}</div>
							{else}
								{if $etat_default.id < $commande.etat}
									<div class="precedent etat-{$etat_default.id}"> {$etat_default.id} {$etat_default.libelle}</div>
								{else}
									<div class="suivant etat-{$etat_default.id}">{$etat_default.id} {$etat_default.libelle}</div>
								{/if}
							{/if}
							
							
						{/foreach}
					</div>
				{/if}
				<div class="row">
					<div id="facturation" class="col-md-6"> 
						<div class="panel panel-default text-center">
							<p class="panel-heading">Adresse de facturation</p>
							{$facturation.prenom} {$facturation.nom}<br />
							{$facturation.adresse}<br />
							{$facturation.code_postal} {$facturation.ville}<br />
							{$facturation.pays}<br />
							{$facturation.email}<br />
							<span class="numero-telephone">{$facturation.telephone}</span>
						</div>
					</div>
					
					{if $livraison}
					<div id="livraison" class="col-md-6"> 
						<div class="panel panel-default text-center">
							<p class="panel-heading">Adresse de livraison</p>
							{$livraison.prenom} {$livraison.nom}<br />
							{$livraison.adresse}<br />
							{$livraison.code_postal} {$livraison.ville}<br />
							{$livraison.pays}<br />
							{$livraison.email}<br />
							<span class="numero-telephone">{$livraison.telephone}</span>
						</div>
					</div>
					{/if}
				</div>
				
				{$liste_produits}

				{if $livraison} 
					<div class="detail-frais-transport">
						<span id="transporteur">Transporteur : {$transporteur.nom}</span><br/>
						<span id="montant-frais_de_port">Total frais de port : {html_price($livraison.frais_port)}</span><br/>
					</div>					
				{/if}
					
				<div class="detail-montant">
					{if $commande.remise>0}
						<span id="montant_remise">Montant de la remise {$commande.remise}</span><br/>
					{/if}
					<span id="montant-total">Total de la commande : {$commande.montant_total}</span><br />
				</div>
				
				{if $commande.etat == 7}
					<div class="text-right"><a href="/boutique/paniers/finalise_commande/{$commande.id}" class="finaliser_commande btn btn-primary hvr-sweep-to-right sweep-black">Finaliser ce devis</a></div>
				{/if}
			</div>
		</div>
	</div>
</div>