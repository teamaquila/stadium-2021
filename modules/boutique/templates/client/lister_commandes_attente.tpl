<div class="client compte liste-commandes">
	<div class="bloc_titre_h1">
		<h1>Vos Devis en attente</h1>
	</div>
	<div class="inner_interne">
	<p class="welcome-client text-center">Bienvenue <span>{$prenom} {$nom}</span></p>
	
		<div class="row">
			<div class="col-md-3 menu-compte">
				{$lien.commandes}
				{$lien.paniers}
			    {$lien.adresses}
			    {$lien.mot_de_passe}
			    <!-- <a href="https://www.stadium.fr/medias/pdf/2021/bon-commande.pdf" class="bt_compte lien-telechargement-inverse">Bon de commande</a> -->
			    {$lien.deconnexion}
<!-- 			    <div id="bloc_renseignements"> -->
<!-- 				    <h4>Une question, <br> un doute ?</h4> -->
<!-- 			    	<span>Contactez-nous !</span> <br/> -->
<!-- 			    	<span class="tel">02.40.48.60.61</span> <br/> -->
<!-- 			    	<br/> -->
<!-- 			    	<span class="horaires">Du lundi au vendredi, de 8h à 18h</span> -->
<!-- 			    </div> -->
		    </div>

		    	
			<div class="col-md-9">
				{$derniere_commande}
			</div>
		</div>
	</div>
	
</div>