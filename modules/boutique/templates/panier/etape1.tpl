<table id="liste-panier" class="table table-striped table-bordered">
    <thead>
    	<tr>
    		<th class="th-1"></th>
   			<th class="th-2">{lang('description')}</th>
   			<th class="th-3">{lang('prix_unitaire')} {lang('ht')}</th>
   			<th class="th-4">{lang('quantite')}</th>
   			<th class="th-8">{lang('prix')} {lang('ht')}</th>
   			{if $colones_remise}
   				<th class="th-6">{lang('remise')}</th>
				<th class="th-5">{lang('prix')} {lang('ttc')}</th>
   				
   			{else}
   				<th class="th-5">{lang('prix')} {lang('ttc')}</th>
   			{/if}
    	</tr>
    </thead>
    <tbody>
    	{$ligne_panier}
    </tbody>
</table>