<tr class="info_panier" id="contenu_panier-{$panier.id}">
	<td class="client_panier" colspan="4">
		
		<div id="infos">
			<span class="date">Le {date_type($panier.date)}</span>
			<span class="numero_panier">Panier n°{$panier.reference}</span>
		</div>
	
		<table class="table liste-produits">
			<tr class="head">
				<th class="image c-th-0">Image</span>
				<th class="reference c-th-1">{lang(reference)}</th>
				<th class="libelle c-th-2">Nom du produit</th>
				<th class="prix c-th-3">{lang(prix)}</th>
				<th class="quantite c-th-4">{lang(quantite_demandee)}</th>
			</tr>
			{$liste_produits}	
		</table>
		<div class="detail-montant">
			<span class="montant-total">Total du panier ( sans frais de port ) : {$panier.montant_total}</span>
		</div>
		<a href="javascript:void(0)" class="commande_panier btn btn-info pull-right" data-panier_id="{$panier.id}">Commander ce panier</a>
	</td>
</tr>