<div id="panier" class="panel">
	<legend>{lang('vos_articles')}</legend>
	<div class="panel-body">
		<table id="liste-panier" class="table table-striped">
		    <thead>
		    	<tr>
   					<th class="th-2">{lang('description')}</th>
   					<th class="th-3">{lang('prix_unitaire')} {lang('ttc')}</th>
   					<th class="th-4">{lang('quantite')}</th>
   					<th class="th-5">{lang('prix')} {lang('ttc')}</th>
		    	</tr>
		    </thead>
		    <tbody>{$ligne_panier}</tbody>
		</table>
		
		<p class="sous-total montants-panier">
			<span class="libelle">{lang(sous_total)}</span>
			<span class="montant">{html_price($sous_total)}</span>
		</p>
		
		{if $remise && ($remise.montant > 0)}
		<p class="remise montants-panier">
			<span class="libelle">{lang('remise')}</span>
			<span class="montant">{html_price($remise.montant)}</span>
		</p>
		{/if}
		
		{if $livraison}
		<p class="frais-port montants-panier">
			<span class="libelle">{lang('frais_port')} <br/>({$livraison.transporteur})</span>
			
			{if $sur_devis}
				<span class="montant">Sur Devis</span>
			{else}
				<span class="montant">{html_price($livraison.frais_port)}</span>
			{/if}
		</p>
		{/if}
				
		{if $acompte.montant}
		<p class="acompte montants-panier">
			<span class="libelle">{lang('acompte')}</span>
			<span class="taux">{html_percentage($acompte.taux, 0)}</span>
			<span class="montant">{html_price($acompte.montant)}</span>
		</p>
		<p class="total montants-panier">
			<span class="libelle">{lang(total_a_payer)}</span>
			<span class="montant">{html_price($acompte.montant)}</span>
		</p>
		{else}
		<p class="total montants-panier">
			<span class="libelle">{lang('total')}</span>
			<span class="montant">{html_price($total)}</span>
		</p>
		{/if}
	</div>
</div>