<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="fr">
	<head>
		<title>Facture</title>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<style type="text/css">
		html, body {
			font-family: Helvetica;
			width: 100%;
			font-size: 11px;
		}
		body {
			margin-top: {$body_top};
		}
		h1, h2, h3, h4 {
			padding: 0;
			margin: 0;
		}
		
		#entete {
			width: 100%;
		}
		#entete #logo{
			width : 180px;
		}
		#entete #vendeur{
			padding-left: 20px;
			width: 280px;
		}
		#entete #facture{
			text-align: left;
			border: 1px solid black;
			border-radius: 8px;
			padding: 10px;
			width: 175px;
		}
		#coordonnees{
			margin-top : 35px;
			width: 100%;
		}
		#coordonnees td.bloc-bordure{
			border: 1px solid black;
			border-radius: 8px;
			padding: 10px;
			width: 48%;
		}
		#coordonnees th{
			font-size: 17px;
			padding-left: 10px;
		}
		#coordonnees td#separateur_coord{
			width : 4%;
		}
		
		#produits{
			border: 1px solid black;
			padding: 3px;
			border-radius: 8px;
			width: 100%;
		}
		
		#produits .td-2 {
			width: 10px;
		}
		#produits .td-5 {
			width: 40px;
		}
		
		#produits .td3,
		#produits .td4, 
		#produits .td5, 
		#produits .td6, 
		#produits .td7, 
		#produits .td8, 
		#produits .td9, 
		#produits .td10{
			text-align : right;
			padding-right : 5px;
		}
		#produits .td2{
			text-align : center;
		}
		#produits .produits_associes{
			color : grey;
			font-weight : normal;
		}
		#produits th{
			border : 1px solid black;
			background-color : #D5D5D5;
			text-align : center;
			padding : 5px 4px;
			font-weight : normal;
			font-size : 12px;
		
		}
		#produits .td1{
			width : 375px;
			
		}
		#produits .produits_associes .td1{
			width : 355px;
			padding-left: 20px;
		}
		.td-1_remise{
			width : 300px !important;
		}
		
		.mode-reglement {
			font-size: 12px;
		}
		
		#totaux{
			border-radius: 8px;
			border: 1px solid black;
			margin-left: 351px;
			width: 350px;
		}
		#totaux table{
			width: 100%;
		}
		#totaux td{
			padding: 5px 9px;
			text-align: right;
		}
		#totaux tr#total_commande{
			font-weight : bold;
			font-size : 18px;
		}
		
		#footer, #header {
			position: fixed;
			left: 0px;
			right: 0px;
		}
		
		#footer{
			bottom: 0px;
			height: 50px;
			text-align: center;
			padding-top: 10px;
			border-top: 1px solid black;
		}
		#header{
			top: 0px;
			font-size : 12px;
		}
		#pagination{
			text-align : right;
			font-style: italic;
		}
		#pagination .curr_page:before{
			content: counter(page);
		}
		
		hr{
			page-break-after: always;
			border: 0;
		}
		</style>
	</head>
	
	<body>
		<div id="header">
			<table id="entete">
				<tr>
					<td id="td_logo">
						{$logo}
					</td>
					<td id="vendeur">
						<h2>{$vendeur.societe}</h2>
						{$vendeur.adresse}<br/>
						{$vendeur.code_postal} {$vendeur.ville}<br/>
						{$vendeur.pays}<br />
						Téléphone : {$vendeur.telephone}
						{if $vendeur.fax}<br />Fax : {$vendeur.fax}{/if}<br/>
						{$vendeur.infos}
					</td>
					<td id="facture">
						Le {datetime_type($commande.date)}<br />
						<br />
						<strong>Devis<br /><br />
					 	<strong>Référence Devis</strong><br />{$commande.reference}
					</td>
				</tr>
			</table>
			<table id="coordonnees">
				<tr>
					{if $option_livraison}
						<th>Livraison</th>
					{else}
						<th>&nbsp;</th>
					{/if}
					<th></th>
					<th>Facturation</th>
				</tr>
				<tr>
					{if $option_livraison}
					<td id="livraison" class="bloc-bordure">
						{$livraison.societe}<br />
						{$livraison.prenom} {$livraison.nom}<br/>
						{$livraison.adresse}<br/>
						{if $livraison.adresse2}{$livraison.adresse2}<br/>{/if}
						{$livraison.code_postal} {$livraison.ville}<br/>
						{$livraison.pays}<br />
						Tél : {$livraison.telephone}
						{if $livraison.mobile}<br />Mobile : {$livraison.mobile}{/if}
						{if $livraison.fax}<br />Fax : {$livraison.fax}{/if}
					</td>
					{else}
					<td>&nbsp;</td>
					{/if}
					<td id="separateur_coord"></td>
					<td id="facturation" class="bloc-bordure">
						{$facturation.societe}<br />
						{$facturation.prenom} {$facturation.nom}<br />
						{$facturation.adresse}<br />
						{if $facturation.adresse2}{$facturation.adresse2}<br/>{/if}
						{$facturation.code_postal} {$facturation.ville}<br />
						{$facturation.pays}<br />
						{$facturation.email}<br />
						Tél : {$facturation.telephone} 
						{if $facturation.mobile}<br />Mobile : {$facturation.mobile}{/if}
						{if $facturation.fax}<br />Fax : {$facturation.fax}{/if}
					</td>
				</tr>
			</table>
			<!-- {$cur_page} / {$count_page} -->
			<div id="pagination"> Page <span class="curr_page"></span> / {$total_page}</div>
		</div>
		<div id="footer">
			{$mentions_legales}
		</div>
		<div id="contenu_page">
			<table id="produits">
				<tr>
					{if $commande.existe_remise}
					<th class="td-1 td-1_remise">Désignation</th>
					{else}
					<th class="td-1">Désignation</th>
					{/if}
					<th class="td-2">Qté</th>
					<th class="td-3">Prix unit. <br/>HT</th>
					<th class="td-4">Montant HT</th>
					<th class="td-5">TVA</th>
					<th class="td-6">Montant TTC</th>
					{if $commande.existe_remise}
						<th class="td-9">Remise HT</th>
						<th class="td-10">Montant remisé TTC</th>
					{/if}
				</tr>
				{$liste_produits}
			</table>
			<br />
			{if $paiement.transaction_date}
			<span class="mode-reglement">Réglée par {$paiement.type_carte} le {datetime_type($paiement.transaction_date)}</span>
			{/if}
			<br />
			{if $saut_page}
			<hr/>
			{/if}
			
		
			<div id="totaux">
				<table>
					{if $commande.existe_remise}
					<tr>
						<td>Montant HT</td>
						<td>{$commande.montant_ht}</td>
					</tr>
					{/if}
					
					{$commande.montant_remise}
					<tr>
						<td>Total net HT</td>
						<td>{$commande.montant_net_ht}</td>
					</tr>
					{$ligne_tva}
					
					{if $commande.existe_remise_montant}
					<tr>
						<td>Total non remisé TTC</td>
						<td>{$commande.total_non_remise_ttc}</td>
					</tr>
					<tr>
						<td>Montant de la remise TTC</td>
						<td>{$commande.remise_montant_ttc}</td>
					</tr>
					
					{/if}
					
					<tr id="total_commande">
						<td>Total en euros</td>
						<td>{$commande.montant_ttc}</td>
					</tr>
				</table>
			</div>
			
			{if $commande.commentaire}
				<div id="commentaire"><strong>Commentaire sur la commande :</strong> <br >{$commande.commentaire}</div>
			{/if}
			
			{if $commande.transporteur}
				<div id="commentaire"><strong>Mode de livraison :</strong> {$commande.transporteur}</div>
			{/if}
		</div>
		
		
	</body>
</html>