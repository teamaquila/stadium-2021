
	Bonjour,<br /><br />

	<h2>{$presentation.nb_relances} devis ont été relancé(s) ce jour ci : </h2><br/>
	<h3>Liste des devis relancés : </h3>
	
	<table>
		 <tr>
		    <th>Référence devis</th>
		    <th>Code client</th>
		    <th>Nom client</th>
		  </tr>
		{foreach $presentation.devis_relancees relance}
		  <tr>
		    <td>{$relance.reference}</td>
		    <td>{$relance.code}</td>
		    <td>{$relance.nom}</td>
		  </tr>
		{/foreach}
	</table>



