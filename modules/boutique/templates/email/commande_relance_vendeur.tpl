
	Bonjour,<br /><br />

	<h2>{$presentation.nb_relances} commandes ont été relancées aujourd'hui : </h2><br/>
	<h3>Liste des commandes relancées : </h3>
	
	<table>
		 <tr>
		    <th>Référence commande</th>
		    <th>Code client</th>
		    <th>Nom client</th>
		  </tr>
		{foreach $presentation.commandes_relancees relance}
		  <tr>
		    <td>{$relance.reference}</td>
		    <td>{$relance.code}</td>
		    <td>{$relance.nom}</td>
		  </tr>
		{/foreach}
	</table>



