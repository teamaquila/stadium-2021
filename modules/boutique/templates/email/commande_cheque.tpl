<p style="margin-top: 0px;">
	{lang('bonjour')}&nbsp;{$presentation.prenom}&nbsp;{$presentation.nom},<br /><br />
	Nous avons le plaisir de vous confirmer la réception de votre commande {$presentation.reference}&nbsp;{lang('du')}&nbsp;{$presentation.date_commande}<br /><br />
	
	Afin de finaliser votre commande, merci de suivre les instructions suivantes : <br/>
		<ul>
		
	    <li> Imprimer votre bon commande ci joint (ou recopier sur papier libre votre nom, prénom, adresse postale, e-mail, N° de commande et N° de téléphone).</li>
	    <li> Etablir votre chèque à l’ordre de Stadium d'un montant de {$presentation.montant}</li>
	    <li> Envoyer une photocopie de votre pièce d’identité (seules les pièces d’identité suivantes sont acceptées : 
	    		Carte Nationale d’Identité, Permis de conduire, Passeport, Carte de résident); 
	    		Deux pièces d’identités sont exigées pour tout paiement supérieur à 200 euros TTC.</li>
	    <li>
		    Envoyer votre confirmation de commande et votre chèque à l’adresse suivante : <br/>
		
		    Stadium <br/>
			16 rue des Granits<br/>
			44100 Nantes <br/>
		</li>
    
	</ul> <br/>
	<b>La commande sera traitée à réception du chèque et après la validation de celui-ci.</b> <br/><br/>
</p>

<table id="adresses">
	<tr>
		<th style="text-align: left;">{lang('adresse_livraison')}</th>
		<th style="text-align: left;">{lang('adresse_facturation')}</th>
	</tr>
	<tr>
		<td style="width: 230px;">
			{$livraison.societe}
			{$livraison.prenom}&nbsp;{$livraison.nom}<br />
			{$livraison.adresse}<br />
			{$livraison.code_postal}&nbsp;-&nbsp;{$livraison.ville}<br />
			{$livraison.pays}
			{$livraison.telephone}
		</td>
		<td style="width: 230px;">
			{$facturation.societe}
			{$facturation.prenom}&nbsp;{$facturation.nom}<br />
			{$facturation.adresse}<br />
			{$facturation.code_postal}&nbsp;-&nbsp;{$facturation.ville}<br />
			{$facturation.pays}
			{$facturation.telephone}
		</td>
	</tr>
</table>

<p>
	<strong>Articles :</strong><br />
	{$articles}
</p>

<p>
	Nous vous recontacterons dans les plus bref délais pour finaliser votre commande.<br />
	<br />
	{lang('remerciement')}&nbsp;{$lien_site}.<br />
	<br />
	{lang('equipe')}&nbsp;{$nom_equipe}
</p>