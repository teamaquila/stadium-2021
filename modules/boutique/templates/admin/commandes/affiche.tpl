<form action="" method="post" class="design" id="detail-commande">
	<fieldset>
		<legend>{lang('informations_commande')}</legend>
		<p class="ligne-champ">
			<label for="">{lang('reference')}</label>
			{$info_commande.reference}
		</p>
			
		<p class="ligne-champ">
			<label for="">{lang('etat')}</label>
			{$info_commande.etat}
			<a href="{$url.historique}" title="{lang('historique')}" class="open-dialogbox">
				<span class=" glyphicon glyphicon-list-alt"></span>
			</a>
			{if $info_commande.etat_id == 4}
			<a href="{$info_commande.suivi}" target="_blank">Suivre le colis</a>
			{/if}
		</p>
		
		<p class="ligne-champ">
			<label for="">{lang('date_creation')}</label>
			{mysql_to_datetime($info_commande.date)}
		</p>
		
		<p class="ligne-champ">
			<label for="">{lang('client')}</label>
			<a href="mailto:{$info_commande.email}">{$info_commande.prenom} {$info_commande.nom}</a><br />
			Tél : {$info_commande.telephone}
		</p>
		
		{if $info_commande.nom_transporteur}
		<p class="ligne-champ">
            <label for="">Mode de livraison</label>
            {$info_commande.nom_transporteur} 
            
            {if $info_commande.etiquetage_colissimo}
            	<a href="/boutique/commandes/etiquetage_colissimo/{$info_commande.id}" style="background: rgb(51, 110, 156) none repeat scroll 0% 0%; color: white; padding: 3px 10px; display: inline-block; margin: 1px 5px;">Télécharger l'étiquette</a>
            {/if}
             {if $info_commande.etiquetage_chronopost}
             	<a href="/boutique/commandes/etiquetage_chronopost/{$info_commande.id}" style="background: rgb(51, 110, 156) none repeat scroll 0% 0%; color: white; padding: 3px 10px; display: inline-block; margin: 1px 5px;">Télécharger l'étiquette</a>
             {/if}
        </p>
        {/if}
		
		<p class="ligne-champ">
			<label for="">Commentaire</label>
			{$info_commande.commentaire_commande}
		</p>
	</fieldset>
	
	<fieldset>
		<legend>Produits commandés</legend>
		<div>
			{$liste_produits}
		</div>
		
		<div class="ligne-champ">
			<label for="">{lang('frais_port')}</label>
			<div class="align-right">{html_price($info_commande.frais_port)}</div>
			<br class="clear_both"/>
		</div>
		
		{if ! is_null($info_commande.remise)}
		<div class="ligne-champ">
			<label for="">{lang('remise')} ({$info_commande.remise.libelle})</label>
			<div class="align-right">{html_price($info_commande.remise.montant)}</div>
			<br class="clear_both"/>
		</div>
		{/if}
		
		<div class="ligne-champ">
			<label for="">{lang('montant')}</label>
			<div class="align-right"><strong>{html_price($info_commande.montant)}</strong></div>
			<br class="clear_both"/>
		</div>
	</fieldset>
	
	<fieldset>
		<legend>{lang('informations_paiement')}</legend>
		<div class="ligne-champ">
			<label for="">{lang('hors_taxe')}</label>
			{$info_commande.hors_taxe}
		</div>
		<div class="ligne-champ">
			<label for="">{lang('type_carte')}</label>
			{$info_paiement.type_carte}
		</div>
		
		<div class="ligne-champ">
			<label for="">{lang('code_pays_banque')}</label>
			{$info_paiement.code_pays_banque}
		</div>
			
		<div class="ligne-champ">
			<label for="">{lang('code_pays_client')}</label>
			{$info_paiement.code_pays_client}
		</div>
	</fieldset>
	
	<table class="table" id="coordonnees_commande">
		<tr>
			<td>
				<fieldset>
					<legend>{lang('adresse_facturation')}</legend>
					
					{$facturation.prenom} {$facturation.nom} <br />
					{if $facturation.societe}{$facturation.societe}<br />{/if}
					{$facturation.adresse} <br />
					{$facturation.code_postal} {$facturation.ville} <br />
					{$facturation.pays} <br />
					{$facturation.email}<br />
					{$facturation.telephone}
				</fieldset>
			</td>
			{if $livraison.nom}
			<td>
				<fieldset>
					<legend>{lang('adresse_livraison')}</legend>
					
					{$livraison.prenom} {$livraison.nom} <br />
					{if $livraison.societe}{$livraison.societe}<br />{/if}
					{$livraison.adresse} <br />
					{$livraison.code_postal} {$livraison.ville} <br />
					{$livraison.pays}<br />
					{$livraison.email}<br />
					{$livraison.telephone}
				</fieldset>
			</td>
			{/if}
		</tr>
	</table>
	
	{if $complements_livraison}
	<fieldset>
		<legend>{lang('complements_livraison')}</legend>
		{foreach $complements_livraison, libelle, valeur}
			<div class="ligne-champ">
				<label for="">{$libelle}</label>
				{$valeur}
			</div>
		{/foreach}
	</fieldset>
	<br />
	{/if}

	<a class="button exporter btn btn-info" href="{$url.import_csv}">Exporter au format CSV</a>
	
	{if ($info_commande.etat_id == 2) or ($info_commande.etat_id == 3) or ($info_commande.etat_id == 4) or ($info_commande.etat_id == 11) or ($info_commande.etat_id == 9) or ($info_commande.etat_id == 8) or ($info_commande.etat_id == 10)}
	<a class="button imprimer btn btn-success" href="{$url.import_pdf}">Imprimer la facture</a>
	{/if}
</form>