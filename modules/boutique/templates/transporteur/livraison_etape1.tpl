<div id="livraison" class="ligne-tranporteur">
	<div id="zones-livraison">
		{$zones_livraison}
	</div>
		
	{$transporteurs}

	<div id="frais-port" class="text-right">
<!-- 		<span id="franco">{$franco}</span><br /> -->
		<span class="libelle">{lang('frais_de_port')}</span>
		{if $sur_devis}
			<span class="montant">Sur Devis</span>
		{else}
			<span class="montant">{$frais_port}</span>
		{/if}	
	</div>
</div>