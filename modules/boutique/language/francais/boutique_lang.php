<?php
$lang['section_boutique_clients']		= "Clients";
$lang['section_boutique_commandes']		= "Commandes";
$lang['section_boutique_statistiques']	= "Statistiques";
$lang['gestion_remises']		= 'Remises';
$lang['supprimer']				= "Supprimer";
$lang['modifier']				= "Modifier";
$lang['export_csv']				= "Export csv";
$lang['tout_cocher']			= "Tout cocher";
$lang['modifier']				= "Tout décocher";
$lang['pour_selection']			= "Selection";
$lang['Action']					= "Action";
$lang['annuler']				= "Annuler";
$lang['Fermer']					= "Fermer";
$lang['titre']					= "Titre";
$lang['libelle']				= "Libellé";
$lang['visible']				= "Visible";
$lang['referencement']			= "Référencement";
$lang['file_upload_label']		= "Téléchargement";
$lang['fichiers_associes']		= "Fichiers associés";
$lang['aucun_produit'] 			= "Aucun produit";
$lang['seuil_min']				= "Seuil Min";
$lang['seuil_max']				= "Seuil Max";
$lang['prix'] 					= "Prix";
$lang['tarifs']					= "Tarifs";
$lang['ajouter_tarif'] 			= "Ajouter un tarif";
$lang['message_suppression_client'] = "Voulez-vous supprimer ce client ?";					
$lang['message_suppression_client_effectuee']  = "Client supprimé";
$lang['message_suppression_client_impossible'] = "Suppression du client impossible";
$lang['suivi_colis']			= "Suivi du colis";
$lang['pas_suivi']				= "Cette expédition n'est pas suivie";
$lang['suivi']					= "Cette expédition possède un numéro de suivi";
$lang['code_suivi']				= "Veuillez saisir le code suivi";
$lang['exporter_commandes']				= "Exporter les commandes (avec produits)";
$lang['exporter_commandes_simplifie']	= "Exporter les commandes (sans produits)";
$lang['commande']				= 'commande';
$lang['Commande']				= 'Commande';
$lang['exporter_clients']		= "Exporter les clients";
$lang['vider_panier']			= "Vider mon panier";
$lang['relance']				= "Relance";
$lang['couleur_dominante_facture'] = "Couleur dominante pour la facture";


$lang['vos_articles']= "Vos articles";
$lang['mode_de_paiement'] = "Mode de paiement";
$lang['terminer_ma_commande'] = "Valider ma commande";
$lang['erreur_validation_cgv'] = "Veuillez cocher les CGV ci-dessous pour finaliser votre commande";
$lang['mobile_relais_obligatoire'] = "Veuillez saisir un numéro de mobile valide pour la livraison en point relais.";

$lang['date_livraison']             = 'Date de livraison ';

/**
 * Filtres
 */
$lang['rechercher_client'] 		= "Rechercher un client";
$lang['rechercher_commande'] 	= "Rechercher une commande";


/**
 * Fiche client
 */
$lang['nomduproduit'] 			= "Nom du produit";
$lang['reference'] 				= "Référence";
$lang['montant'] 				= "Montant";
$lang['datecreation'] 			= "Commandé le";
$lang['apercu'] 				= "Aperçu";
$lang['etat'] 					= "Etat";
$lang['client'] 				= "Client";
$lang['type_compte']			= "Type de compte";
$lang['aucune_commande'] 		= "Aucune commande";
$lang['aucun_changement_etat'] 	= "Aucun changement d'état";
$lang['historique'] 			= "Historique";

$lang['recapitulatif_commande']	= "Récapitulatif de la commande";
$lang['informations_commande'] 	= "Informations sur la commande";
$lang['informations_produits'] 	= "Informations sur les produits";
$lang['reference']				= "Référence";
$lang['etat'] 					= "Etat";
$lang['date_creation'] 			= "Date de création";
$lang['date_modification']  	= "Dernière modification";
$lang['montant'] 				= "Montant total";
$lang['frais_port'] 			= "Frais de Port";
$lang['mode_livraison'] 		= "Mode de livraison";
$lang['transporteur'] 			= "Transporteur";
$lang['date'] 					= "Date";

$lang['informations_paiement'] 	= "Informations sur le paiement";
$lang['code_pays_banque'] 		= "Code pays de la banque";
$lang['code_pays_client'] 		= "Code pays du client";
$lang['type_carte'] 			= "Type de carte";
$lang['hors_taxe'] 				= "Hors taxe";

/* Devise */
$lang['devise'] 				= "€";
$lang['frais_port_gratuit']		= "Frais de port offerts ";
$lang['a_partir_de']			= "&Agrave; partir de ";

$lang['email'] 					= "E-Mail";
$lang['nom'] 					= "Nom";
$lang['prenom'] 				= "Prénom";
$lang['commandes']				= "Commandes";

/**
 * Formulaire des options
 */
$lang['version_id'] 				= 'Version de la langue';
$lang['paybox'] 					= 'Paybox';
$lang['pbx_site'] 					= 'Numéro de site';
$lang['pbx_rang'] 					= 'Numéro du rang';
$lang['pbx_identifiant'] 			= 'Identifiant Paybox';
$lang['pbx_mode'] 					= 'Mode';
$lang['pbx_devise'] 				= 'Devise';
$lang['pbx_langue'] 				= 'Langue';
$lang['test'] 						= 'Activer le mode TEST de Paybox';
$lang['transaction_log'] 			= 'Générer un log de transaction';
$lang['vendeur'] 					= 'Vendeur';
$lang['vendeur_email'] 				= 'Email';
$lang['vendeur_nom'] 				= 'Nom';
$lang['vendeur_adresse'] 			= 'Adresse';
$lang['vendeur_code_postal'] 		= 'Code postal';
$lang['vendeur_ville'] 				= 'Ville';
$lang['vendeur_telephone'] 			= 'Téléphone';
$lang['vendeur_fax'] 				= 'Fax';
$lang['vendeur_infos'] 				= 'Informations complémentaires';
$lang['vendeur_copie1'] 			= 'Email en copie 1';
$lang['vendeur_copie2'] 			= 'Email en copie 2';
$lang['emails'] 					= 'E-mails';
$lang['sujet_inscription'] 			= 'Sujet pour l\'inscription';
$lang['sujet_nouveau_mdp'] 			= 'Sujet pour le nouveau mot de passe';
$lang['sujet_commande_payee'] 		= 'Sujet pour le paiement de la commande';
$lang['sujet_commande_expediee'] 	= 'Sujet pour l\'expédition de la commande';
$lang['sujet_commande_annulee'] 	= 'Sujet pour l\'annulation de la commande';
$lang['sujet_commande_relance'] 	= 'Sujet pour la relance de la commande';
$lang['commandes'] 					= 'Commandes';
$lang['prefixe_reference'] 			= 'Préfixe du numéro de commande';
$lang['type_seuil_frais_port'] 		= 'Type seuil frais de port';
$lang['tva_livraison'] 				= 'Taux de TVA de la livraison';
$lang['commentaire_produit'] 		= 'Autoriser les commentaires pour les produits';
$lang['commentaire_commande'] 		= 'Autoriser un commentaire pour la commande';
$lang['activer_livraison'] 			= 'Activer la livraison';
$lang['activer_remises'] 			= 'Activer les remises';
$lang['activer_cgv'] 				= 'Validation des CGV obligatoire';
$lang['lien_cgv']					= 'Lien vers les CGV';
$lang['quantite']					= 'Quantité';
$lang['quantite_demandee']			= 'Quantité souhaitée';
$lang['quantite_dispo']				= 'Quantité disponible';
$lang['poids']						= 'Poids';
$lang['prix_total']					= 'Montant Total';
$lang['mentions_facture']			= 'Mentions sur la facture';

/**
 * Gestion des remises
 */
$lang['code_remise'] = 'Code remise';
$lang['date_debut'] = 'Début';
$lang['date_fin'] = 'Fin';
$lang['usage_unique'] = 'Usage unique par client';
$lang['aucune_remise'] = 'Aucune remise';
$lang['ajouter_remise'] = 'Ajouter une remise';
$lang['type_reduction'] = 'Type de réduction';
$lang['reduction'] = 'Réduction';
$lang['reduction_pourcentage'] = 'En pourcentage';
$lang['reduction_valeur'] = 'En valeur';
$lang['duree_validite'] = 'Durée de validité';
$lang['quota'] = 'Quota';
$lang['produits_offerts'] 	= 'Produits offerts';
$lang['selection_produits_offerts'] = "Sélection des produits offerts";
$lang['selectionner_produits_offerts'] = "Sélectionner les produits offerts";
$lang['produits_restriction'] = "Restriction à certains produits";
$lang['selectionner_produits_concernes'] = "Sélectionner les produits concernés";
$lang['montant_minimal'] 	= "Montant minimal du panier";
$lang['frais_port_gratuit'] = "Frais de port offerts";
$lang['duree_illimitee']	= "Durée illimitée";
$lang['illimite']			= "Illimité";

//Remises - log
$lang['log_ajout_remise'] 			= "Ajout de la remise";
$lang['log_modification_remise'] 	= "Mise à jour de la remise";
$lang['log_suppression_remise'] 	= "Suppression de la remise";

$lang['recupere_articles']			= "Récupérez les articles de votre commande dans votre panier";

$lang['ttc']		= " TTC";
$lang['ht']		= " HT";

$lang['etat_erreur']	= "Erreur";
$lang['etat_enregistre']= "Enregistrée";
$lang['etat_payee']		= "Payée";
$lang['etat_preparee']	= "Préparée";
$lang['etat_expediee']	= "Expédiée";
$lang['etat_annulee']	= "Annulée";
$lang['etat_retournee']	= "Refusée";
$lang['etat_attente_paiement']	= "En attente de paiement";
$lang['etat_devis_enregistre']	= "Devis à finaliser";
$lang['etat_paiement_compte']	= "En compte";
$lang['etat_sur_devis_frais_port']	= "Sur devis pour Frais de port";
$lang['etat_paiement_cheque']	= "En attente de réception du chèque";