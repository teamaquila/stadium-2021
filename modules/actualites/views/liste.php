<?php
$liste = NULL;

//Liste des actualités
if ( ! is_null($actualites))
{
	$i = 1;
	foreach ($actualites as $actualite)
	{
		//Catégorie
		$lien_categorie = NULL;
		$categorie_id = NULL;
		$categories_actualite = $actualite->categories_associees();
		
		if ($categories_actualite->exists())
		{
			foreach ($categories_actualite as $categorie_actualite)
			{
				$lien_categorie .= anchor(construire_url(array(
					'section' 	=> $section->segment_url(),
					'categorie'	=> $categorie_actualite->url()
				)), $categorie_actualite->libelle, 'class="btn btn-primary"').nbs();
				$categorie_id = $categorie_actualite->id;
			}
		}
		
		$data = array(
			'titre'		=> $actualite->titre,
			'image' 	=> html_media($actualite->medias_associes(), array('taille' => $module->taille_image_liste, 'libelle'	=> $actualite->titre)),
			'contenu'	=> truncate($actualite->contenu, $module->longueur_texte_liste),
			'date'		=> date_type($actualite->date_publication, $module->type_date_liste),
			'important' => ($actualite->important) ? 'important' : NULL,
			'id'		=> $actualite->id,
			'url'		=> $actualite->url(),
			'categorie'	=> $lien_categorie,
			'categorie_id'	=> $categorie_id,
			'index'		=> $i,
		    'section_id'=> $section->id
		);
		$liste .= $this->dwootemplate->get(tpl_path('actualites/item_actualite.tpl'), $data);
		$i++;
	}
}
else
{
	$liste = '<div class="liste-vide">'.lang('actualite_vide').'</div>';
}

//Titre avec catégorie
if (isset($categorie_parent))
{
	$_categorie = array(
		'libelle'	=> $categorie_parent->libelle,
		'id'		=> $categorie_parent->id,
		'contenu'	=> $categorie_parent->contenu,
		'url' => array('retour' => $section->url())
	);
}

//Liste des catégories pour toutes les actualités
$liste_categories = NULL;

if (isset($categories) && ($categories->result_count() > 0))
{
	$liste_categories = '<ul class="liste-categories">';
	
	$liste_categories .= (isset($categorie_parent)) ? '<li>' : '<li class="active">';
	$liste_categories .= anchor(construire_url(array('section' => $section->segment_url())), 'Toutes les catégories').'</li>';
	
	foreach ($categories as $categorie)
	{
		if ($categorie->visible)
		{
			$liste_categories .= (isset($categorie_parent) && ($categorie_parent->id == $categorie->id)) ? '<li class="active">' : '<li>';
			$liste_categories .= anchor(construire_url(array(
				'section' 	=> $section->segment_url(),
				'categorie'	=> $categorie->url()
			)), $categorie->libelle).'</li>';
		}
	}
	$liste_categories .= '</ul>';
}

$data = array(
	'titre_page' 	=> ( ! empty($section->titre)) ? $section->titre : $section->libelle,
	'section_id'	=> $section->id,
	'pagination'	=> $pagination,
	'categories'	=> $liste_categories,
	'categorie'		=> (isset($_categorie)) ? $_categorie : NULL,
	'actualites'	=> $liste,
	'url' => array(
		'newsletter' => '/abonnement-newsletter.html'
	),
	'image_section'	=> $image_section,
	'fil_ariane'	=> $fil_ariane
);
$this->dwootemplate->output(tpl_path('actualites/liste.tpl'), $data);