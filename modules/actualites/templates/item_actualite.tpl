{if $section_id == 35}
	<div class="actualites{$important} {if $categorie}categorie-actu-{$categorie_id}{/if}" id="actualite-{$id}">
<!-- 		<a href="{$url}" class="bloc-image">{$image}</a> -->
		<div class="contenu">
			{$contenu}					
		</div>
		<span class="auteur">{$titre}</span>
<!-- 		<a href="{$url}" class="auteur">{$titre}</a> -->
	</div>
{else}
	<div class="actualites{$important} {if $categorie}categorie-actu-{$categorie_id}{/if}" id="actualite-{$id}">
		{if $image}
			<a href="{$url}" class="bloc-image text-center">{$image}</a>
		{else}
			<a href="{$url}" class="bloc-image text-center"><img src="/assets/img/img-non-disponible-liste-actu.jpg" alt=""></a>
		
		{/if}
		<div class="contenu">
			<h2>
				<a href="{$url}">{$titre}</a>
			</h2>
			<p>{$contenu}</p>
			<p class="text-right">
				<a href="{$url}" class="bouton">
					{lang('actualite_savoir_plus')}
				</a>	
			</p>			
		</div>
	</div>
{/if}