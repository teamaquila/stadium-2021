<div class="actualite module-actualite module-{$module_id} {$important}" id="actualite-{$id}">
	{$fil_ariane}

	<div class="bloc_titre_h1">
		{if $module_id == 1}
			<h1>Témoignage de {$titre}</h1>
		{else}
			<h1>{$titre}</h1>
		{/if}
	</div>

	<div class="inner_interne">		
		<div class="contenu">
			{$contenu}
		</div>
		<br />
		<a href="{$url.retour}" class="btn btn-retour">
			<span class="glyphicon glyphicon-chevron-left"></span>
			{lang('actualite_retour_liste')}
		</a>
	
	</div>
</div>