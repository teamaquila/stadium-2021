<div id="section-{$section_id}" class="module-actualite">
	{$fil_ariane}

	<div class="bloc_titre_h1">
		<h1>{$titre_page}</h1>
	</div>		

	
	<div class="inner_interne">
<!-- 		{if $categorie.libelle} -->
<!-- 			<span class="categorie_page">{$categorie.libelle}</span> -->
<!-- 		{/if} -->
<!-- 		{if $categories} -->
<!-- 			{$categories} -->
<!-- 		{/if} -->
		
		<div id="liste-actualites" class="liste">
			{$actualites}
		</div>
		
		<div class="text-center">{$pagination}</div>
	</div>
</div>