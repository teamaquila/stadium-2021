<?php
$liste = NULL;

$utilisateur = $this->clients->est_connecte();

if ( ! is_null($produits) && ($produits->result_count() > 0))
{
	foreach ($produits as $produit)
	{
		$module = $this->module_catalogue->get_by_id($produit->module_catalogue_id);
		$medias = $produit->medias_associes();
		
		$data = array(
			'id'			=> $produit->id,
			'libelle'		=> $produit->libelle,
			'reference'		=> $produit->reference,
			//'image' 		=> html_media($produit->medias_associes(), array('taille' => $module->taille_image_liste)),
			'description'	=> ($produit->resume) ? $produit->resume : truncate($produit->description, $module->longueur_texte_liste),
			
			'nouveau'		=> $produit->nouveau,
			'commandable'	=> !$produit->non_commandable,
		    'libelle2'		=> $produit->libelle2,
			'url'			=> $produit->url(),
			'panier'		=> ($module->afficher_panier_liste) ? modules::run('boutique/boutique/bouton_ajouter', $produit) : NULL,
			'poids' 		=> array(
				'valeur' => $produit->poids,
				'unite'  => $module->unite_poids
			)
		);
		
		if ($medias->exists())
		{
			$data['image'] = html_media($medias, array('taille' => $module->taille_image_liste, 'libelle' => $produit->libelle));
		}
		else
		{
			$data['image'] = '<img src="/assets/img/img-non-disponible.jpg" alt="Photo indisponible '.$produit->libelle.'" />';
		}
		
		//Si les enfants du produits sont des articles collections composés d'autres articles on calcule le prix en fonction des enfants
		$trouve_compose = FALSE;
		$tab_tarifs_compose = array();
		foreach ($produit->produits_enfants() as $enfant)
		{
		    $produits_composes = $this->db->query("
				SELECT t_produits.id
				FROM t_produits
				INNER JOIN t_produits_produits ON t_produits.id = t_produits_produits.related_produit_id
				WHERE t_produits_produits.compose = 1
				AND t_produits_produits.produit_id = ".$enfant->id."
			");
		    if ($produits_composes->num_rows() != 0)
		    {
		        $trouve_compose = TRUE;
		        
		        $tab_tarifs_compose[$enfant->id]	= array(
		            'prix'	=> 0,
		            'prix_sans_remise'	=> 0
		        );
		        foreach ($produits_composes->result() as $row)
		        {
		            $prod_compo = new Produit();
		            $prod_compo->get_by_id($row->id);
		            $tab_tarifs_compose[$enfant->id]['prix'] +=  $prod_compo->tarif($utilisateur)['prix'];
		            $tab_tarifs_compose[$enfant->id]['prix_sans_remise'] +=  $prod_compo->tarif($utilisateur)['prix'];
		        }
		    }
		}
		
		if ($trouve_compose)
		{
		    $tarif_precedent = 0;
		    
		    foreach ($tab_tarifs_compose as $enfant_id => $tarif_enfant)
		    {
		        if (($tarif_precedent == 0 ) or ($tarif_enfant['prix'] < $tarif_precedent))
		        {
		            $data['tarif']['prix']	= html_price($tarif_enfant['prix']);
		            $data['tarif']['prix_sans_remise']	= html_price($tarif_enfant['prix_sans_remise']);
		            $tarif_precedent = $tarif_enfant['prix'];
		        }
		    }
		    
		}
		else
		{
		    //Tarif
		    $tarif = $produit->tarif($utilisateur);
		    
		    if ($tarif !== FALSE)
		    {
		        //Tarif unique
		        $data['tarif'] = array(
		            'prefixe'		=> ($tarif['unique']) ? NULL : $this->lang->line('a_partir_de'),
		            'suffixe'		=> $this->lang->line('devise'),
		            'prix'			=> html_price($tarif['prix']),
		            'prix_ht'			=> html_price($tarif['prix']),
		            'prix_sans_remise' => html_price($tarif['prix_sans_remise']),
		            'taux_remise'	=> html_percentage($tarif['taux_remise'], 0),
		            'tva'			=> html_percentage($tarif['tva'], 2),
		            'taxe'			=> $this->lang->line('prix_taxe_'.$tarif['taxe'])
		        );
		        
		        //Grille tarifaire
		        if (is_array($tarif['grille']))
		        {
		            $tarifs = '<ul class="liste-tarifs">';
		            foreach ($tarif['grille'] as $grille)
		            {
		                $tarifs .= '<li>';
		                $tarifs .= '<span class="minimum">'.$grille['minimum'].'</span>';
		                $tarifs .= '<span class="maximum">'.$grille['maximum'].'</span>';
		                $tarifs .= '<span class="prix">'.html_price($grille['prix']).nbs();
		                $tarifs .= $this->lang->line('devise').nbs().$this->lang->line('prix_taxe_'.$tarif['taxe']).'</span>';
		                $tarifs .= '</li>';
		            }
		            $tarifs .= '</ul>';
		            
		            $data['tarif']['grille'] = $tarifs;
		        }
		    }
		}
		
		$liste .= $this->dwootemplate->get(tpl_path('catalogue/item_produit.tpl'), $data);
	}
}
else
{
	$liste .= '<div class="liste-vide bg-info text-info">'.lang('catalogue_vide').'</div>';
}
$data = array(
	'titre_page'	=> NULL,
	'section_id'	=> NULL,
	'categorie'		=> NULL,
	'pagination'	=> $pagination,
	'liste'			=> $liste,
	'filtres'		=> NULL,
	'tri'			=> NULL,
);


$this->dwootemplate->output(tpl_path('recherches/liste_produits.tpl'), $data);