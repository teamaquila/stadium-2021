<div id="resultats_recherche">
	{assets('recherche/widget_recherche.js')}
	<div class="bloc_titre_h1"><h1><span>Résultat de votre recherche :</span> {$recherche}</h1> </div>
	<div class="inner_interne">
	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" role="tablist">
	    {if $tab_actualites}<li role="presentation" class="active {if $nb_actualites > 0}non_vide{else}vide{/if} active"><a href="#actualites" aria-controls="actualites" role="tab" data-toggle="tab">Actualités <span class="badge">{$nb_actualites}</span></a></li>{/if}
	   	{if $tab_editeurs}<li role="presentation" class="{if $nb_editeurs > 0}non_vide{else}vide{/if}"><a href="#pages" aria-controls="pages" role="tab" data-toggle="tab">Pages <span class="badge">{$nb_editeurs}</span></a></li>{/if}
	    {if $tab_catalogue}{if $nb_catalogue > 0 }<li role="presentation" class="{if $nb_catalogue > 0}non_vide{else}vide{/if}"><a href="#catalogue" aria-controls="catalogue" role="tab" data-toggle="tab">Produit(s) <span class="badge">{$nb_catalogue}</span></a></li>{/if}{/if}
	  </ul>
	
	  <!-- Tab panes -->
	  <div class="tab-content">
	    {if $tab_actualites}<div role="tabpanel" class="tab-pane active" id="actualites">{$contenu_tab_actualites}</div>{/if}
	    {if $tab_editeurs}<div role="tabpanel" class="tab-pane" id="pages">{$contenu_tab_editeurs}</div>{/if}
	    {if $tab_catalogue}<div role="tabpanel" class="tab-pane" id="catalogue">{$contenu_tab_catalogue}</div> 
	    	{if $nb_catalogue == 0}
	    	<div id="resultat-vide">
	    		<p>Votre recherche n'a donné aucun résultat.</p>
	    		<p><a class="bouton" href="/produits,2/medaille-du-travail,35/">Découvrez nos meilleures ventes</a><br />	    		
	    		Vous avez une question ? Notre équipe commerciale est à votre disposition <br /><a class="bouton" href="/contactez-nous,19.html">Contactez-nous</a></p>
	    	</div>
	    	{/if}
	    {/if}
	  </div>
	</div>
</div>