<?php
$liste = NULL;

//Si le client est connecté, on peut utiliser sont statut pour le tarif (ex : professionnel)
$client = NULL;
if ($compte = json_decode($this->encrypt->decode(get_cookie('client'))))
{
	$this->load->model('boutique/client');
	$client = $this->client->get_by_id($compte->client_id);

	if ( ! $client->exists())
	{
		$client = NULL;
	}
}

$utilisateur = $client;

if ( ! is_null($produits) && ($produits->result_count() > 0))
{
	if ($produit_parent->type_associe == Produit::TYPE_ASSOCIE_LISTE)
	{
		$liste .= '<div class="produits-associes type-liste">';
		$i = 0;
		foreach ($produits as $produit)
		{
			if (strpos($produit->reference, 'GRV' ) === FALSE)
			{
				//Bouton panier
				$panier = NULL;
				if ($module->boutique && ($produit->stock > 0) && $module->panier_liste)
				{
					$panier = modules::run('boutique/boutique/bouton_ajouter', $produit);
				}
					
					
				//Si le produit associé esr une médaille et que c'est un produit complémentaire
				$gravure_complementaire = FALSE;
				$nb_criteres_gravure = FALSE;
				if ((strpos($produit->reference, 'GRVM' ) !== FALSE) && ($complement == TRUE))
				{
					//affichage spécifique pour que la gravure soit saisie directement et ajouté en liaison avec le prodyut parent
					$gravure_complementaire = TRUE;
				
					//Nombre de caractères autorisés pour la gravure
					$critere_taille = $produit->criteres_associes(2);
					$nb_criteres_gravure = $critere_taille->valeur;
				
				}
					
				//Produit
				$data = array(
						'id'			=> $produit->id,
						'libelle'		=> $produit->libelle,
				        'libelle2'		=> $produit->libelle2,
						'produit_complementaire'	=> $complement,
						'gravure_complementaire'	=> $gravure_complementaire,
						'nb_criteres_gravure'	=> $nb_criteres_gravure,
						'produit_referent_id'	=> $produit_parent->id,
						'reference'		=> $produit->reference,
				        'commandable'	=> !$produit->non_commandable,
						'image' 		=> html_media($produit->medias_associes(), array('taille' => $module->taille_image_liste )),
						'description'	=> ($produit->resume) ? $produit->resume : truncate($produit->description, 200),
						'stock'			=> statut_stock($produit->stock, $module->seuil_alerte_stock),
						'nouveau'		=> $produit->nouveau,
						'poids' 		=> array(
								'valeur' => $produit->poids,
								'unite'  => $module->unite_poids
						),
						'url'			=> $produit->url(),
						'panier'		=> $panier,
						'index'			=> $i
				);				
					
				//Si les enfants du produits sont des articles collections composés d'autres articles on calcule le prix en fonction des enfants
				$trouve_compose = FALSE;
				$tab_tarifs_compose = array();
				foreach ($produit->produits_enfants() as $enfant)
				{
				    $produits_composes = $this->db->query("
				SELECT t_produits.id
				FROM t_produits
				INNER JOIN t_produits_produits ON t_produits.id = t_produits_produits.related_produit_id
				WHERE t_produits_produits.compose = 1
				AND t_produits_produits.produit_id = ".$enfant->id."
			");
				    if ($produits_composes->num_rows() != 0)
				    {
				        $trouve_compose = TRUE;
				        
				        $tab_tarifs_compose[$enfant->id]	= array(
				            'prix'	=> 0,
				            'prix_sans_remise'	=> 0
				        );
				        foreach ($produits_composes->result() as $row)
				        {
				            $prod_compo = new Produit();
				            $prod_compo->get_by_id($row->id);
				            $tab_tarifs_compose[$enfant->id]['prix'] +=  $prod_compo->tarif($utilisateur)['prix'];
				            $tab_tarifs_compose[$enfant->id]['prix_sans_remise'] +=  $prod_compo->tarif($utilisateur)['prix'];
				        }
				    }
				}
				
				
				if ($trouve_compose)
				{
				    $tarif_precedent = 0;
				    
				    foreach ($tab_tarifs_compose as $enfant_id => $tarif_enfant)
				    {
				        if (($tarif_precedent == 0 ) or ($tarif_enfant['prix'] < $tarif_precedent))
				        {
				            $data['tarif']['prix']	= html_price($tarif_enfant['prix']);
				            $data['tarif']['prix_sans_remise']	= html_price($tarif_enfant['prix_sans_remise']);
				            $tarif_precedent = $tarif_enfant['prix'];
				        }
				    }
				    
				}
				else
				{
				    //Tarif
				    $tarif = $produit->tarif($utilisateur);
				    
				    if ($tarif !== FALSE)
				    {
				        //Tarif unique
				        $data['tarif'] = array(
				            'prefixe'		=> ($tarif['unique']) ? NULL : $this->lang->line('a_partir_de'),
				            'suffixe'		=> $this->lang->line('devise'),
				            'prix'			=> html_price($tarif['prix']),
				            'prix_ht'			=> html_price($tarif['prix']),
				            'prix_sans_remise' => html_price($tarif['prix_sans_remise']),
				            'taux_remise'	=> html_percentage($tarif['taux_remise'], 0),
				            'tva'			=> html_percentage($tarif['tva'], 2),
				            'taxe'			=> $this->lang->line('prix_taxe_'.$tarif['taxe'])
				        );
				        
				        //Grille tarifaire
				        if (is_array($tarif['grille']))
				        {
				            $tarifs = '<ul class="liste-tarifs">';
				            foreach ($tarif['grille'] as $grille)
				            {
				                $tarifs .= '<li>';
				                $tarifs .= '<span class="minimum">'.$grille['minimum'].'</span>';
				                $tarifs .= '<span class="maximum">'.$grille['maximum'].'</span>';
				                $tarifs .= '<span class="prix">'.html_price($grille['prix']).nbs();
				                $tarifs .= $this->lang->line('devise').nbs().$this->lang->line('prix_taxe_'.$tarif['taxe']).'</span>';
				                $tarifs .= '</li>';
				            }
				            $tarifs .= '</ul>';
				            
				            $data['tarif']['grille'] = $tarifs;
				        }
				    }
				}
					
				$liste .= $this->dwootemplate->get(tpl_path('catalogue/item_produit_associe_type_1.tpl'), $data);
				$i++;
			}
			
		}
		$liste .= '</div>';
	}
	elseif ($produit_parent->type_associe == Produit::TYPE_ASSOCIE_FICHE)
	{
		$liste .= '<div class="produits-associes type-fiche">';
		$i = 0;
		foreach ($produits as $produit)
		{
			//Bouton panier
			$panier = NULL;
			if ($module->boutique && ($produit->stock > 0) && $module->panier_liste)
			{
				$panier = modules::run('boutique/boutique/bouton_ajouter', $produit);
			}
			
			//Produit
			$data = array(
				'id'			=> $produit->id,
				'libelle'		=> $produit->libelle,
				'reference'		=> $produit->reference,
				'image' 		=> html_media($produit->medias_associes(), array('taille' => $module->taille_image_liste)),
				'description'	=> ($produit->resume) ? $produit->resume : truncate($produit->description, 200),
				'stock'			=> statut_stock($produit->stock, $module->seuil_alerte_stock),
				'nouveau'		=> $produit->nouveau,
				'poids' 		=> array(
					'valeur' => $produit->poids,
					'unite'  => $module->unite_poids
				),
				'url'			=> $produit->url(),
				'panier'		=> $panier,
				'index'			=> $i
			);
			
			//Tarif
			$tarif = $produit->tarif();
			
			if ($tarif !== FALSE)
			{
				//Tarif unique
				$data['tarif'] = array(
					'prefixe'		=> ($tarif['unique']) ? NULL : lang('a_partir_de'),
					'suffixe'		=> lang('devise'),
					'prix'			=> prix($tarif['prix']),
					'prix_sans_remise' => prix($tarif['prix_sans_remise']),
					'prix_ht'			=> prix($tarif['prix_ht']),
					'taux_remise'	=> taux_pourcentage($tarif['taux_remise'], 0),
					'tva'			=> taux_pourcentage($tarif['tva'], 2),
					'taxe'			=> lang('prix_taxe_'.$tarif['taxe'])
				);
				
				//Grille tarifaire
				if (is_array($tarif['grille']))
				{
					$tarifs = '<ul class="liste-tarifs">';
					foreach ($tarif['grille'] as $grille)
					{
						$tarifs .= '<li>';
						$tarifs .= '<span class="minimum">'.$grille['minimum'].'</span>';
						$tarifs .= '<span class="maximum">'.$grille['maximum'].'</span>';
						$tarifs .= '<span class="prix">'.prix($grille['prix']).nbs();
						$tarifs .= lang('devise').nbs().lang('prix_taxe_'.$tarif['taxe']).'</span>';
						$tarifs .= '</li>';
					}
					$tarifs .= '</ul>';
					
					$data['tarif']['grille'] = $tarifs;
				}
			}
			
			$liste .= $this->dwootemplate->get(tpl_path('catalogue/item_produit_associe_type_2.tpl'), $data);
			$i++;
		}
		$liste .= '</div>';
	}
}
echo $liste;