<?php
$liste = NULL;

$utilisateur = $this->clients->est_connecte();
$liste .= assets('ajax_prix_liste.js', FALSE, TRUE);

if ( ! is_null($produits) && ($produits->result_count() > 0))
{
	foreach ($produits as $produit)
	{


		$module = new Module_catalogue();
		$module->get_by_id($produit->module_catalogue_id);
		
		$note = $produit->note();
		//Note
		$note_produit = NULL;
		if ( ! is_null($note))
		{
		    $note_produit .= '<div id="note">';
		    
		    for ($i = 1; $i <= 5; $i++)
		    {
		        if ($i <= $note)
		        {
		            $note_produit .= '<span class="glyphicon glyphicon-star"></span>';
		        }
		        else
		        {
		            $note_produit .= '<span class="glyphicon glyphicon-star-empty"></span>';
		        }
		    }
		    $note_produit .= '</div>';
		}
		
		
		//Test catégorie première pour savoir si on est dans les falluches
// 		$categories = $produit->get_by_id($produit->id)->categorie->where('root_id', $categorie_parent->root_id)->where('left_id > ',1)->get();
// 		$categorie_referente = NULL;
// 		$level = 9999;
// 		foreach ($categories as $categorie)
// 		{
// 		    if (($categorie->level() <= $level))
// 		    {
// 		        $categorie_referente = $categorie;
// 		        $level = $categorie->level();
// 		    }
// 		}
// 		$categorie_faluches = ($categorie_referente->id == 55) ? TRUE : FALSE;
// 		//Bouton panier
// 		$panier = NULL;
// 		if ($categorie_faluches)
// 		{
// 		    $panier = modules::run('boutique/boutique/bouton_ajouter', $produit);
// 		}
		
		$data = array(
			'id'			=> $produit->id,
		    'libelle'		=> $produit->libelle,
		    'libelle2'		=> $produit->libelle2,
			'reference'		=> $produit->reference,
		    'description'	=> ($produit->resume) ? truncate($produit->resume,$module->longueur_texte_liste) : truncate($produit->description, $module->longueur_texte_liste),
			'nouveau'		=> $produit->nouveau,
			'url'			=> $produit->url(),
			'commandable'	=> !$produit->non_commandable,
			//'tarif'			=> $produit->tarif($utilisateur),
			//'panier'		=> ($module->afficher_panier_liste) ? (modules::run('boutique/boutique/bouton_ajouter_opt', $produit)) : NULL,
			'poids' 		=> array(
				'valeur' => $produit->poids,
				'unite'  => $module->unite_poids
			),
		    //'note'			=> $note_produit,
		    //'chiffre_note'	=> $note,
		    //'nb_note'       => $produit->nb_note(),
// 		    'categorie_faluches' => $categorie_faluches,
// 		    'panier'		=> $panier,
		);
		
		$medias = $produit->medias_associes();
		if ($medias->exists())
		{
			$data['image'] = html_media($medias, array('taille' => $module->taille_image_liste, 'libelle' => $produit->libelle));
		}
		else
		{
			$data['image'] = '<img src="/assets/img/img-non-disponible-liste.jpg" alt="Photo indisponible '.$produit->libelle.'" />';
		}
				
		$liste .= $this->dwootemplate->get(tpl_path('catalogue/item_produit.tpl'), $data);
	}
}
else
{
	$liste .= '<div class="liste-vide bg-info text-info">'.lang('catalogue_vide').'</div>';
}

$data = array(
	'titre_page'				=> ( ! empty($section->titre)) ? $section->titre : $section->libelle,
	'section_id'				=> $section->id,
	'categorie'					=> NULL,
	'pagination'				=> $pagination,
	'liste'						=> $liste,
	'filtres'					=> $filtres,
	'tri'						=> $tri,
	'total_produit'				=> $total_produit,
	'afficher_criteres_liste'	=> $module->afficher_criteres_liste,
	'afficher_categories_liste'	=> $module->afficher_categories_liste,
	'image_section'				=> $image_section,
    'texte_seo'                 => $texte_seo,
	'fil_ariane'				=> $fil_ariane,
    'titre_seo'     => $titre_seo,
);

//Catégorie
if ( ! is_null($categorie))
{
	$data['categorie'] = array(
		'id'		=> $categorie->id,
		'libelle'	=> $categorie->libelle,
		'contenu'	=> $categorie->contenu,
		'image'		=> html_media($categorie->medias_associes(), array('taille' => $module->taille_image_liste)),
	);
}

$this->dwootemplate->output(tpl_path('catalogue/liste_produits.tpl'), $data);