<?php
$html = NULL;
$html = assets('modules/catalogue/js/ajax_criteres.js', TRUE, TRUE);

$url = ( ! is_null($categorie) && ($categorie->left_id != 1)) ? $categorie->url($section) : construire_url(array('section' => $section->segment_url()));

$html .= '<div id="zone-filtre">';

if ($get = $this->input->get())
{
    $html .= '<div id="vos-filtres">';
    $html .= '<span class="lib-filtre">Vos filtres :</span>';

    $criteres_actifs = get_criteres($get['filtre']);
    foreach($criteres_actifs as $critere => $valeurs)
    {
        foreach($valeurs as $valeur_id => $valeur)
        {
            $critere_valeur = new Criteres_valeur();
            $critere_valeur->get_by_id($valeur);

            $temp_valeur = array(
                "critere_id"    =>  $critere_valeur->critere_id,
                "valeur"    =>      $critere_valeur->valeur,
                "id"    =>          $critere_valeur->id,
            );
            $html .= '<div class="critere-actif" >'.anchor($url.set_criteres_without_selected($temp_valeur),$critere_valeur->valeur.'<span> (X)</span>').'</div><br>';
        }
    }

    $html .= anchor('boutique,3.html', lang('retirer_les_criteres'), array('class' => 'reset-filtre'));  
    $html .= '</div>';
}

$html .= '<div id="filtres-critere">';

if ($criteres->result_count())
{
    $html .= '<span class="lib-filtre">Filtrer par :</span>';

	foreach ($criteres as $critere)
	{
		$html_critere = NULL;
		$html .= '<div class="bloc_critere" data-categorie="'.$categorie->id.'" data-section="'.$section->id.'" data-id="'.$critere->id.'" data-get="'.$this->input->get('filtre').'">';
		//$html .= heading($critere->libelle, 4, 'class="panel-heading"');
		//$html_critere .= '<span class="titre-filtre" data-critere_id="'.$critere->id.'">'.$critere->libelle.'</span>';
		//$html_critere .= '<ul class="criteres level-0 panel-body nav nav-pills nav-stacked" id="liste-critere-'.$critere->id.'">';
		
		$html .= '<span class="titre-filtre" id="critere-'.$critere->id.'" data-critere_id="'.$critere->id.'">'.$critere->libelle.'</span>';
		
		
		$html .= '<ul class="criteres level-0" if="liste-critere-'.$critere->id.'">';
		$html .= '</ul>';
		$html .= '</div>';
			
	}
	$html .= '<hr class="clear-both">';
}

$html .= '<hr class="clear-both">';
$html .= '</div>';
$html .= '</div>';

echo $html;