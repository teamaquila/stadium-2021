<?php
$html = NULL;
$html2 = NULL;
$criteres_actifs = 0;
$listes_valeurs = array();

if ($criteres->result_count())
{	    
	$html2 .= '<span class="lib-filtre">Filtrer par :</span>';
	foreach ($criteres as $critere)
	{
	    $criteres_valeur = new Criteres_valeur();
	    $nb_choix_criteres = 0;
	    //$criteres_produit =	$criteres_valeur->where('module_catalogue_id',$module->id)->where_related('critere', 'libelle', $critere->libelle)->group_by('valeur')->order_by('valeur')->include_related('criteres_valeurs_produit')->get();
	    $sql = 'SELECT `t_criteres_valeurs`.*
        FROM (`t_criteres_valeurs`, `t_criteres`)
        WHERE `t_criteres`.`id` = `t_criteres_valeurs`.`critere_id`
        AND `t_criteres`.`libelle` = "'.$critere->libelle.'"
        AND `t_criteres`.`module_catalogue_id` = "'.$module->id.'"
        GROUP BY `t_criteres_valeurs`.`valeur`
        ORDER BY `t_criteres_valeurs`.`valeur`';
	    
	    $criteres_produit = $criteres_valeur->query($sql);
	    
		if ($criteres_produit->result_count() > 0)
		{
			$html_critere = NULL;
			$html2 .= '<div class="bloc_critere">';
			//$html .= heading($critere->libelle, 4, 'class="panel-heading"');
			//$html_critere .= '<span class="titre-filtre" data-critere_id="'.$critere->id.'">'.$critere->libelle.'</span>';
			//$html_critere .= '<ul class="criteres level-0 panel-body nav nav-pills nav-stacked" id="liste-critere-'.$critere->id.'">';
			
			foreach ($criteres_produit as $critere_produit)
			{
			    if ($critere_produit->valeur != '')
			    {
    				$class = NULL;
    				$options = array(
    					'critere_id'=> $critere_produit->critere_id,
    					'valeur' 	=> $critere_produit->valeur,
    					'id' 		=> $critere_produit->id
    				);
    				
    				if (exist_critere($options))
    				{
    					$class = 'enabled ';
    					$nb_choix_criteres++;
    					$criteres_actifs++;
    					array_push($listes_valeurs, $options);//$critere_produit->valeur);
    				}
    				else
    				{
    					$class = 'disabled ';
    				}
    				
    				$url = ( ! is_null($categorie) && ($categorie->left_id != 1)) ? $categorie->url($section) : construire_url(array('section' => $section->segment_url()));
    				
    				//Si on souhiate ne pas cumuler les critères, passer 2dn param FALSE à set_criteres
    				$html_critere .= '<li class="'.$class.'">';
    				$html_critere .= anchor($url.set_criteres($options), $critere_produit->valeur, array('rel' => 'nofollow'));
    				$html_critere .= '</li>';
			    }
			}
			if ($nb_choix_criteres > 0)
			{
				$html2 .= '<span class="titre-filtre active" id="critere-'.$critere->id.'" data-critere_id="'.$critere->id.'">'.$critere->libelle.' ('.$nb_choix_criteres.')</span>';
			}
			else
			{
				$html2 .= '<span class="titre-filtre" id="critere-'.$critere->id.'" data-critere_id="'.$critere->id.'">'.$critere->libelle.'</span>';
			}
			
			
			$html2 .= '<ul class="criteres level-0" if="liste-critere-'.$critere->id.'">';
			$html2 .= $html_critere;
			$html2 .= '</ul>';
			$html2 .= '</div>';
			
			
		}
	    
	}
	$html2 .= '<hr class="clear-both">';
	$html2 .= '</div>';
	$html2 .= '</div>';
	
	$html .= '<div id="zone-filtre">';
	//Réinitialiser les critères
	if ($criteres_actifs > 0)
	{
	$html .= '<div id="vos-filtres">';
	$html .= '<span class="lib-filtre">Vos filtres :</span>';
	
	foreach($listes_valeurs as $criteres_produit)
	{
	    $html .= '<div class="critere-actif" >'.anchor($url.set_criteres_without_selected($criteres_produit),$criteres_produit['valeur'].'<span> (X)</span>').'</div><br>';
	    //anchor($url.set_criteres_without_selected($options), $critere_produit->valeur
	}
	

	    $html .= anchor('boutique,3.html', lang('retirer_les_criteres'), array('class' => 'reset-filtre'));

	$html .= '</div>';
	}
	$html .= '<div id="filtres-critere">';
	$html .= $html2;
	
}



echo $html;