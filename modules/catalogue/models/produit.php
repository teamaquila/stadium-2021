<?php
/**
 * Modèle de la table "t_produits'
 * 
 * @author 		Pauline Martin, Pierre-Etienne Raby
 * @package 	catalogue
 * @category 	Models
 * @version 	2.0
 */
class Produit extends DataMapper
{
	const TYPE_ASSOCIE_LISTE = 1;
	const TYPE_ASSOCIE_FICHE = 2;
	
	//Relations
	var $has_many = array(
	    'medias_produit', 'produits_produit', 'categorie', 'tarif',
	    'critere', 'criteres_valeurs_produit', 'code_remise', 'gamme_tarif', 'gamme_client_tarif',
	    'related_produit' => array(
	        'class' 		=> 'produits_produit',
	        'join_self_as' 	=> 'id',
	        'join_other_as' => 'related_produit_id',
	    ),
	    'commentaire','module_selection','lieux_produit', 'lignes_resa'
	);
	var $has_one = array('module_catalogue');
	
	/**
	 * Constructeur
	 */
    public function __construct()
    {
    	parent::add_model_path(array(
			APPPATH.'modules/categories',
			APPPATH.'modules/medias',
			APPPATH.'modules/boutique',
			APPPATH.'modules/catalogue'
		));
		parent::__construct();
		$this->load->helper('catalogue/catalogue');
    }
    
	/**
     * Enregistrer un produit
     * avec données du formulaire
     * 
     * @param 	array 	$post
     * @return	object or FALSE
     */
    public function enregistrer($post)
    {
    	$liaisons = array();
    	
    	//Catégories
    	if (isset($post['categories']))
    	{
    		$categorie 	= new Categorie();
    		$categories = $categorie->where_in('id', $post['categories'])->get()->all;
    		array_push($liaisons, $categories);
    		
    		if ($this->id)
    		{
    			$this->delete($this->categorie->get()->all);
    		}
    	}
    	else
    	{
    		if ($this->id)
    		{
    			$this->delete($this->categorie->get()->all);
    		}
    	}
    	
    	/*Si une donnée video rentre la table, nous enlevons la protection Tinymce qui empêche l'intégration de code vidéo Youtube*/
    	if (isset($post['video']))
    	{
    	    $post['video'] = html_entity_decode($post['video'], ENT_COMPAT, 'UTF-8');
    	}
    	
    	//Durée remise
    	if (isset($post['remise_date_debut']) && isset($post['remise_date_fin']))
    	{
    	    $post['remise_date_debut']	= date_to_mysql($post['remise_date_debut']);
    	    $post['remise_date_fin']	= date_to_mysql($post['remise_date_fin']);
    	}
    	
    	//Dates de publication
    	if (isset($post['debut_publication']) && isset($post['fin_publication']))
    	{
    		$post['debut_publication'] 	= date_to_mysql($post['debut_publication']);
    		$post['fin_publication'] 	= date_to_mysql($post['fin_publication']);
    	}

		//Description
		if (isset($post['description']))
		{
			$post['description'] = html_entity_decode($post['description'], ENT_COMPAT, 'UTF-8');
		}
		        
		if ($this->from_array($post, array_keys($post), TRUE))
		{
			$this->save($liaisons);
			
			//Produits associés
	    	if (isset($post['produits-associes']))
	    	{	    		
	    		$this->db->delete('t_produits_produits', array(
    				'produit_id' => $this->id,
    				'enfant' 	=> 0,
	    			'impose'		=> 0,
	    			'compose'		=> 0,
	    			'complement'	=> 0
	    		));
				
				foreach($post['produits-associes'] as $produit_id)
				{
					$this->produits_produit->clear();
					$this->produits_produit->enregistrer(array(
						'produit_id' => $this->id,
						'related_produit_id' => $produit_id,
						'enfant' => 0
					));
					
				}
	    	}
	    	else
	    	{
	    		$this->db->delete('t_produits_produits', array(
    				'produit_id' => $this->id,
    				'enfant' 	=> 0,
	    			'impose'		=> 0,
	    			'compose'		=> 0,
	    			'complement'	=> 0
	    		));
	    	}
	    	
	    	//Produits composes
	    	if (isset($post['produits-composes']))
	    	{
	    		$this->db->delete('t_produits_produits', array(
    				'produit_id' => $this->id,
    				'enfant' => 0,
	    			'compose'	=> 1
	    		));
	    	
	    		foreach($post['produits-composes'] as $produit_id)
	    		{
	    			$this->produits_produit->clear();
	    			$this->produits_produit->enregistrer(array(
	    				'produit_id' => $this->id,
	    				'related_produit_id' => $produit_id,
	    				'enfant' => 0,
	    				'compose'	=> 1
	    			));
	    				
	    		}
	    	}
	    	else
	    	{
	    		$this->db->delete('t_produits_produits', array(
    				'produit_id' => $this->id,
    				'enfant' => 0,
	    			'compose'	=> 1
	    		));
	    	}
	    	
	    	
	    	//Produits composes
	    	if (isset($post['produits-imposes']))
	    	{
	    		$this->db->delete('t_produits_produits', array(
    				'produit_id' => $this->id,
    				'enfant' => 0,
	    			'impose'	=> 1
	    		));
	    	
	    		foreach($post['produits-imposes'] as $produit_id)
	    		{
	    			$this->produits_produit->clear();
	    			$this->produits_produit->enregistrer(array(
	    					'produit_id' => $this->id,
	    					'related_produit_id' => $produit_id,
	    					'enfant' => 0,
	    					'impose'	=> 1
	    			));
	    			 
	    		}
	    	}
	    	else
	    	{
	    		$this->db->delete('t_produits_produits', array(
    				'produit_id' => $this->id,
    				'enfant' => 0,
	    			'impose'	=> 1
	    		));
	    	}
	    	
	    	//Produits composes
	    	if (isset($post['produits-complementaires']))
	    	{
	    		$this->db->delete('t_produits_produits', array(
    				'produit_id' => $this->id,
    				'enfant' => 0,
	    			'complement'	=> 1
	    		));
	    	
	    		foreach($post['produits-complementaires'] as $produit_id)
	    		{
	    			$this->produits_produit->clear();
	    			$this->produits_produit->enregistrer(array(
	    					'produit_id' => $this->id,
	    					'related_produit_id' => $produit_id,
	    					'enfant' => 0,
	    					'complement'	=> 1
	    			));
	    			 
	    		}
	    	}
	    	else
	    	{
	    		$this->db->delete('t_produits_produits', array(
    				'produit_id' => $this->id,
    				'enfant' => 0,
	    			'complement'	=> 1
	    		));
	    	}
			
			//Produits enfants
	    	if (isset($post['produits_enfants']))
	    	{	    		
	    		$this->produits_produit->supprimer($this->id, TRUE);
				
				foreach($post['produits_enfants'] as $produit_id)
				{
					$this->produits_produit->clear();
					$this->produits_produit->enregistrer(array(
						'produit_id' => $this->id,
						'related_produit_id' => $produit_id,
						'enfant' => 1
					));
				}
	    	}
	    	else
	    	{
	    		$this->produits_produit->supprimer($this->id, TRUE);
	    	}
			
			//Médias
			if (isset($post['medias']))
	    	{
	    		$this->medias_produit->supprimer($this->id);
	    		
	    		foreach ($post['medias'] as $index => $media)
	    		{
	    			$this->medias_produit->clear();
	    			$this->medias_produit->enregistrer(array(
	    				'media_id' 	=> $media,
	    				'produit_id'=> $this->id,
	    				'ordre' 	=> $index
	    			));
	    		}
	    	}
	    	else
	    	{
	    		//$this->medias_produit->supprimer($this->id);
	    	}
	    	
			//Tarifs
	    	if (isset($post['type_client']))
	    	{
	    		$tarif = new Tarif();
	    		$this->delete($this->tarif->get()->all);
	    		
	    		foreach ($post['type_client'] as $type_client => $champ)
	    		{
	    			$tarif->clear();
	    			
	    			if (isset($champ['tarif_id']))
		    		{
		    			$tarif->id = $champ['tarif_id'];
		    		}
	    			$tarif->type_client_id  = $type_client;
	    			$tarif->remise 	        = $champ['remise'];
                    $tarif->prix_remise     = $champ['prix_remise'];
	    			$tarif->taxe 	        = $champ['taxe'];
		    		
		    		if ($champ['unique'])
			    	{
			    		$tarif->unique = TRUE;
			    		$tarif->prix_vente = $champ['prix_vente'];
			    	}
			    	else
			    	{
			    		$tarif->unique = FALSE;
			    		
			    		for ($i = 1; $i <= 10; $i++)
			    		{
			    			if ( ! empty($champ['seuil_min'][$i]) && ! empty($champ['seuil_max'][$i]) && ! empty($champ['prix'][$i]))
			    			{
				    			eval('$tarif->seuil_min_'.$i.' = '.$champ['seuil_min'][$i].';');
				    			eval('$tarif->seuil_max_'.$i.' = '.$champ['seuil_max'][$i].';');
				    			eval('$tarif->prix_'.$i.' = '.$champ['prix'][$i].';');
			    			}
			    			else
			    			{
			    				eval('$tarif->seuil_min_'.$i.' = 0;');
				    			eval('$tarif->seuil_max_'.$i.' = 0;');
				    			eval('$tarif->prix_'.$i.' = 0;');
			    			}
			    		}
			    	}
			    	
			    	if (isset($champ['remisable']))
			    	{
			    	    $this->remisable = $champ['remisable'];
			    	    $this->save();
			    	}
			    	$tarif->save($this);
	    		}
	    	}
			else
	    	{
	    		$this->delete($this->tarif->get()->all);
	    	}
			
	    	//Critères
	    	if (isset($post['criteres']))
	    	{
	    	    $this->criteres_valeurs_produit->supprimer($this->id);
	    	    
	    	    foreach ($post['criteres'] as $critere_id => $valeur_id)
	    	    {
	    	        if (is_array($valeur_id))
	    	        {
	    	            foreach ($valeur_id as $val)
	    	            {
	    	                $this->criteres_valeurs_produit->clear();
	    	                $this->criteres_valeurs_produit->enregistrer(array(
	    	                    'critere_id'=> $critere_id,
	    	                    'produit_id'=> $this->id,
	    	                    'criteres_valeur_id' 	=> $val
	    	                ));
	    	            }
	    	        }
	    	        else
	    	        {
	    	            $this->criteres_valeurs_produit->clear();
	    	            $this->criteres_valeurs_produit->enregistrer(array(
	    	                'critere_id'=> $critere_id,
	    	                'produit_id'=> $this->id,
	    	                'criteres_valeur_id' 	=> $valeur_id
	    	            ));
	    	        }
	    	        
	    	    }
	    	}
	    	else
	    	{
	    	    //$this->criteres_valeurs_produit->supprimer($this->id);
	    	}
			
			return $this;
		}
		return FALSE;
    }
    
    /**
     * Supprimer un produit et ses liaisons
     *
     * @return	boolean
     */
    public function supprimer()
    {
        if ($this->id)
        {
            $produits_enfants = $this->produits_enfants();
            
            foreach ($produits_enfants as $enfant)
            {
                $enfant->delete();
                
            }
            $this->delete_all();
            
            
            return TRUE;
        }
        return FALSE;
    }
    
    /**
     * Retourne la liste des produits du module
     * 
     * @param	array	$conditions
     * @param 	integer $offset
     * @param 	integer $item_par_page
     * @return	object
     */
    public function lister($conditions = array(), $offset = 0, $item_par_page = 0)
    {
        $produit = ( ! empty($conditions)) ? $this->_conditions($conditions) : new Produit();
        /**
         * ORDRE
         */
        //Spécifique
        if (isset($conditions['tri']))
        {

            //Cas pour trier par prix
            if ($conditions['tri']['champ'] == 'tarif_prix_vente')
            {
                $produit->include_related('tarif', 'prix_vente');
                $produit->order_by($conditions['tri']['champ'], $conditions['tri']['ordre']);
            }
            elseif ($this->db->field_exists($conditions['tri']['champ'], 't_produits'))
            {
                $produit->order_by($conditions['tri']['champ'], $conditions['tri']['ordre']);
            }
            else
            {
                $produit->_ranger();
            }
        }
        
        //Aléatoire
        elseif (isset($conditions['random']) && $conditions['random'] == TRUE)
        {
            $produit->order_by('id', 'random');
        }
        
        //Par défaut
        else
        {
            $produit->_ranger();
        }
        
        /**
         * GROUPEMENT
         */
        if (isset($conditions['group_by']))
        {
            $produit->group_by($conditions['group_by']);
        }
        else
        {
            $produit->group_by('id');
        }
        
        /**
         * LIMITE
         */
        if (isset($conditions['item_par_page']))
        {
            $produit->limit($conditions['item_par_page'], $offset);
        }
        elseif ($item_par_page > 0)
        {
            $produit->limit($item_par_page, $offset);
        }
        
        /**
         * LIMITE DE STOCK
         */
        if (isset($conditions['stock_limite']))
        {

            $produit->where('stock >', $conditions['stock_limite']);
        }

        //Exécution
        return $produit->get();
    }
    
	/**
	 * Retourne le nombre de produits
	 * 
	 * @param	array 	$conditions
	 * @return	integer
	 */
    public function compter($conditions = array())
    {
        $produit = ( ! empty($conditions)) ? $this->_conditions($conditions) : new Produit();
        
        $produit->group_by('id');
        $produit->get();

        return $produit->result_count();

    }
    
    /**
     * Retourne le tarif en fonction du client connecté ou non
     * Si pas connecté, tarif des particuliers retourné
     * 
     * @param 	object 	$client
     * @param	integer	$quantite
     * @return	array
     */
	public function tarif($client = NULL, $quantite = 1)
    {
    	$this->load->library('encrypt');
    	$this->load->model('boutique/clients_produit');
    	if (isset($this->id))
    	{
    		$module = $this->module_catalogue->get();
    		
    		//Tarification activé
    		if ($module->afficher_tarif)
    		{

    			$client_produit = new Clients_produit();

    			if (!is_null($client) && $client != FALSE)
				{
				    
					$client_produit->where('client_id', $client->id)->where('produit_id', $this->id)->get();
				}
    			
    			if ($client_produit->exists())
    			{
    				$tva = $this->tva->get();
    				$tva_calcule = 1 + ($tva->taux / 100);
    				
    				
    				$tarif = round($client_produit->prix_net*$tva_calcule, 10);
    				
    				$grille = FALSE;
    				$prix 	= $tarif ;
    				$prix_ht = $client_produit->prix_net; 
    				
    				return array(
    					'id'			=> $tarif->id,
    					'unique' 		=> TRUE,
    					'type_client'	=> $client->type_client_id,
    					'prix'			=> $prix,
    					'prix_ht'		=> $prix_ht,
    					'tva'			=> $tva->taux,
    					'taxe'			=> 0,
    					'taux_remise'	=> $client_produit->remise,
    					'prix_sans_remise' => $client_produit->prix_brut,
    					'grille'		=> $grille,
    					'conditionnement' => NULL
    				);
    			}
    			else 
    			{

    			    //Particulier
    			    if (($client && ($client->pro == 0)) && ($module->type_boutique != 2))
    			    {
    			        
    			        if (!is_null($client->zone_tarif) && ($client->zone_tarif !== 0) && ($client->zone_tarif != ''))
    			        {
    			            $this->tarif->where('zone_tarif',$client->zone_tarif);
    			        }
    			        $tarif = $this->tarif->get_by_pro(0);
    			        
    			    }
    			    //Pro
    			    elseif ($client && ($client->pro == 1) && ($module->type_boutique == 2 or $module->type_boutique == 3))
    			    {
    			        if (!is_null($client->zone_tarif) && ($client->zone_tarif !== 0) && ($client->zone_tarif !== '0') && ($client->zone_tarif != ''))
    			        {
    			            $this->tarif->where('zone_tarif',$client->zone_tarif);
    			        }
    			        $tarif = $this->tarif->get_by_pro($client->pro);
    			        if (!$tarif->exists())
    			        {
    			            
    			            $this->tarif->clear();
    			            if (!is_null($client->zone_tarif) && ($client->zone_tarif !== 0) && ($client->zone_tarif !== '0') && ($client->zone_tarif != ''))
    			            {
    			                $this->tarif->where('zone_tarif',$client->zone_tarif);
    			            }
    			            $tarif = $this->tarif->get_by_pro(0);
    			        }
    			    }
    			    else
    			    {

    			        $tarif = $this->tarif->where_in('zone_tarif', array('', NULL, 0))->get_by_pro(0);
    			    }
    			    
    			    if ( ! is_null($tarif) && $tarif->exists())
    			    {
    			        if ($tarif->unique)
    			        {
    			            $grille = FALSE;
    			            $prix 	= $tarif->prix_vente;
    			        }
    			        //Grille tarifaire
    			        else
    			        {
    			            $grille = array();
    			            
    			            for ($i = 1; $i <= 10; $i++)
    			            {
    			                eval('$seuil_min = $tarif->seuil_min_'.$i.';');
    			                eval('$prix_test = $tarif->prix_'.$i.';');
    			                eval('$seuil_max = $tarif->seuil_max_'.$i.';');
    			                
    			                if (($seuil_min >= 0) && ($seuil_max > 0) && ($prix_test > 0))
    			                {
    			                    array_push($grille, array(
    			                        'minimum' 	=> $seuil_min,
    			                        'maximum' 	=> $seuil_max,
    			                        'prix'		=> $prix_test
    			                    ));
    			                    
    			                    if (($quantite >= $seuil_min) && ($quantite <= $seuil_max) && ! isset($prix))
    			                    {
    			                        $prix = $prix_test;
    			                    }
    			                }
    			            }
    			            
    			            /*if ( ! isset($prix))
    			             {
    			             return FALSE;
    			             }*/
    			        }
    			        
    			        $options = array(
    			            'tarif_id'		=> $tarif->id,
    			            'unique' 		=> $tarif->unique,
    			            'type_client'	=> ($client) ? $client->pro : 0,
    			            'prix'			=> $prix,
    			            'tva'			=> taux_tva($this->tva_id),
    			            'taxe'			=> $tarif->taxe,
    			            'taux_remise'	=> 0,
    			            'prix_sans_remise' => $prix,
    			            'grille'		=> $grille
    			        );
    			        
    			        //Remise
    			        if ($prix > 0)
    			        {
    			            $taux_remise = 0;
    			            
    			            if (($tarif->prix_remise > 0) && ($tarif->prix_remise <= $prix))
    			            {
    			                $taux_remise = (($prix-$tarif->prix_remise)*100)/$prix;
    			                $prix = $tarif->prix_remise;
    			            }
    			            elseif ($tarif->remise > 0)
    			            {
    			                $taux_remise = $tarif->remise;
    			                $prix = $prix*(1-($tarif->remise/100));
    			            }
    			            
    			            if ($taux_remise > 0)
    			            {
    			                if ($this->remise_illimitee)
    			                {
    			                    $options['taux_remise'] = $taux_remise;
    			                    $options['prix'] = $prix;
    			                }
    			                else
    			                {
    			                    $date_jour = new DateTime();
    			                    
    			                    if (isset($this->remise_date_fin) && isset($this->remise_date_debut))
    			                    {
    			                        $date_fin = new DateTime($this->remise_date_fin." 23:59:59");
    			                        $date_debut = new DateTime($this->remise_date_debut." 00:00:01");
    			                        
    			                        if (($date_fin > $date_jour) && ($date_jour > $date_debut))
    			                        {
    			                            $options['taux_remise'] = $taux_remise;
    			                            $options['prix'] = $prix;
    			                        }
    			                    }
    			                }
    			            }
    			        }

    			        if ( ! isset($prix))
    			        {
    			            return FALSE;
    			        } else {
    			            return $options;
    			        }
    			    }
    			}
    			
    		}
    	}
    	return FALSE;
    }
    
    /**
     * Créer l'url
     *
     * @return	string
     */
    public function url()
    {
        if (isset($this->id))
        {
            $section = new Section();
            
            $produit = new Produit();
            $produit->include_related('module_catalogue', array('section_id'), FALSE)->get_by_id($this->id);
            
            $url = construire_url(array(
                'section' 	=> $section->segment_url($produit->section_id),
                'module'	=> $this->_reecriture($produit).','.$produit->id
            ));
            return $url;
        }
    }
    
    /**
     * Retourne le module associé au produit
     *
     * @return	object
     */
    public function module()
    {
        if ($this->id)
        {
            $produit = new Produit();
            return $produit->get_by_id($this->id)->module_catalogue->get();
        }
    }
    
    /**
     * Retourne les catégories associées
     *
     * @return	object
     */
    public function categories_associees()
    {
        if ($this->id)
        {
            $produit = new Produit();
            return $produit->get_by_id($this->id)->categorie->get();
        }
    }
    
	/**
     * Retourne les filtres associées
     * 
     * @return	object
     */
    public function filtres_associes()
    {
    	if ($this->id)
    	{
    		$categorie 	= new Categorie();
    		$produit 	= new Produit();
    		
    		$categories_racine = $categorie->select('id, root_id')->where('filtre', 1)->group_by('root_id')->get()->all_to_single_array('root_id');
    		
    		return $produit->get_by_id($this->id)->categorie->where_in('root_id', $categories_racine)->get();
    	}
    }
    
    /**
     * Retourne les produits associés
     * 
     * @return	object
     */
    public function produits_associes($complement = 0, $impose = 0, $compose = 0, $visible = 1)
    {
    	if ($this->id)
    	{
    		$related = new Produits_produit();
    		$related->select('related_produit_id')->where(array(
				'produit_id' => $this->id,
				'enfant' => 0,
    			'complement'	=> $complement,
    			'impose'	=> $impose,
    			'compose'	=> $compose 
			));
    		
    		$produit = new Produit();
    		return $produit->where('visible', $visible)->where_in_subquery('id', $related)->get();
    	}
    }
    
	
	/**
     * Retourne les produits enfants
     * 
	 * @param	array
	 * @param	integer
	 * @param	integer
     * @return	object
     */
    public function produits_enfants($conditions = array(), $offset = NULL, $limit = NULL)
    {
        if ($this->id)
        {
            $related = new Produits_produit();
            $related->select('related_produit_id')->where(array(
                'produit_id' => $this->id,
                'enfant' => 1
            ));
            
            $produit = new Produit();
            $produit->where_in_subquery('id', $related);
            // En stock selon segmentation
            if (isset($conditions['segmentation']) && ! empty($conditions['segmentation']))
            {
                
                $catalogue_config = $this->load->config('catalogue/catalogue', TRUE);
                $liste_segmentation = $catalogue_config['segmentation']; // liste des segmentations
                $segmentation = 'A';
                if (in_array($conditions['segmentation'],$liste_segmentation))
                {
                    $segmentation = $conditions['segmentation'];
                }
                //En stock
                if (isset($conditions['stock']) && ($conditions['stock'] == TRUE))
                {
                    $produit->where('`t_produits`.`stock` * `t_produits`.`coef_stk_segment_'.$segmentation.'` > ', 0);
                }
                
                //SEUIL STOCK INDISPONIBLE
                if (isset($conditions['stock_limite']) && ($conditions['stock_limite'] == TRUE))
                {
                    $produit->where('`t_produits`.`stock` * `t_produits`.`coef_stk_segment_'.$segmentation.'` > ',  $this->module_catalogue->seuil_stock_indisponible);
                }
            } else {
                //Stock
                if (isset($conditions['stock']) && ($conditions['stock'] == TRUE))
                {
                    $produit->where('stock > ', 0);
                }
                
                //Stock
                if (isset($conditions['stock_limite']) && ($conditions['stock_limite'] == TRUE))
                {
                    $produit->where('stock > ', $this->module_catalogue->seuil_stock_indisponible);
                }
            }
            
            
            //Visibilité
            if (isset($conditions['visible']) && ($conditions['visible'] == TRUE))
            {
                $produit->where('visible', 1);
            }
            
            //Limite
            if ( ! is_null($offset) && ! is_null($limit))
            {
                $produit->limit($limit, $offset);
            }
            
            return $produit->group_by('id')->_ranger()->get();
        }
    }
    
    /**
     * Retourne le produit parent
     * 
     * @return  object
     */
    public function produit_parent()
    {
        if ($this->id)
        {
           	$produit_parent = new Produits_produit();
           	$produit_parent->select('produit_id')->where(array(
				'related_produit_id' => $this->id,
				'enfant' => 1
			))->get(1);
		   
         	$produit = new Produit();
			return $produit->where('id', $produit_parent->produit_id)->get();
        }
		return FALSE;
    }
	
    /**
     * Retourne les criteres associés
     *
     * @return	object
     */
    public function criteres_associes($critere_id = NULL, $critere_libelle = NULL, $affichage = FALSE, $order = 'libelle')
    {
        if ($this->id)
        {
            $criteres_produit = new Criteres_valeurs_produit();
            if (!is_null($critere_id))
            {
                $criteres_produit->where('critere_id',$critere_id);
            }
            elseif(!is_null($critere_libelle)){
                $criteres_produit->where_related('critere', 'libelle', $critere_libelle);
            }
            $criteres_produit->where('produit_id', $this->id)->include_related('criteres_valeur')->include_related('critere', 'libelle');
            
            if($affichage == TRUE)
                $criteres_produit->where('affichage', TRUE);
                
                return $criteres_produit->order_by($order)->get();
        }
    }
    
    /**
     * Retourne les médias associés
     * 
     * @param 	integer $categorie
     * @return	object
     */
    public function medias_associes($categorie = Media::IMAGE)
    {
    	if ($this->id)
    	{
    		$media = new Media();
    		$media->include_related('medias_produit', 'ordre');
    		$media->where('produit_id', $this->id);
			
			if ($categorie !== FALSE)
			{
    			$media->where('categorie', $categorie);
			}
    		return $media->order_by('ordre', 'ASC')->get();
    	}
    }
    
    /**
     * Retourne les produits visibles et différents
     * du produit en instance
     *
     * @param	array	$conditions
     * @return	object
     */
    public function produits_disponibles($conditions = array())
    {
        $produit = new Produit();
        
        if (isset($conditions['recherche']))
        {
            $tab_recherche = explode(' ', $conditions['recherche']);
            $produit->group_start();
            foreach ($tab_recherche as $recherche)
            {
                $produit->like('libelle', $recherche);
            }
            $produit->group_end();
        }
        
        /*if (isset($conditions['grade']))
         {
         $produit->where('grade', $conditions['grade']);
         }*/
        
        if ($this->id)
        {
            $produit->where('module_catalogue_id', $this->module_catalogue_id);
            return $produit->where('id <>', $this->id)->where('visible', TRUE)->order_by('reference ASC, libelle ASC')->get();
        }
        else
        {
            return $produit->where('visible', TRUE)->order_by('reference ASC, libelle ASC')->get();
        }
    }
    
	/**
     * Retourne les produits visibles
     * 
	 * @param	array $conditions
     * @return	object
     */
    public function visibles($conditions = array())
    {
    	if ( ! empty($conditions['champ']))
		{
			$this->order_by($conditions['champ'], $conditions['ordre']);
		}
		else
		{
			$this->where($conditions);
		}
		
    	return $this->where('visible', TRUE)->get();
    }
    
    /**
     * Retourne la catégorie de plus haut niveau d'un produit
     * Utile pour construire le fil d'ariane du produit
     * 
     * @return	object
     */
    public function categorie_referente()
    {
    	if ($this->id)
    	{
	    	$produit = new Produit();
			$produit->include_related('module_catalogue', array('categorie_racine'), FALSE)->get_by_id($this->id);
			$categories = $this->categorie->where('root_id', $produit->categorie_racine)->get();
			
			$categorie_referente = NULL;
			$level = 0;
			
			if($categories->result_count() > 1)
			{
			    foreach ($categories as $categorie)
			    {
			        if ($categorie->level() > $level)
			        {
			            $categorie_referente = $categorie;
			            $level = $categorie->level();
			        }
			    }
			}
			else
			{
			    foreach ($categories as $categorie)
			    {
			        if ($categorie->level() >= $level)
			        {
			            $categorie_referente = $categorie;
			            $level = $categorie->level();
			        }
			    }
			}
			
			
			return $categorie_referente;
    	}
    }

	/**
	 * Retourne les critères du produit
	 * 
	 * @param	object	$produit
	 * @return	array
	 */
	public function lister_critere_produit($produit)
	{
		$criteres = new Critere();
		$criteres_produit = $criteres->produit($produit);
		
		//Critères
		$liste_criteres = array();
		
		//Initialisation des variables de template
		foreach($criteres as $critere)
		{
			$liste_criteres[$critere->id] = array(
				'libelle' 	=> NULL,
				'valeur'	=> NULL,
				'media'		=> NULL
			);
		}
		
		//Critères produit
		if ( ! is_null($criteres_produit) && ($criteres_produit->result_count() > 0))
		{
			foreach ($criteres_produit as $critere_produit)
			{
				$liste_criteres[$critere_produit->id] = array(
					'libelle' 	=> $critere_produit->libelle,
					'valeur' 	=> $critere_produit->criteres_produit_valeur,
				);
			}
		}
		return $liste_criteres;
	}

	/**
     * Retourne si le produit est visible ou non
     * 
     * @return	boolean
     */
	public function visible()
    {
    	if ($this->id)
		{
			$produit = new Produit();
			$module  = $this->module_catalogue->get();
			
			if ( ! $module->afficher_produits_stock_nul)
			{
				$produit->where('stock > ', $module->seuil_alerte_stock);
			}
			
    		return $produit->where('id', $this->id)->where('visible', TRUE)->get()->exists();
		}
		return FALSE;
    }
	
    /**
     * Retourne un tableau de résultat avec un seul champ
     * 
     * @param 	string $champ
     * @return	array
     */
    public function to_single_array($champ)
    {
    	$resultats = array();
    	
    	foreach ($this as $item)
    	{
    		array_push($resultats, $item->$champ);
    	}
    	return $resultats;
    }
	
	/**
	 * Retourne les références avec conditions par critères/catégorie
	 * 
	 * @param	array
	 * @return	object
	 */
	private function _references($conditions)
	{
		$produit_related = new Produit();
		
		$produit_related->has_many('produits_produit', array(
			'class' => 'produits_produit',
			'join_other_as' => 'produit',
			'join_self_as' => 'related_produit',
		));
		$produit_related->select('t_produits_produits.produit_id');
		$produit_related->where_related('produits_produit', 'enfant', 1);
		
		//Visible
		if (isset($conditions['visible']) && ($conditions['visible'] == TRUE))
		{
			$produit_related->where('visible', TRUE);
		}
		
		//En stock
		if (isset($conditions['stock']))
		{
			$produit_related->where('stock > ', $conditions['stock']);
		}
		
		//Catégorie
		if (isset($conditions['categorie']))
		{
			$produit_related->where_related('categorie', 'id', $conditions['categorie']);
		}
		
		//Lisaion pour la fonction EXISTS
		$produit_related->where('t_produits.id', 't_produits_produits.produit_id', FALSE);
		
		//Critères
		$i = 1;
		
		foreach ($conditions['criteres'] as $critere_id => $valeurs)
		{
			$produit_related->has_many('criteres_produit_'.$i, array(
				'class' => 'criteres_produit',
				'join_other_as' => 'criteres_produit'
			));
			
			$produit_related->group_start()->where_related('criteres_produit_'.$i, 'critere_id', $critere_id);
			
			if (is_array($valeurs))
			{
				$produit_related->group_start();
				
				foreach ($valeurs as $valeur)
				{
					$produit_related->or_where('criteres_produit_'.$i.'_t_criteres_produits.valeur', $valeur);
				}
				
				$produit_related->group_end();
			}
			else
			{
				$produit_related->where('criteres_produit_'.$i.'_t_criteres_produits.valeur', $valeurs);
			}
			
			$produit_related->group_end();
			
			$i++;
		}
		$produit_related->group_by('produit_id');
		
		return $produit_related;
	}
	
	/**
	 * Ajouter les conditions à une requête sur les produits
	 * 
	 * @param	array 	$conditions
	 * @return	object
	 */
	private function _conditions($conditions)
	{
		$produit = new Produit();

		//Sélection
		if (isset($conditions['selection']))
		{
			$produit->select($conditions['selection']);
		}
		
		//Filtre par critères
		if (isset($conditions['criteres']))
		{
		    //Par valeurs
		    if (is_array($conditions['criteres']))
		    {
		        $i = 1;
		        
		        //Supprime le critère "Tous" associé à l'ID 0
		        if (is_array($conditions['criteres']) && (($key = array_search(0, $conditions['criteres'])) !== FALSE))
		        {
		            unset($conditions['criteres'][$key]);
		        }
		        
		        foreach ($conditions['criteres'] as $critere_id => $valeurs)
		        {
		            $produit->has_many('criteres_valeurs_produit_'.$i, array(
		                'class' => 'criteres_valeurs_produit',
		                'join_other_as' => 'criteres_valeurs_produit'
		            ));
		            
		            $critere_cours = new Critere();
		            $critere_cours->get_by_id($critere_id);
		            
		            $criteres_sim = new Critere();
		            $criteres_finaux = $criteres_sim->where('libelle', $critere_cours->libelle)->get()->all_to_single_array('id');
		            $criteres_finaux = array_values($criteres_finaux);
		            
		            $produit->group_start()->include_related('criteres_valeurs_produit_'.$i)->where_in('criteres_valeurs_produit_'.$i.'_t_criteres_valeurs_produits.critere_id', $criteres_finaux);
		            
		            if (is_array($valeurs))
		            {
		                $produit->group_start();
		                
		                foreach ($valeurs as $valeur)
		                {
		                    $valeur_cours = new Criteres_valeur();
		                    $valeur_cours->get_by_id($valeur);
		                    
		                    $val_sim = new Criteres_valeur();
		                    $valeurs_finales = $val_sim->where('valeur', $valeur_cours->valeur)->get()->all_to_single_array('id');
		                    $valeurs_finales = array_values($valeurs_finales);
		                    $produit->or_where_in('criteres_valeurs_produit_'.$i.'_t_criteres_valeurs_produits.criteres_valeur_id', $valeurs_finales);
		                }
		                
		                $produit->group_end();
		            }
		            else
		            {
		                $produit->has_many('criteres_valeur_'.$i, array(
		                    'class' => 'criteres_valeur',
		                    'join_other_as' => 'criteres_valeur'
		                ));
		                $produit->or_where('criteres_valeurs_produit_'.$i.'_t_criteres_valeurs_produits.criteres_valeur_id', $valeurs);
		            }
		            $produit->group_end();
		            
		            /*$produit->include_related('criteres_valeurs_produit')->group_start();
		             $produit->where('critere_id', $critere_id)->where_in('criteres_valeur_id',$valeurs);
		             $produit->group_end();*/
		            
		            $i++;
		        }
		    }
		    //Par ID
		    else
		    {
		        $produit->where_related('criteres_valeurs_produit_', 'critere_id', $conditions['criteres']);
		    }
		}
		
		//Catégorie
		if (isset($conditions['categorie']))
		{
			$produit->where_related('categorie', 'id', $conditions['categorie']);
		}
		
		//Par module
		if (isset($conditions['module_id']))
		{
			$produit->where('module_catalogue_id', $conditions['module_id']);
		}
		
		//Produits remisés
		if (isset($conditions['remise']))
		{
			$produit->include_related('tarif')->where('remise >', 0)->or_where('prix_remise >',0);
		}
		
		//Visible
		if (isset($conditions['visible']) && ($conditions['visible'] == TRUE))
		{
			$produit->where('visible', TRUE);
		}
		
		//Accueil
		if (isset($conditions['accueil']) && ($conditions['accueil'] == TRUE))
		{
			$produit->where('accueil', TRUE);
		}
		
		//Important
		if (isset($conditions['important']) && ($conditions['important'] == TRUE))
		{
			$produit->where('important', TRUE);
		}
		
		//Nouveauté
		if (isset($conditions['nouveau']) && ($conditions['nouveau'] == TRUE))
		{
			$produit->where('nouveau', TRUE);
		}
		
		//Indexé
		if (isset($conditions['indexe']) && ($conditions['indexe'] == TRUE))
		{
			$produit->where('indexe', TRUE);
		}
		
		//En stock
		if (isset($conditions['stock']) && ($conditions['stock'] !== FALSE))
		{
			$produit->where('stock > ', $conditions['stock']);
		}

		//Recherche
		if (isset($conditions['recherche']) && is_array($conditions['recherche']) && ! empty($conditions['recherche']))
		{
			$produit->group_start();
			
			foreach ($conditions['recherche'] as $champ => $valeur)
			{
				$produit->or_like($champ, $valeur);
			}
			
			$produit->group_end();
		}
		
		//Enfants
		if (isset($conditions['enfants']))
		{
			if ($conditions['enfants'] == TRUE)
			{
				$produit->where_related('produits_produit', 'enfant', 1);
			}
			
			$produits_lies = new Produits_produit();
			$produits_lies->select('related_produit_id')->where('enfant', 1);
			
			$produit->where_not_in_subquery('id', $produits_lies);
		}		
		
		return $produit;
	}
	
	/**
	 * Requête commune pour l'ordre des produits
	 * 1- important
	 * 2- ordre
	 * 3- libellé
	 *
	 * @return	object
	 */
	private function _ranger()
	{
	    return $this->order_by('important DESC, ordre ASC, libelle ASC');
	}
	
	/**
	 * Retourne la réécriture d'une URL
	 *
	 * @param 	object	$produit
	 * @return 	string
	 */
	private function _reecriture($produit)
	{
	    $libelle = ( ! empty($produit->seo_reecriture)) ? $produit->seo_reecriture : $produit->libelle;
	    
	    return url_title($libelle);
	}
	
	/**
	 * Retourne un dispo produit lié au produit
	 *
	 *
	 * @return	Object or FALSE
	 */
	public function has_dispo_produit()
	{
	    
	    if($this->id)
	    {
	        $date = new DateTime();
	        $date_now = $date->format('Y-m-d');
	        
	        $dispo_produit = new Dispo_produit();
	        $dispo_produit->where('produit_id', $this->id);
	        $dispo_produit->where_related('module_disponible', 'date_fin >=', $date_now)->where('date_deb <=', $date_now);
	        $dispo_produit->limit(1)->get();
	        
	        if($dispo_produit->exists())
	            return($dispo_produit);
	            
	    }
	    return FALSE;
	}
	
	/**
	 * Retourne la note du produit
	 *
	 *
	 * @return	int or FALSE
	 */
	public function note()
	{
	    
	    if($this->id)
	    {
	        
	        $i = 0;
	        $note = 0;
	        
	        foreach ($this->commentaires() as $commentaire)
	        {
	            
	            if ($commentaire->afficher)
	            {
	                $i++;
	                $note += $commentaire->note;
	            }
	        }
	        if ($i > 0)
	        {
	            $note = round($note/$i, 1);
	        }
	        return $note;
	        
	    }
	    return FALSE;
	}
	
	/**
	 * Retourne le nombre d'avis/commentaires
	 *
	 *
	 * @return	int or FALSE
	 */
	public function nb_note()
	{
	    
	    if($this->id)
	    {
	        
	        $i = 0;
	        foreach ($this->commentaires() as $commentaire)
	        {
	            if ($commentaire->afficher)
	            {
	                $i++;
	            }
	        }
	        return $i;
	        
	    }
	    return FALSE;
	}
	
	/**
	 * Retourne les commentaires associés
	 *
	 * @param 	number $offset
	 * @param 	number $item_par_page
	 * @return	object
	 */
	public function commentaires($offset = 0, $item_par_page = 0)
	{
	    if ($this->id)
	    {
	        $commentaire = new Commentaire();
	        return $commentaire->where('produit_reference', $this->reference)->where('afficher', 1)->get();
	    }
	}
	
	/**
	 * Retourne le nombre de commentaires par note
	 *
	 * @param 	number $offset
	 * @param 	number $item_par_page
	 * @return	object
	 */
	public function commentaires_par_note()
	{
	    if ($this->id)
	    {
	        $array_commentaires_par_note = NULL;
	        for($i=5;$i>=1;$i--)
	        {
	            $commentaire = new Commentaire();
	            $nb_note = $commentaire->where('produit_reference', $this->reference)->where('afficher', 1)->where('note', $i)->count();
	            
	            $array_commentaires_par_note[$i] = $nb_note;
	        }
	        
	        return $array_commentaires_par_note;
	    }
	    return FALSE;
	}
}

/* End of file produit.php */
/* Location: ./modules/catalogue/models/produit.php */