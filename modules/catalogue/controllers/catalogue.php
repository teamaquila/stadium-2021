<?php
require_once APPPATH.'controllers/front.php';

/**
 * Catalogue
 * 
 * @author 		Pauline Martin, Pierre-Etienne Raby
 * @package		catalogue
 * @category	Controllers
 * @version 	2.0
 */
class Catalogue extends Front
{
	public $url_section;
	
	private $_section;
	private $_module;
	private $_categorie;
	
	/**
	 * Constructeur
	 */
	public function __construct()
	{
	    parent::__construct();
	    
	    $this->load->config('catalogue/catalogue');
	    $this->lang->load('catalogue/catalogue');
	    $this->load->config('catalogue/recherche');
	    $this->load->libraries(array('cart', 'pagination', 'email'));
	    $this->load->helpers(array('catalogue/catalogue', 'categories/categorie'));
	    $this->load->models(array(
	        'section', 'catalogue/module_catalogue', 'catalogue/produit', 'catalogue/produits_produit', 'catalogue/tarif',
	        'medias/media', 'categories/categorie', 'catalogue/critere', 'catalogue/criteres_valeur', 'catalogue/criteres_valeurs_produit', 'catalogue/commentaire'
	    ));
	    
	    $this->load->modules(array(
	        'boutique/clients'
	    ));
	}
	
	/**
	 * Point d'entrée du catalogue
	 * 
	 * @param	integer	$section_id
	 * @return	VIEW
	 */
	public function index($section_id)
	{
	    $this->_section = $this->section->get_by_id($section_id);
	    $this->_module 	= $this->_section->module_catalogue->get();
	    $this->_categorie = NULL;
		
	    
	    //POUR SEO SUITE NOUVEAU SITE ON VERIFIE SI LA REQUETE N'EST PAS UNE URL CREEE POUR UNE ANCIENNE CATEGORIE
	    $custom_url = $this->config->item('oldcat');
	    if(array_key_exists($_SERVER['REQUEST_URI'], $custom_url))
	    {
	        $_GET['filtre'] = $custom_url[$_SERVER['REQUEST_URI']];
	    }
	    
	    //Catégorie
	    if ( ! is_null($this->_categorie_id()))
	    {
	        $this->_categorie = $this->categorie->get_by_id($this->_categorie_id());
	    }
	    elseif ($this->_module->categorie_racine > 0)
	    {
	        $this->_categorie = $this->categorie->where(array(
	            'root_id' => $this->_module->categorie_racine,
	            'left_id' => 1
	        ))->get();
	    }
		
	    //Impose le type 4 pour le moteur de recherche
	    if ($this->input->get('recherche'))
	    {
	        $this->_module->type_catalogue = 2;
	    }
	    
	    //Produits par page
	    if (($item_par_page = $this->input->get('item_par_page')) && $item_par_page < 50)
	    {
	        $this->_item_par_page = $item_par_page;
	    }
	    else
	    {
	        $this->_item_par_page = $this->_module->item_par_page;
	    }
	    
	    //Fiche produit
	    if ( ! is_null($this->_id()))
	    {
	        $contenu = $this->_produit();
	        $seo = $this->_seo_produit();
	    }
	    //Liste
	    else
	    {
	        $custom_url = $this->config->item('oldcat_titleseo');
	        if(array_key_exists($_SERVER['REQUEST_URI'], $custom_url))
	        {
	            $title = $custom_url[$_SERVER['REQUEST_URI']];
	        }
	        else
	        {
	            if($this->_section->seo_titre)
	            {
	                $title = $this->_section->titre;
	            }
	            else
	            {
	                $title = $this->_section->libelle;
	            }
	        }
	        
	        $custom_meta = $this->config->item('oldcat_metaseo');
	        if(array_key_exists($_SERVER['REQUEST_URI'], $custom_meta))
	        {
	            $meta = $custom_meta[$_SERVER['REQUEST_URI']];
	        }
	        else
	        {
	            $meta = $this->_section->seo_description;
	        }
	        
	        
	        $seo = meta_tag(array(
	            'title' => $title,
	            'keywords' => $this->_section->seo_mots_cles,
	            'description' => $meta,
	            'robots' => ($this->_section->indexee) ? 'index, follow' : 'noindex, nofollow'
	        ));

	        //Produits par Catégories, seulement si on est pas a la catégorie racine, affichage de tous les produits
	        if ( ! is_null($this->_categorie) && ($this->_categorie->left_id != 1))
	        {
	            $seo = $this->_seo_categorie();
	            $this->_url_pagination = $this->_categorie->url($this->_section);
	        }
	        //Produits
	        else
	        {
	            $this->_url_pagination = construire_url(array(
	                'section' => $this->_section->segment_url()
	            ));
	        }
	        switch ($this->_module->type_catalogue)
	        {
	            case 1:
	                $contenu = $this->_lister_par_categorie_deroule();
	                $seo = $this->_seo_categorie();
	                break;
	                
	                
	            case 2:
	                $contenu = $this->_lister_produits();
	                break;
	                
	            default: redirect(404); break;
	        }
	        
	        
	    }
	    
	    $this->_afficher('catalogue/page', array(
	        'fil_ariane'=> $this->_fil_ariane(),
	        'module'	=> $contenu,
	        'metas'		=> $seo
	    ));
	}
	
	/**
	 * Affiche la liste des produits
	 * TYPE 2
	 * 
	 * @return	VIEW
	 */
	protected function _lister_produits()
	{		

	    $total_produit = $this->produit->compter($this->_conditions());	

		$suffixe = (query_string()) ? '?'.query_string() : '';
		//Pagination
		
		$custom_url = $this->config->item('oldcat');
		if(array_key_exists($_SERVER['REQUEST_URI'], $custom_url))
		{
		    $suffixe = '?filtre='.$custom_url[$_SERVER['REQUEST_URI']];
		}

		//URL section
		$this->pagination->initialize(array(
				'base_url'		=> base_url().rtrim($this->_url_pagination, '.html'),
				'first_url'		=> base_url().$this->_url_pagination.$suffixe,
				'total_rows'	=> $total_produit,
		        'per_page'		=> $this->_module->item_par_page,
				'cur_page'		=> $this->_page(),
				'prefix'		=> 'page,',
				'suffix'		=> $suffixe,
				'uri_segment'	=> 0,
		        'num_links'     => 7
		));

		$texte_seo = '';
		if(array_key_exists($_SERVER['REQUEST_URI'], $this->config->item('oldcat_seo')))
		{
		    $texte_seo = $this->config->item('oldcat_seo')[$_SERVER['REQUEST_URI']];
		}
		
		$titre_seo = '';
		if(array_key_exists($_SERVER['REQUEST_URI'], $this->config->item('oldcat_titre')))
		{
		    $titre_seo = $this->config->item('oldcat_titre')[$_SERVER['REQUEST_URI']];
		}

		$data = array(
			'section'		=> $this->_section,
			'module'		=> $this->_module,
			'pagination'	=> $this->pagination->create_links(),
			'fil_ariane'	=> $this->_fil_ariane(),
			'filtres'		=> array(
				'categories' 	=> $this->_filtrer_categories(),
			    'criteres' 		=> ($this->_module->afficher_criteres_liste) ? $this->_filtrer_criteres_opt() : NULL
			),
			'tri'			=> $this->_tri(),
			'total_produit'	=> $total_produit,
			'categorie'		=> $this->_categorie,
		    'texte_seo'     => $texte_seo,
		    'produits'		=> $this->produit->lister($this->_conditions(), $this->_page(), $this->_module->item_par_page),
			'image_section'	=> $this->_get_image_section($this->_section->id),
			'fil_ariane'	=> $this->_fil_ariane($titre_seo),
		    'titre_seo'     => $titre_seo,
		);
       	
		
		return $this->load->view('catalogue/liste_produits', $data, TRUE);
	}
	
	/**
	 * Affiche les options de tri
	 *
	 * @return	VIEW
	 */
	protected function _tri()
	{
	    $data = array(
	        'url' => ( ! is_null($this->_categorie)) ? $this->_categorie->url($this->_section) : '/'.uri_string(),
	        'section'	=> $this->_section,
	        'module'	=> $this->_module
	    );
	    
	    return $this->load->view('catalogue/trier', $data, TRUE);
	}
	
	/**
	 * Affiche la liste des produits par catégorie
	 * TYPE 2
	 * 
	 * @return 	VIEW
	 */
	private function _lister_par_categorie()
	{
		$total_produit = $this->produit->compter($this->_conditions());
		$suffixe = (query_string()) ? '?'.query_string() : '';
		
		//Pagination
		$this->pagination->initialize(array(
			'base_url'		=> $this->_categorie->url($this->_section),
			'first_url'		=> $this->_categorie->url($this->_section).$suffixe,
			'total_rows'	=> $total_produit,
			'per_page'		=> $this->_module->item_par_page,
			'cur_page'		=> $this->_page(),
			'prefix'		=> 'page,',
			'suffix'		=> $suffixe,
			'uri_segment'	=> 0
		));
		
		$data = array(
			'section'		=> $this->_section,
			'module'		=> $this->_module,
			'pagination'	=> $this->pagination->create_links(),
			'produits'		=> $this->produit->lister($this->_conditions(), $this->_page(), $this->_module->item_par_page),
			'filtres'		=> array(
				//'categories'=> $this->_filtrer_categories(),
				'criteres' 	=> $this->_filtrer_criteres()
			),
			'tri'			=> $this->trier(),
			'total_produit'	=> $total_produit,
			'categorie'		=> $this->_categorie,
			'fil_ariane'	=> $this->_fil_ariane()
		);
		
		$data['liste'] = $this->load->view('catalogue/liste_type_2_ajax', $data, TRUE);
		
		return $this->load->view('catalogue/liste_type_2', $data, TRUE);
	}
	
	/**
	 * Affiche la liste des enfants de la catégorie
	 * TYPE 1
	 * 
	 * @return 	VIEW
	 */
	private function _lister_par_categorie_parent()
	{
		$categories_enfant = $this->categorie->enfants($this->_categorie->id);
		
		if (count($categories_enfant) == 0)
		{
			//Pagination
			$this->pagination->initialize(array(
				'base_url'		=> $this->_categorie->url($this->_section),
				'total_rows'	=> $this->produit->compter($this->_conditions()),
				'per_page'		=> $this->_module->item_par_page,
				'cur_page'		=> $this->_page(),
				'prefix'		=> 'page,',
				'suffix'		=> '.html',
				'uri_segment'	=> 0
			));
			
			$data = array(
				'section'		=> $this->_section,
				'module'		=> $this->_module,
				'pagination'	=> $this->pagination->create_links(),
				'produits'		=> $this->produit->lister($this->_conditions(), $this->_page(), $this->_module->item_par_page),
				'filtres'		=> array(
					'criteres' 	=> $this->_filtrer_criteres(),
					//'categories'=> $this->_filtrer_categories()
				),
				'categorie_parent'	=> $this->_categorie,
				'fil_ariane'		=> $this->_fil_ariane(),
			);
			
			return $this->load->view('catalogue/liste_type_1_produits', $data, TRUE);
		}
		else
		{
			$data = array(
				'section'			=> $this->_section,
				'module'			=> $this->_module,
				'categorie_parent'	=> $this->_categorie,
				'categories'		=> $categories_enfant,
				'fil_ariane'		=> $this->_fil_ariane(),
				'conditions'		=> $this->_conditions()
			);
			
			return $this->load->view('catalogue/liste_type_1_categories', $data, TRUE);
		}
	}
	
	/**
	 * Affiche la liste des produits classés par catégorie
	 * TYPE 3
	 * 
	 * @return 	VIEW
	 */
	private function _lister_parent_enfant_produit()
	{
		$data = array(
			'section'			=> $this->_section,
			'module'			=> $this->_module,
			'categorie_parent'	=> $this->_categorie,
			'categories'		=> $this->categorie->arbre($this->_categorie->root_id, NULL, FALSE),
			'fil_ariane'		=> $this->_fil_ariane(),
			'conditions'		=> $this->_conditions()
		);
		
		return $this->load->view('catalogue/liste_type_3', $data, TRUE);
	}
	
	/**
	 * Affiche la fiche du produit
	 * 
	 * @return	VIEW
	 */
	private function _produit()
	{		
		//$this->_produit = $this->_visible();
		
		$visible = ($this->_visible()->exists()) ? TRUE : FALSE;
		
		$this->_produit = $this->produit->where('id', $this->_id())->get();
		
		$formulaire = FALSE;
		/*Test si contient un article composé gravure pour affichage du formulaire de gravure ou non*/
		$produit_gravure = $this->db->query("
						SELECT t_produits.id, t_produits.reference
						FROM t_produits
						INNER JOIN t_produits_produits ON t_produits.id = t_produits_produits.related_produit_id
						WHERE t_produits_produits.compose = 1
						AND t_produits.reference LIKE 'GRV%'
						AND t_produits_produits.produit_id = ".$this->_produit->id."
					");
		
		if ($produit_gravure->num_rows() != 0)
		{
		    foreach ($produit_gravure->result() as $row)
		    {
		        $formulaire = $row->reference;
		    }
		}

		if ($this->_produit->exists())
		{
		    
		    $data = array(
		        'visible'			=> $visible,
		        'section' 			=> $this->_section,
		        'module'			=> $this->_module,
		        'produit' 			=> $this->_produit,
		        'categorie'			=> $this->_produit->categorie_referente(),
		        'criteres_associes'	=> $this->_produit->criteres_associes(),
		        'produits_associes_enfants'	=> $this->_produits_associes($this->_produit, TRUE),
		        'produits_associes'			=> $this->_produits_associes($this->_produit),
		        'produits_complement'		=> $this->_produits_associes($this->_produit, FALSE, TRUE),
		        //'produits_imposes'			=> $this->_produits_associes($produit, FALSE, FALSE, TRUE),
		        //'produits_composes'			=> $this->_produits_associes($produit, FALSE, FALSE, FALSE, TRUE),
		        'panier'			=> modules::run('boutique/boutique/bouton_ajouter', $this->_produit, NULL, $formulaire),
		        'note'				=> $this->_note_produit($this->_produit),
		        'commentaire'		=> $this->_commentaire($this->_produit)/*.$this->_formulaire($this->_produit)*/,
		        'image_section'	=> $this->_get_image_section($this->_section->id),
		        'fil_ariane'	=> $this->_fil_ariane(),
		        'criteres_affiches' => $this->_get_critere_affiche($this->produit),
		    );
		    return $this->load->view('catalogue/fiche', $data, TRUE);
		}
		show_404();
		
	}
	
	/**
	 * Affiche la liste des produits associés
	 * 
	 * @param 	object 	$produit
	 * @param	boolean	$enfant
	 * @return	VIEW
	 */
	private function _produits_associes($produit, $enfant = FALSE, $complement = FALSE, $impose = FALSE, $compose = FALSE)
	{
		$data = array(
			'module' => $this->_module,
			'produit_parent' => $produit,
			'produits' => ($enfant) ? $produit->produits_enfants() : $produit->produits_associes($complement, $impose, $compose),
			'enfant'	=> $enfant,
			'complement'=> $complement,
			'impose'	=> $impose,
			'compose'	=> $compose
		);
		
		return $this->load->view('catalogue/produits_associes', $data, TRUE);
	}
	
	
	/**
	 * Référencement du produit
	 *
	 * @return	array
	 */
	protected function _seo_produit()
	{
	    $produit = $this->produit->get_by_id($this->_id());
	    
	    if ($produit->exists())
	    {
	        $description = ($produit->seo_description) ? $produit->seo_description : $produit->description;
	        
	        return meta_tag(array(
	            'title' 		=> ($produit->seo_titre) ? $produit->seo_titre : $produit->libelle,
	            'keywords' 		=> $produit->seo_mots_cles,
	            'description' 	=> truncate($description, 256, NULL),
	            'robots' 		=> 'index, follow'
	        ));
	    }
	    return FALSE;
	}
	
	/**
	 * Référencement de la catégorie
	 *
	 * @return 	array
	 */
	protected function _seo_categorie()
	{
	    $description = ($this->_categorie->seo_description) ? $this->_categorie->seo_description : $this->_categorie->contenu;
	    
	    return meta_tag(array(
	        'title' 		=> ($this->_categorie->seo_titre) ? $this->_categorie->seo_titre : $this->_categorie->libelle,
	        'keywords' 		=> $this->_categorie->seo_mots_cles,
	        'description' 	=> truncate($description, 256, NULL),
	        'robots' 		=> 'index, follow'
	    ));
	}
	
	/**
	 * Fil d'ariane du catalogue
	 *
	 * @see 	Front::_fil_ariane()
	 * @return	VIEW
	 */
	protected function _fil_ariane($titre_seo = '')
	{
	    //Section
	    $section_url	= $this->_section->segment_url();
	    $categorie_id 	= NULL;
	    
	    //Catégorie du produit
	    if ($this->_id())
	    {
	        $categorie = $this->_produit->categorie_referente();
	    }
	    //Catégorie
	    else
	    {
	        $categorie = $this->_categorie;
	    }
	    
	    //Par défaut, l'accueil est ajouté au fil
	    $urls = array(
	        anchor('/', lang('section_accueil'), array('class' => 'accueil'))
	    );
	    
	    //Catégories catalogue
	    if ( ! is_null($categorie))
	    {
	        $enfants 	= array();
	        $left_id 	= $categorie->left_id;
	        $root_id	= $categorie->root_id;
	        $categorie_url = $categorie->url();
	        
	        //URL de la section du module
	        $urls[] = anchor(construire_url(array(
	            'section' => $this->_section->segment_url()
	        )), $this->_section->libelle, array('class' => 'catalogue'));
	        
	        //Arborescence inversée depuis la catégorie de référence
	        $categories_precedentes = new Categorie();
	        $noeud = $categories_precedentes->select_root($root_id)->get_root()->get_node_where_left($left_id);
	        $level = $noeud->level();
	        
	        for ($i = 1; $i < $level; $i++)
	        {
	            $parent = $noeud->get_parent();
	            $enfants[$parent->id] = $parent->libelle;
	        }
	        $enfants = array_reverse($enfants, TRUE);
	        
	        //Construit les urls des enfants
	        foreach ($enfants as $id => $libelle)
	        {
	            $categorie_enfant = new Categorie();
	            $categorie_enfant->get_by_id($id);
	            
	            $urls[] = anchor(construire_url(array(
	                'section' 	=> $section_url,
	                'module'	=> $categorie_enfant->url()
	            ), ''), $libelle, array('id' => 'categorie_'.$categorie_enfant->id));
	        }
	        
	        //Ajoute la catégorie de référence à la fin de l'url
	        if(!($categorie->libelle === $this->_section->libelle)){
	            $urls[] = anchor(construire_url(array(
	                'section' 	=> $section_url,
	                'module'	=> $categorie_url
	            ), ''), $categorie->libelle, array('class' => 'active'));
	        }
	    }
	    //Section
	    else
	    {
	        $urls[] = anchor(construire_url(array('section' => $section_url)), $this->_section->libelle);
	    }
	    
	    if($titre_seo != '')
	    {
	        $urls[] = anchor(current_url(), $titre_seo);
	    }
	    
	    return $this->load->view('page/fil_ariane', array('fil_ariane' => $urls), TRUE);
	}
	
	/**
	 * Moteur de recherche des produits
	 * 
	 * @return	JSON
	 */
	public function rechercher()
	{
		if ($this->input->get('requete'))
		{
			$data = array();
			
			$conditions = array(
				'enfants'	=> FALSE,
				'recherche' => $this->_conditions_recherche($this->input->get('requete')),
				'visible' 	=> TRUE,
				'group_by'	=> $this->config->item('group_by')
			);
			
			$stock = $this->input->get('s');
			if ($stock =! 'n'){
				$conditions['stock'] = TRUE;
			}
			
			$produits = $this->produit->lister($conditions, 0, 10);

			/*echo $this->load->view('contenu_rechercher', array('produits' => $produits), TRUE);	
			return;*/
			
			foreach ($produits as $produit)
			{
				array_push($data, array(
						'name'  => $produit->libelle,
						'label' => $produit->libelle,
      				  	'sous_label' => $produit->libelle2,
						'value' => $produit->libelle
				));
			}
			echo json_encode($data);
			return;
		}
		else
		{
			redirect('/');
		}
	}
	
	/**
	 * Retourne les infos d'une référence d'un produit
	 * 
	 * @param	integer	$produit_id
	 * @return	JSON
	 */
	public function reference($produit_id)
	{
		if ($this->input->is_ajax_request())
		{
			$produit = $this->produit->get_by_id($produit_id);
			
			if ($produit->exists())
			{
				

				$client = NULL;
				if ($compte = json_decode($this->encrypt->decode(get_cookie('client'))))
				{
					$this->load->model('boutique/client');
					$client = $this->client->get_by_id($compte->client_id);
						
					if ( ! $client->exists())
					{
						$client = NULL;
					}
				}
				
				$tarif 	= $produit->tarif($client);
				$module = $produit->module();
				
				$formulaire = FALSE;
				
				/*Test si contient un article composé gravure pour affichage du formulaire de gravure ou non*/
				$produit_gravure = $this->db->query("
						SELECT t_produits.id
						FROM t_produits
						INNER JOIN t_produits_produits ON t_produits.id = t_produits_produits.related_produit_id
						WHERE t_produits_produits.compose = 1
						AND t_produits.reference LIKE 'GRV%'
						AND t_produits_produits.produit_id = ".$produit->id."
					");
					
				if ($produit_gravure->num_rows() != 0)
				{
					$formulaire = TRUE;
				}
				
				
				/*Test si contient au moins un article composé, on calcule son prix en fonction des composants*/
				$produits_composes = $this->db->query("
						SELECT t_produits.id
						FROM t_produits
						INNER JOIN t_produits_produits ON t_produits.id = t_produits_produits.related_produit_id
						WHERE t_produits_produits.compose = 1
						AND t_produits_produits.produit_id = ".$produit->id."
					");
					
				if ($produits_composes->num_rows() != 0)
				{
					$tarif['prix']	= 0;
					foreach ($produits_composes->result() as $row)
					{
						$prod_compo = new Produit();
						$prod_compo->get_by_id($row->id);
						$tarif['prix'] +=  $prod_compo->tarif($client)['prix'];
						$tarif['prix_sans_remise'] +=  $prod_compo->tarif($client)['prix'];
					}
				}
				
				
				//Images
				$index_diapo =0;
				$diaporama = FALSE;
				$thumbnail = FALSE;
				if ($produit->medias_associes()->exists())
				{					
				    $alt_images = 'Acheter '.$produit->libelle.' Stadium ';
					$diaporama = '<ul class="diaporama_produit">';
					$thumbnail = '<div id="bx-pager">';
					foreach ($produit->medias_associes() as $media)
					{
					    $diaporama .= '<li><img itemprop="image" src="/medias/images/size/'.$module->taille_image_fiche.'/'.$media->dossier.$media->fichier.'" alt="'.$alt_images.'"></li>';
					    $thumbnail .= '<span><a href="javascript:void(0)" data-slide-index="'.$index_diapo.'"><img src="/medias/images/size/80x70/'.$media->dossier.$media->fichier.'"  alt="'.$alt_images.'"></a></span>';
					    
					    $index_diapo ++;
					}
					$thumbnail .= '</div>';
					$diaporama .= '</ul>';
				}
				elseif ($produit->produit_parent()->medias_associes()->exists())
				{	
				    $alt_images = 'Acheter '.$produit->libelle.' Stadium ';
					$diaporama = '<ul class="diaporama_produit">';
					$thumbnail = '<div id="bx-pager">';
					foreach ($produit->produit_parent()->medias_associes() as $media)
					{
					    $diaporama .= '<li><img itemprop="image" src="/medias/images/size/'.$module->taille_image_fiche.'/'.$media->dossier.$media->fichier.'" alt="'.$alt_images.'"></li>';
					    $thumbnail .= '<span><a href="javascript:void(0)" data-slide-index="'.$index_diapo.'"><img src="/medias/images/size/80x70/'.$media->dossier.$media->fichier.'"  alt="'.$alt_images.'"></a></span>';
					    
					    $index_diapo ++;
					}
					$thumbnail .= '</div>';
					$diaporama .= '</ul>';
				}
				
				
				
				echo json_encode(array(
					'libelle' 		=> $produit->libelle,
					'stock'			=> statut_stock($produit->stock, $module->seuil_alerte_stock),
					'prix' 			=> array(
						'vente' 	=> html_price($tarif['prix_sans_remise']).nbs().'HT',
					    'achat' 	=> html_price($tarif['prix']).nbs().lang('devise'),
					    'HT'		=> html_price($tarif['prix']).nbs().lang('devise')
					),
					'poids' 		=> number_format($produit->poids, 3).$module->unite_poids,
					'formulaire'	=> $formulaire,
				    'images'		=> $thumbnail.$diaporama,
					'resume'		=> $produit->description
				));
			}
		}
		else
		{
			redirect('/');
		}
	}
	
	/**
	 * Retourne les infos concernant la gravure complémentaire choisie, sur fiche produit 
	 * AJAX
	 */
	public function reference_gravure($produit_id)
	{
		if ($this->input->is_ajax_request())
		{
			$produit = $this->produit->get_by_id($produit_id);
				
			if ($produit->exists())
			{
				$tarif 	= $produit->tarif();
				$module = $produit->module();
		

				$nb_caracteres = $produit->criteres_associes(2);
		
				$data = array(
					'libelle' 		=> $produit->libelle,
					'stock'			=> statut_stock($produit->stock, $module->seuil_alerte_stock),
					'prix' 			=> array(
							'vente' 	=> prix($tarif['prix_sans_remise']).nbs().lang('devise'),
							'achat' 	=> prix($tarif['prix_ht']).nbs().lang('devise').' HT'
					),
					'produit_id'	=> $produit_id,
					'nb_caracteres'	=> $nb_caracteres->valeur,
					'poids' 		=> number_format($produit->poids, 3).$module->unite_poids,
				);
				echo json_encode($data);
				return ;
			}
		}
	}
	
	/**
	 * Retourne le prix en fonction de la quantité
	 * 
	 * @return	boolean
	 */
	public function calculer_prix()
	{
		if ($this->input->is_ajax_request())
		{
			$this->lang->load('boutique/boutique');
			$this->load->model('boutique/client');
			
			$client = NULL;
	    	$quantite = 1;
	    	
			//Si le client est connecté, on peut utiliser sont statut pour le tarif (ex : professionnel)
			$client = NULL;
			if ($compte = json_decode($this->encrypt->decode(get_cookie('client'))))
			{
				$this->load->model('boutique/client');
				$client = $this->client->get_by_id($compte->client_id);
			
				if ( ! $client->exists())
				{
					$client = NULL;
				}
			}
			
			if ($this->input->post('quantite') && is_numeric($this->input->post('quantite')) && $this->input->post('produit_id'))
			{
				$prix_unitaire 	= 0;
				$produits_id	= $this->input->post('produit_id');
				
				if (is_array($produits_id))
				{
					foreach ($produits_id as $produit_id)
					{
						$this->produit->clear();
						$this->produit->get_by_id($produit_id);
						$tarif  = $this->produit->tarif($client,$this->input->post('quantite'));
						$prix_unitaire += $tarif['prix_ht'];
					}
				}
				else
				{
					$this->produit->clear();
					$this->produit->get_by_id($produits_id);
					$tarif = $this->produit->tarif($client, $this->input->post('quantite'));
					$prix_unitaire += $tarif['prix_ht'];
				}
				
				/*Test si contient au moins un article composé, on calcule son prix en fonction des composants*/
				$produits_composes = $this->db->query("
						SELECT t_produits.id
						FROM t_produits
						INNER JOIN t_produits_produits ON t_produits.id = t_produits_produits.related_produit_id
						WHERE t_produits_produits.compose = 1
						AND t_produits_produits.produit_id = ".$this->input->post('produit_id')."
					");
					
				if ($produits_composes->num_rows() != 0)
				{
					$tarif['prix_ht']	= 0;
					foreach ($produits_composes->result() as $row)
					{
						$prod_compo = new Produit();
						$prod_compo->get_by_id($row->id);
						$tarif['prix_ht'] +=  $prod_compo->tarif($client)['prix_ht'];
						$tarif['prix_sans_remise'] +=  $prod_compo->tarif($client)['prix'];
					}
					$prix_unitaire = $tarif['prix_ht'];
				}
				
				
				if ($prix_unitaire > 0)
				{
					$prix_total = $prix_unitaire * $this->input->post('quantite');
					
					$json = array(
						'erreur'=> FALSE,
		    			'unite'	=> $this->load->view('catalogue/prix_unitaire', $tarif, TRUE),
						'total'	=> prix($prix_total).' '.lang('devise')
		    		);
				}
				else
				{
					$json = array(
						'erreur'	=> TRUE,
		    			'message'	=> lang('err_prix_produit'),
						'unite'		=> '',
						'total'		=> ''
		    		);
				}
			}
			else
			{
				$json = array(
					'erreur'	=> TRUE,
	    			'message'	=> lang('err_produit_inexistant')
	    		);
			}
			echo json_encode($json);
		}
		else
		{
			redirect('/');
		}
	}
	
	/**
	 * Trier les produits par nouveautés ou prix
	 * 
	 * @param	string	$champ
	 * @param	string	$ordre
	 * @return	VIEW
	 */
	public function trier($champ = 'nouveau', $ordre = 'desc')
	{
		if ($this->input->post('section_id') && $this->input->post('module_id'))
		{
			$this->_module 	= $this->module_catalogue->get_by_id($this->input->post('module_id'));
			$this->_section = $this->_module->section->get();
			
			//URL
			$url_section = construire_url(array(
				'section' => $this->_section->segment_url()
			));
			$url_pagination = construire_url(array(
				'section' => $this->_section->segment_url()
			), '');
			
			$this->session->set_userdata('tri_catalogue', array(
				'champ' => $champ,
				'ordre'	=> $ordre
			));
			
			$total_produit = $this->produit->compter($this->_conditions());
			
			//Pagination
			$this->pagination->initialize(array(
				'base_url'		=> base_url().$url_section,
				'first_url'		=> base_url().$url_pagination,
				'prefix'		=> 'page,',
				'suffix'		=> '.html',
				'total_rows'	=> $total_produit,
				'per_page'		=> $this->_module->item_par_page,
				'cur_page'		=> $this->_page(),
				'uri_segment'	=> 0,
			));
			
			$data = array(
				'module'	=> $this->_module,
				'produits'	=> $this->produit->lister($this->_conditions(), $this->_page(), $this->_module->item_par_page),
			);
			
			echo json_encode(array(
				'vue' 			=> $this->load->view('catalogue/liste_type_4_ajax', $data, TRUE),
				'pagination' 	=> $this->pagination->create_links(),
				'total_produit' => $total_produit,
			));
			
			return;
		}
		
		$data = array(
			'section'	=> $this->_section,
			'module'	=> $this->_module,
			'selection'	=> $this->session->userdata('tri_catalogue')
		);
		return $this->load->view('catalogue/trier', $data, TRUE);
	}
	
	

	/**
	 * Upload le fichier de logo sur le serveur
	 *
	 * @return	JSON
	 */
	public function envoyer_fichier()
	{
		$this->load->library('upload', array(
				'allowed_types' => 'xls|xlsx',
				'max_size'		=> '3000', //1Mo
				'upload_path' 	=> root_path().'echanges/export/fichiers_gravure/',
				'overwrite'		=> FALSE,
		));
	
		if ($this->upload->do_upload('import_fichier'))
		{
			$data = $this->upload->data();
			$data['file_url'] = base_url().'echanges/export/fichiers_gravure/'.$data['file_name'];
			$data['delete_file'] = base_url().'catalogue/supprimer_fichier_gravure/'.$data['file_name'];
				
			echo json_encode($data);
		}
		else
		{
			echo json_encode(array(
					'erreur' => $this->upload->display_errors('', '')
			));
		}
	}
	
	/**
	 * Supprimer le logo du serveur
	 *
	 * @param	string	$nom_fichier
	 * @return	JSON
	 */
	public function supprimer_fichier_gravure($nom_fichier)
	{
		if (file_exists(root_path().'echanges/export/fichiers_gravure/'.$nom_fichier))
		{
			unlink(root_path().'echanges/export/fichiers_gravure/'.$nom_fichier);
		}
	
		echo json_encode(array());
	}
	
	/**
	 * Filtrer les produits selon catégories 
	 * 
	 * @return	VIEW
	 */
	private function _filtrer_categories()
	{
		$data = array(
			'section'	=> $this->_section,
			'module'	=> $this->_module,
			'categorie'	=> $this->_categorie,
			'conditions'=> $this->_conditions(),
		);
		
		return $this->load->view('catalogue/filtrer_categories', $data, TRUE);
	}
	
	/**
	 * Filtrer les produits selon critères
	 * 
	 * @return	VIEW
	 */
	private function _filtrer_criteres()
	{
		$data = array(
			'criteres' 	=> $this->critere->filtres(),
			'section'	=> $this->_section,
			'module'	=> $this->_module,
			'categorie'	=> $this->_categorie,
			'conditions'=> $this->_conditions(),
		);
		
		return $this->load->view('catalogue/filtrer_criteres', $data, TRUE);
	}
	
	/**
	 * Filtrer les produits selon critères
	 *
	 * @return	VIEW
	 */
	protected function _filtrer_criteres_opt($produits = NULL)
	{
	    if ($this->_module->afficher_criteres_liste)
	    {
	        $data = array(
	            'section'	=> $this->_section,
	            'module'	=> $this->_module,
	            'categorie'	=> $this->_categorie,
	            'criteres' 	=> $this->critere->filtres($this->_module->id,$this->categorie->id, FALSE)
	        );
	        
	        return $this->load->view('catalogue/filtrer_criteres_opt', $data, TRUE);
	    }
	    return FALSE;
	}
	
	public function mise_en_place_criteres($id_critere = NULL)
	{
	    
	    $conditions = $this->_conditions();
	    
	    if (isset($id_critere))
	    {
	        $critere = new Critere();
	        $critere->get_by_id($id_critere);
	        
	        $this->_section = $this->section->get_by_id($this->input->post('section'));
	        $this->_module 	= $this->_section->module_catalogue->get();
	        $this->_categorie = NULL;
	        
	        //Catégorie
	        if ( ! is_null($this->input->post('categorie')))
	        {
	            $this->_categorie = $this->categorie->get_by_id($this->input->post('categorie'));
	        }
	        elseif ($this->_module->categorie_racine > 0)
	        {
	            $this->_categorie = $this->categorie->where(array(
	                'root_id' => $this->_module->categorie_racine,
	                'left_id' => 1
	            ))->get();
	        }
	        $module = $this->_module;
	        $section = $this->_section;
	        $categorie = $this->_categorie;
	        
	        if(isset($critere))
	        {
	            $criteres_valeur = new Criteres_valeur();
	            $nb_choix_criteres = 0;
	            
	            if($critere->code != '')
	            {
	                $sql = 'SELECT `t_criteres_valeurs`.*
                    FROM (`t_criteres_valeurs`, `t_criteres`)
                    WHERE `t_criteres`.`id` = `t_criteres_valeurs`.`critere_id`
                    AND `t_criteres`.`id` = "'.$critere->id.'"
                    AND `t_criteres`.`module_catalogue_id` = "'.$module->id.'"
                    GROUP BY `t_criteres_valeurs`.`valeur`
                    ORDER BY `t_criteres_valeurs`.`valeur`';
	            }
	            else
	            {
	                $sql = 'SELECT `t_criteres_valeurs`.*
                    FROM (`t_criteres_valeurs`, `t_criteres`)
                    WHERE `t_criteres`.`id` = `t_criteres_valeurs`.`critere_id`
                    AND `t_criteres`.`libelle` = "'.$critere->libelle.'"
                    AND `t_criteres`.`module_catalogue_id` = "'.$module->id.'"
                    GROUP BY `t_criteres_valeurs`.`valeur`
                    ORDER BY `t_criteres_valeurs`.`valeur`';
	            }
	            
	            $criteres_produit = $criteres_valeur->query($sql);
	            $html_critere = '';
	            $nb_choix_criteres = 0;
	            $criteres_actifs = 0;
	            
	            if ($criteres_produit->result_count() > 0)
	            {
	                
	                foreach ($criteres_produit as $critere_produit)
	                {
	                    if ($critere_produit->valeur != '')
	                    {

	                        $class = NULL;
	                        $options = array(
	                            'critere_id'=> $critere_produit->critere_id,
	                            'valeur' 	=> $critere_produit->valeur,
	                            'id' 		=> $critere_produit->id
	                        );                        
	                        
	                        if (exist_critere_ajax($options, $this->input->post('get')))
	                        {
	                            $class = 'enabled ';
	                            $nb_choix_criteres++;
	                            $criteres_actifs++;
	                        }
	                        else
	                        {
	                            $class = 'disabled ';
	                        }
	                        
	                        $url = ( ! is_null($categorie) && ($categorie->left_id != 1)) ? $categorie->url($section) : construire_url(array('section' => $section->segment_url()));
;
	                        //Si on souhiate ne pas cumuler les critères, passer 2dn param FALSE à set_criteres
	                        $html_critere .= '<li class="'.$class.'">';
	                        $html_critere .= anchor($url.set_criteres_ajax($options, TRUE, $this->input->post('get')), $critere_produit->valeur, array('rel' => 'nofollow'));
	                        $html_critere .= '</li>';
	                    }
	                }
	                $active = '';
	                if($nb_choix_criteres > 0)
	                {
	                    $active = ' active';
	                }
	            }
	        }
	        
	        exit(json_encode(array(
	            'html' 	=> $html_critere,
	            'active'   => $active,
	            'erreur'		=> FALSE
	        )));
	    }
	    exit(json_encode(array(
	        'erreur'		=> TRUE
	    )));
	}
	
	public function mise_en_place_enfants($id_reference = NULL)
	{
	    
	    if (isset($id_reference))
	    {
	        $produit = new Produit();
	        $produit->get_by_id($id_reference);
	        
	        exit(json_encode(array(
	            'html' 	=> modules::run('boutique/boutique/bouton_ajouter', $produit),
	            'erreur'		=> FALSE
	        )));
	    }
	    exit(json_encode(array(
	        'erreur'		=> TRUE
	    )));
	}
	
	public function mise_en_place_enfants_stadium($id_reference = NULL)
	{
	    
	    if (isset($id_reference))
	    {
	        $produit = new Produit();
	        $produit->get_by_id($id_reference);
	        
	        $utilisateur = NULL;
	        if ($compte = json_decode($this->encrypt->decode(get_cookie('client'))))
	        {
	            $this->load->model('boutique/client');
	            $utilisateur = $this->client->get_by_id($compte->client_id);
	            
	            if ( ! $utilisateur->exists())
	            {
	                $utilisateur = NULL;
	            }
	        }
	        
	        
	        $trouve_compose = FALSE;
	        $tab_tarifs_compose = array();
	        foreach ($produit->produits_enfants() as $enfant)
	        {
	            $produits_composes = $this->db->query("
				SELECT t_produits.id
				FROM t_produits
				INNER JOIN t_produits_produits ON t_produits.id = t_produits_produits.related_produit_id
				WHERE t_produits_produits.compose = 1
				AND t_produits_produits.produit_id = ".$enfant->id."
			");
	            if ($produits_composes->num_rows() != 0)
	            {
	                $trouve_compose = TRUE;
	                
	                $tab_tarifs_compose[$enfant->id]	= array(
	                    'prix'	=> 0,
	                    'prix_sans_remise'	=> 0
	                );
	                foreach ($produits_composes->result() as $row)
	                {
	                    $prod_compo = new Produit();
	                    $prod_compo->get_by_id($row->id);
	                    $tab_tarifs_compose[$enfant->id]['prix'] +=  $prod_compo->tarif($utilisateur)['prix'];
	                    $tab_tarifs_compose[$enfant->id]['prix_sans_remise'] +=  $prod_compo->tarif($utilisateur)['prix'];
	                }
	            }
	        }
	        
	        
	        if ($trouve_compose)
	        {
	            $tarif_precedent = 0;
	            
	            foreach ($tab_tarifs_compose as $enfant_id => $tarif_enfant)
	            {
	                if (($tarif_precedent == 0 ) or ($tarif_enfant['prix'] < $tarif_precedent))
	                {
	                    $data['tarif']['prix']	= html_price($tarif_enfant['prix']);
	                    $data['tarif']['prix_sans_remise']	= html_price($tarif_enfant['prix_sans_remise']);
	                    $tarif_precedent = $tarif_enfant['prix'];
	                }
	            }
	            
	        }
	        else
	        {
	            //Tarif
	            $tarif = $produit->tarif($utilisateur);
	            
	            if ($tarif !== FALSE)
	            {
	                //Tarif unique
	                $data['tarif'] = array(
	                    'prefixe'		=> ($tarif['unique']) ? NULL : $this->lang->line('a_partir_de'),
	                    'suffixe'		=> $this->lang->line('devise'),
	                    'prix'			=> html_price($tarif['prix']),
	                    'prix_ht'			=> html_price($tarif['prix']),
	                    'prix_sans_remise' => html_price($tarif['prix_sans_remise']),
	                    'taux_remise'	=> html_percentage($tarif['taux_remise'], 0),
	                    'tva'			=> html_percentage($tarif['tva'], 2),
	                    'taxe'			=> $this->lang->line('prix_taxe_'.$tarif['taxe'])
	                );
	                
	                //Grille tarifaire
	                if (is_array($tarif['grille']))
	                {
	                    $tarifs = '<ul class="liste-tarifs">';
	                    foreach ($tarif['grille'] as $grille)
	                    {
	                        $tarifs .= '<li>';
	                        $tarifs .= '<span class="minimum">'.$grille['minimum'].'</span>';
	                        $tarifs .= '<span class="maximum">'.$grille['maximum'].'</span>';
	                        $tarifs .= '<span class="prix">'.html_price($grille['prix']).nbs();
	                        $tarifs .= $this->lang->line('devise').nbs().$this->lang->line('prix_taxe_'.$tarif['taxe']).'</span>';
	                        $tarifs .= '</li>';
	                    }
	                    $tarifs .= '</ul>';
	                    
	                    $data['tarif']['grille'] = $tarifs;
	                }
	            }
	        }

	        exit(json_encode(array(
	            'result' 	    => $data,
	            'erreur'		=> FALSE
	        )));
	    }
	    exit(json_encode(array(
	        'erreur'		=> TRUE
	    )));
	}
	
	/**
	 * Récupère le type du catalogue à partir de l'url
	 * Utile si on souhaite un type différent de celui configuré dans le module
	 * 
	 * @return	integer
	 */
	private function _type_catalogue()
	{
		if (in_array('type', $this->uri->rsegment_array()))
		{
			foreach ($this->uri->rsegment_array() as $key => $value)
			{
				if ($value == 'type')
				{
					return $this->uri->rsegment($key+1);
				}
			}
		}
		return FALSE;
	}
	
	/**
	 * Retourne les conditions pour la requête
	 *
	 * @param	array
	 * @return	array
	 */
	protected function _conditions($conditions = array())
	{
	    //Paramètres du module
	    if (isset($this->_module))
	    {
	        $conditions['module_id'] = $this->_module->id;
	        
	        $conditions = array_merge($conditions, array(
	            'stock' 	=> ( ! $this->_module->afficher_indisponibles),
	            //'grade' 	=> 0,
	            'enfants'	=>  ( ! $this->_module->afficher_references)
	        ));
	        
	        if ( ! empty($this->_module->tri_defaut_champ) && ! empty($this->_module->tri_defaut_ordre))
	        {
	            $conditions = array_merge($conditions, array( 'tri' => array(
	                'champ' => $this->_module->tri_defaut_champ,
	                'ordre' => $this->_module->tri_defaut_ordre
	            )));
	        }
	        
	    }
	    
	    //Paramètres via URL
	    if ($get = $this->input->get())
	    {
	        if (isset($get['recherche']))
	        {
	            $conditions = array_merge($conditions, array(
	                'recherche' => $get['recherche'],
	            ));
	        }
	        
	        if (isset($get['filtre']))
	        {
	            $conditions = array_merge($conditions, array(
	                'criteres' => get_criteres($get['filtre']),
	            ));
	        }
	        
	        if (isset($get['tri']))
	        {
	            $liste_tri = array(
	                'nouveautes' => array('champ' => 'nouveau', 'ordre' => 'asc'),
	                'prix-croissant' => array('champ' => 'tarif_prix_vente', 'ordre' => 'asc'),
	                'prix-decroissant' => array('champ' => 'tarif_prix_vente', 'ordre' => 'desc'),
	            );
	            
	            if (isset($liste_tri[$get['tri']]))
	            {
	                $conditions = array_merge($conditions, array(
	                    'tri' => $liste_tri[$get['tri']],
	                ));
	            }
	        }
	        
	        if (isset($get['item_par_page']))
	        {
	            $conditions = array_merge($conditions, array(
	                'item_par_page' => $get['item_par_page'],
	            ));
	        }
	    }
	    
	    //Catégorie
	    if ($categorie_id = $this->_categorie_id())
	    {
	        $conditions['categorie'] = $categorie_id;
	    }
	    
	    //Visibilité
	    $conditions['visible'] = TRUE;
	    
	    //$conditions['enfant'] = TRUE;
	    
	    return $conditions;
	}
	
	/**
	 * Retourne un tableau avec les champs de la table
	 * 
	 * @param	string	$requete
	 * @return	array
	 */
	private function _conditions_recherche($requete)
	{
		$champs = array_flip($this->config->item('champs'));
				
		foreach ($champs as $champ => $valeur)
		{
			$champs[$champ] = $requete;
		}
		
		return $champs;
	}
	
	/**
	 * Vérifier la visiblité d'un produit en fonction des sélecteurs
	 *
	 * @return	object
	 */
	protected function _visible()
	{
	    $conditions = array(
	        'id' => $this->_id(),
	        'visible' => TRUE
	    );
	    
	    //Paramètres du module
	    if (isset($this->_module))
	    {
	        $conditions['module_catalogue_id'] = $this->_module->id;
	        
	        if ( ! $this->_module->afficher_indisponibles)
	        {
	            $conditions = array_merge($conditions, array(
	                'stock > ' => 0
	            ));
	        }
	    }
	    
	    return $this->produit->where($conditions)->get();
	}
	
	/**
	 * Permet de retourner la liste des critères à afficher pour le produit en cours
	 *
	 * @param 	object 	$produit
	 * @return 	array
	 */
	protected function _get_critere_affiche($produit)
	{
	    $i = 0;
	    $note = 0;
	    
	    foreach ($this->commentaire->where('produit_reference', $produit->reference)->get() as $commentaire)
	    {
	        if ($commentaire->afficher)
	        {
	            $i++;
	            $note += $commentaire->note;
	        }
	    }
	    if ($i > 0)
	    {
	        $note = ceil($note/$i);
	    }
	    return $note;
	}
	
	/**
	 * Permet de retourne la note d'un produit en fonction des ses commentaires
	 *
	 * @param 	object 	$produit
	 * @return 	integer
	 */
	protected function _note_produit($produit)
	{
	    $i = 0;
	    $note = 0;
	    
	    foreach ($this->commentaire->where('produit_reference', $produit->reference)->get() as $commentaire)
	    {
	        if ($commentaire->afficher)
	        {
	            $i++;
	            $note += $commentaire->note;
	        }
	    }
	    if ($i > 0)
	    {
	        $note = ceil($note/$i);
	    }
	    return $note;
	}
	
	
	
	/**
	 * Envoie la vue de la note d'un produit
	 *
	 * @param 	object 	$produit
	 * @return	VIEW
	 */
	protected function _view_note_produit($produit)
	{
	    $data = array(
	        'note'	=> $this->_note_produit($produit)
	    );
	    
	    return $this->load->view('catalogue/commentaire/note', $data, TRUE);
	}
	
	/**
	 * Permet de retourne la note donnée pour cet avis sur ce produit
	 *
	 * @param 	object 	$produit
	 * @return 	integer
	 */
	public function affiche_etoiles_note($note)
	{
	    $data = array(
	        'note'	=> $note
	    );
	    
	    echo json_encode(array(
	        'vue'	=> $this->load->view('catalogue/commentaire/affiche_note', $data, TRUE)
	    ));
	}
	
	
	/**
	 * Permet d'ajouter un commentaire à un produit
	 *
	 * @return	JSON
	 */
	public function ajouter_commentaire()
	{
	    if ($post = $this->input->post())
	    {
	        $produit = $this->produit->get_by_reference($post['produit_reference']);
	        
	        if ($this->commentaire->enregistrer($post))
	        {
	            /*On envoie un mail à l'utilisateur*/
	            $this->load->model('utilisateurs/utilisateur');
	            $utilisateur = new Utilisateur();
	            $utilisateur = $this->utilisateur->get_by_id($post['utilisateur_id']);
	            
	            $data = array(
	                'parametres'     => $this->_parametres,
	                'module'         => $this->_module,
	                'commentaire'    => $this->commentaire,
	                'produit'        => $produit,
	                'utilisateur'    => $utilisateur->prenom.' '.$utilisateur->nom,
	                'email'          => $utilisateur->email,
	                'logo'           => '<img src="'.root_path().'/assets/img/logo.png" id="logo" alt="logo" />',
	            );
	            
	            $template  	= $this->load->view('catalogue/commentaire/email_commentaire_utilisateur', $data, TRUE);
	            //$templateb 	= $this->load->view('catalogue/commentaire/email_commentaire_admin', $data, TRUE);
	            
	            //Client
	            $this->email->from($this->_parametres->expediteur_email, $this->_parametres->expediteur_libelle);
	            $this->email->to($utilisateur->email);
	            $this->email->subject('Commentaire sur un produit - APPI');
	            $this->email->message($template);
	            $this->email->send();
	            
	            //Vendeur
	            // 			    $this->email->clear(TRUE);
	            // 			    $this->email->from($this->_parametres->expediteur_email, $this->_parametres->expediteur_libelle);
	            // 			    $this->email->to($this->_module->vendeur_email);
	            // 			    $this->email->subject($this->_module->sujet_commande_payee);
	            // 			    $this->email->message($templateb);
	            // 			    $this->email->send();
	            
	            if ($this->input->is_ajax_request())
	            {
	                exit(json_encode(array(
	                    'erreur'	=> FALSE,
	                    'message'	=> lang('succes_ajout'),
	                    'commentaire' => $this->_commentaire($produit),
	                    'note'		=> $this->_view_note_produit($produit)
	                )));
	            }
	            else
	            {
	                header("Refresh:0");
	            }
	            
	        }
	        else
	        {
	            exit(json_encode(array(
	                'erreur'	=> TRUE,
	                'message'=> lang('erreur_ajout'),
	            )));
	        }
	    }
	}
	
	/**
	 * Permet d'afficher les commentaires du produit
	 *
	 * @param 	object	$produit
	 * @return	VIEW
	 */
	protected function _commentaire($produit)
	{
	    $module = $produit->module();
	    
	    if ($module->afficher_commentaires)
	    {
	        $data = array(
	            'module' 			=> $module,
	            'commentaires' 		=> $produit->commentaires(),
	            'produit'			=> $produit,
	            'utilisateur'		=> modules::run('boutique/clients/est_connecte'),
	            'parametre' 		=> $this->_parametres,
	        );
	        return $this->load->view('catalogue/commentaire/commentaire', $data, TRUE);
	    }
	}
}
/* End of file catalogue.php */
/* Location: ./modules/catalogue/controllers/catalogue.php */