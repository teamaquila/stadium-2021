<?php
require_once APPPATH.'controllers/admin/back.php';

/**
 * Critères de produit
 * 
 * @author 		Pauline MARTIN
 * @package 	catalogue
 * @category 	Controllers
 * @version 	1.0
 */
class Criteres extends Back
{
	const ITEM_PAR_PAGE = 15;
	
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->lang->load('catalogue');
		$this->load->models(array('module_catalogue', 'critere', 'medias/media'));
	}
	
	/**
	 * Lister les critères
	 * 
	 * @param	integer	$module_id
	 * @param	integer	$offset
	 * @return	VIEW or JSON
	 */
	public function lister($module_id, $offset = 0)
	{
		$this->_module	= $this->module_catalogue->get_by_id($module_id);
		$this->_section = $this->_module->section->get();
		$this->_offset 	= $offset;
		
		/*
		 * Par défaut
		 */
		$conditions = array(
			'module_id'	=> $this->_module->id,
			'tri' => array(
				'champ' => 'libelle',
				'ordre'	=> 'ASC'
			)
		);
		
		if ($post = $this->input->post())
		{
			$conditions = array_merge($conditions, $post);
		}
		
		$total_item = $this->critere->lister($conditions);
		
		//Pagination
		$this->pagination->initialize(array(
			'base_url'		=> '/catalogue/admin/criteres/lister/'.$this->_module->id,
			'first_url'		=> '/catalogue/admin/criteres/lister/'.$this->_module->id.'?reload=true',
			'suffix'		=> '?reload=true',
			'total_rows'	=> $total_item,
			'per_page'		=> self::ITEM_PAR_PAGE,
			'cur_page'		=> $offset
		));
		
		$data = array(
			'criteres'		=> $this->critere->lister($conditions, $this->_offset, self::ITEM_PAR_PAGE),
			'pagination'	=> $this->pagination->create_links(),
			'module'		=> $this->_module,
			'offset'		=> $offset,
			'total_item'	=> $total_item
		);
		
		if ($this->input->get('reload'))
		{
			exit(json_encode(array(
				'liste'			=> $this->load->view('admin/criteres/lister_ajax', $data, TRUE),
				'pagination'	=> $this->pagination->create_links(),
				'total_item'	=> $total_item
			)));
		}
		else
		{
			$data['lister_ajax'] = $this->load->view('admin/criteres/lister_ajax', $data, TRUE);
			$this->load->view('admin/criteres/lister', $data);
		}
	}
	
	/**
	 * Ajouter un critère
	 * 
	 * @param	integer	$module_id
	 * @return	VIEW or JSON
	 */
	public function ajouter($module_id)
	{
		if ($this->input->post('submit'))
		{
			$this->form_validation->set_rules(array(
				array(
					'field' => 'libelle',
					'label' => lang('libelle'),
					'rules'	=> 'required'
				)
			));
			
			if ($this->form_validation->run($this) === FALSE)
			{ 
				exit(json_encode(array(
					'erreur' => TRUE,
					'message'=> validation_errors()
				)));
			}
			else
			{
				if ($this->critere->enregistrer($this->input->post()) !== FALSE)
				{
					$this->log->enregistrer(array(
						'administrateur_id' => $this->administrateur_connecte->id,
						'type'			=> LOG_AJOUT,
						'description'	=> lang('log_ajout_critere').' '.$this->critere->libelle,
						'adresse_ip'	=> $this->input->ip_address(),
						'user_agent'	=> $this->input->user_agent()
					));
					
					exit(json_encode(array(
						'erreur'	=> FALSE,
						'message'	=> lang('succes_ajout'),
						'url'		=> array(
							'liste' => '/catalogue/admin/criteres/lister/'.$module_id.'?reload=true&reset=true',
						)
					)));
				}
				else
				{
					exit(json_encode(array(
						'erreur'	=> TRUE,
						'message'=> lang('erreur_ajout'),
					)));
				}
			}
		}
		
		$data = array(
			'parametres'=> $this->_parametres,
			'critere'	=> NULL,
			'valeurs'	=> NULL,
			'module_id'	=> $module_id,
			'medias' 	=> modules::run('medias/admin/medias/inserer')
		);
		$this->load->view('admin/criteres/formulaire', $data);
	}

	/**
	 * Modifier un critère
	 * 
	 * @param  	integer	$id
	 * @return	VIEW or JSON
	 */
	public function modifier($id)
	{
		$critere = $this->critere->get_by_id($id);

		if ($critere->exists())
		{
			if ($this->input->post('submit'))
			{

				$this->form_validation->set_rules(array(
					array(
						'field' => 'libelle',
						'label' => lang('libelle'),
						'rules'	=> 'required'
					)
				));
				
				if ($this->form_validation->run($this) === FALSE)
				{ 
					exit(json_encode(array(
						'erreur' => TRUE,
						'message'=> validation_errors()
					)));
				}
				else
				{
					if ($this->critere->enregistrer($this->input->post()) !== FALSE)
					{
					    $post = $this->input->post();
					    $criteres = new Critere();
					    $criteres->where('libelle', $this->critere->libelle)->where('id !=', $this->critere->id)->get();
				    
					    if($criteres && array_key_exists('medias', $post)){
					        foreach($criteres as $crit){
					            $data_media = array(
					                'id' => $crit->id,
					                'medias' => array($post['medias'][0])
					            );
					            $crit->enregistrer($data_media);
					        }
					    }
					    
						$this->log->enregistrer(array(
							'administrateur_id' => $this->administrateur_connecte->id,
							'type'			=> LOG_MODIFICATION,
							'description'	=> lang('log_modification_critere').' '.$this->critere->libelle,
							'adresse_ip'	=> $this->input->ip_address(),
							'user_agent'	=> $this->input->user_agent()
						));
						
						exit(json_encode(array(
							'erreur'	=> FALSE,
							'message'	=> lang('succes_modification'),
							'url'		=> array(
								'liste' => '/catalogue/admin/criteres/lister/'.$critere->module_catalogue_id.'?reload=true&reset=true'
							)
						)));
					}
					else
					{
						exit(json_encode(array(
							'erreur'	=> TRUE,
							'message'	=> lang('erreur_modification'),
						)));
					}
				}
			}
			
			
			$data = array(
				'parametres'=> $this->_parametres,
				'critere'	=> $critere,
				'valeurs'	=> $critere->criteres_valeur->where('valeur <>', '')->order_by('valeur')->get(),
				'module_id'	=> $critere->module_catalogue_id,
				'medias' 	=> modules::run('medias/admin/medias/inserer', $critere->media_associe(Media::IMAGE, FALSE)),
			);
			$this->load->view('admin/criteres/formulaire', $data);
		}
	}
	
	/**
	 * Dupliquer un critère
	 */
	public function dupliquer()
	{
		/**
		 * @todo Multi-sélection
		 */
		
		$data = array(
			'parametres'=> $this->_parametres,
			'critere'	=> $critere
		);
		$this->load->view('admin/criteres/duppliquer', $data);
	} 
	
	/**
	 * Supprimer un critère
	 * 
	 * @param	integer	$offset
	 * @return	VIEW
	 */
	public function supprimer($offset = 0)
	{
		$criteres = $this->input->post('critere');
		
		if ( ! $criteres)
		{
			return;
		}
		
		if ($this->input->post('submit'))
		{
			$offset = $this->input->post('offset');
				
			if ($offset > 0)
			{
				$total_item = $this->produit->compter($post);
		
				if (($total_item - count($ids)) == 0)
				{
					$offset = $offset-self::ITEM_PAR_PAGE;
				}
			}
			
			foreach ($criteres as $id)
			{
				$critere = $this->critere->get_by_id($id);
				$libelle = $critere->libelle;
				$module_id = $critere->module_catalogue_id;
				
				if ($critere->supprimer())
				{
				    $log = new Log();
				    $log->enregistrer(array(
				        'utilisateur_id' => $this->administrateur_connecte->id,
						'type'			=> LOG_SUPPRESSION,
						'description'	=> lang('log_suppression_critere').' '.$libelle,
						'adresse_ip'	=> $this->input->ip_address(),
						'user_agent'	=> $this->input->user_agent()
					));
				}
				else
				{
					exit(json_encode(array(
						'erreur'	=> TRUE,
						'message'	=> lang('erreur_suppression')
					)));
				}
			}
			
			exit(json_encode(array(
				'erreur'	=> FALSE,
				'message'	=> lang('succes_suppression'),
				'url'		=> array(
					'liste' => '/catalogue/admin/criteres/lister/'.$module_id.'?reload=true&reset=true',
				)
			)));
		}
		else
		{
			exit(json_encode(array(
				'title'	=> lang('titre_suppression'),
				'html'	=> $this->load->view('admin/criteres/supprimer', array(
					'offset' 	=> $offset,
					'criteres'	=> $criteres
				), TRUE)
			)));
		}
	}
}

/* End of file criteres.php */
/* Location: ./modules/catalogue/controllers/admin/criteres.php */