<?php
require_once APPPATH.'controllers/admin/back.php';

/**
 * Produits : 
 * Contrôleur permettant les actions sur les produits.
 * 
 * @author 		Pauline Martin, Pauline MARTIN
 * @package		catalogue
 * @category	Controllers
 * @version 	2.0
 */
class Produits extends Back
{	
	const ITEM_PAR_PAGE = 25;
	
	private $_module;
	private $_section;
	private $_offset;
	
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
				
		$this->lang->load('catalogue');
		$this->load->config('catalogue');
		$this->load->helpers(array('categories/categorie', 'catalogue'));
		$this->load->library('form_validation');
		$this->load->models(array(
			'module_catalogue', 'produit', 'categories/categorie', 
			'medias/media', 'tarif', 'criteres_valeur', 'criteres_valeurs_produit','critere', 
		    'boutique/code_remise', 'type_module', 'selection/module_selection'
		));
	}
	
	/**
	 * Liste des produits non-triées et triées
	 * 
	 * @param	integer	$module_id
	 * @param	integer	$offset
	 * @return	mixed
	 */
	public function lister($module_id, $offset = 0)
	{
		$this->_module	= $this->module_catalogue->get_by_id($module_id);
		$this->_offset 	= $offset;
		
		/*
		 * Filtres : catégories et critères
		 * Tri : champ et ordre
		 * Recherche : champ libre
		 */
		$conditions = array(
			'grade' 	=> 0,
			'enfants'	=> FALSE,
			'module_id'	=> $this->_module->id,
			'tri' => array(
				'champ' => 'libelle',
				'ordre'	=> 'ASC'
			)
		);
		
		if ($post = $this->input->post())
		{
			$conditions = array_merge($conditions, $post);
			
			if($this->input->post('recherche'))
			{
				$conditions['recherche'] = array(
					'libelle'	=> $this->input->post('recherche'),
					'reference'	=> $this->input->post('recherche'),
				);
			}
		}
		$total_item = $this->produit->compter($conditions);
		
		//Pagination
		$this->pagination->initialize(array(
			'base_url'		=> '/catalogue/admin/produits/lister/'.$this->_module->id,
			'first_url'		=> '/catalogue/admin/produits/lister/'.$this->_module->id.'?reload=true',
			'suffix'		=> '?reload=true',
			'total_rows'	=> $total_item,
			'per_page'		=> self::ITEM_PAR_PAGE,
			'cur_page'		=> $offset
		));
		
		/*$this->produit->lister($conditions, $this->_offset, self::ITEM_PAR_PAGE);
		$this->produit->check_last_query();
		die();*/
		
		$data = array(
			'produits'		=> $this->produit->lister($conditions, $this->_offset, self::ITEM_PAR_PAGE),
			'pagination'	=> $this->pagination->create_links(),
			'module'		=> $this->_module,
			'offset'		=> $offset,
			'total_item'	=> $total_item,
			'filtres'		=> array(
				'categories' => $this->_filtrer_categories()
			),
		);
		
		if ($this->input->get('reload'))
		{
			exit(json_encode(array(
				'liste'			=> $this->load->view('admin/lister_ajax', $data, TRUE),
				'pagination'	=> $this->pagination->create_links(),
				'total_item'	=> $total_item
			)));
		}
		else
		{
			$data['lister_ajax'] = $this->load->view('admin/lister_ajax', $data, TRUE);
			return $this->load->view('admin/lister', $data);
		}
	}
	
	/**
	 * Liste des produits enfants d'un produit
	 * 
	 * @param	integer	$produit_id
	 * @return	JSON
	 */
	public function lister_enfants($produit_id)
	{
		$produit = $this->produit->get_by_id($produit_id);
		
		$data = array(
			'produits' 	=> $produit->produits_enfants(),
			'module' 	=> $produit->module()
		);
		
		echo json_encode(array(
			'vue' => $this->load->view('admin/lister_enfants', $data, TRUE)
		));
	}
	
	/**
	 * Formulaire et traitement pour ajouter
	 * 
	 * @param 	integer	$module_id
	 * @return 	void
	 */
	public function ajouter($module_id)
	{
		if ($this->input->post())
		{
			$this->form_validation->set_rules(array(
				array(
					'field' => 'libelle',
					'label' => lang('libelle'),
					'rules'	=> 'required'
				),
				array(
					'field' => 'reference',
					'label' => lang('reference'),
					'rules'	=> 'required'
				)
			));
			
			if ($this->form_validation->run($this) === FALSE)
			{ 
				echo json_encode(array(
					'erreur' => TRUE,
					'message'=> validation_errors()
				));
			}
			else
			{
				if ($this->produit->enregistrer($this->input->post()) !== FALSE)
				{
					$this->log->enregistrer(array(
						'administrateur_id' => $this->administrateur_connecte->id,
						'type'			=> LOG_AJOUT,
						'description'	=> lang('log_ajout_produit').' '.$this->produit->libelle,
						'adresse_ip'	=> $this->input->ip_address(),
						'user_agent'	=> $this->input->user_agent()
					));
					
					echo json_encode(array(
						'erreur' => FALSE,
						'message'=> lang('succes_ajout'),
						'url' => array(
							'liste' => '/catalogue/admin/produits/lister/'.$module_id.'?reload=true&reset=true',
							'sitemap' => '/admin/back/generer_sitemap'
						)
					));
				}
				else
				{
					echo json_encode(array(
						'erreur' => TRUE,
						'message'=> lang('erreur_ajout')
					));
				}
			}
			return;
		}
		
		$module = $this->module_catalogue->get_by_id($module_id);
		
		$boutique_bool = FALSE;
		$boutique_active = $this->type_module->where('modele', 'module_boutique')->get();
		if($boutique_active->actif == 1){
		    $boutique_bool = TRUE;
		}
		
		$data = array(
			'produit'	=> NULL,
			'module'	=> $module,
		    'boutique_active' => $boutique_bool,
			'tarifs'	=> $this->_gestion_tarif(NULL, $module),
			'medias_associes' 		=> modules::run('medias/admin/medias/inserer'),
			'produits_enfants'		=> NULL,
			'produits_associes'		=> NULL,
			'categories_associees'	=> NULL,
			'offset'				=> NULL,
		    'produits_composess'	=> NULL,
		    'produits_complementaires'=> NULL,
		    'produits_imposes'		=> NULL,
		    'filtres_associes'		=> NULL,
			'criteres_associes'		=> $this->_criteres(NULL, $module),
			'admin'					=> $this->administrateur_connecte
		);
		$this->load->view('admin/formulaire', $data);
	}
	
	/**
	 * Formulaire et traitement pour modifier
	 * 
	 * @param 	integer	$id
	 * @return 	void
	 */
	public function modifier($id, $offset = 0)
	{
		$produit = $this->produit->get_by_id($id);
		
		if ($produit->exists())
		{
			$module = $produit->module_catalogue->get();
			
			if ($this->input->post())
			{
				$offset = $this->input->post('offset');
				$this->form_validation->set_rules(array(
					array(
						'field' => 'libelle',
						'label' => lang('libelle'),
						'rules'	=> 'required'
					),
					array(
						'field' => 'reference',
						'label' => lang('reference'),
						'rules'	=> 'required'
					)
				));
				
				if ($this->form_validation->run($this) === FALSE)
				{
					echo json_encode(array(
						'erreur' => TRUE,
						'message'=> validation_errors()
					));
				}
				else
				{
					if ($produit->enregistrer($this->input->post()) !== FALSE)
					{
						$this->log->enregistrer(array(
							'administrateur_id' => $this->administrateur_connecte->id,
							'type'			=> LOG_MODIFICATION,
							'description'	=> lang('log_modification_produit').' '.$produit->libelle,
							'adresse_ip'	=> $this->input->ip_address(),
							'user_agent'	=> $this->input->user_agent()
						));
						
						echo json_encode(array(
							'erreur' => FALSE,
							'message'=> lang('succes_modification'),
							'url' => array(
								'liste' => '/catalogue/admin/produits/lister/'.$module->id.'/'.$offset.'?reload=true&reset=true',
								'sitemap' => '/admin/back/generer_sitemap'
							)
						));
					}
					else
					{
						echo json_encode(array(
							'erreur' => TRUE,
							'message'=> lang('erreur_modification')
						));
					}
				}
				return;
			}
			
			
			$boutique_bool = FALSE;
			$boutique_active = $this->type_module->where('modele', 'module_boutique')->get();
			if($boutique_active->actif == 1){
			    $boutique_bool = TRUE;
			}
			
			$data = array(
				'produit'	=> $produit,
				'module'	=> $module,
			    'boutique_active' => $boutique_bool,
			    'offset'	=> $offset,
				'tarifs'	=> $this->_gestion_tarif($produit, $module),
				'medias_associes' 		=> modules::run('medias/admin/medias/inserer', $produit->medias_associes(FALSE)),
				'produits_associes'		=> $this->_associes($produit),
				'produits_enfants'		=> $this->_enfants($produit),
				'categories_associees'	=> $this->_categories($produit),
				'criteres_associes'		=> $this->_criteres($produit, $module),
			    'produits_composes'		=> $this->_associes($produit,0,0,1),
			    'produits_complementaires'=> $this->_associes($produit,1),
			    'produits_imposes'		=> $this->_associes($produit,0,1),
				'admin'					=> $this->administrateur_connecte
			);
			$this->load->view('admin/formulaire', $data);
		}
	}
	
	/**
	 * Supprimer un ou plusieurs produits sélectionnés
	 * 
	 * @param number $offset
	 * @return void|boolean
	 */
	public function supprimer($offset = 0)
	{
		$post = $this->input->post();
	
		$ids = $post['produit'];
		if (empty($ids))
		{
			return;
		}
		
		if ($this->input->post('submit'))
		{
			$offset = $this->input->post('offset');
			
			if ($offset > 0)
			{
				$total_item = $this->produit->compter($post);
				
				if (($total_item - count($ids)) == 0)
				{
					$offset = $offset-self::ITEM_PAR_PAGE;
				}
			}
			
			foreach ($ids as $id)
			{
				$produit = $this->produit->get_by_id($id);
				$libelle = $produit->libelle;
				$module_id = $produit->module_catalogue_id;
				
				if ($produit->supprimer())
				{
				    $log = new Log();
				    $log->enregistrer(array(
				        'utilisateur_id' => $this->administrateur_connecte->id,
						'type'			=> LOG_SUPPRESSION,
						'description'	=> lang('log_suppression_produit').' '.$libelle,
						'adresse_ip'	=> $this->input->ip_address(),
						'user_agent'	=> $this->input->user_agent()
					));
				}
				else
				{
					exit(json_encode(array(
						'erreur'	=> TRUE,
						'message'	=> lang('erreur_suppression')
					)));
				}
			}
			
			exit(json_encode(array(
				'erreur'	=> FALSE,
				'message'	=> lang('succes_suppression'),
				'url'		=> array(
					'liste' => '/catalogue/admin/produits/lister/'.$module_id.'/'.$offset.'?reload=true',
				)
			)));
		}
		else
		{
			$data = array(
				'ids'	 => $ids,
				'offset' => $offset
			);
			 
			exit(json_encode(array(
				'title'	=> lang('titre_suppression'),
				'html'	=> $this->load->view('admin/supprimer', $data, TRUE)
			)));
		}
	}
	
	/**
	 * Dupliquer un ou plusieurs produits sélectionnés
	 * 
	 * @param number $offset
	 */
	function dupliquer($offset = 0)
	{
		$ids = $this->input->post('produit');
		if (empty($ids))
		{
			return;
		}
		
		if ($this->input->post('submit'))
		{
			$offset = $this->input->post('offset');
				
			foreach ($ids as $id)
			{
				$produit = $this->produit->get_by_id($id);
				$module = $produit->module_catalogue_id;
				$produit->reference = $produit->reference.'-COPIE';
				$produit->get_copy()->save();
				
				$this->log->enregistrer(array(
					'administrateur_id' => $this->administrateur_connecte->id,
					'type'			=> LOG_DUPLICATION,
					'description'	=> lang('log_duplication_produit').' '.$this->produit->libelle,
					'adresse_ip'	=> $this->input->ip_address(),
					'user_agent'	=> $this->input->user_agent()
				));
			}
			exit(json_encode(array(
				'erreur'	=> FALSE,
				'message'	=> lang('succes_ajout'),
				'url'		=> array(
					'liste' => '/catalogue/admin/produits/lister/'.$module.'/'.$offset.'?reload=true',
				)
			)));
		}
		else
		{
			$data = array(
				'ids'	 => $ids,
				'offset' => $offset
			);
			
			echo json_encode(array(
				'title'	=> lang('titre_duplication'),
				'html'	=> $this->load->view('admin/dupliquer', $data, TRUE)
			));
		}
	}
	
	/**
	 * Formulaire de sélection des catégories
	 * 
	 * @param 	integer $produit_id
	 * @param	integer	$module_id
	 * @return	VIEW
	 */
	public function selection_categories($produit_id = 0, $module_id = 0)
	{
		$this->load->helper('categories/categorie');
		
		if ($this->input->post('submit'))
		{			
			$data = array(
				'categories' => $this->categorie->where_in('id', $this->input->post('categories'))->get()
			);
			
			echo json_encode(array(
				'element' => 'categories-associees',
				'vue' => $this->load->view('admin/categories_associees', $data, TRUE)
			));
			
			return;
		}

		if ($produit_id > 0)
		{
			$produit = $this->produit->get_by_id($produit_id);
			$module  = $produit->module();
			
			$data = array(
				'produit_id'	=> $produit->id,
				'module'		=> $module,
				'categories' 	=> $this->categorie->arbre($module->categorie_racine, FALSE),
				'selection'		=> $produit->categories_associees()->all_to_single_array('id')
			);
		}
		elseif ($module_id > 0)
		{
			$module = $this->module_catalogue->get_by_id($module_id);
			
			$data = array(
				'produit_id'	=> $produit_id,
				'module'		=> $module,
				'categories' 	=> $this->categorie->arbre($module->categorie_racine, FALSE),
				'selection'		=> NULL
			);
		}
		
		if ($this->input->post('recherche'))
		{
			$data['recherche'] = $this->input->post('recherche');
			echo json_encode(array(
				'vue'	=> $this->load->view('admin/selection_categories_ajax', $data, TRUE)
			));
		}
		else 
		{
			$this->load->view('admin/selection_categories', $data);	
		}
	}
	
	/**
	 * Afficher les catégories associées
	 * 
	 * @param	object	$produit
	 * @return	VIEW
	 */
	private function _categories($produit)
	{
		$data = array(
			'categories' => $produit->categories_associees()
		);
		
		return $this->load->view('admin/categories_associees', $data, TRUE);
	}
	
	/**
	 * Afficher les critères du produit
	 * 
	 * @param	object	$produit
	 * @return	VIEW
	 */
	private function _criteres($produit, $module)
	{
		$data = array(
			'produit'	=> $produit,
			'criteres' 	=> $this->critere->par_module($module->id,$produit)
		);
		
		return $this->load->view('admin/criteres_associes', $data, TRUE);
	}
	
	/**
	 * Formulaire de sélection des produits associés
	 * 
	 * @param 	integer $id
	 * @return	VIEW
	 */
	public function selection_associes($complement = 0, $impose = 0, $compose = 0, $id = NULL)
	{
		$conditions = array('grade' => 0);
		
		if ($this->input->post('submit'))
		{
		    $element = 'produits-associes';
		    if ($complement != 0)
		    {
		        $element = 'produits-complementaires';
		    }
		    elseif ($impose != 0)
		    {
		        $element = 'produits-imposes';
		    }
		    elseif ($compose != 0)
		    {
		        $element = 'produits-composes';
		    }

			$data = array(
			    'type_assoc'	=> $element,
				'produits' => $this->produit->where_in('id', $this->input->post('produits'))->get()
			);
			
			exit(json_encode(array(
			    'element' 	=> $element,
				'vue'	 => $this->load->view('admin/produits_associes', $data, TRUE),
				'append'	=> TRUE
			)));
		}

		if ( ! is_null($id))
		{
			$produit = $this->produit->get_by_id($id);

			$data = array(
				'produit_id'=> $produit->id,
				'produits' 	=> $produit->produits_disponibles($conditions),
			    //'selection'	=> $produit->produits_associes($complement, $impose, $compose)->all_to_single_array('id'),
			    'selection'	=> $produit->produits_associes()->all_to_single_array('id')
			);
		}
		else
		{

			$data = array(
				'produits' 	=> $this->produit->produits_disponibles($conditions),
				'selection'	=> NULL
			);
		}
		
		if ($this->input->post('recherche'))
		{
			$conditions = array(
				'grade' => 0,
				'recherche' => $this->input->post('recherche')
			);
			
			if ( ! is_null($id))
			{
				$produit = $this->produit->get_by_id($id);
				$data['produits'] = $produit->produits_disponibles($conditions);
			}
			else
			{
				$data['produits'] = $this->produit->produits_disponibles($conditions);
			}

			$data['recherche'] = $this->input->post('recherche');
			
			exit(json_encode(array(
				'vue'	=> $this->load->view('admin/selection_associes_ajax', $data, TRUE)
			)));
		}
		else
		{
			$this->load->view('admin/selection_associes', $data);
		}
	}
	
	/**
	 * Afficher la liste des produits associés
	 * 
	 * @param	object	$produit
	 * @return	VIEW
	 */
	private function _associes($produit, $complement = 0, $impose = 0, $compose = 0)
	{		
	    $element = 'produits-associes';
	    if ($complement != 0)
	    {
	        $element = 'produits-complementaires';
	    }
	    elseif ($impose != 0)
	    {
	        $element = 'produits-imposes';
	    }
	    elseif ($compose != 0)
	    {
	        $element = 'produits-composes';
	    }
	    
		$data = array(
			'produit_id'=> $produit->id,
			'produits' 	=> $produit->produits_associes()
		);
		$data = array(
		    'type_assoc'=> $element,
		    'produit_id'=> $produit->id,
		    'produits' 	=> $produit->produits_associes($complement, $impose, $compose)
		);
		
		return $this->load->view('admin/produits_associes', $data, TRUE);
	}	
	
	/**
	 * Formulaire de sélection des produits enfants
	 * 
	 * @param 	integer $id
	 * @return	VIEW
	 */
	public function selection_enfants($id = NULL)
	{
		$conditions = array('grade' => 1);
		
		if ($this->input->post('submit'))
		{
			$data = array(
				'produits' => $this->produit->where_in('id', $this->input->post('produits_enfants'))->get()
			);
			
			exit(json_encode(array(
				'element' 	=> 'produits-enfants',
				'vue' 		=> $this->load->view('admin/produits_enfants', $data, TRUE)
			)));
		}
		
		if ( ! is_null($id))
		{
			$produit = $this->produit->get_by_id($id);
			
			$data = array(
				'produit_id'=> $produit->id,
				'produits' 	=> $produit->produits_disponibles($conditions),
				'selection'	=> $produit->produits_enfants()->all_to_single_array('id')
			);
		}
		else
		{
			$data = array(
				'produits' 	=> $this->produit->produits_disponibles($conditions),
				'selection'	=> NULL
			);
		}
		
		if ($this->input->post('recherche'))
		{
			$conditions = array(
				'grade' => 1,
				'recherche' => $this->input->post('recherche')
			);
			
			if ( ! is_null($id))
			{
				$produit = $this->produit->get_by_id($id);
				$data['produits'] = $produit->produits_disponibles($conditions);
			}
			else
			{
				$data['produits'] = $this->produit->produits_disponibles($conditions);
			}

			$data['recherche'] = $this->input->post('recherche');
			
			exit(json_encode(array(
				'vue'	=> $this->load->view('admin/selection_enfants_ajax', $data, TRUE)
			)));
		}
		else
		{
			$this->load->view('admin/selection_enfants', $data);
		}
	}
	
	/**
	 * Afficher la liste des produits enfants
	 * 
	 * @param	object	$produit
	 * @return	VIEW
	 */
	private function _enfants($produit)
	{		
		$data = array(
			'produit_id'=> $produit->id,
			'produits' 	=> $produit->produits_enfants()
		);
		
		return $this->load->view('admin/produits_enfants', $data, TRUE);
	}
	
	/**
	 * Formulaire pour la gestion des tarifs pro/particulier
	 * 
	 * @param 	object $produit
	 * @param 	object $module
	 * @return	VIEW
	 */
	private function _gestion_tarif($produit, $module)
	{
		$data = array(
			'module'		=> $module,
			'produit'		=> $produit,
			'liste_tva'		=> taux_tva(1),
			'types_client'	=> array(0 => 'Particulier', 1 => 'Professionnel'),
			'tarif' 		=> NULL,
		);
		
		return $this->load->view('admin/gestion_tarif', $data, TRUE);
	}
	
	/**
	 * Retourne une nouvelle ligne de tarif dégressif
	 * 10 au maximum
	 * 
	 * @param	integer	$nb_ligne
	 * @param	integer	$type_client
	 * @return	void
	 */
	public function ligne_tarif($nb_ligne, $type_client)
	{
		if ($nb_ligne < 10)
		{
			$nb_ligne++;
			$data = array(
				'ligne' 		=> $nb_ligne,
				'type_client'	=> $type_client
			);
			echo $this->load->view('admin/ligne_tarif', $data);
		}
	}
	
	/**
	 * Exportation de la table t_produits
	 * 
	 * @return	void
	 */
	public function exporter()
	{
		$this->load->dbutil();
		
		header('Content-type: text/csv'); 
		header('Content-disposition: attachment; filename='.mdate('%Y-%m-%d').'-catalogue.csv');
				
		$produits = $this->db->query('SELECT * FROM t_produits');
		echo utf8_decode($this->dbutil->csv_from_result($produits, ';', "\r\n"));
	}
	
	/**
	 * Filtrer les produits selon catégories
	 *
	 * @return	VIEW
	 */
	private function _filtrer_categories()
	{
		$data = array(
			'module'	=> $this->_module,
			'categorie'	=> NULL
		);
	
		return $this->load->view('catalogue/admin/filtrer_categories', $data, TRUE);
	}
	
	/**
	 * Filtrer les produits selon critères
	 *
	 * @return	VIEW
	 */
	private function _filtrer_criteres()
	{
		$data = array(
			'section'	=> $this->_section,
			'module'	=> $this->_module,
			'categorie'	=> $this->_categorie,
			'criteres' 	=> $this->critere->filtres(),
			'conditions'=> $this->_conditions(),
		);
	
		return $this->load->view('catalogue/admin/filtrer_criteres', $data, TRUE);
	}
}
/* End of file produits.php */
/* Location: ./modules/catalogue/controllers/admin/produits.php */