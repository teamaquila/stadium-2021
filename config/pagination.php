<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['full_tag_open'] = '<ul class="pagination">';
$config['full_tag_close'] = '</ul>';
$config['first_link'] = 'Début';
//$config['next_link'] = 'Suivant &gt;';
$config['prev_link'] = 'Début';
$config['last_link'] = 'Fin';
//$config['next_tag_open'] = '<li class="next">';
//$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li class="prev">';
$config['prev_tag_close'] = '</li>';
$config['first_tag_open'] = '<li class="first">';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li class="last">';
$config['last_tag_close'] = '</li>';
$config['num_tag_open'] = '<li class="cercle">';
$config['num_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class="active cercle"><span>';
$config['cur_tag_close'] = '</span></li>';

/* End of file pagination.php */
/* Location: ./application/config/pagination.php */