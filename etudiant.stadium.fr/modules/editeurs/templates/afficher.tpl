<div id="section-{$section_id}" class="module-editeur">
	{$fil_ariane}

	<div class="bloc_titre_h1">
		<h1>{$titre_page}</h1>			
	</div>

	<div class="inner_interne">
		<div class="contenu">{$contenu}</div>
	</div>
</div>