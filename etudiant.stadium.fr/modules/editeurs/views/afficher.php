<?php
//Remplace le chemin de l'image par le domaine racine
$cont = (isset($previsualiser))?$previsualiser:$module->contenu;
$contenu = preg_replace('/<img(.*)src="\/(.*)"[\s]*(.*)\/?>/', '<img$1src="'.SOUSDOMAIN_PATH.'$2"$3>', $cont);

$data = array(
	'titre_page'=> ( ! empty($section->titre)) ? $section->titre : $section->libelle,
	'section_id'=> $module->section_id,
	'contenu' 	=> $contenu,
	'id'	  	=> $module->id,
	'image_section'	=> $image_section,
	'fil_ariane'	=> $fil_ariane
);
$this->dwootemplate->output(tpl_path('editeurs/afficher.tpl'), $data);