<fieldset id="mode-livraison" class="panel">
	<legend>Mode de livraison</legend>
	<div class="zone-livraison">
		<div id="transporteurs">
			<div id="loader_transporteurs">
				<h4>Chargement en cours</h4>
				<img src="{assets('loader.gif')}">
			</div>
			{$transporteurs}
		</div>
		<p id="frais-port" class="text-right">
			<span class="">{lang('frais_de_port')} : </span>
			
			{if $sur_devis}
				<span class="montant">Sur Devis</span>
			{else}
				<span class="montant">{$frais_port}</span>
			{/if}
		</p>
	</div>
</fieldset>