<div class="module-boutique etape4">
	<div class="inner_interne">
		{$guide_etapes}
	
		{if $cgv_non_valide}
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-danger col-md-12">{lang('erreur_validation_cgv')}</div>
				</div>	
			</div>
		{/if}
		
		<form action="" method="post" role="form" data-action_compte="/commande-compte.html" data-action_cheque="/commande-cheque.html" data-action_carte="">
			<fieldset>
				
				<div class="row">
					<div class="col-md-12">
						{$panier}
						{$commentaire}
					</div>
					
					<div class="col-md-6">
						<div id="panel-facturation" class="panel">
							<legend>{lang('adresse_facturation')}</legend>
							<div class="panel-body">
								{$adresses.facturation.prenom} {$adresses.facturation.nom}<br />
								{if $adresses.facturation.societe} {$adresses.facturation.societe} <br/> {/if}
								{$adresses.facturation.adresse}<br />
								{$adresses.facturation.code_postal} {$adresses.facturation.ville}<br />
								{$adresses.facturation.pays}<br />
								<span class="numero-telephone">{$adresses.facturation.telephone}</span>
								
							</div>
						</div>
					</div>
					{if $livraison}
					
					<div class="col-md-6">
						<div id="panel-livraison" class="panel">
							<legend>{lang('adresse_livraison')}</legend>					
							<div class="panel-body">
								{$adresses.livraison.prenom} {$adresses.livraison.nom}<br />
								{if $adresses.livraison.societe} {$adresses.livraison.societe} <br/> {/if}
								{$adresses.livraison.adresse}<br />
								{$adresses.livraison.code_postal} {$adresses.livraison.ville}<br />
								{$adresses.livraison.pays}<br />
								<span class="numero-telephone">{$adresses.livraison.telephone}</span>
							</div>
						</div>
					</div>
					{/if}
					<br class="clear-both">
						{if !$sur_devis}
							{if $est_pro}
								<div id="enregistrement_devis" class="col-md-6">
									<h2>Enregistrez votre Devis</h2>
									<p>Vous pouvez enregistrer votre devis, l'imprimer pour y renenir ultérieurement</p>
									<a href="/boutique/enregistrer-commande.html" class="hvr-sweep-to-right sweep-black btn btn-inverse"> Enregistrer mon devis</a>
								</div>
							{/if}	
							
							<div id="choix_mode_paiement" class="col-md-6">
								<h2>Choisissez votre mode de paiement</h2>
								{if $est_pro}
								<div>
										<input type="radio" name="choix_type_paiement" value="compte" id="choix_paiement_compte" autocomplete="off"> 
										<label for="choix_paiement_compte">Paiement à réception de facture</label>
								</div>	
								{/if}
								<div>
										<input type="radio" name="choix_type_paiement" value="carte" id="choix_paiement_carte" autocomplete="off">
										<label for="choix_paiement_carte" class="img-secu">Paiement par carte de crédit  <img src="/assets/img/ico-paiement-s.png" alt="" /></label>
								</div>
								<div>
										<input type="radio" name="choix_type_paiement" value="cheque" id="choix_paiement_cheque" autocomplete="off">
										<label for="choix_paiement_cheque">Paiement par chèque</label>
								</div>
							</div>
							<br class="clear-both">
							{$paiement}
							{$cgv}
							<input type="hidden" name="commande_id" value="{$commande_id}">
							<button type="submit" class="btn btn-primary pull-right" name="submit-etape4" value="1">{lang('terminer_ma_commande')}</button>
						{else}
						
							<a href="/boutique/commandes/envoyer_commande_sur_devis_frais_port" class="hvr-sweep-to-right sweep-black envoi_devis btn btn-primary">Envoyer mon devis pour estimation</a>
						{/if}
				</div>
			</fieldset>
		</form>
	</div>
</div>