<div class="row">

	<div class="tableau-panier">{$panier}</div>
	<div class="colonne-montant">
		<div id="sous-total" class="ligne-montant">
			<span class="libelle">{lang('sous_total')} {lang('ttc')}</span>
			<span class="montant">{html_price($montants.sous_total)}</span>
		</div>
		{if $remise}{$remise}{/if}
		{if $livraison}{$livraison}{/if}
		{if $acompte}
			<div id="total-commande" class="ligne-montant">
				<span class="libelle">{lang('total')}</span>
				<span class="montant">{html_price($montants.total)}</span>
			</div>
			<div id="total-paye" class="ligne-montant">
				<span class="libelle">{lang('total_a_payer')} <span>{if $accompte > 0}({$acompte}){/if}</span></span>
				<span class="montant">{html_price($montants.acompte)}</span>
			</div>
		{else}
			<div id="total-commande" class="ligne-montant">
				<span class="libelle">{lang('total')} {lang('ttc')}</span>
				<span class="montant">{html_price($montants.total)}</span>
			</div>
		{/if}
	</div>
	<div class="col-md-12">	
	
		
		{$commentaire}
	</div>
	
	<div class="clearfix"></div>

	<div class="col-md-6">
		<a href="/" id="bt-continuer" class="btn btn-default btn-lg pull-left">
<!-- 			<span class="glyphicon glyphicon-chevron-left"></span> -->
			{lang('continuer_achat')}
		</a>
	</div>
	
	<div class="col-md-6">
		<button type="submit" id="bt-commander" class="btn btn-primary btn-lg pull-right" name="submit-etape1" value="1" >
			{lang('commander')}
<!-- 			<span class="glyphicon glyphicon-chevron-right"></span> -->
		</button>
	</div>
</div>

