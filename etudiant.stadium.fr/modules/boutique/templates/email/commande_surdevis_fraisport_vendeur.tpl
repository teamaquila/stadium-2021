<p style="margin-top: 0px;">
	Le {$presentation.date_jour},<br /><br />
	Une nouvelle commande a été effectuée sur le site, impliquant un devis concernant les frais de port<br /><br />
	<span style="font-weight: bold; font-size: 14px;">Devis&nbsp;{$presentation.reference}&nbsp;{lang('du')}&nbsp;{$presentation.date_commande}</span><br /><br />
</p>

<table id="adresses">
	<tr>
		<th style="text-align: left;">{lang('adresse_livraison')}</th>
		<th style="text-align: left;">{lang('adresse_facturation')}</th>
	</tr>
	<tr>
		<td style="width: 230px;">
			{$livraison.prenom}&nbsp;{$livraison.nom}<br />
			{$livraison.adresse}<br />
			{$livraison.code_postal}&nbsp;-&nbsp;{$livraison.ville}<br />
			{$livraison.pays}<br />
			{$livraison.telephone}
		</td>
		<td style="width: 230px;">
			{$facturation.prenom}&nbsp;{$facturation.nom}<br />
			{$facturation.adresse}<br />
			{$facturation.code_postal}&nbsp;-&nbsp;{$facturation.ville}<br />
			{$facturation.pays}<br />
			{$facturation.telephone}
		</td>
	</tr>
</table>

<p>
	{$articles}
</p>
{if $commmentaire}
<p>
	<strong>Commentaire sur la commande : </strong>
	{$commmentaire}
</p>
{/if}
