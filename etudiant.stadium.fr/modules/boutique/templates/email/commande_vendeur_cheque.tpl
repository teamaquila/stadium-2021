<p style="margin-top: 0px;">
	Bonjour,<br />
	<br />
	Une nouvelle commande en attente de paiement par chèque est disponible sous la référence <strong>{$presentation.reference}</strong>
</p>
<br />
<strong>Client :</strong> {$presentation.prenom} {$presentation.nom} ({$presentation.email})
<br /><br />
<strong>Montant total :</strong> {$presentation.montant}{lang('devise')}
<br /><br />
<strong>Liste des articles :</strong><br />
{$articles}