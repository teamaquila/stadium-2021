<p style="margin-top: 0px;">
	Bonjour {$presentation.prenom}&nbsp;{$presentation.nom},<br /><br />
 
	Vous avez enregistré un devis sur notre site www.stadium.fr il y a deux semaines, et vous ne l'avez pas finalisé depuis.<br />
	 
	<a href="http://www.stadium.fr">Finaliser mon devis </a><br />

	Dans l’attente de votre retour, nous restons à votre disposition pour tous renseignements complémentaires.<br /><br />
	 
	Cordialement,<br />
	L’équipe Stadium<br />
</p>

