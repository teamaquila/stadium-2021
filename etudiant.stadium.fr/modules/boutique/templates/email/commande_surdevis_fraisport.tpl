<p style="margin-top: 0px;">
	Le {$presentation.date_jour},<br /><br />
	{lang('bonjour')}&nbsp;{$presentation.prenom}&nbsp;{$presentation.nom},<br /><br />
	Nous avons le plaisir de vous confirmer l'envoi de votre devis au service commercial, pour deviser le montant des frais de port.<br /><br />
	<span style="font-weight: bold; font-size: 14px;">Devis&nbsp;{$presentation.reference}&nbsp;{lang('du')}&nbsp;{$presentation.date_commande}</span><br /><br />
</p>

<table id="adresses">
	<tr>
		<th style="text-align: left;">{lang('adresse_livraison')}</th>
		<th style="text-align: left;">{lang('adresse_facturation')}</th>
	</tr>
	<tr>
		<td style="width: 230px;">
			{$livraison.prenom}&nbsp;{$livraison.nom}<br />
			{$livraison.adresse}<br />
			{$livraison.code_postal}&nbsp;-&nbsp;{$livraison.ville}<br />
			{$livraison.pays}<br />
			{$livraison.telephone}
		</td>
		<td style="width: 230px;">
			{$facturation.prenom}&nbsp;{$facturation.nom}<br />
			{$facturation.adresse}<br />
			{$facturation.code_postal}&nbsp;-&nbsp;{$facturation.ville}<br />
			{$facturation.pays}<br />
			{$facturation.telephone}
		</td>
	</tr>
</table>

<p>
	{$articles}
</p>
{if $commmentaire}
<p>
	<strong>Commentaire sur la commande : </strong>
	{$commmentaire}
</p>
{/if}
<p>
	Nous vous recontacterons dans les plus bref délais pour finaliser votre commande.<br />
	<br />
	{lang('remerciement')}&nbsp;{$lien_site}.<br />
	<br />
	{lang('equipe')}&nbsp;{$nom_equipe}
</p>