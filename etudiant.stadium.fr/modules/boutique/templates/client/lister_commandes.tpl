<div class="client compte liste-commandes">
	<div class="bloc_titre_h1">
		<h1>Vos commandes</h1>
	</div>
	<div class="inner_interne">
	<p class="welcome-client text-center">Bienvenue <span>{$prenom} {$nom}</span></p>
	
		<div class="row">
			<div class="col-md-3 menu-compte">
				{$lien.commandes}
				{$lien.paniers}
			    {$lien.adresses}
			    {$lien.mot_de_passe}
			    <!-- <a href="https://www.stadium.fr/medias/pdf/2021/bon-commande.pdf" class="bt_compte lien-telechargement-inverse">Bon de commande</a>-->
			    {$lien.deconnexion}
		    </div>
			<div class="col-md-9">
				<h2>Historique des commandes ({$nb_commandes})</h2>
				{$commandes}
			</div>
		</div>
	</div>
	
</div>
