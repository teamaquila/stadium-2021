<?php
//Présentation
$data1['presentation'] = array(
	'date_jour'	=> date_type(mdate('%Y-%m-%d'), 1),
	'prenom'	=> $client->prenom,
	'nom'		=> $client->nom,
	'reference'	=> $commande->reference,
    'date_commande'	=> datetime_type($commande->created),
    'montant'	=> html_price($commande->montant),
);

$data1['commentaire'] = $commande->commentaire;

//Adresses
$data1['livraison'] = array(
	'societe'	=> $livraison->societe,
	'prenom'	=> $livraison->prenom,
	'nom'		=> $livraison->nom,
	'adresse'	=> $livraison->adresse,
	'code_postal'=> $livraison->code_postal,
	'ville'		=> $livraison->ville,
	'pays'		=> $livraison->pays,
	'telephone'	=> $livraison->telephone,
);
$data1['facturation'] = array(
	'societe'	=> $facturation->societe,
	'prenom'	=> $facturation->prenom,
	'nom'		=> $facturation->nom,
	'adresse'	=> $facturation->adresse,
	'code_postal'=> $facturation->code_postal,
	'ville'		=> $facturation->ville,
	'pays'		=> $facturation->pays,
	'telephone'	=> $facturation->telephone,
);

//Articles commandés
$articles = NULL;
foreach( $produits as $produit )
{
	$articles .= $produit->quantite.' x'.nbs(2).$produit->libelle.br();
}

$data1['articles']		= $articles;
$data1['lien_site']		= anchor(base_url(), $version->intitule);
$data1['nom_equipe']	= $version->intitule;

if (file_exists(ROOTPATH.'assets/img/newsletter/logo.png'))
{
    $attributs['src'] = 'assets/img/newsletter/logo.png';
    $logo = img($attributs);
}
elseif (file_exists(ROOTPATH.'assets/img/newsletter/logo.jpg'))
{
    $attributs['src'] = 'assets/img/newsletter/logo.jpg';
    $logo = img($attributs);
}

$data = array(
	'contenu' => $this->dwootemplate->get(tpl_path('boutique/email/commande_cheque.tpl'), $data1),
	'email_vendeur' => mailto($module->vendeur_email, $module->vendeur_email),
	'logo'     => $logo
);

$this->dwootemplate->output(tpl_path('boutique/client/email.tpl'), $data);