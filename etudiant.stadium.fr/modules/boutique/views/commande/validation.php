<?php
switch($commande->etat())
{
	//Payée
	case 2:
		//Acompte
		if ($module->option_acompte && ($commande->acompte > 0))
		{
			$message = heading('Votre acompte de la commande est payé', 3).br();
			$message .= '<strong>Montant de votre acompte :</strong> <span class="montant">'.html_price($commande->acompte).'</span>'.br();
			$message .= anchor('/boutique/commandes/exporter/'.$commande->id, 'Télécharger la facture<br/>d\'acompte au format PDF', 'class="nouvel_onglet btn btn-primary"').br();
			$message .= '<p>Vous avez des questions ? Contactez-notre équipe commerciale.<br/><a class="bouton" href="/contactez-nous,19.html">Contactez-nous</a></p>'.br();
			$message .= '<p id="remerciements">Nous vous remercions d\'avoir commandé sur notre site.</p>'.br();
			$conversion = TRUE;
		}
		//Paiement intégral
		else
		{
			$message = heading('Votre commande est payée', 3).br();
			$message .= '<strong>Montant de votre commande :</strong> <span class="montant">'.html_price($commande->montant).'</span>'.br();
			$message .= anchor('/boutique/commandes/exporter/'.$commande->id, 'Télécharger la facture<br/>au format PDF', 'class="nouvel_onglet btn btn-primary"').br();
			$message .= '<p>Vous avez des questions ? Contactez-notre équipe commerciale.<br/><a class="bouton" href="/contactez-nous,19.html">Contactez-nous</a></p>'.br();
			$message .= '<p id="remerciements">Nous vous remercions d\'avoir commandé sur notre site.</p>'.br();
			$conversion = TRUE;
		}
	break;
	
	//Annulée
	case 5:
		$message  = heading('Votre commande est annulée', 3).br();
		$message .= '<p>Le traitement de votre commande ne sera pas pris en compte.'.br();
		$message .= '<p>Vous rencontrez un problème ? Contactez-notre équipe commerciale.<br/><a class="bouton" href="/contactez-nous,19.html">Contactez-nous</a></p>'.br();
		$message .= 'Nous vous souhaitons une agréable visite sur notre site.</p>'.br();
		$conversion = FALSE;
	break;
	
	//Réfusée
	case 6:
		$message  = heading('Votre paiement est refusée par la banque', 3).br();
		$message .= '<strong>Le règlement de votre commande a été refusé par la banque.'.br();
		$message .= 'Nous vous invitons à vérifier vos informations ou autorisations bancaires.</strong>'.br(2);
		$message .= '<p>Vous rencontrez un problème ? Contactez-notre équipe commerciale.<br/><a class="bouton" href="/contactez-nous,19.html">Contactez-nous</a></p>'.br();
		$message .= 'Nous vous souhaitons une agréable visite sur notre site.'.br();
		$conversion = FALSE;
	break;
	
	//En attente de paiement
	case 7:
	    $message  = heading('Votre commande est enregistrée', 3).br();
	    $message .= '<strong>Vous pouvez imprimer votre devis, et revenir finaliser votre commande ultérieurement.'.br(2);
	    $message .= 'Nous vous souhaitons une agréable visite sur notre site.'.br();
	    $conversion = FALSE;
    break;
    
	case 8:
	    $message = heading('Votre commande est acceptée sur votre compte', 3).br();
	    $message .= '<strong>Montant de votre commande :</strong> <span class="montant">'.html_price($commande->montant).'</span>'.br();
	    $message .= anchor('/boutique/commandes/exporter/'.$commande->id, 'Télécharger le bon de commande au format PDF', 'class="_blank telechargement btn btn-primary"').br();
	    $message .= '<p>Vous avez des questions ? Contactez-notre équipe commerciale.<br/><a class="bouton" href="/contactez-nous,19.html">Contactez-nous</a></p>'.br();
	    $message .= '<p id="remerciements">Nous vous remercions d\'avoir commandé sur notre site.</p>'.br();
	    $conversion = TRUE;
	    break;
	    
	case 9:
	    $message  = heading('Votre devis a été envoyé', 3).br();
	    $message .= '<strong>Le devis correspondant à votre commande vient d\'être envoyé au service commercial pur estimation des frais de port.'.br();
	    $message .= 'Nous vous recontacterons dans un délais de 48h.</strong>'.br(2);
	    $message .= 'Nous vous souhaitons une agréable visite sur notre site.';
	    $conversion = FALSE;
	    break;
    
	case 10:
	    $message = heading('Votre commande est en attente de réception du chèque', 3).br();
	    $message .= '<div class="message-cheque text-left"><strong>Montant de votre commande :</strong> <span class="montant">'.html_price($commande->montant).'</span>'.br();
	    $message .= anchor('/boutique/commandes/exporter/'.$commande->id, 'Télécharger le bon de commande au format PDF', 'class="_blank telechargement btn btn-primary"').br();
	    $message .= "Afin de finaliser votre commande, merci de suivre les instructions suivantes : <br/>
					<ul>
	        
					    <li> Imprimer votre bon commande ci dessus (ou recopier sur papier libre votre nom, prénom, adresse postale, e-mail, N° de commande et N° de téléphone).</li>
					    <li> Etablir votre chèque à l’ordre de Stadium d'un montant de <span class='montant'>".html_price($commande->montant)."</span></li>
					        
					    <li> Envoyer une photocopie de votre pièce d’identité (seules les pièces d’identité suivantes sont acceptées :
					    		Carte Nationale d’Identité, Permis de conduire, Passeport, Carte de résident);
					    		Deux pièces d’identités sont exigées pour tout paiement supérieur à <span class='montant'>200</span> euros TTC.</li>
					    <li>
						    Envoyer votre confirmation de commande et votre chèque à l’adresse suivante : <br/>
					        
						    STADIUM<br/>
                            16 rue des Granits
                            44100 Nantes<br/>
						</li>
					        
					</ul> <br/>
					<b>La commande sera traitée à réception du chèque et après la validation de celui-ci.</b> <br/><br/>";
	    $message .= '<p>Vous rencontrez un problème ? Contactez-notre équipe commerciale.<br/><a class="bouton" href="/contactez-nous,19.html">Contactez-nous</a></p>';
	    $message .= '<p id="remerciements">Nous vous remercions d\'avoir commandé sur notre site.</p></div>';
	    $conversion = TRUE;
	break;
	
	default:
		$message = 'ERREUR ETAT';
		$conversion = FALSE;
	break;
}

$this->load->model('livraison');
$livraison = new Livraison();
$livraison->where('commande_id', $commande->id)->order_by('id', 'desc')->limit(1)->get();

$frais_port = '';
if($livraison->exists())
{
    $frais_port = $livraison->frais_port;
}

$data = array(
	'commande'	=> get_object_vars($commande),
    'frais_port'    => $frais_port,
	'produits'	=> $commande->produits()->all_to_array(),
	'code_ua'	=> $parametres->ga_code_ua,
    'conversion'    => $conversion,
	'message' 	=> $message,
	'url' 		=> array('accueil' => base_url())
);

$this->dwootemplate->output(tpl_path('boutique/commande/validation.tpl'), $data);