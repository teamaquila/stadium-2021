<?php
$html = NULL;

$attributs = array('id' => 'gestion-cgv');

if (isset($cgv_non_validee) && $cgv_non_validee)
{
	$attributs['class'] = 'alert alert-danger';
}

$html .= form_fieldset(NULL, $attributs);
$html .= form_checkbox(array(
	'name' 		=> 'cgv_validee',
	'value' 	=> 1,
	'checked' 	=> FALSE
));
$html .= '<p>'.insert_anchor($module->lien_cgv, lang('boutique_cgv'), array('class'=>"nouvel_onglet lien-standard")).'</p>';

$html .= form_fieldset_close();

echo $html;