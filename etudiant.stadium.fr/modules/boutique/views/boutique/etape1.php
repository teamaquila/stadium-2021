<?php
//Panier plein
if ($this->cart->total_items() > 0)
{
	$data = array(
		'guide_etapes'	=> $guide_etapes,
		'panier'		=> $panier,
		'livraison'		=> $livraison,
		'remise'		=> $remise,
		'acompte'		=> $acompte,
		'montants'		=> $montants,
		'commentaire'	=> NULL,
	    'date_livraison'=> NULL,
		'client'		=> $client,
		'url' => array(
			'formulaire' => '/boutique/etape,1.html'
		)
	);
	
	// Date de livraison
	if ($module->option_date_livraison)
	{
	    $html = '<div id="date_livraison" class="">';
	    $html .= form_label(lang('date_livraison'), 'date_livraison');
	    $html .= form_input(array(
	        'id'	=> 'date_livraison',
	        'name'	=> 'date_livraison',
	        'class'	=> 'form-control',
	        'data-provide'			=> "datepicker",
	        'data-date-language'	=> "fr-FR",
	        'data-date-format'		=>"dd/mm/yyyy",
	        'value'	=> date_type($date_livraison,mdate('%Y-%m-%d'), 2)
	    ));
	    $html .= '</div>';
	    
	    $data['date_livraison'] = $html;
	}
	
	//Commentaire sur la commande
	if ($module->option_commentaire_commande)
	{
		$html  = '<div id="commentaire"><h4>Commentaires</h4>';
		$html .= form_textarea(array(
			'name' 	=> 'commentaire',
			'class'	=> 'form-control',
			'rows'	=> 3,
			'value'	=> $commentaire,
			'placeholder' => lang('boutique_ajouter_commentaire_commande')
		));
		$html .= '</div>';
				
		$data['commentaire'] = $html;
	}
	
	$data['contenu_ajax'] = $this->dwootemplate->get(tpl_path('boutique/boutique/etape1_ajax.tpl'), $data);
	
	$this->dwootemplate->output(tpl_path('boutique/boutique/etape1.tpl'), $data);
}
//Panier vide
else
{
	$html  = '<div class="module-boutique etape1">'.$guide_etapes;
	$html .= '<div class="liste-vide bg-info text-info"><img src="'.assets('boutique/emptycart.png').'" class="img_panier_vide"><br><br/>'.lang('panier_vide').'</div></div>';
	echo $html;
}