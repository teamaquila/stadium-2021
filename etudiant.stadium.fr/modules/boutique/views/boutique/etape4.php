<?php
if ( ! is_null($commande))
{
	$data = array(
		'guide_etapes'		=> $guide_etapes,
		'paiement' 			=> $paiement,
		'cgv' 				=> $cgv,
		'cgv_non_valide' 	=> $cgv_non_valide,
		'panier'			=> $panier,
		'adresses' 			=> array(
			'facturation'	=> get_object_vars($commande->facturation())
		),
	    'est_pro'			=> $est_pro,
		'livraison'	=> FALSE,
	    'commande_id'		=> $commande->id,
	    'sur_devis'	=> (isset($this->session->userdata('livraison')['sur_devis']) && ($this->session->userdata('livraison')['sur_devis'] == TRUE)) ? TRUE : FALSE
	);
	
	//Livraison (en option)
	$livraison = $commande->livraison();
	
	if ($livraison->exists())
	{
		$data['livraison'] = TRUE;
		$data['frais_port'] = $commande->frais_port;
		$data['adresses']['livraison'] = get_object_vars($livraison);
		$data['transporteur'] = $commande->transporteur()->nom;
	}
	//Commentaire sur la commande
	if ($module->option_commentaire_commande)
	{
	    $html = NULL;
	    if($commande->commentaire != '')
	    {
	        $html = '<div class="col-md-12">';
	        $html .= ' <span id="legend-commentaire">Commentaire</span>';
	        $html .= ' <div id="bloc-commentaire">';
	        $html .= $commande->commentaire;
	        $html .= ' </div>';
	        $html .= '</div>';
	    }

	    $data['commentaire'] = $html;
	}
	
	$this->dwootemplate->output(tpl_path('boutique/boutique/etape4.tpl'), $data);
}
//Erreur de commande
else
{
	$html  = '<div class="module-boutique etape4">'.$guide_etapes;
	$html .= '<div class="liste-vide bg-danger text-danger">'.lang('erreur_enregistrement_commande').'</div></div>';
	echo $html;
}