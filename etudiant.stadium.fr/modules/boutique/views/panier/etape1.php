<?php
//Si le client est connecté, on peut utiliser sont statut pour le tarif (ex : professionnel)
$client = NULL;
if ($compte = json_decode($this->encrypt->decode(get_cookie('client'))))
{
    $client = $this->client->get_by_id($compte->client_id);
    
    if ( ! $client->exists())
    {
        $client = NULL;
    }
}
$une_gravure = FALSE;

if ($this->cart->total_items() > 0)
{
	$ligne_panier = NULL;
	$total_ttc = 0;
	$colones_remises = FALSE;
	$utilisateur = $this->clients->est_connecte();
	
	foreach ($panier as $item)
	{
	    $url_produit = NULL;
		$produit = $this->produit->include_related('module_catalogue', array('gestion_stock'), FALSE)->get_by_id($item['id']);
		
		//Image du produit
		$media = html_media($produit->medias_associes(), array(
			'max_item' 	=> 1,
			'taille'	=> '50x50'
		));
		
		$ligne_panier .= '<tr id="'.$item['rowid'].'">';
		
		if (!isset($item['options']['produit_impose']) && (!isset($item['options']['pack_parent'])))
		{
		    
		    $ligne_panier .= '<td class="td-1">';
		    $ligne_panier .= form_button(array(
		        'class' => 'supprimer-produit',
		        'content' => '<span class="glyphicon glyphicon-remove"></span>',
		        'data-id' => $item['rowid']
		    ));
		    $ligne_panier .= '</td>';
		}
		else
		{
		    
		    $ligne_panier .= '<td class="td-1" style="text-align : right">';
		    $ligne_panier .= '<img src="/assets/img/fleche_categorie.png">';
		    $ligne_panier .= '</td>';
		}

		$ligne_panier .= '<td class="td-2">';
		$ligne_panier .= '<span class="image">'.$media.'</span>';
		
		if (strpos($produit->reference, 'GRV' ) === FALSE)
		{
		    $url_produit = (($produit->produit_parent()->url())) ? ($produit->produit_parent()->url()) : ($produit->url());
		}
		
		$ligne_panier .= '<span class="description">'.anchor($url_produit, $produit->libelle).'</span>'.br();		
		$ligne_panier .= '<span class="reference">Référence : '.$produit->reference.'</span>';
		
		if((isset($item['options']['gravure_complementaire']) || (isset($item['options']['associe_gravure']))) && isset($item['options']['produit_referent_id']))
		{
		    $produit_referent = new Produit();
		    $produit_referent->get_by_id($item['options']['produit_referent_id']);
		    if (isset($item['options']['gravure_complementaire']))
		    {
		        $ligne_panier .= br().'<span class="gravure">Gravure : '.$item['options']['gravure_complementaire'].'</span>';
		    }
		    if (isset($item['options']['associe_gravure']))
		    {
		        $ligne_panier .= br().'<span class="gravure">Fichier associe : '.$item['options']['associe_gravure'].'</span>';
		    }
		    $ligne_panier .= br().'<span class="gravure">Produit référent : '.$produit_referent->reference.'</span>';
		    
		    $une_gravure = TRUE;
		}
		
		
		//Permettre la modification des options
		if (isset($item['options']['gravure_complementaire']) || isset($item['options']['associe_gravure']))
		{
		    $ligne_panier .= '<div class="modification_gravure">';
		    $ligne_panier .= '<a href="javascript:void(0)" class="toggle_modif hvr-sweep-to-right"> > Modifier la gravure</a>';
		    $ligne_panier .= '<div class="zone-modifie-options">';
		    $ligne_panier .= '<div class="row">
										<textarea class="col-md-6" placeholder="Entrez votre texte" name="options[gravure_complementaire]" cols="" rows="" >'.$item['options']['gravure_complementaire'].'</textarea>
										<div class="col-md-6">
											<input type="hidden" value="'.$produit->id.'" name="produit_id">
											<label for="">Joindre un fichier (format .xls ou .xlsx)</label>
											<input class="fichier_gravure" type="hidden" name="options[associe_gravure]">
											<span class="fileinput-button">
												<span>Ajouter votre fichier</span>
												<input type="file" name="import_fichier" value="'.$item['options']['associe_gravure'].'" class="import_fichier">
											</span>
											<span class="indicateur"></span>
											<div class="bloc_import_fichier">'.$item['options']['associe_gravure'].'</div>
										</div></div>';
		    $ligne_panier .= '<a href="javascript:void(0)" data-row_id="'.$item['rowid'].'" class="save_modif_options">Enregistrer les modifications</a>';
		    $ligne_panier .= '</div>';
		    $ligne_panier .= '</div>';
		}
		
		$ligne_panier .= '</td>';
		
		//Produit payant
		if ($item['price'] > 0)
		{
		    //Si le produit en lui même est remisé
		    $tarif = $produit->tarif($utilisateur);

		    $ligne_panier .= '<td class="td-3">'.html_price($tarif['prix']).lang('ht');
			

			if ($tarif['prix_sans_remise'] != $tarif['prix'])
			{
				$ligne_panier .= br();
				$ligne_panier .= '<span class="zone_remise small">';
				$ligne_panier .= '<span class="prix-non-remise">'.html_price($tarif['prix_sans_remise']).'<span class="taxe">'.lang('ttc').'</span></span>';
				$ligne_panier .= '<span class="taux-remise label label-info">- '.html_percentage($tarif['taux_remise']).'</span>';
				$ligne_panier .= '</span>';
			}
			$ligne_panier .= '</td>';
			
			if (!isset($item['options']['pack_parent']))
			{
			    //Gestion du stock
			    if ($produit->gestion_stock)
			    {
			        $stock = range(0, $produit->stock, $produit->conditionnement);
			        unset($stock[0]);
			        
			        $ligne_panier .= '<td class="td-4">'.form_dropdown('quantite['.$item['rowid'].']', $stock, $item['qty'], 'class="quantite form-control"').'</td>';
			    }
			    //Achat illimité
			    else
			    {
			        $quantite = '<div class="input-group input-qte">';
			        $quantite .= '<span class="bt-moins bt-qte">'.form_button(array(
			            'class' => 'btn btn-default',
			            'content' => '-'
			        )).'</span>';
			        $quantite .= form_input(array(
			            'type'  => 'text',
			            'name'	=> 'quantite['.$item['rowid'].']',
			            'class' => 'quantite form-control',
			            'value'	=> $item['qty']
			        ));
			        $quantite .= '<span class="bt-plus bt-qte">'.form_button(array(
			            'class' => 'btn btn-default',
			            'content' => '+'
			        )).'</span>';
			        $quantite .= '</div>';
			        
			        $ligne_panier .= '<td class="td-4">'.$quantite.'</td>';

			    }
			    $ligne_panier .= '<td class="td-5">'.html_price($tarif['prix']*$item['qty']).nbs().lang('ht').'</td>';
			}
			else
			{
			    $ligne_panier .= '<td class="td-4"></td>';
			    $ligne_panier .= '<td class="td-5">'.html_price($tarif['prix']*$item['qty']).nbs().lang('ht').'</td>';
			}
			
			
			
			
			
			//Si remise en pourcentage, et produit remisable afficher le pourcentage et le montant de la remise
			
			if ($remise = $this->order->discount())
			{
				$code_remise = $this->code_remise->get_by_code($remise['code']);
				if (($code_remise->type_reduction == 1) && ($produit->remisable))
				{
					if (isset($remise['produit_associes']))
					{
						if (array_key_exists($item['id'], $remise['produit_associes']))
						{
							$colones_remises = TRUE;
								
							$montant_remise = ((($item['price']*$item['qty'])*($code_remise->reduction/100)));
								
							$ligne_panier.= '<td class="td-6"><span>'.html_price($montant_remise).lang('ttc').br().'</span><span class="mont_remise_panier label label-info">-'.$code_remise->reduction.'%</span></td>';
							$ligne_panier.= '<td class="td-5">'.html_price(($item['price']*$item['qty'])-($montant_remise)).lang('ttc').'</td>';
								
			
							$cart_contents = $this->session->userdata('cart_contents');
							$cart_contents[$item['rowid']]['options']['html_price_promo']= ($item['price']*$item['qty'])-($montant_remise);
							$this->session->set_userdata('cart_contents', $cart_contents);
			
						}
						else
						{
							$ligne_panier .= '<td class="td-6"></td>';
							$ligne_panier .= '<td class="td-5">'.html_price($item['price']*$item['qty']).lang('ttc').'</td>';
								
						}
					}
					else
					{
						$colones_remises = TRUE;
			
						$montant_remise = ((($item['price']*$item['qty'])*($code_remise->reduction/100)));
			
						$ligne_panier.= '<td class="td-6"><span>'.html_price($montant_remise).br().'</span><span class="mont_remise_panier label label-info">-'.$code_remise->reduction.'%</span></td>';
			
						$ligne_panier.= '<td class="td-5">'.html_price(($item['price']*$item['qty'])-($montant_remise)).lang('ttc').'</td>';
					}
				}
				else
				{
					$html_price_ht = ($item['price']*$item['qty'])*(100/($produit->tarif($utilisateur)['tva'] + 100));
					$ligne_panier .= '<td class="td-5">'.html_price($item['price']*$item['qty']).lang('ttc').'</td>';
				}
			}
			else
			{
				
				$html_price_ht = ($item['price']*$item['qty'])*(100/($produit->tarif($utilisateur)['tva'] + 100));
				$ligne_panier .= '<td class="td-6">'.html_price($item['price']*$item['qty']).lang('ttc').'</td>';
			
			}
			
		}
		//Produit gratuit
		else
		{
			
			if (isset($item['options']['is_pack']))
			{
			    
			    $ligne_panier .= '<td class="td-3">-----</td>';
			    //Gestion du stock
			    if ($produit->gestion_stock)
			    {
			        $stock = range(0, $produit->stock, $produit->conditionnement);
			        unset($stock[0]);
			        
			        $ligne_panier .= '<td class="td-4">'.form_dropdown('quantite['.$item['rowid'].']', $stock, $item['qty'], 'class="quantite form-control"').'</td>';
			    }
			    //Achat illimité
			    else
			    {
			        $quantite = '<div class="input-group input-qte">';
			        $quantite .= '<span class="bt-moins bt-qte">'.form_button(array(
			            'class' => 'btn btn-default',
			            'content' => '-'
			        )).'</span>';
			        $quantite .= form_input(array(
			            'type'  => 'text',
			            'name'	=> 'quantite['.$item['rowid'].']',
			            'class' => 'quantite form-control',
			            'value'	=> $item['qty']
			        ));
			        $quantite .= '<span class="bt-plus bt-qte">'.form_button(array(
			            'class' => 'btn btn-default',
			            'content' => '+'
			        )).'</span>';
			        $quantite .= '</div>';
			        
			        $ligne_panier .= '<td class="td-4">'.$quantite.'</td>';
			    }
			    $ligne_panier .= '<td class="td-5">-----</td>';
			}
			else
			{
			    $ligne_panier .= '<td class="td-3">'.lang('gratuit').'</td>';
			    //Gestion du stock
			    if ($produit->gestion_stock)
			    {
			        $stock = range(0, $produit->stock, $produit->conditionnement);
			        unset($stock[0]);
			        
			        $ligne_panier .= '<td class="td-4">'.form_dropdown('quantite['.$item['rowid'].']', $stock, $item['qty'], 'class="quantite form-control"').'</td>';
			    }
			    //Achat illimité
			    else
			    {
			        $quantite = '<div class="input-group input-qte">';
			        $quantite .= '<span class="bt-moins bt-qte">'.form_button(array(
			            'class' => 'btn btn-default',
			            'content' => '-'
			        )).'</span>';
			        $quantite .= form_input(array(
			            'type'  => 'text',
			            'name'	=> 'quantite['.$item['rowid'].']',
			            'class' => 'quantite form-control',
			            'value'	=> $item['qty']
			        ));
			        $quantite .= '<span class="bt-plus bt-qte">'.form_button(array(
			            'class' => 'btn btn-default',
			            'content' => '+'
			        )).'</span>';
			        $quantite .= '</div>';
			        
			        $ligne_panier .= '<td class="td-4">'.$quantite.'</td>';
			    }
			    $ligne_panier .= '<td class="td-5">'.lang('gratuit').'</td>';
			}
		}
		
		$ligne_panier .= '</tr>';
	}
	
	$data = array(
		'ligne_panier'	=> $ligne_panier,
		'colones_remise'	=> $colones_remises
	);
	
	$this->dwootemplate->output(tpl_path('boutique/panier/etape1.tpl'), $data);
}
else
{
	echo lang('panier_vide');
}