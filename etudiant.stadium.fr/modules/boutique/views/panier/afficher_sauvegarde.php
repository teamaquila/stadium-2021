<?php
//Liste des articles commandés
$liste_produits = NULL;

foreach ($produits as $produit)
{
	$obj_produit = $this->produit->get_by_reference($produit->reference);
	
	
	if ($obj_produit->exists())
	{
		$liste_produits .= '<tr>';
		
		$liste_produits .= '<td class="image c-td-0">'.html_media($obj_produit->produit_parent()->medias_associes(), array(
				'max_item' 	=> 1,
				'taille'	=> '50x51'
		)).'</td>';
		$liste_produits .= '<td class="reference c-td-1">'.$produit->reference.'</td>';
		$liste_produits .= '<td class="libelle c-td-2">'.$produit->libelle.'</td>';
		$liste_produits .= '<td class="prix c-td-3">'.html_price($produit->prix).'</td>';
		
		if ($produit->quantite > $obj_produit->stock)
		{

			$liste_produits .= '<td class="quantite c-td-4"> <span class="label label-info">'.$produit->quantite.'</span></td>';
		}
		else
		{

			$liste_produits .= '<td class="quantite c-td-4">'.$produit->quantite.'</td>';
		}
		
		$liste_produits .= '</tr>';
			
	}
	else
	{
		$liste_produits .= '<tr class="danger">';
		$liste_produits .= '<td class="image c-td-0"></td>';
		$liste_produits .= '<td class="reference c-td-1">'.$produit->reference.'</td>';
		$liste_produits .= '<td class="libelle c-td-2">'.$produit->libelle.'</td>';
		$liste_produits .= '<td class="prix c-td-3">'.html_price($produit->prix).'</td>';
		$liste_produits .= '<td class="quantite c-td-4">'.$produit->quantite.'</td>';
		$liste_produits .= '<td class="quantite_dispo c-td-8"><span class="indispo label label-danger">Indisponible</span></td>';
		$liste_produits .= '</tr>';
	}

}

$data = array(
	'panier' 	=> array(
		'id'			=> $panier->id,
		'reference'		=> $panier->reference,
		'date'			=> $panier->created,
		'montant_total'	=> html_price($panier->montant),
	),
	'liste_produits'=> $liste_produits,
	'url' => array(
	)
);

$this->dwootemplate->output(tpl_path('boutique/panier/afficher_sauvegarde.tpl'), $data);