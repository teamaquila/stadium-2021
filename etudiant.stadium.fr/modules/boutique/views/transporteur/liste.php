<?php
$html = NULL;

if ($transporteurs->result_count() > 0)
{
	$html.= '<div class="lignes_transporteurs">';
	foreach($transporteurs as $transporteur)
	{
		$html .= '<div class="item_transporteur">';
			$selection = (($selectionne == $transporteur->id) or ($transporteurs->result_count() == 1));
			
			$html .= form_radio(array(
				'id' 	=> 'transporteur-'.$transporteur->id,
				'name'	=> 'transporteur_id',
				'class'	=> 'transporteur',
				'value'	=> $transporteur->id
			), NULL, $selection);
			
			$label = NULL;
			if ($transporteur->picto)
			{
				$label .= '<img src="'.assets($transporteur->picto).'"/>';
			}
			
			$label .= '<span class="name-transporteur">'.$transporteur->nom.nbs().'</span><span class="delais-transporteur">('.$transporteur->delai_minimum.' à '.$transporteur->delai_maximum.nbs().$transporteur->unite.')</span>';
			//			$label .= $transporteur->nom.nbs().'<span>('.$transporteur->delai_minimum.' à '.$transporteur->delai_maximum.nbs().$transporteur->unite.')</span>';
			
			$frais_port =  modules::run('boutique/transporteurs/get_frais_port',$pays_id, $transporteur->id);
			//$label .= ($frais_port) ? ('<span class="tarif_transport">'.html_price($frais_port->tarif).'</span>') : ('<span class="tarif_transport">Offerts</span>');
			
			
			if($transporteur->id == 3)
			{
			    $html .= '<label for="transporteur-3" class="toolitptp" >'.$label.'<span class="tooltiptext"><u>Horaires de retrait en magasin :</u><br />8h30 - 12h et 13h-17h, du Lundi au Vendredi.<br />Venez avec votre bon de commande.</span></label>';
			}
			else
			{
			    $html .= form_label($label, 'transporteur-'.$transporteur->id);
			}
			
			
			

			
			//Module colissimo
			if ($selection && $transporteur->relais_colissimo && ($etape == 3))
			{
				$html .= modules::run('boutique/transporteurs/module_colissimo');
			}
			
			//Module chronopost
			if ($selection && $transporteur->relais_chronopost && ($etape == 3))
			{
				$html .= modules::run('boutique/transporteurs/module_chronopost');
			}
			
			
		$html .= '</div>';
	}
	$html .= '</div>';
}

echo $html;