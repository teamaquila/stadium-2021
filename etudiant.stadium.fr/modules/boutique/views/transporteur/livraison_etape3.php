<?php
$data = array(
	'zones_livraison' 	=> $zones_livraison,
	'transporteurs'		=> $transporteurs,
	'franco' 			=> $franco,
	'frais_port'		=> html_price($frais_port),
    'sur_devis'			=> ((isset($this->session->userdata('livraison')['sur_devis'])) && ($this->session->userdata('livraison')['sur_devis'] == TRUE)) ? TRUE : FALSE
);

$this->dwootemplate->output(tpl_path('boutique/transporteur/livraison_etape3.tpl'), $data);