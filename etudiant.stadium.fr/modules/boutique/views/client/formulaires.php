<?php
$html  = NULL;
//Page d'inscription, on doit rajouter les balises du module
if ($type != 2)
{
	$html .= '<div class="module-boutique">';
		$html .= '<div class="bloc_image_h1 sans_image">';
			$html .= '<div class="bloc_titre_h1">';
				$html .= '<h1>Inscription</h1>';
			$html .= '</div>';
		$html .= '</div>';
		$html  .= '<div class="inner_interne">';	
}
//Erreurs
if ( ! is_null($erreur))
{
	$html .= form_fieldset(NULL, array('class' => 'alert alert-danger'));
	$html .= $erreur;
	$html .= form_fieldset_close();
}

$html .= '<div class="row">';

$html .= '<div class="col-md-6 .col-md-offset-1">';

if ($type == 2)
{
    $html .= form_open('/boutique/clients/connexion/2', array('id' => 'form-connexion', 'class' => ' zone-authentification ', 'role' => 'form'));
}
else
{
    $html .= form_open('/connexion-client.html', array('id' => 'form-connexion', 'class' => ' zone-authentification', 'role' => 'form'));
}

$html .= form_fieldset(lang('possede_un_compte'), array('class' => 'panel-body'));


$html .= '<div class="form-group">';
$html .= form_label(lang('identifiant'), 'identifiant', array('class' => 'control-label'));
$html .= form_input(array(
		'name'	=> 'identifiant',
		'id'	=> 'identifiant',
		'class' => 'form-control',
		'value' => isset_value($post, 'identifiant')
)).'</div>';

$html .= '<div class="form-group">';
$html .= form_label(lang('mot_de_passe'), 'mot_de_passe', array('class' => 'control-label'));
$html .= form_password(array(
		'name'	=> 'mot_de_passe',
		'id'	=> 'mot_de_passe',
		'class' => 'form-control',
		'value' => ''
)).'</div>';

$html .= anchor('/boutique/clients/nouveau_mdp', lang('mot_de_passe_oublie'), array('rel' => 'nofollow', 'class' => 'oubli-mdp'));

$html .= '<div class="zone-btn-validation">';
$html .= form_button(array(
		'type'		=> 'submit',
		'name'		=> 'submit-connexion',
		'value'		=> TRUE,
		'class'		=>'btn btn-primary',
		'content'	=> lang('se_connecter')
));
$html .= '</div>';
$html .= form_fieldset_close();
$html .= form_close();

$html .= '</div>';

$html .= '<div class="col-md-6  .col-md-offset-1">';
if ($type == 1)
{
	$html .= form_open('/inscription-client.html', array('id' => 'form-inscription', 'class' => 'zone-authentification', 'role' => 'form'));	
}
else 
{
	$html .= form_open('/boutique/etape,2.html', array('id' => 'form-inscription', 'class' => 'zone-authentification', 'role' => 'form'));
}
$html .= form_fieldset(lang('creer_mon_compte'), array('class' => 'panel-body'));

$html .= form_hidden('groupes',2);

$html .= '<div class="zone-identifiants">';
$html .= '<h4>Identifiants de connexion</h4>';
$html .= '<div class="form-group">';
$html .= form_label(lang('email').' *', 'email');
$html .= form_input(array(
		'name'	=> 'email',
		'id'	=> 'email',
		'class' => 'form-control',
		'value' => isset_value($post, 'email')
)).'</div>';
$html .= '<div class="form-group colonne_demi colonne_gauche">';
$html .= form_label(lang('mot_de_passe').' *', 'mot_de_passe');
$html .= form_password(array(
		'name'	=> 'mot_de_passe',
		'id'	=> 'mot_de_passe',
		'class' => 'form-control',
		'value' => isset_value($post, 'mot_de_passe')
)).'</div>';

$html .= '<div class="form-group colonne_demi colonne_droite">';
$html .= form_label(lang('mot_de_passe_confirm').' *', 'mot_de_passe_confirm');
$html .= form_password(array(
		'name'	=> 'mot_de_passe_confirm',
		'id'	=> 'mot_de_passe_confirm',
		'class' => 'form-control',
		'value' => isset_value($post, 'mot_de_passe_confirm')
)).'</div>';
$html .= '</div>';

$html .= '<h4>Coordonnées</h4>';

$html .= '<div class="form-group groupe_type_client">';
    
$html .= '<span class="libelle-radio titre-champ">'.lang('vous_etes').' * : </span>';
$html .= form_radio(array(
		'name'	=> 'pro',
		'id'	=> 'particulier',
		'class' => 'form-control type_client',
		'checked'	=> 'checked',
		'value' => 0
));
$html .= form_label(lang('particulier'), 'particulier');

$html .= form_radio(array(
    'name'	=> 'pro',
    'id'	=> 'pro',
    'class' => 'form-control type_client',
    'value' => 1
));

$html .= form_label(lang('professionnel'), 'pro');

$html .='</div>';

$html .= '<div class="form-group colonne_demi colonne_gauche">';
$html .= form_label(lang('nom').' *', 'nom');
$html .= form_input(array(
	'name'	=> 'nom',
	'id'	=> 'nom',
	'class' => 'form-control',
	'value' => isset_value($post, 'nom')
)).'</div>';

$html .= '<div class="form-group colonne_demi colonne_droite">';
$html .= form_label(lang('prenom').' *', 'prenom');
$html .= form_input(array(
	'name'	=> 'prenom',
	'id'	=> 'prenom',
	'class' => 'form-control',
	'value' => isset_value($post, 'prenom')
)).'</div>';

$html .= '<div class="form-group">';
$html .= form_label(lang('societe').'', 'societe');
$html .= form_input(array(
    'name'	=> 'societe',
    'id'	=> 'societe',
    'class' => 'form-control',
    'value' => isset_value($post, 'societe')
)).'</div>';

$html .= '<div class="form-group">';
$html .= form_label(lang('adresse').' *', 'adresse');
$html .= form_textarea(array(
	'name'	=> 'adresse',
	'id'	=> 'adresse',
	'class' => 'form-control',
	'cols'	=> 50,
	'rows'	=> 3,
	'value' => isset_value($post, 'adresse')
)).'</div>';

$html .= '<div class="form-group colonne_demi colonne_gauche">';
$html .= form_label(lang('code_postal').' *', 'code_postal');
$html .= form_input(array(
    'name'  => 'code_postal',
    'id'    => 'code_postal',
	'class' => 'form-control',
    'value' => isset_value($post, 'code_postal')
)).'</div>';

$html .= '<div class="form-group colonne_demi colonne_droite">';
$html .= form_label(lang('ville').' *', 'ville');
$html .= form_input(array(
    'name'  => 'ville',
    'id'    => 'ville',
	'class' => 'form-control',
    'value' => isset_value($post, 'ville')
)).'</div>';

$html .= '<div class="form-group">';
$html .= modules::run('boutique/transporteurs/liste_zone_livraison', ((isset($post['pays'])) ? $post['pays'] : 66));
$html .= '</div>';

$html .= '<div class="form-group">';
$html .= form_label(lang('telephone').' *', 'telephone');
$html .= form_input(array(
		'name'	=> 'telephone',
		'id'	=> 'telephone',
		'class' => 'form-control',
		'value' => isset_value($post, 'telephone')
)).'</div>';

// $html .= '<div class="form-group checkbox">';
// $html .=form_checkbox(array(
// 		'name'	=> 'newsletter',
// 		'id'	=> 'newsletter',
//         'checked' => isset($post['newsletter'])//isset_value($post, 'newsletter')
// ));
// $html .= form_label(lang('acceptation_newsletter'), 'newsletter', array('class' => 'label-inverse label-newsletter'));
// $html .= '</div>';

$html .= '<small>* '.lang('champs_obligatoire').'</small>'.br();


$html .= '<div class="zone-btn-validation">';
$html .= form_button( array(
	'type'	=> 'submit',
	'name'	=> 'submit-inscription',
	'class' => 'btn btn-primary',
	'value'	=> TRUE,
	'content' => lang('valider')
));
$html .= '</div>';

$html .= form_fieldset_close();
$html .= form_close();

$html .= '<p class="donnee-prive">Vos données sont privées <a href="/confidentialite,12.html" class="lien-standard" target="_blank">en savoir plus</a> !</p>';
$html .= '</div>';

//Page d'inscription, on doit rajouter les balises du module
if ($type != 2)
{
	$html .= '</div>';
	$html .= '</div>';
}

echo $html;