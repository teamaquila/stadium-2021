<?php

/*Génération des tableaux pour la date de naissance*/
$formulaire = NULL;
$formulaire = '<div class="compte changer-pass">';
$formulaire .= '<div class="bloc_titre_h1"><h1>Changer votre mot de passe</h1></div>';

$formulaire .= '<div class="inner_interne">';
$formulaire .= '<p class="welcome-client text-center">Bienvenue <span>'.$client->prenom.' '.$client->nom.'</span></p>';
$formulaire .= '<div class="row">';
	$formulaire .= '<div class="col-md-3 menu-compte">';
		$formulaire .= anchor('/boutique/clients/commandes', 'Afficher vos commandes', array('class' => 'bt_compte'));
		$formulaire .= anchor('/boutique/clients/lister_commandes_attente', 'Devis en attente', array('class' => 'bt_compte'));
		$formulaire .= anchor('/boutique/clients/compte', 'Modifier vos coordonnées', array('class' => 'bt_compte'));
		$formulaire .= anchor('/boutique/clients/formulairemotdepasse', 'Modifier mon mot de passe', array('class' => 'bt_compte'));		
		//$formulaire .= '<a href="https://www.stadium.fr/medias/pdf/2021/bon-commande.pdf" class="bt_compte lien-telechargement-inverse">Bon de commande</a>';		
		$formulaire .= anchor('/boutique/clients/deconnecter', 'Se déconnecter', array('class' => 'bt_compte'));
	$formulaire .= '</div>';
	
	$formulaire .= '<div class="col-md-9">';


$formulaire .= form_open('', 'class="form-contact" id="changer_motdepasse"');

if ( ! is_null($message) && !is_null($erreur))
{
	$formulaire .= '<div class="alert '.(($erreur === TRUE) ? 'alert-danger' : 'alert-success').'">';
	$formulaire .= '<p>'.$message.'</p>';
	//$formulaire .= anchor('/utilisateurs/connecter', 'Se connecter');
	$formulaire .= '</div>'.br();
	
	
}

$formulaire .= form_fieldset();
$formulaire .= '<div class=" form-group">';
$attributs = array(
	'id'			=> 'ancien_mdp',
	'name'			=> 'ancien_mdp',
	'value'			=> $this->input->post('ancien_mdp'),
	'placeholder'	=> ''
);
$formulaire .= form_label('Ancien mot de passe', $attributs['id']);
$formulaire .= form_password($attributs).br(2);
$formulaire .= '</div>';

$formulaire .= '<div class="form-inline">';
$formulaire .= '<div class="form-group colonne_demi colonne_gauche">';
$attributs = array(
    'id'            => 'nouveau_mdp',
    'name'          => 'nouveau_mdp',
    'value'         => $this->input->post('nouveau_mdp'),
    'placeholder'   => ''
);

$formulaire .= form_label('Nouveau mot de passe', $attributs['id']);
$formulaire .= form_password($attributs).br(2);
$formulaire .= '</div>';

$formulaire .= '<div class="form-group colonne_demi colonne_droite">';
$attributs = array(
    'id'            => 'verif_nouveau_mdp',
    'name'          => 'verif_nouveau_mdp',
    'value'         => $this->input->post('verif_nouveau_mdp'),
    'placeholder'   => ''
);
$formulaire .= form_label('Resaisissez votre nouveau mot de passe', $attributs['id']);
$formulaire .= form_password($attributs).br(2);
$formulaire .= '</div>';

$formulaire .= '</div>';
$formulaire .= '<div id="groupe-validation">';
$attributs = array(
	'type'		=> 'submit',
	'class'		=> 'submit btn btn-primary',
	'name'      => 'submit_mdp',
	'value'    	=> 1,
	'content' 	=> 'Valider'
);
$formulaire .= form_button($attributs);
$formulaire .= '</div>';
$formulaire .= form_fieldset_close();
$formulaire .= form_close();

$formulaire .= '</div>';
$formulaire .= '</div>';
$formulaire .= '</div>';
$formulaire .= '</div>';

echo $formulaire;
