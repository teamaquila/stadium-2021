<?php
$liste_paniers = NULL;
$nb_paniers = 0;

$liste_paniers .= '<table class="table" id="liste_paniers_client">';
$liste_paniers .= '<tr>
						<th class="c-th-5">Date</th>
						<th class="c-th-1">Référence</th>
						<th class="c-th-3">Montant</th>
						<th class="c-th-7">Actions</th>
				   </tr>';
foreach ($paniers as $panier)
{
	$liste_paniers .= '<tr id="panier-'.$panier->id.'">';
		$liste_paniers .= '<td class="c-td-5">'.date_type($panier->created, 2).'</td>';
		$liste_paniers .= '<td class="c-td-1">'.anchor(NULL, $panier->reference, 'class="bt_voir_panier compte-ref" data-panier_id="'.$panier->id.'"').'</td>';
		$liste_paniers .= '<td class="c-td-3">'.html_price($panier->montant).'</td>';
		$liste_paniers .= '<td class="c-td-7">'.
							anchor(NULL, '<span class="glyphicon glyphicon-eye-open"></span>', 'title="Voir ce panier" class="bt_voir_panier" data-panier_id="'.$panier->id.'"').
							anchor(NULL, '<span class="glyphicon glyphicon-remove"></span>', 'title="Supprimer ce panier" class="bt_suppr_panier" data-panier_id="'.$panier->id.'"')
							.'</td>';
	$liste_paniers .= '</tr>';
	$nb_paniers++;

}
$liste_paniers .= '</table>';

$data = array(
	'nb_paniers'=> $nb_paniers,
	'paniers'	=> $liste_paniers,
    'prenom'        => $client->prenom,
    'nom'        => $client->nom,
    'client_pro'    => $client->pro,
	'lien' 		=> array(
		'commandes'		=> anchor('/boutique/clients/commandes', 'Afficher vos commandes', array('class' => 'bt_compte')),
		'adresses'		=> anchor('/boutique/clients/compte', 'Modifier vos coordonnées', array('class' => 'bt_compte')),
	    'mot_de_passe'	=> anchor('/boutique/clients/formulairemotdepasse', 'Modifier mon mot de passe', array('class' => 'bt_compte')),
		'paniers'		=> anchor('/boutique/clients/lister_commandes_attente', 'Devis en attente', array('class' => 'bt_compte')),
		'deconnexion'	=> anchor('/boutique/clients/deconnecter', 'Se déconnecter', array('class' => 'bt_compte')),
	)
);

$this->dwootemplate->output(tpl_path('boutique/client/lister_paniers.tpl'), $data);