<?php
$liste_commandes = NULL;
$nb_commandes = 0;

$liste_commandes .= '<table class="table">';
$liste_commandes .= '<tr><th class="c-th-5">Date</th><th class="c-th-1">Référence</th><th class="c-th-6">Etat</th><th class="c-th-1">Montant</th></tr>';

foreach ($commandes as $commande)
{
	$liste_commandes .= '<tr class="ligne_liste_commandes">';
	$liste_commandes .= '<td class="c-td-5">'.date_type($commande->created, 2).'</td>';
	$liste_commandes .= '<td class="c-td-1">'.anchor('/boutique/clients/commande/'.$commande->id, $commande->reference, array('class' =>'compte-ref')).'</td>';
	$liste_commandes .= '<td class="c-td-6">'.anchor('/boutique/clients/commande/'.$commande->id, $etats[$commande->etat], array('class' =>'compte-etat')).'</td>';
	$liste_commandes .= '<td class="c-td-3">'.anchor('/boutique/clients/commande/'.$commande->id, html_price($commande->montant), array('class' =>'compte-prix')).'</td>';
	$liste_commandes .= '</tr>';
	$nb_commandes++;
}
$liste_commandes .= '</table>';

$data = array(
	'nb_commandes'=> $nb_commandes,
	'commandes'	=> $liste_commandes,
    'prenom'        => $client->prenom,
    'nom'        => $client->nom,
    'client_pro'    => $client->pro,
	'lien' 		=> array(
		'commandes'		=> anchor('/boutique/clients/commandes', 'Afficher vos commandes', array('class' => 'bt_compte')),
		'adresses'		=> anchor('/boutique/clients/compte', 'Modifier vos coordonnées', array('class' => 'bt_compte')),
		'mot_de_passe'	=> anchor('/boutique/clients/formulairemotdepasse', 'Modifier mon mot de passe', array('class' => 'bt_compte')),
		'paniers'		=> anchor('/boutique/clients/lister_commandes_attente', 'Devis en attente', array('class' => 'bt_compte')),
		'deconnexion'	=> anchor('/boutique/clients/deconnecter', 'Se déconnecter', array('class' => 'bt_compte')),
	)
);

$this->dwootemplate->output(tpl_path('boutique/client/lister_commandes.tpl'), $data);