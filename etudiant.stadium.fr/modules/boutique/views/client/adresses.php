<?php
$html  = NULL;
$html .= form_open(NULL, array('id' => 'adresse-boutique', 'class' => 'adresses row', 'role' => 'form'));

//Erreurs
if ( ! is_null($erreur))
{
	$html .= form_fieldset(NULL, array('class' => 'alert alert-danger'));
	$html .= $erreur;
	$html .= form_fieldset_close();
}
$html .= '<div class="row">';
$html .= '<div class="col-md-6">';
$html .= form_fieldset(lang('adresse_facturation'), array('id' => 'panel-facturation', 'class' => 'panel'));

$html .= '<div class="panel-body">';

$html .= '<div class="form-group colonne_demi colonne_gauche">';
$html .= form_label(lang('nom').' *', 'facturation-nom');
$html .= form_input(array(
	'name'	=> 'facturation[nom]',
	'id'	=> 'facturation-nom',
	'class' => 'form-control',
	'value'	=> isset_value($facturation, 'nom')
)).'</div>';

$html .= '<div class="form-group colonne_demi colonne_droite">';
$html .= form_label(lang('prenom').' *', 'facturation-prenom');
$html .= form_input(array(
	'name'	=> 'facturation[prenom]',
	'id'	=> 'facturation-prenom',
	'class' => 'form-control',
	'value'	=> isset_value($facturation, 'prenom')
)).'</div>';

$html .= '<div class="form-group">';
$html .= form_label(lang('societe').' *', 'facturation-societe');
$html .= form_input(array(
		'name'	=> 'facturation[societe]',
		'id'	=> 'facturation-societe',
		'class' => 'form-control',
		'value'	=> isset_value($facturation, 'societe')
)).'</div>';

$html .= '<div class="form-group">';
$html .= form_label(lang('adresse').' *', 'facturation-adresse');
$html .= form_textarea(array(
	'name'	=> 'facturation[adresse]',
	'id'	=> 'facturation-adresse',
	'class' => 'form-control',
	'cols'	=> 50,
	'rows'	=> 3,
	'value'	=> isset_value($facturation, 'adresse')
)).'</div>';

$html .= '<div class="form-group colonne_demi colonne_gauche">';
$html .= form_label(lang('code_postal').' *', 'facturation-code_postal');
$html .= form_input(array(
    'name'  => 'facturation[code_postal]',
    'id'    => 'facturation-code_postal',
	'class' => 'form-control',
    'value' => isset_value($facturation, 'code_postal')
)).'</div>';

$html .= '<div class="form-group colonne_demi colonne_droite">';
$html .= form_label(lang('ville').' *', 'facturation-ville');
$html .= form_input(array(
    'name'  => 'facturation[ville]',
    'id'    => 'facturation-ville',
	'class' => 'form-control',
    'value' => isset_value($facturation, 'ville')
)).'</div>';

$html .= '<div class="form-group colonne_demi colonne_gauche">';
$html .= form_label(lang('pays').' *');
$html .= form_dropdown('facturation[pays_id]', $zone_livraison['facturation'], isset_value($facturation, 'pays_id', 66), 'class="form-control"');
$html .= '</div>';

/*$html .= '<div class="form-group">';
$html .= form_label(lang('email').' *', 'facturation-email');
$html .= form_input(array(
	'name'	=> 'facturation[email]',
	'id'	=> 'facturation-email',
	'class' => 'form-control',
	'value'	=> isset_value($facturation, 'email')
));
$html .= '</div>';*/

$html .= '<div class="form-group colonne_demi colonne_droite">';
$html .= form_label(lang('telephone').' *', 'facturation-telephone');
$html .= form_input(array(
	'name'	=> 'facturation[telephone]',
	'id'	=> 'facturation-telephone',
	'class' => 'form-control',
	'value'	=> isset_value($facturation, 'telephone')
));
$html .= '</div>';


$html .= '</div>';

$html .= form_fieldset_close();
$html .= '</div>';
if ($module->option_livraison)
{

	$html .= '<div class="col-md-6">';
	$html .= form_fieldset(lang('adresse_livraison'), array('id' => 'panel-livraison', 'class' => 'panel'));
	
	if (($this->session->userdata('livraison_choisie')))
	{
	
		$livraisons_autres = new Adresses_utilisateur();
		$livraisons_autres->where('utilisateur_id', $client->id)->where('id !=', $this->session->userdata('livraison_choisie'))->get();
		
		$livraison = new Adresses_utilisateur();
		$livraison->get_by_id($this->session->userdata('livraison_choisie'));
	}
	else
	{
		//$livraisons_autres = $client->adresses(FALSE);
		$livraisons_autres = new Adresses_utilisateur();
		$livraisons_autres->where('utilisateur_id', $client->id)->where('defaut', FALSE)->get();
		

		$livraison = new Adresses_utilisateur();
		$livraison->where('utilisateur_id', $client->id)->where('defaut', TRUE)->get(1);
	}
	
	
	$html .= '<div class="panel-body">';
	
	$html .= '<div class="form-group colonne_demi colonne_gauche">';
	$html .= form_label(lang('nom').' *', 'livraison-nom');
	$html .= form_input(array(
		'name'	=> 'livraison[nom]',
		'id'	=> 'livraison-nom',
		'class' => 'form-control',
		'value'	=> isset_value($livraison, 'nom')
	)).'</div>';
	
	$html .= '<div class="form-group colonne_demi colonne_droite">';
	$html .= form_label(lang('prenom').' *','livraison-prenom');
	$html .= form_input(array(
		'name'	=> 'livraison[prenom]',
		'id'	=> 'livraison-prenom',
		'class' => 'form-control',
		'value'	=> isset_value($livraison, 'prenom')
	)).'</div>';
	
	$html .= '<div class="form-group">';
	$html .= form_label(lang('societe').' *','livraison-societe');
	$html .= form_input(array(
			'name'	=> 'livraison[societe]',
			'id'	=> 'livraison-societe',
			'class' => 'form-control',
			'value'	=> isset_value($livraison, 'societe')
	)).'</div>';
	
	$html .= '<div class="form-group">';
	$html .= form_label(lang('adresse').' *', 'livraison-adresse');
	$html .= form_textarea(array(
		'name'	=> 'livraison[adresse]',
		'id'	=> 'livraison-adresse',
		'class' => 'form-control',
		'cols'	=> 50,
		'rows'	=> 3,
		'value'	=> isset_value($livraison, 'adresse')
	)).'</div>';
	
	$html .= '<div class="form-group colonne_demi colonne_gauche">';
	$html .= form_label(lang('code_postal').' *', 'livraison-code_postal');
	$html .= form_input(array(
		'name'  => 'livraison[code_postal]',
		'id'    => 'livraison-code_postal',
		'class' => 'form-control',
		'value' => isset_value($livraison, 'code_postal')
	)).'</div>';
		
	$html .= '<div class="form-group colonne_demi colonne_droite">';
	$html .= form_label(lang('ville').' *', 'livraison-ville');
	$html .= form_input(array(
		'name'  => 'livraison[ville]',
		'id'    => 'livraison-ville',
		'class' => 'form-control',
		'value' => isset_value($livraison, 'ville')
	)).'</div>';
	
	$html .= '<div class="form-group colonne_demi colonne_gauche">';
	$html .= $zone_livraison['livraison'];
	$html .= '</div>';
	
/*	$html .= '<div class="form-group">';
	$html .= form_label(lang('email').' *', 'livraison-email');
	$html .= form_input(array(
		'name'	=> 'livraison[email]',
		'id'	=> 'livraison-email',
		'class' => 'form-control',
		'value'	=> isset_value($livraison, 'email')
	)).'</div>';*/
	
	$html .= '<div class="form-group colonne_demi colonne_droite">';
	$html .= form_label(lang('telephone').' *', 'livraison-telephone');
	$html .= form_input(array(
		'name'	=> 'livraison[telephone]',
		'id'	=> 'livraison-telephone',
		'class' => 'form-control',
		'value'	=> isset_value($livraison, 'telephone')
	)).'</div>';
		

	//Choix d'autres adresses de livraison
	if ($livraisons_autres->exists())
	{
		$html .= '<div id="adresses_livraison_supplem">';
		$html .= '<h4>Ou choisir une autre adresse de livraison</h4>';
		$tab_livraisons_autres = array();
		$tab_livraisons_autres[''] = 'Adresses de livraison enregistrées';
		foreach ($livraisons_autres as $livraison_autre) {
		    $tab_livraisons_autres[$livraison_autre->id] = ($livraison_autre->defaut && !$livraison_autre->libelle) ? lang('adresse_par_defaut') :  $livraison_autre->libelle;
		}
		$html .= form_dropdown('adresses_livraisons',$tab_livraisons_autres,NULL,'id="select_livraisons"').br();
		//$html .= '<a href="javascript:void(0)" id="creer_livraison" data-client_id="'.$client->id.'">Créer une nouvelle adresse de livraison</a>';
		
		$html .= '</div>';
	}

	
	$html .= '</div>';
	$html .= form_fieldset_close();

	$html .= '</div>';
	
	//$html .= '<br class="clear-both">';
	
// 	$html .= '<div class="col-md-12">';
// 	$html .= $transporteur;
// 	$html .= '</div>';
	$html .= '</div>'; 
}

$html .= '<div class="col-md-12">';
$html .= form_fieldset();

$html .= form_button(array(
	'type'		=> 'submit',
	'value'		=> 1,
	'class'		=> 'btn btn-primary pull-right',
	'content' 	=> lang('valider')
));
$html .= form_fieldset_close();
$html .= '</div>'; 

$html .= form_close();

echo $html;