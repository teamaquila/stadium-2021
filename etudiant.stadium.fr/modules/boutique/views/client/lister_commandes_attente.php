<?php
$liste = NULL;

$liste_commandes = NULL;
$derniere_commande = NULL;
$i = 0;

foreach ($commandes as $commande)
{
	if (in_array($commande->etat, array(7)))
	{	
		$derniere_commande .= '<div class="ligne_liste_commandes row">';
		$derniere_commande .= '<a href="/boutique/clients/commande/'.$commande->id.'" class="date col-md-3">Devis du '.date_type($commande->created, 2).'</a>';
		$derniere_commande .= '<span class="montant_ligne_commande col-md-2">'.html_price($commande->montant).' €</span>';
		$derniere_commande .= '<span class="etat col-md-3">'.$etats[$commande->etat].'</span>';
		$derniere_commande .= anchor('/boutique/commandes/exporter/'.$commande->id.'/0/1', "<span class='jaune glyphicon glyphicon-save'></span> Télécharger votre devis", "class='col-md-4'").br();
// 		$derniere_commande .= '<div id="detail-'.$commande->id.'" class="detail_commande">'.Modules::run('boutique/clients/commande',$commande->id).'</div>';
		$derniere_commande .= '</div>';
		
		
	
		$i++;
	}
}

$data = array(
	'nb_commandes'		=> $i,
	'derniere_commande'	=> $derniere_commande,
	'liste_commandes'	=> $liste_commandes,
    'prenom'        => $client->prenom,
    'nom'        => $client->nom,
    'client_pro'    => $client->pro,
	'lien_retour' 		=> anchor('/boutique/clients/compte', 'Retourner sur votre compte', array('class' => 'bt_retour')),
    'lien' 		=> array(
        'commandes'		=> anchor('/boutique/clients/commandes', 'Afficher vos commandes', array('class' => 'bt_compte')),
        'adresses'		=> anchor('/boutique/clients/compte', 'Modifier vos coordonnées', array('class' => 'bt_compte')),
        'mot_de_passe'	=> anchor('/boutique/clients/formulairemotdepasse', 'Modifier mon mot de passe', array('class' => 'bt_compte')),
        'paniers'		=> anchor('/boutique/clients/lister_commandes_attente', 'Devis en attente', array('class' => 'bt_compte')),
        'deconnexion'	=> anchor('/boutique/clients/deconnecter', 'Se déconnecter', array('class' => 'bt_compte')),
    )
);
$this->dwootemplate->output(tpl_path('boutique/client/lister_commandes_attente.tpl'), $data);