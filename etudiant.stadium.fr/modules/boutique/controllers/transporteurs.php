<?php
require_once APPPATH.'controllers/front.php';

/**
 * Transporteurs
 * 
 * @author 		Pauline MARTIN
 * @package 	boutique
 * @category 	Controllers
 * @version 	1.1
 */
class Transporteurs extends Front
{
	/**
	 * Type de seuil pour les frais de port
	 * 
	 * @var integer
	 */
	const SEUIL_QUANTITE = 1;
	const SEUIL_POIDS 	 = 2;
	const SEUIL_TOTAL 	 = 3;
	
	private $_module;
	private $_tva_livraison;
	private $_pays_defaut;
	private $_transporteur_defaut;
	
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->load->libraries(array('cart', 'order'));
		$this->load->models(array('module_boutique', 'transporteur', 'frais_port', 'zone_livraison', 'pays'));
		$this->lang->load('boutique');
		
		$this->_module = $this->module_boutique->get(1);
		$this->_tva_livraison = 1 + ($this->_module->tva_livraison / 100);
		$this->_pays_defaut = $this->pays->defaut();
		$this->_transporteur_defaut = $this->transporteur->defaut();
	}
	
	/**
     * Calcul des frais de port
     * en fonction de la zone du pays
     * 
     * @param	integer $pays_id
     * @param	integer	$transporteur_id
     * @return	object OR FALSE
     */
    public function frais_port($pays_id, $transporteur_id)
    {
        $this->load->module('boutique/clients');
    	//Transporteur lié au pays
    	$transporteur = $this->transporteur->liaison_pays($pays_id, $transporteur_id);
    	
    	if ( ! $transporteur)
    	{
    		$pays_id = $this->_pays_defaut;
    	}
		
		//Zone à partir du pays sélectionné
		$zone = $this->zone_livraison->par_pays($pays_id);
		$client = $this->clients->est_connecte();

		//Si c'est un pro et connecté
		if($client !== FALSE && $client->pro == 1)
		{

		    if ($zone->id == 1 )
		    {
		        if (($transporteur_id == 1))
		        {
		            $this->session->unset_userdata('livraison');
		            $this->session->set_userdata(array(
		                'livraison' => array(
		                    'fraisport_id'      => NULL,
		                    'pays_id'           => $pays_id,
		                    'transporteur_id'   => $transporteur_id,
		                    'zone_id'           => $zone->id,
		                    'montant_fraisport' => 16.80
		                )
		            ));	            
		            
		            $this->order->insert_transporter(array(
		                'pays_id'           => $pays_id,
		                'transporteur_id'   => $transporteur_id,
		                'zone_id'           => $zone->id,
		                'frais_port' 		=> 16.80
		            ));
		            
		            $frais_port = new Frais_port();
		            $frais_port->tarif = 16.80;
		            $frais_port->tarif_HT = 14;
		            $frais_port->id = 0;
		            return $frais_port;
		        }
		        else
		        {
		            //Calcul des frais de port
		            if ( ! $this->_frais_port_gratuit_remise())
		            {
		                $frais_port = $this->frais_port->calculer($zone->id, $transporteur_id, $this->_total_valeur());
		                
		                if ($frais_port !== FALSE)
		                {
		                    $frais_port->tarif_HT   = $frais_port->tarif;
		                    $frais_port->tarif      = $frais_port->tarif*$this->_tva_livraison;
		                    
		                    $this->order->insert_transporter(array(
		                        'pays_id'           => $pays_id,
		                        'transporteur_id'   => $transporteur_id,
		                        'zone_id'           => $zone->id,
		                        'frais_port' 		=> $frais_port->tarif
		                    ));
		                    
		                    return $frais_port;
		                }
		            }
		            else
		            {
		                $this->order->insert_transporter(array(
		                    'pays_id'           => $pays_id,
		                    'transporteur_id'   => $transporteur_id,
		                    'zone_id'           => $zone->id,
		                    'frais_port' 		=> 0
		                ));
		                
		                $frais_port = new Frais_port();
		                $frais_port->tarif = 0;
		                $frais_port->tarif_HT = 0;
		                $frais_port->id = 0;
		                
		                return $frais_port;
		            }
		        }
		    }
		    else
		    {
		        $this->session->unset_userdata('livraison');
		        $this->session->set_userdata(array(
		            'livraison' => array(
		                'fraisport_id'      => NULL,
		                'pays_id'           => $pays_id,
		                'transporteur_id'   => $transporteur_id,
		                'zone_id'           => $zone->id,
		                'montant_fraisport' => NULL,
		                'sur_devis'			=> TRUE
		            )
		        ));
		        
		        $this->order->insert_transporter(array(
		            'pays_id'           => $pays_id,
		            'transporteur_id'   => $transporteur_id,
		            'zone_id'           => $zone->id,
		            'frais_port' 		=> 0
		        ));
		        
		        $frais_port = new Frais_port();
		        $frais_port->tarif = NULL;
		        $frais_port->tarif_HT = NULL;
		        $frais_port->id = 0;
		        return $frais_port;
		    }
		    
		}
		else
		{
		    //Calcul des frais de port
		    if ( ! $this->_frais_port_gratuit_remise())
		    {
		        $frais_port = $this->frais_port->calculer($zone->id, $transporteur_id, $this->_total_valeur());
		        
		        if ($frais_port !== FALSE)
		        {
		            $frais_port->tarif_HT   = $frais_port->tarif;
		            $frais_port->tarif      = $frais_port->tarif*$this->_tva_livraison;
		            
		            $this->order->insert_transporter(array(
		                'pays_id'           => $pays_id,
		                'transporteur_id'   => $transporteur_id,
		                'zone_id'           => $zone->id,
		                'frais_port' 		=> $frais_port->tarif
		            ));
		            
		            return $frais_port;
		        }
		    }
		    else
		    {
		        $this->order->insert_transporter(array(
		            'pays_id'           => $pays_id,
		            'transporteur_id'   => $transporteur_id,
		            'zone_id'           => $zone->id,
		            'frais_port' 		=> 0
		        ));
		        
		        $frais_port = new Frais_port();
		        $frais_port->tarif = 0;
		        $frais_port->tarif_HT = 0;
		        $frais_port->id = 0;
		        
		        return $frais_port;
		    }
		}
		
		return FALSE;
	}
	
	/**
	 * Calcul des frais de port seulement pour affichage
	 * en fonction de la zone du pays
	 *
	 * @param	integer $pays_id
	 * @param	integer	$transporteur_id
	 * @return	object OR FALSE
	 */
	public function get_frais_port($pays_id, $transporteur_id)
	{

		//Zone à partir du pays sélectionné
		$zone = $this->zone_livraison->par_pays($pays_id);
	
		//Calcul des frais de port
		if ( ! $this->_frais_port_gratuit_remise())
		{
			$frais_port = $this->frais_port->calculer($zone->id, $transporteur_id, $this->_total_valeur());
	
			if ($frais_port !== FALSE)
			{
				$frais_port->tarif_HT   = $frais_port->tarif;
				$frais_port->tarif      = $frais_port->tarif*$this->_tva_livraison;
				
	
				return $frais_port;
			}
		}
		else
		{
			
			$frais_port = new Frais_port();
			$frais_port->tarif = 0;
			$frais_port->tarif_HT = 0;
			$frais_port->id = 0;
	
			return $frais_port;
		}
		return FALSE;
	}
	
	/**
     * Affiche les modes de livraison :
     * - zones de livraison
     * - transporteurs
     * 
     * @return	VIEW
     */
    public function livraison($etape = 1)
    {
    	$this->load->module('boutique/clients');
    	
    	//Valeurs par défaut
    	$pays_id = $this->_pays_defaut->id;
    	$transporteur_id = $this->_transporteur_defaut->id;
    	
    	//Si connecté, on utilise le pays de livraison du client
    	if (($client = $this->clients->est_connecte()) !== FALSE)
    	{
    		//Priorité au pays de la session
    		if ($livraison = $this->order->transporter())
    		{
    			$pays_id = $livraison['pays_id'];
				$transporteur_id = $livraison['transporteur_id'];
    		}
    		else
    		{				
				$transporteur = $this->transporteur->par_pays($client->pays_id, TRUE);
    			
	    		if ($transporteur->exists())
	    		{
	    			$transporteur_id = $transporteur->id;
	    			$pays_id = $client->pays_id;
	    		}
	    		else
	    		{
	    			$pays_id = $this->_pays_defaut;
	    		}
    		}
    		
    		$frais_port = $this->frais_port($pays_id, $transporteur_id);
    	}
    	else
    	{
    		//Non connecté mais un choix de livraison fait
    		if ($livraison = $this->order->delivery())
    		{
    			$frais_port = $this->frais_port($livraison['pays_id'], $livraison['transporteur_id']);
				$pays_id	= $livraison['pays_id'];
				$transporteur_id = $livraison['transporteur_id'];
    		}
    		elseif ($livraison = $this->order->transporter())
    		{
    		    $frais_port = $this->frais_port($livraison['pays_id'], $livraison['transporteur_id']);
    		    $pays_id = $livraison['pays_id'];
    		    $transporteur_id = $livraison['transporteur_id'];
    		}
    		//Situation initiale (pas de session, pas connecté)
    		else
    		{
    			$frais_port = $this->frais_port($pays_id, $transporteur_id);
    		}
    	}
        
    	if ($frais_port !== FALSE)
    	{
    		//$transporteur = $this->transporteur->get_by_id($transporteur_id);

	    	$data = array(
	    		'zones_livraison'	=> $this->liste_zone_livraison($pays_id),
	    		'transporteurs'		=> $this->_liste_transporteur($pays_id, $transporteur_id, $etape),
	    		'frais_port'		=> $frais_port->tarif,
	    	    'franco'			=> ($frais_port->id > 0) ? $this->_afficher_franco_port($frais_port) : NULL,
	    	);
	    	return $this->load->view('boutique/transporteur/livraison_etape'.$etape, $data, TRUE);
    	}
		return FALSE;
    }
    
    /**
     *  Module colissimo permettant l'affichage des points de retraits
     * */
    public function module_colissimo()
    {
    	if ($this->_module->active_colissimo)
    	{

    		$tab_asso_type_picto = array(
    			'BPR'	=> 'boutique/picto_poste.png',
    			'ACP'	=> 'boutique/picto_poste.png',
    			'CDI'	=> 'boutique/picto_poste.png',
    			'BDP'	=> 'boutique/picto_poste.png',
    			'A2P'	=> 'boutique/picto_commerce.png',
    			'CMT'	=> 'boutique/picto_commerce.png',
    			//'A2P'	=> 'picto_cityssimo.png',
    			//'PCS'	=> 'picto_cityssimo.png',
    		);
    		
    		$this->_client = modules::run('boutique/clients/est_connecte');

    		$shipping_date = new DateTime();
    		$shipping_date->modify('+ 2 Days');
    	

    		$postfields = array(
    				"accountNumber"		=> $this->_module->identifiant_colissimo,
    				"password"			=> $this->_module->mdp_colissimo,
    				"countryCode"		=> "FR",
    				"shippingDate"		=> $shipping_date->format('d/m/Y'),
    				"filterRelay"		=> 1,
    				"requestId"			=> now(),
    				"lang"				=> "FR",
    				"adress"			=> NULL,
    				"zipCode"			=> NULL,
    				"city"			=> NULL,
    		);
    		
    		
    		//Infos générales du client
    		if (!$this->input->post('recherche_relais'))
    		{
    			$livraison 	 = $this->_client;
    			
    			$adresse = $livraison->adresse;
    			$code_postal = $livraison->code_postal;
    			$ville = $livraison->ville;
    			
    		}
    		else
    		{
    			$adresse = $this->input->post('adress');
    			$code_postal = $this->input->post('code_postal');
    			$ville = $this->input->post('ville');
    		}
    		
    		$postfields["adress"]			= $adresse;
    		$postfields["zipCode"]			= $code_postal;
    		$postfields["city"]				= $ville;
    		
    		
    		ini_set('soap.wsdl_cache_enabled', '0');
    		$wsdl="https://ws.colissimo.fr/pointretrait-ws-cxf/PointRetraitServiceWS/2.0/findRDVPointRetraitAcheminement?wsdl";
    		$service=new SoapClient($wsdl, $postfields);
    		$result =  $service->findRDVPointRetraitAcheminement($postfields);
    			
    		if ($result->return->errorCode == 0)
    		{
    			$data = array(
    					'points_retraits' 	=> $result->return->listePointRetraitAcheminement,
    					'tab_asso_type_picto'	=> $tab_asso_type_picto,
    					'adresse'			=> $adresse,
    					'code_postal'		=> $code_postal,
    					'ville'				=> $ville,
    					'erreur'			=> FALSE,
    			);
    		}
    		else
    		{
    			$data = array(
    					'erreur'		=>  $result->return->errorMessage
    			);
    		}
    		if (!$this->input->post('recherche_relais'))
    		{
    			return $this->load->view('boutique/transporteur/module_colissimo', $data);
    		}
    		else
    		{
    			echo json_encode(array(
    				'vue'	=> $this->load->view('boutique/transporteur/module_colissimo', $data, TRUE)
    			));
    		}
    	}
    	return FALSE;
    }
    
    /**
     * AJAX
     * Choix du point relais colissimo, mise à jour de la session puis affichage du point relais sélectionné
     */
    public function choix_relais_colissimo()
    {
    	if ($this->input->is_ajax_request())
    	{
    		if ($this->input->post('relais_id'))
    		{
    			$shipping_date = new DateTime();
    			$shipping_date->modify('+ 2 Days');
    			$postfields = array(
    					"accountNumber"		=> $this->_module->identifiant_colissimo,
    					"password"			=> $this->_module->mdp_colissimo,
    					"id"		=> $this->input->post('relais_id'),
    					"date"		=> $shipping_date->format('d/m/Y'),
    			);
    			ini_set('soap.wsdl_cache_enabled', '0');
    			$wsdl="https://ws.colissimo.fr/pointretrait-ws-cxf/PointRetraitServiceWS/2.0/findPointRetraitAcheminementByID?wsdl";
    			$service=new SoapClient($wsdl, $postfields);
    			$result =  $service->findPointRetraitAcheminementByID($postfields);
    			
    			if ($result->return->errorCode == 0)
    			{
    				//Mise à jour de la session
    				

    				$this->session->set_userdata('livraison_relais',array(
    						'relais_id'		=> $this->input->post('relais_id'),
    						/*'nom'			=> $result->return->pointRetraitAcheminement->nom,
    						'adresse'		=> $result->return->pointRetraitAcheminement->adresse1.' '.$result->return->pointRetraitAcheminement->adresse2.' '.$result->return->pointRetraitAcheminement->adresse3,
    						'code_postal'	=> $result->return->pointRetraitAcheminement->codePostal,
    						'ville'			=> $result->return->pointRetraitAcheminement->localite,
    						'pays'			=> $result->return->pointRetraitAcheminement->libellePays,
    						'options'		=> json_encode(array(
    							'distributionSort'	=> $result->return->pointRetraitAcheminement->distributionSort,
    							'lotAcheminement'	=> $result->return->pointRetraitAcheminement->lotAcheminement ,
    							'versionPlanTri'	=> $result->return->pointRetraitAcheminement->versionPlanTri ,
    							'typeDePoint'		=> $result->return->pointRetraitAcheminement->typeDePoint ,
    							'shipping_date'		=> $shipping_date
    						))*/
    						
    				));
    				$tab_asso_type_picto = array(
    						'BPR'	=> 'boutique/picto_poste.png',
    						'ACP'	=> 'boutique/picto_poste.png',
    						'CDI'	=> 'boutique/picto_poste.png',
    						'BDP'	=> 'boutique/picto_poste.png',
    						'A2P'	=> 'boutique/picto_commerce.png',
    						'CMT'	=> 'boutique/picto_commerce.png',
    						//'A2P'	=> 'picto_cityssimo.png',
    						//'PCS'	=> 'picto_cityssimo.png',
    				);

    				$this->_client = modules::run('boutique/clients/est_connecte');
    				
    				$data = array(
    					'point' 	=> $result->return->pointRetraitAcheminement ,
    					'erreur'	=> FALSE,
    					'mobile'	=> $this->client->telephone,
    					'tab_asso_type_picto'	=> $tab_asso_type_picto,
    				);
    				echo json_encode(array(
    						'vue'	=> $this->load->view('boutique/transporteur/choix_relais_colissimo', $data, TRUE)
    				));
    			}
    			else
    			{
    				$data = array(
    						'erreur'		=>  $result->return->errorMessage
    				);
    			}
    		}
    	}
    }
    
    /**
     *  Module chronopost permettant l'affichage des points de retraits
     * */
    public function module_chronopost()
    {
    	if ($this->_module->active_chronopost)
    	{
    		$shipping_date = new DateTime();
    		$shipping_date->modify('+ 2 Days');
    
    		$postfields = array(
    				"accountNumber"		=> intval($this->_module->identifiant_chronopost),
    				"password"			=> $this->_module->mdp_chronopost,
    				"countryCode"		=> "FR",
    				"shippingDate"		=> $shipping_date->format('d/m/Y'),
    				"filterRelay"		=> 1,
    				"requestId"			=> now(),
    				"language"			=> "FR",
    				"maxPointChronopost"=> 25,
    				"maxDistanceSearch"	=> 50,
    				"holidayTolerant" 	=> 1,
    				'productCode'		=> 86,
    				"service"			=> "L",
    				"type"				=> "T",
    				"adress"			=> "",
    				"zipCode"			=> "",
    				"city"				=> "",
    		);
    
    		//Infos générales du client

    		$this->_client = modules::run('boutique/clients/est_connecte');
    		if (!$this->input->post('recherche_relais'))
    		{
    			$livraison 	 = $this->_client;
    
    			$adresse = $livraison->adresse;
    			$code_postal = $livraison->code_postal;
    			$ville = $livraison->ville;
    
    		}
    		else
    		{
    			$adresse = $this->input->post('adress');
    			$code_postal = $this->input->post('code_postal');
    			$ville = $this->input->post('ville');
    		}
    
    		$postfields["address"]			= $adresse;
    		$postfields["zipCode"]			= $code_postal;
    		$postfields["city"]				= $ville;
    		$postfields["trace"]				= TRUE;
    
    		ini_set('soap.wsdl_cache_enabled', '0');
    		$wsdl="https://ws.chronopost.fr/recherchebt-ws-cxf/PointRelaisServiceWS?wsdl";
    		$service=new SoapClient($wsdl, $postfields);
    		$result =  $service->recherchePointChronopost($postfields);
    
    		/*echo "REQUEST:\n" . htmlentities($service->__getLastRequest()) . "\n";
    		 echo "RESPONSE:\n" . htmlentities($service->__getLastResponse()) . "\n";
    
    		die();*/
    
    		if ($result->return->errorCode == 0)
    		{
    			$data = array(
    					'points_retraits' 		=> $result->return->listePointRelais,
    					'adresse'			=> $adresse,
    					'code_postal'		=> $code_postal,
    					'ville'				=> $ville,
    					'erreur'			=> FALSE
    			);
    		}
    		else
    		{
    			echo $result->return->errorMessage;
    			return;
    			
    		}
    		/*echo '<pre>';
    		 var_dump($result);
    		 echo '</pre>';
    		 die();*/
    		if (!$this->input->post('recherche_relais'))
    		{
    			return $this->load->view('boutique/transporteur/module_chronopost', $data);
    		}
    		else
    		{
    			echo json_encode(array(
    					'vue'	=> $this->load->view('boutique/transporteur/module_chronopost', $data, TRUE)
    			));
    		}
    	}
    	return FALSE;
    }
    
    /**
     * AJAX
     * Choix du point relais chronopost, mise à jour de la session puis affichage du point relais sélectionné
     */
    public function choix_relais_chronopost()
    {
    	if ($this->input->is_ajax_request())
    	{
    		if ($this->input->post('relais_id'))
    		{
    			$shipping_date = new DateTime();
    			$shipping_date->modify('+ 2 Days');
    			$postfields = array(
    					"accountNumber"		=> intval($this->_module->identifiant_chronopost),
    					"password"			=> $this->_module->mdp_chronopost,
    					"identifiant"		=> $this->input->post('relais_id'),
    					"language"			=> "FR",
    					"countryCode"		=> "FR",
    					"version"			=> "2.0"
    			);
    			ini_set('soap.wsdl_cache_enabled', '0');
    			$wsdl="https://ws.chronopost.fr/recherchebt-ws-cxf/PointRelaisServiceWS?wsdl";
    			$service=new SoapClient($wsdl, $postfields);
    			$result =  $service->rechercheDetailPointChronopostInter($postfields);
    			 
    			if ($result->return->errorCode == 0)
    			{
    				//Mise à jour de la session
    
    
    				$this->session->set_userdata('livraison_relais',array(
    						'relais_id'		=> $this->input->post('relais_id'),
    						 
    
    				));

    				$this->_client = modules::run('boutique/clients/est_connecte');
    				
    				$data = array(
    					'point' 	=> $result->return->listePointRelais ,
    					'erreur'	=> FALSE,
    					'mobile'	=> $this->client->telephone
    				);
    				echo json_encode(array(
    						'vue'	=> $this->load->view('boutique/transporteur/choix_relais_chronopost', $data, TRUE)
    				));
    			}
    			else
    			{
    				$data = array(
    						'erreur'		=>  $result->return->errorMessage
    				);
    			}
    		}
    	}
    }
    
    /**
     * Recalcul les frais de port en 
     * fonction du pays sélectionné et du transporteur
     * qui permet de retrouver la zone de livraison
     * 
     * @return	JSON
     */
    public function ajax_livraison()
    {
    	$post = $this->input->post();
    	
    	if ($this->input->is_ajax_request() && (($pays_id = $this->input->post('pays_id')) || ($pays_id = $post['livraison']['pays_id'])) && ($transporteur_id = $this->input->post('transporteur_id')))
    	{
    		//Retrouver le transporteur du pays
    		if ( ! $this->transporteur->liaison_pays($pays_id, $transporteur_id))
    		{
    			$transporteur_id = $this->transporteur->par_pays($pays_id)->id;
    		}
    		//Frais de port
    		if ($frais_port = $this->frais_port($pays_id, $transporteur_id))
    		{
    			$etape = ($this->input->post('pays_id')) ? 1 : 3;
				$data = array(
		    		'zones_livraison'	=> $this->liste_zone_livraison($pays_id),
		    		'transporteurs'		=> $this->_liste_transporteur($pays_id, $transporteur_id, $etape),
		    		'frais_port'		=> $frais_port->tarif,
		    		'franco'			=> $this->_afficher_franco_port($frais_port),
		    	);
				
                $totaux = modules::run('boutique/boutique/calcul_commande');
                $total = $totaux['total'];
                
		    	if ($this->input->post('pays_id'))
		    	{
		    		echo json_encode(array(
			    		'erreur'=> FALSE,
			    		'vue'	=> $this->load->view('boutique/transporteur/livraison_etape1', $data, TRUE),
			    		'total'	=> html_price($total),
			    	));
		    		return;
		    	}
		    	else
		    	{

		    		echo json_encode(array(
			    		'erreur'=> FALSE,
			    		'vue'	=> $this->load->view('boutique/transporteur/livraison_etape3', $data, TRUE),
			    		'total'	=> html_price($total),
			    	));
		    		return;
		    	}
    		}
    		else
    		{
    			echo json_encode(array(
    				'erreur' 	=> TRUE,
    				'message' 	=> lang('erreur_transporteur_incompatible')
    			));
    			return;
    		}
    	}
    }
    
    /**
     * Retourne un tableau avec les infos de livraisons
     * 
     * @return	array or boolean
     */
    public function informations()
    {
    	if ($livraison = $this->order->transporter())
    	{
    		if ($frais_port = $this->frais_port($livraison['pays_id'], $livraison['transporteur_id']))
    		{
    			$pays = $this->pays->select('nom')->include_related('zone_livraison', 'libelle')->get_by_id($livraison['pays_id']);
	    		
	    		return array(
	    			'transporteur' 	=> $this->transporteur->get_by_id($livraison['transporteur_id'])->nom,
	    			'frais_port'	=> $frais_port->tarif,
	    			'zone_livraison'=> $pays->zone_livraison_libelle,
	    			'pays'			=> $pays->nom
	    		);
	    	}
    	}
    	return FALSE;
    }
    
    /**
     * Liste des transporteurs par pays
     * 
     * @param 	integer $pays_id
     * @param 	integer $transporteur_id
     * @return	VIEW
     */
    private function _liste_transporteur($pays_id = 66, $transporteur_id, $etape = 1)
    {
    	$transporteurs = $this->transporteur->par_pays($pays_id);
    	
    	if ($transporteurs->exists())
    	{
    		$data = array(
    			'selectionne'	=> $transporteur_id,
    			'transporteurs' => $transporteurs,
    			'etape'			=> $etape,
    			'pays_id'		=> $pays_id,
    		);
    		return $this->load->view('boutique/transporteur/liste', $data, TRUE);
    	}
    	return FALSE;
    }
    
    /**
     * Liste des pays par zone de livraison
     * 
     * @param 	integer $pays_id
     * @param	integer	$etape
     * @return	VIEW
     */
	public function liste_zone_livraison($pays_id, $etape = 1)
	{
    	$zones = $this->zone_livraison->disponibles();
    	
    	if ($zones->exists())
    	{
    		//Multi-zones
    		if ($zones->result_count() > 1)
    		{
	    		$zone_livraison = array();
	    	
		    	foreach ($zones as $zone)
		    	{
		    		$zone_livraison[$zone->libelle] = array();
		    		
			    	//Pays associé à la zone
			    	$pays = array();
					
			    	foreach ($this->pays->par_zone($zone->id) as $item)
			    	{
			    		$pays[$item->id] = $item->nom;
			    	}
			    	$zone_livraison[$zone->libelle] = $pays;
		    	}
    		}
    		//Zone unique
    		else
    		{
    			//Pays associé à la zone
		    	$pays = array();
		    	$liste_pays = $this->pays->par_zone($zones->id);
		    	
		    	//plusieurs pays
		    	if ($liste_pays->result_count() > 1)
		    	{
			    	foreach ($liste_pays as $item)
			    	{
			    		$pays[$item->id] = $item->nom;
			    	}
		    	}
		    	//un pays
		    	else
		    	{
		    		$pays[$liste_pays->id] = $liste_pays->nom;
		    	}
    			$zone_livraison[0] = $pays;
    		}
    		
	    	$data = array(
	    		'zones_livraison'=> $zone_livraison,
	    		'selectionne'	 => $pays_id
	    	);
	    	return $this->load->view('boutique/transporteur/zones_etape'.$etape, $data, TRUE);
    	}
    	return;
    }
    
    /**
     * Retourne la valeur pour le calcul des frais de port
     * Choix : quantité, poids, total de la commande
     * 
     * @return	integer or decimal
     */
    private function _total_valeur()
    {    	
    	switch($this->_module->type_seuil_frais_port)
    	{
    		case self::SEUIL_QUANTITE: $seuil = $this->cart->total_items(); break;
    		case self::SEUIL_POIDS: $seuil = $this->cart->total_weight(); break;
    		case self::SEUIL_TOTAL: $seuil = $this->cart->total(); break;
    		default: $seuil = 0; break;
    	}
    	return $seuil;
    }
    
    /**
     * Indique au client s'il bénéficie des
     * frais de ports gratuits
     * 
     * @param 	object 	$fraisport
     * @return	string
     */
    private function _afficher_franco_port($frais_port)
    {
    	$franco = NULL;
    	$livraison = $this->order->transporter();
		
    	if ((($seuil_gratuit = $frais_port->est_gratuit($livraison['transporteur_id'], $livraison['zone_id'])) !== FALSE) or $this->_frais_port_gratuit_remise())
    	{
	    	switch($this->_module->type_seuil_frais_port)
	    	{
	    		case self::SEUIL_QUANTITE:
					
					$frais_calcules = $frais_port->calculer($livraison['zone_id'], $livraison['transporteur_id'], $this->cart->total_items());
					
	    			if ($frais_calcules->tarif == 0)
	    			{
			    		$franco = lang('frais_port_gratuit');
			    	}
			    	else
			    	{
			    		$franco = lang('frais_port_gratuit').nbs().lang('a_partir_de').round($seuil_gratuit+1).nbs().lang('produits');
			    	}
	    			break;
	    		
	    		case self::SEUIL_POIDS:
					
					//L'unité de poids est renseigné pour un catalogue donc on prend le premier 
					$this->load->model('catalogue/module_catalogue');
					$module_catalogue = $this->module_catalogue->get(1);
					
					$frais_calcules = $frais_port->calculer($livraison['zone_id'], $livraison['transporteur_id'], $this->cart->total_weight());
					
	    			if ($frais_calcules->tarif == 0)
	    			{
			    		$franco = lang('frais_port_gratuit');
			    	}
			    	else
			    	{
			    		$franco = lang('frais_port_gratuit').nbs().lang('a_partir_de').round($seuil_gratuit).nbs().$module_catalogue->unite_poids;
			    	}
	    			break;
	    		
	    		case self::SEUIL_TOTAL: 
					
					$frais_calcules = $frais_port->calculer($livraison['zone_id'], $livraison['transporteur_id'], $this->cart->total());
					
			    	if ($frais_calcules->tarif == 0)
			    	{
			    		$franco = lang('frais_port_gratuit');
			    	}
			    	else
			    	{
			    		$franco = lang('frais_port_gratuit').nbs().lang('a_partir_de').html_price($seuil_gratuit);
			    	}
	    			break;
	    		
	    		default: $franco = NULL; break;
	    	}
    	}
    	return $franco;
    }

    /**
     * Si une remise de type 3 (FDP gratuit) est activée et que montant du panier > mini alors TRUE 
     * 
     * @return	boolean
     */
    private function _frais_port_gratuit_remise()
    {
        if ($remise = $this->order->discount())
        {
        	$this->load->model('code_remise');
            $this->code_remise->get_by_code($remise['code']);
            
            if (($this->code_remise->type_reduction == 3) && ($this->cart->total() >= $this->code_remise->montant_mini))
            {
                return TRUE;
            }
        }
        return FALSE;
    }
}

/* End of file transporteurs.php */
/* Location: ./modules/boutique/controllers/transporteurs.php */ 