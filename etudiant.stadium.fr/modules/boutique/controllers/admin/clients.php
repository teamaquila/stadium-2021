<?php
require_once APPPATH.'controllers/admin/back.php';

/**
 * Clients de la boutique
 * Contrôleur permettant les actions sur les clients
 * 
 * @author 		Pauline Martin, Pauline MARTIN
 * @category 	Controller
 * @version 	1.1
 */
class Clients extends Back
{
	const ITEM_PAR_PAGE = 20;
	
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->load->models(array('client', 'commande'));
		$this->lang->load(array('client', 'boutique'));
	}
	
	/**
	 * Affiche la liste des clients
	 * 
	 * @return	VIEW
	 */
	public function lister($offset = 0)
	{
	    //Paramétrage temps et mémoire allouée au script
	    ini_set('memory_limit', '512M');
	    
		$this->load->library('pagination');
		
		$conditions = $this->input->post();
		
		if ($this->input->post('recherche') || ($this->input->post('recherche') === ""))
		{
			$this->session->set_userdata('recherche_client', $this->input->post('recherche'));
			$conditions['recherche'] = $this->input->post('recherche');
		}
		elseif ($this->session->userdata('recherche_client') && ( ! $this->input->post()))
		{
			$conditions['recherche'] = $this->session->userdata('recherche_client');
		}
        
		$total_item = $this->client->lister($conditions)->result_count();
		
		$this->pagination->initialize(array(
			'base_url'		=> '/boutique/admin/clients/lister/',
			'total_rows'	=> $total_item,
			'per_page'		=> self::ITEM_PAR_PAGE,
			'cur_page'		=> $offset,
			'uri_segment'	=> 5,
			'next_tag_open'	=> '<li class="next">',
			'next_tag_close'=> '</li>'
		));
		
		$data = array(
			'clients'	=> $this->client->lister($conditions, $offset, self::ITEM_PAR_PAGE),
			'total_item'=> $total_item,
			'pagination'=> $this->pagination->create_links()
		);
		
		if ( ! isset($conditions['recherche']))
		{
			$conditions['recherche'] = NULL;
		}
		$data['recherche'] = $conditions['recherche'];
		
		if ($this->input->post('reload'))
		{
			echo json_encode(array(
				'liste'	=> $this->load->view('admin/clients/lister_ajax', $data, TRUE),
				'total_item'=> $total_item,
			    'pagination'=> $this->pagination->create_links(),
			    
			));
		}
		elseif (($offset != 0))
		{		
			echo json_encode(array(
				'liste'		=> $this->load->view('/admin/clients/lister_ajax', $data, TRUE),
				'total_item'=> $total_item,
			    'page'			=> $offset,
				'pagination'=> $this->pagination->create_links(),
			));
		}
		else
		{
			$data['liste_ajax'] = $this->load->view('/admin/clients/lister_ajax', $data, TRUE);
			echo $this->load->view('admin/clients/lister', $data);
		}
	}

	/**
	 * Formulaire et traitement pour modifier
	 * 
	 * @param 	integer	$id
	 * @return 	void
	 */
	public function modifier($id)
	{
		if ($post = $this->input->post())
		{
			if ($this->client->enregistrer($post) !== FALSE)
			{
				$this->log->enregistrer(array(
					'administrateur_id' => $this->administrateur_connecte->id,
					'type'			=> LOG_MODIFICATION,
					'description'	=> lang('log_modification_client').' '.$this->client->prenom.' '.$this->client->nom,
					'adresse_ip'	=> $this->input->ip_address(),
					'user_agent'	=> $this->input->user_agent()
				));
				
				echo json_encode(array(
					'erreur' => FALSE,
					'message'=> lang('succes_modification'),
					'url' 	 => array(
						'liste' => '/boutique/admin/clients/lister?reload=true'
					)
				));
			}
			else
			{
				echo json_encode(array(
					'erreur' => TRUE,
					'message'=> lang('erreur_modification')
				));
			}
			return;
		}
		
		$data = array(
			'client' => $this->client->get_by_id($id),
		);
		$this->load->view('admin/clients/formulaire', $data);
	}
	
	/**
	 * Exporter les clients
	 * 
	 * @return	CSV
	 */
	public function exporter()
	{
		$this->load->dbutil();
		
		header('Content-type: text/csv'); 
		header('Content-disposition: attachment; filename='.mdate('%Y-%m-%d').'-clients.csv');

		$client = $this->db->query("SELECT * FROM t_utilisateurs WHERE id != 1 AND identifiant != '';");
		echo utf8_decode($this->dbutil->csv_from_result($client, ';', "\r\n"));
		
	}
	
	/**
	 * Liste des commandes du client en attente (à finaliser)
	 *
	 * @return	VIEW
	 */
	public function lister_commandes_attente()
	{
	    if (($client = $this->_authentification()) !== FALSE)
	    {
	        $commandes = new Commande();
	        
	        if (empty($client->code))
	        {
	            $liste_commande = $commandes->include_related('client')->where('client_id', $client->id)->where('etat', 7)->order_by('created', 'DESC')->get();
	        }
	        else
	        {
	            $liste_commande = $commandes->include_related('client')->where('code', $client->code)->where('etat', 7)->order_by('created', 'DESC')->get();
	        }
	        
	        $data = array(
	            'commandes' => $liste_commande,
	            'etats'		=> $this->etats_commandes
	        );
	        
	        parent::_afficher('clients/page', array(
	            'module' => $this->load->view('boutique/client/lister_commandes_attente', $data, TRUE)
	        ));
	    }
	    else
	    {
	        redirect('/');
	    }
	}
}
/* End of file clients.php */
/* Location: ./modules/boutique/controllers/admin/clients.php */