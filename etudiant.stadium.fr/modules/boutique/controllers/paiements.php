<?php
require_once APPPATH.'controllers/front.php';

/**
 * Paiements
 * 
 * @author 		Pauline MARTIN
 * @package 	boutique
 * @category 	Controllers
 * @version 	1.1
 */
class Paiements extends Front
{
	/**
	 * Parametres du module boutique
	 * 
	 * @var object
	 */
	private $_module;
	
	/**
	 * Total de la commande
	 * 
	 * @var integer(10)
	 */
	private $_total_commande;
	
	/**
	 * Numéro de commande
	 * 
	 * @var varchar(250)
	 */
	private $_numero_commande;
	
	/**
	 * Email de l'acheteur
	 * 
	 * @var varchar(80)
	 */
	private $_email_acheteur;
	
	/**
	 * Facturation de commande
	 *
	 * @var varchar(250)
	 */
	private $_facturation;
	
	/**
	 * Quantite
	 *
	 * @var varchar(250)
	 */
	private $_quantite;
	
    /**
	 * Association carte / type de paiement
	 * 
	 * @var	array
	 */
    private $_assoc_paiement_carte = array(
        'VISA' => 'CARTE',
        'CB' => 'CARTE',
        'EUROCARD_MASTERCARD' => 'CARTE',
        'E_CARD' => 'CARTE',
        'PAYPAL' => 'PAYPAL'
    );
	
    /**
	 * Type de carte
	 * 
	 * @var varchar(20)
	 */
    private $_type_carte = 'CB';
	
    /**
	 * Type de paiement
	 * 
	 * @var string
	 */
    private $_type_paiement = 'CARTE';
	
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->load->models(array('commande', 'paiement', 'module_boutique'));
		
		$this->_module = $this->module_boutique->get(1);
	}
	
	/**
	 * Déclenche l'API fourni par la banque
	 * Par défaut : Paybox
	 *
	 * @param	integer	$commande_id
	 * @return	VIEW
	 */
	public function index($commande_id)
	{
	    $commande = $this->commande->get_by_id($commande_id);
	    
	    if ($commande->exists() && ! $commande->paiement()->exists())
	    {
	        //Paiement acompte
	        if ($this->_module->option_acompte && ($commande->acompte > 0))
	        {
	            $this->_total_commande = round($commande->acompte, 2);
	        }
	        //Paiement commande
	        else
	        {
	            $this->_total_commande = round($commande->montant, 2);
	        }
	        
	        $this->_numero_commande = $commande->reference;
	        $this->_email_acheteur	= $commande->facturation()->email;
	        $this->_facturation	= $commande->facturation();
	        
	        $count = 0;
	        foreach($commande->produits() as $produit)
	        {
	            $count = $count + $produit->quantite;
	        }
	        $this->_quantite = $count;
   
	        if ($this->input->get('type_carte'))
	        {
	            $this->_type_carte = $this->input->get('type_carte');
	            $this->_type_paiement = $this->_assoc_paiement_carte[$this->_type_carte];
	        }
	        
	        if(isset($this->_module->pbx_hmac) && $this->_module->pbx_hmac != '')
	        {
	            echo $this->_hmac();
	        }
	        else
	        {
	            echo $this->_paybox();
	        }
	        
	        return;
	    }
	    redirect('/boutique/etape,4.html');
	}
	
	/**
	 * PAYBOX
	 * Execute le CGI de paybox avec les paramètres
	 * 
	 * @return	string OR FALSE
	 */
	private function _paybox()
	{
	    
	    if (is_localhost() && (strpos($_SERVER['DOCUMENT_ROOT'], 'C:/') !== FALSE))
	    {
	        // si on se trouve en local (C:\xampp) : paybox pas possible  car module cgi-bin executable que sur unix !
	        // appel à la page suivante en passant les paramètres
	        $retour_paybox_test = base_url().'boutique/etape,5.html?';
	        $retour_paybox_test .= "maref=".$this->_numero_commande;
	        $retour_paybox_test .= "&erreur=00000&auto=XXXXXX&trans=22674555&idtrans=11252835&carte=CB&pays_client=FRA";
	        $retour_paybox_test .= "&datetrans=".date('dmY');
	        $retour_paybox_test .= "&heuretrans=".date('H')."%3A".date('i')."%3A".date('s');
	        $retour_paybox_test .= "&sign=mzsQ4ais%2FTX%2BOhWxc%2FGnykh2K13nMSypw4LerpZY7h71WbPlisaIZoCFP64JzFpPzT7h7VbMTlvNeLdYgjCAHeO90nsIfm4YHTeA3QQIWZyalZZPzHVoNP%2BMTtVB4Mnf6%2Be%2BXUXE%2BvJstd9LwKisUDxgdCkCXHn0cIREAaDRHf0%3D";
	        
	        $page_html_paybox = "<form>";
	        $page_html_paybox .= "<p>Montant : ".$this->_total_commande."</p>"; 
	        $page_html_paybox .= "<p>Commande : ".$this->_numero_commande."</p>";
	        $page_html_paybox .= "<p>Paiement OK</p>";
	        $page_html_paybox .= "<a href='".$retour_paybox_test."'>Retour boutique</a>";
	        $page_html_paybox .= "</form>";

	        echo $page_html_paybox;
	        
	    } else {
	        
    		$url_cgi = 'cgi-bin/paybox/modulev303_CentOS_42.cgi';
    		
    		//Variables du commerçant
    		if ($this->_module->pbx_test)
    		{
    			$PBX_SITE			= 1999888;
    			$PBX_RANG 			= 99;
    			$PBX_IDENTIFIANT	= 2;
    			$PBX_PAYBOX			= 'https://preprod-tpeweb.paybox.com/cgi/MYchoix_pagepaiement.cgi';
    			$PBX_BACKUP1		= 'https://preprod-tpeweb.paybox.com/cgi/MYchoix_pagepaiement.cgi';
    			$PBX_BACKUP2		= 'https://preprod-tpeweb.paybox.com/cgi/MYchoix_pagepaiement.cgi';
    		}
    		else
    		{
    			$PBX_SITE		= $this->_module->pbx_site;
    			$PBX_RANG 		= $this->_module->pbx_rang;
    			$PBX_IDENTIFIANT= $this->_module->pbx_identifiant;
    			$PBX_PAYBOX		= 'https://tpeweb.paybox.com/cgi/MYchoix_pagepaiement.cgi';
    			$PBX_BACKUP1	= 'https://tpeweb1.paybox.com/cgi/MYchoix_pagepaiement.cgi';
    			$PBX_BACKUP2	= 'https://tpeweb2.paybox.com/cgi/MYchoix_pagepaiement.cgi';
    		}
    		
    		//Paramètres Paybox
    		$PBX_MODE 		= $this->_module->pbx_mode;
    		$PBX_LANGUE 	= $this->_module->pbx_langue;
    		$PBX_DEVISE 	= $this->_module->pbx_devise;
    		$PBX_TOTAL 		= $this->_total_commande*100;
    		$PBX_3DS		= 'N'; //Désactive le 3D-secure
    		$PBX_CMD 		= $this->_numero_commande;
    		$PBX_PORTEUR 	= $this->_email_acheteur;
    		
    		//URLs
    		$PBX_RETOUR 	= 'maref:R\;erreur:E\;auto:A\;trans:T\;idtrans:S\;carte:C\;pays_banque:Y\;pays_client:I\;datetrans:W\;heuretrans:Q\;sign:K';
    		$PBX_REPONDRE_A	= base_url().'boutique/paiements/enregistrement';
    		$PBX_EFFECTUE 	= base_url().'boutique/etape,5.html';
    		$PBX_REFUSE 	= base_url().'boutique/etape,5.html';
    		$PBX_ANNULE 	= base_url().'boutique/etape,5.html';
    		
    		//Page intermédiaire
    		$PBX_WAIT 	= 0;
    		$PBX_TXT 	= '';
    		$PBX_BOUTPI = NULL;
            
            //Choix carte paiements pour ne pas rechoisir avant de payer
            $PBX_TYPEPAIEMENT 	= $this->_type_paiement;
            $PBX_TYPECARTE 		= $this->_type_carte;
    		
    		//Chemin du CGI
    		if (is_localhost() or preg_match('/^http:\/\/(test).*/', base_url()) or preg_match('/^http:\/\/(mobile).*/', base_url()))
    		{
    			if (base_url() == 'http://demo.ocea-manager/')
    			{
    				$MOD = ROOTPATH.'../../../'.$url_cgi;
    			}
    			else if (preg_match('/^http:\/\/([a-z.]*)(mobile).*/', base_url()))
    			{
    				$MOD = ROOTPATH.'../../../'.$url_cgi;
    			}
    			else
    			{
    				$MOD = ROOTPATH.'../../'.$url_cgi;
    			}
    		}
    		else
    		{
    			$MOD = ROOTPATH.'../'.$url_cgi;
    		}
    		
    		//$MOD = CMSFULLPATH.'application/cgi-bin/paybox_v303_CentOS_42.cgi';
    		$PBX  = ' PBX_MODE='.$PBX_MODE.' PBX_LANGUE='.$PBX_LANGUE.' PBX_SITE='.$PBX_SITE;
    		$PBX .= ' PBX_PAYBOX='.$PBX_PAYBOX.' PBX_BACKUP1='.$PBX_BACKUP1.' PBX_BACKUP2='.$PBX_BACKUP2;
    		$PBX .= ' PBX_RANG='.$PBX_RANG.' PBX_IDENTIFIANT='.$PBX_IDENTIFIANT.' PBX_TOTAL='.$PBX_TOTAL;
    		$PBX .= ' PBX_DEVISE='.$PBX_DEVISE.' PBX_CMD='.$PBX_CMD.' PBX_PORTEUR='.$PBX_PORTEUR.' PBX_REPONDRE_A='.$PBX_REPONDRE_A;
    		$PBX .= ' PBX_RETOUR='.$PBX_RETOUR.' PBX_EFFECTUE='.$PBX_EFFECTUE.' PBX_REFUSE='.$PBX_REFUSE.' PBX_ANNULE='.$PBX_ANNULE;
            $PBX .= ' PBX_TYPEPAIEMENT='.$PBX_TYPEPAIEMENT.' PBX_TYPECARTE='.$PBX_TYPECARTE;
    
    		//Execution du script shell
    		return shell_exec($MOD.$PBX);
	    }
	}
	
	/**
	 * Execute le mode HMAC de paybox avec les paramètres
	 *
	 * @return	string OR FALSE
	 */
	private function _hmac()
	{
	    //Variables du commerçant
	    if ($this->_module->pbx_test)
	    {
	        $PBX_SITE			= 1999888;
	        $PBX_RANG 			= 32;
	        $PBX_IDENTIFIANT	= 2;
	        $keyTest = '0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF';
	        
	        $PBX_PAYBOX		= 'https://preprod-tpeweb.paybox.com/';
	        $PBX_BACKUP1	= 'https://preprod-tpeweb.paybox.com/';
	        //$PBX_BACKUP2		= 'https://preprod-tpeweb.paybox.com/cgi/MYchoix_pagepaiement.cgi';
	        
	        $MOD = $PBX_PAYBOX;
	    }
	    else
	    {
	        $PBX_SITE          = $this->_module->pbx_site;
	        $PBX_RANG 		   = $this->_module->pbx_rang;
	        $PBX_IDENTIFIANT   = $this->_module->pbx_identifiant;
	        $keyTest           = $this->_module->pbx_hmac;
	        
	        $PBX_PAYBOX		= 'https://tpeweb.paybox.com/';
	        $PBX_BACKUP1	= 'https://tpeweb1.paybox.com/';
	        //$PBX_BACKUP2	= 'https://tpeweb2.paybox.com/cgi/MYchoix_pagepaiement.cgi';
	    }
	    
	    $serveurs = array(
	        $PBX_PAYBOX,
	        $PBX_BACKUP1
	    );
	    
	    $serveurOK = FALSE;
	    
	    foreach($serveurs as $serveur)
	    {
	        $doc = new DOMDocument();
	        $doc->loadHTMLFile($serveur.'load.html');
	        $server_status = "";
	        $element = $doc->getElementById('server_status');
	        if($element){
	            $server_status = $element->textContent;
	        }
	        
	        if($server_status == "OK")
	        {
	            $serveurOK = TRUE;
	            $MOD = $serveur.'php/';
	            break;
	        }
	        
	    }
	    
	    if(!$serveurOK)
	    {
	        $MOD = $PBX_PAYBOX.'php/';
	    }
	    
	    $PBX_MODE 		= $this->_module->pbx_mode;
	    $PBX_LANGUE 	= $this->_module->pbx_langue;
	    $PBX_DEVISE 	= $this->_module->pbx_devise;
	    $PBX_TOTAL 		= $this->_total_commande*100;
	    $PBX_3DS		= 'N'; //Désactive le 3D-secure
	    $PBX_CMD 		= $this->_numero_commande;
	    $PBX_PORTEUR 	= $this->_email_acheteur;
	    
	    $PBX_HASH = 'SHA512';
	    $PBX_TIME = date("c");
	    
	    //URLs
	    $PBX_RETOUR 	= 'montant:M;maref:R;auto:A;trans:T;paiement:P;carte:C;idtrans:S;pays:Y;erreur:E;IP:I;digest:H;datetrans:W;heuretrans:Q;sign:K;secure:v';
	    $PBX_REPONDRE_A	= base_url().'boutique/paiements/enregistrement';
	    $PBX_REPONDRE_A	= base_url().'boutique/paiements/enregistrement';
	    $PBX_EFFECTUE 	= base_url().'boutique/etape,5.html';
	    //$PBX_EFFECTUE	= base_url().'boutique/paiements/enregistrement';
	    $PBX_REFUSE 	= base_url().'boutique/etape,5.html';
	    $PBX_ANNULE 	= base_url().'boutique/etape,5.html';
	    
	    //Page intermédiaire
	    // 	    $PBX_WAIT 	= 0;
	    // 	    $PBX_TXT 	= '';
	    // 	    $PBX_BOUTPI = NULL;
	    
	    //Choix carte paiements pour ne pas rechoisir avant de payer
	    $PBX_TYPEPAIEMENT 	= $this->_type_paiement;
	    $PBX_TYPECARTE 		= $this->_type_carte;
	    
	    $pays = array(
	        "4" => 'Afghanistan',
	        "248" => 'Aland (Îles',
	        "8" => 'Albanie',
	        "12" => 'Algérie',
	        "16" => 'Samoa américaines',
	        "20" => 'Andorre',
	        "24" => 'Angola',
	        "660" => 'Anguilla',
	        "10" =>  'Antarctique',
	        "28" =>  'Antigua-et-Barbuda',
	        "32" =>  'Argentine',
	        "51" =>  'Arménie',
	        "533" => 'Aruba',
	        "36" =>  'Australie',
	        "40" =>  'Autriche',
	        "31" =>  'Azerbaïdjan',
	        "44" => 'Bahamas',
	        "48" => 'Bahreïn',
	        "50" => 'Bangladesh',
	        "52" => 'Barbade',
	        "112" => 'Bélarus',
	        "56" => 'Belgique',
	        "84" => 'Belize',
	        "204" => 'Bénin',
	        "60" => 'Bermudes',
	        "64" => 'Bhoutan',
	        "68" => 'Bolivie (État plurinational de',
	        "535" => 'Bonaire, Saint-Eustache et Saba',
	        "70" => 'Bosnie-Herzégovine',
	        "72" => 'Botswana',
	        "74" => 'Bouvet (Île',
	        "76" => 'Brésil',
	        "86" => 'Indien (Territoire britannique de océan',
	        "96" => 'Brunéi Darussalam',
	        "100" => 'Bulgarie',
	        "854" => 'Burkina Faso',
	        "108" => 'Burundi',
	        "132" => 'Cabo Verde',
	        "116" => 'Cambodge',
	        "120" => 'Cameroun',
	        "124" => 'Canada',
	        "136" => 'Caïmans (Îles',
	        "140" => 'République centrafricaine',
	        "148" =>'Tchad',
	        "152" => 'Chili',
	        "156" => 'Chine',
	        "162" => 'Christmas (Île',
	        "166" => 'Cocos (Îles / Keeling (Îles',
	        "170" => 'Colombie',
	        "174" => 'Comores',
	        "180" => 'Congo (République démocratique du',
	        "178" => 'Congo',
	        "184" => 'Cook (Îles',
	        "188" => 'Costa Rica',
	        "384" => 'Côte d\'Ivoire',
	        "191" => 'Croatie',
	        "192" => 'Cuba',
	        "531" => 'Curaçao',
	        "196" => 'Chypre',
	        "203" => 'République Tchèque',
	        "208" => 'Danemark',
	        "262" =>	 'Djibouti',
	        "212" => 'Dominique',
	        "214" => 'dominicaine (République',
	        "218" => 'Equateur',
	        "818" => 'Egypte',
	        "222" => 'El Salvador',
	        "226" => 'Guinée équatoriale',
	        "232" => 'Erythrée',
	        "233" => 'Estonie',
	        "231" => 'Ethiopie',
	        "238" => 'Falkland (Îles / Malouines (Îles',
	        "234" => 'Féroé (Îles',
	        "242" => 'Fidji',
	        "246" => 'Finlande',
	        "250" => 'France',
	        "254" => 'Guyane française',
	        "258" => 'Polynésie française',
	        "260" => 'Terres austrafrançaises',
	        "266" => 'Gabon',
	        "270" => 'Gambie',
	        "268" => 'Géorgie',
	        "276" => 'Allemagne',
	        "288" =>  'Ghana',
	        "292" => 'Gibraltar',
	        "300" => 'Grèce',
	        "304" => 'Groenland',
	        "308" => 'Grenade',
	        "312" => 'Guadeloupe',
	        "316" => 'Guam',
	        "320" => 'Guatemala',
	        "831" => 'Guernesey',
	        "324" => 'Guinée',
	        "624" => 'Guinée-Bissau',
	        "328" => 'Guyana',
	        "332" => 'Haïti',
	        "334" => 'Heard-et-MacDonald (Île',
	        "336" => 'Saint-Siège',
	        "340" => 'Honduras',
	        "344" => 'Hong Kong',
	        "348" => 'Hongrie',
	        "352" => 'Islande',
	        "356" => 'Inde',
	        "360" => 'Indonésie',
	        "364" => 'Iran (République Islamique d\'',
	        "368" => 'Iraq',
	        "372" => 'Irlande',
	        "833" => 'Ile de Man',
	        "376" => 'Israël',
	        "380" => 'Italie',
	        "388" => 'Jamaïque',
	        "392" => 'Japon',
	        "832" => 'Jersey',
	        "400" => 'Jordanie',
	        "398" => 'Kazakhstan',
	        "404" => 'Kenya',
	        "296" => 'Kiribati',
	        "408" => 'Corée (République populaire démocratique de',
	        "410" => 'Corée (République de',
	        "414" => 'Koweït',
	        "417" => 'Kirghizistan',
	        "418" => 'Lao, République démocratique populaire',
	        "428" => 'Lettonie',
	        "422" => 'Liban',
	        "426" => 'Lesotho',
	        "430" => 'Libéria',
	        "434" => 'Libye',
	        "438" => 'Liechtenstein',
	        "440" => 'Lituanie',
	        "442" => 'Luxembourg',
	        "446" => 'Macao',
	        "807" => 'Macédoine (ex-République yougoslave de',
	        "450" => 'Madagascar',
	        "454" => 'Malawi',
	        "458" => 'Malaisie',
	        "462" => 'Maldives',
	        "466" => 'Mali',
	        "470" => 'Malte',
	        "584" => 'Marshall (Îles',
	        "474" => 'Martinique',
	        "478" => 'Mauritanie',
	        "480" => 'Maurice',
	        "175" => 'Mayotte',
	        "484" => 'Mexique',
	        "583" => 'Micronésie (États fédérés de',
	        "498" => 'Moldova (République de',
	        "492" => 'Monaco',
	        "496" => 'Mongolie',
	        "499" => 'Monténégro',
	        "500" => 'Montserrat',
	        "504" => 'Maroc',
	        "508" => 'Mozambique',
	        "104" =>'Myanmar',
	        "516" =>'Namibie',
	        "520" => 'Nauru',
	        "524" => 'Népal',
	        "528" => 'Pays-Bas',
	        "540" => 'Nouvelle-Calédonie',
	        "554" => 'Nouvelle-Zélande',
	        "558" => 'Nicaragua',
	        "562" => 'Niger',
	        "566" => 'Nigéria',
	        "570" => 'Niue',
	        "574" => 'Norfolk (Île',
	        "580" => 'Mariannes du Nord (Îles',
	        "578" => 'Norvège',
	        "512" => 'Oman',
	        "586" => 'Pakistan',
	        "585" => 'Palaos',
	        "275" => 'Palestine, État de',
	        "591" => 'Panama',
	        "598" => 'Papouasie-Nouvelle-Guinée',
	        "600" => 'Paraguay',
	        "604" => 'Pérou',
	        "608" => 'Philippines',
	        "612" => 'Pitcairn',
	        "616" => 'Pologne',
	        "620" => 'Portugal',
	        "630" => 'Porto Rico',
	        "634" => 'Qatar',
	        "638" => 'Réunion',
	        "642" => 'Roumanie',
	        "643" => 'Russie (Fédération de',
	        "646" => 'Rwanda',
	        "652" => 'Saint-Barthélemy',
	        "654" => 'Sainte-Hélène, Ascension et Tristan da Cunha',
	        "659" => 'Saint-Kitts-et-Nevis',
	        "662" => 'Sainte-Lucie',
	        "663" => 'Saint-Martin (partie française',
	        "666" => 'Saint-Pierre-et-Miquelon',
	        "670" => 'Saint-Vincent-et-Grenadines',
	        "882" => 'Samoa',
	        "674" => 'Saint-Marin',
	        "678" => 'Sao Tomé-et-Principe',
	        "682" => 'Arabie saoudite',
	        "686" => 'Sénégal',
	        "688" => 'Serbie',
	        "690" => 'Seychelles',
	        "694" => 'Sierra Leone',
	        "702" => 'Singapour',
	        "534" => 'Saint-Martin (partie néerlandaise',
	        "703" => 'Slovaquie',
	        "705" => 'Slovénie',
	        "90" => 'Salomon (Îles',
	        "706" => 'Somalie',
	        "710" => 'Afrique du Sud',
	        "239" => 'Géorgie du Sud-et-les Îles Sandwich du Sud',
	        "728" => 'Soudan du Sud',
	        "724" => 'Espagne',
	        "144" => 'Sri Lanka',
	        "729" => 'Soudan',
	        "740" => 'Suriname',
	        "744" => 'Svalbard et Île Jan Mayen',
	        "748" => 'Swaziland',
	        "752" => 'Suède',
	        "756" => 'Suisse',
	        "760" => 'République arabe syrienne',
	        "158" => 'Taïwan (Province de Chine',
	        "762" => 'Tadjikistan',
	        "834" => 'Tanzanie, République-Unie de',
	        "764" => 'Thaïlande',
	        "626" => 'Timor-Leste',
	        "768" => 'Togo',
	        "772" => 'Tokelau',
	        "776" => 'Tonga',
	        "780" => 'Trinité-et-Tobago',
	        "788" => 'Tunisie',
	        "792" => 'Turquie',
	        "795" => 'Turkménistan',
	        "796" => 'Turks-et-Caïcos (Îles',
	        "798" => 'Tuvalu',
	        "800" => 'Ouganda',
	        "804" => 'Ukraine',
	        "784" => 'Emirats arabes unis',
	        "826" => 'Royaume-Uni',
	        "581" => 'Iles mineures éloignées des États-Unis',
	        "840" => 'Etats-Unis d\'Amérique',
	        "858" => 'Uruguay',
	        "860" => 'Ouzbékistan',
	        "548" => 'Vanuatu',
	        "862" => 'Venezuela (République bolivarienne du',
	        "704" => 'Viet Nam',
	        "92" =>  'Vierges britanniques (Îles',
	        "850" => 'Vierges des États-Unis (Îles',
	        "876" => 'Wallis-et-Futuna ',
	        "732" => 'Sahara occidental',
	        "887" => 'Yémen',
	        "894" => 'Zambie',
	        "716" => 'Zimbabwe'
	    );
	    
	    $country_code = array_keys($pays, $this->_facturation->pays);
	    if(isset($country_code[0]))
	    {
	        $code = $country_code[0];
	    }
	    else
	    {
	        $code = 250;
	    }
	    
	    $PBX_SHOPPINGCART = "<?xml version='1.0' encoding='utf-8'?><shoppingcart><total><totalQuantity>".$this->_quantite."</totalQuantity></total></shoppingcart>";
	    $PBX_BILLING = "<?xml version='1.0' encoding='utf-8'?><Billing><Address><FirstName>".substr(str_replace("\r\n", ' ', $this->_facturation->prenom), 0, 30)."</FirstName><LastName>".substr(str_replace("\r\n", ' ', $this->_facturation->nom), 0, 30)."</LastName><Address1>".substr(str_replace(array("\r\n", "\n"), array(' ', ' '), $this->_facturation->adresse), 0, 50)."</Address1><ZipCode>".$this->_facturation->code_postal."</ZipCode><City>".substr(str_replace("\r\n", ' ', $this->_facturation->ville), 0, 50)."</City><CountryCode>".$code."</CountryCode></Address></Billing>";
	    
	    $msg = "PBX_SITE=".$PBX_SITE.
	    "&PBX_RANG=".$PBX_RANG.
	    "&PBX_IDENTIFIANT=".$PBX_IDENTIFIANT.
	    "&PBX_LANGUE=".$PBX_LANGUE.
	    "&PBX_MODE=".$PBX_MODE.
	    "&PBX_TYPEPAIEMENT=".$PBX_TYPEPAIEMENT.
	    "&PBX_TYPECARTE=".$PBX_TYPECARTE.
	    "&PBX_TOTAL=".$PBX_TOTAL.
	    "&PBX_DEVISE=".$PBX_DEVISE.
	    "&PBX_CMD=".$PBX_CMD.
	    "&PBX_PORTEUR=".$PBX_PORTEUR.
	    "&PBX_REPONDRE_A=".$PBX_REPONDRE_A.
	    "&PBX_RETOUR=".$PBX_RETOUR.
	    "&PBX_EFFECTUE=".$PBX_EFFECTUE.
	    "&PBX_REFUSE=".$PBX_REFUSE.
	    "&PBX_ANNULE=".$PBX_ANNULE.
	    "&PBX_HASH=".$PBX_HASH.
	    "&PBX_TIME=".$PBX_TIME.
	    "&PBX_SHOPPINGCART=".$PBX_SHOPPINGCART.
	    "&PBX_BILLING=".$PBX_BILLING;
	    
	    $binKey = pack("H*", $keyTest);
	    $PBX_HMAC = strtoupper(hash_hmac('sha512', $msg, $binKey));
	    
	    
	    $data = array(
	        'PBX'	=> array(
	            'PBX_SITE' => $PBX_SITE,
	            'PBX_RANG' => $PBX_RANG,
	            'PBX_IDENTIFIANT' => $PBX_IDENTIFIANT,
	            'PBX_LANGUE' => $PBX_LANGUE,
	            'PBX_MODE' => $PBX_MODE,
	            'PBX_TYPEPAIEMENT' => $PBX_TYPEPAIEMENT,
	            'PBX_TYPECARTE' => $PBX_TYPECARTE,
	            'PBX_TOTAL' => $PBX_TOTAL,
	            'PBX_DEVISE' => $PBX_DEVISE,
	            'PBX_CMD' => $PBX_CMD,
	            'PBX_PORTEUR' => $PBX_PORTEUR,
	            'PBX_REPONDRE_A' => $PBX_REPONDRE_A,
	            'PBX_RETOUR' => $PBX_RETOUR,
	            'PBX_EFFECTUE' => $PBX_EFFECTUE,
	            'PBX_REFUSE' => $PBX_REFUSE,
	            'PBX_ANNULE' => $PBX_ANNULE,
	            'PBX_HASH' => $PBX_HASH,
	            'PBX_TIME' => $PBX_TIME,
	            'PBX_SHOPPINGCART' => $PBX_SHOPPINGCART,
	            'PBX_BILLING' => $PBX_BILLING,
	            'PBX_HMAC' => $PBX_HMAC,
	        ),
	        'MOD'	=> $MOD
	    );

	    return $this->load->view('boutique/boutique/attente_paybox', $data, TRUE);
	}
	
	/**
	 * Enregistrement du paiement en base
	 * 
	 * @return	boolean
	 */
	public function enregistrement()
	{
		$this->load->helper('file');
		
		$log_file 	 = root_path().'logs/paybox.log';
		$log_string  = date('Y-m-d H:i:s').' | ';
		$ip_autorise = array(
			'192.168.1.112',
			'194.2.122.158',
			'194.2.160.66',
			'195.25.7.146',
			'195.25.7.157',
			'195.25.7.159',
			'195.25.67.0',
			'195.25.7.166',
			'194.2.160.80',
			'195.101.99.72',
			'195.101.99.73',
			'195.101.99.76',
			'194.2.122.190',
			'194.2.160.75',
			'195.25.67.22',
			'195.25.67.2',
			'195.25.67.11',
			'194.2.160.82',
			'194.2.160.91',
			'194.2.160.64',
			'81.80.66.70' //Aquila
		);
				
		//Identification du paiement
		if ($this->input->get('maref') && $this->input->get('idtrans'))
		{				
			$commande = $this->commande->get_by_reference($this->input->get('maref'));
			
			if ($commande->exists())
			{
				//Paiement validé
				if ($this->input->get('auto') && ($this->input->get('erreur') == '00000'))
				{
					$paiement = $commande->paiement();
					
					if ( ! $paiement->exists())
					{
						//Formatage de la date
						$date = date_to_array($this->input->get('datetrans'));
						$date = $date[1].'/'.$date[2].'/'.$date[3].' ';
						$transaction_date = datetime_to_mysql($date.$this->input->get('heuretrans'));
						
						$this->paiement->clear();
						$paiement = $this->paiement->enregistrer(array(
							'commande_id' 		=> $commande->id,
							'type_carte' 		=> $this->input->get('carte'),
							'num_autorisation' 	=> $this->input->get('auto'),
							'num_transaction' 	=> $this->input->get('trans'),
							'id_transaction' 	=> $this->input->get('idtrans'),
							'transaction_date' 	=> $transaction_date,
							'code_pays_banque' 	=> $this->input->get('pays'),
							'code_pays_client' 	=> $this->input->get('IP')
						));
						
						if ($paiement !== FALSE)
						{
							if ($commande->changer_etat(2)) //Payée
							{
							    if ($this->_module->type_export_commande == 1)
							    {
							        modules::run('boutique/exporter/temps_reel', $commande->id);
							    }
							    
							    //Envoi mail client et vendeur
							    modules::run('boutique/emails/commande_payee', $commande);

								//Mise à jour du stock
								$this->_changer_stock($commande);

								$log_string .= $this->input->get('trans').' | P ';
							}
							else
							{
								$log_string .= $this->input->get('trans').' | Erreur de sauvegarde commande ';
							}
						}
						else
						{
							$log_string .= $this->input->get('trans').' | Erreur de sauvegarde paiement ';
						}
					}
					else
					{
						$log_string .= $this->input->get('trans').' | Paiement existant ';
					}
				}
				//Erreur de paiement
				else
				{
					$commande->changer_etat(5); //Annulée
					$log_string .= $this->input->get('trans').' | A | ERR'.$this->input->get('erreur');
				}
                
			}
			else
			{
				$log_string .= 'Commande inexistante | '.$this->input->get('maref');
			}
		}
		else
		{
			$log_string .= 'Erreur identification commande';
		}

		$log_string .= "\n";
        
        
		
		return write_file($log_file, $log_string, FOPEN_WRITE_CREATE);
	}

	/**
	 * Enregistre un paiement quand Paybox est en TEST 
	 * 
	 * @param	object	$commande
	 * @return	boolean
	 */
	public function test($commande)
	{
		$paiement = $commande->paiement();

		if ( ! $paiement->exists())
		{
			//Formatage de la date
			$date = date_to_array($this->input->get('datetrans'));
			$date = $date[1].'/'.$date[2].'/'.$date[3].' ';
			$transaction_date = datetime_to_mysql($date.$this->input->get('heuretrans'));
			
			$this->paiement->clear();
			$paiement = $this->paiement->enregistrer(array(
				'commande_id' 		=> $commande->id,
				'type_carte' 		=> $this->input->get('carte'),
				'num_autorisation' 	=> $this->input->get('auto'),
				'num_transaction' 	=> $this->input->get('trans'),
				'id_transaction' 	=> $this->input->get('idtrans'),
				'transaction_date' 	=> $transaction_date,
				'code_pays_banque' 	=> $this->input->get('pays'),
				'code_pays_client' 	=> $this->input->get('IP')
			));
			
			if ($paiement !== FALSE)
			{
				if ($commande->changer_etat(2)) //Payée
				{
				    if ($this->_module->type_export_commande == 1)
				    {
				        modules::run('boutique/exporter/temps_reel', $commande->id);
				    }
				    
				    //Envoi mail client et vendeur
				    modules::run('boutique/emails/commande_payee', $commande);
				    
				    //Mise à jour du stock
				    $this->_changer_stock($commande);
				    
				    $log_string .= $this->input->get('trans').' | P ';
				    
				    return TRUE;
				}
			}
		}
		return FALSE;
	}
	
	/**
	 * Met à jour le stock du produit
	 * 
	 * @param	object	$commande
	 * @return	TRUE
	 */
	public function _changer_stock($commande)
	{
		$this->load->models(array('catalogue/produit', 'catalogue/module_catalogue'));
		
		foreach ($commande->produits() as $produit_commande)
		{
			$this->produit->clear();
			
			$produit = $this->produit->get_by_id($produit_commande->produit_id);
			$module  = $produit->module();
			
			if ($module->gestion_stock)
			{
				$produit->stock -= $produit_commande->quantite;
				$produit->save();
				
				//Si référence, on décrémente aussi le produit parent
				if ($produit = $produit->produit_parent())
				{
					$produit->stock -= $produit_commande->quantite;
					$produit->save();
				}
			}
		}
		return TRUE;
	}
}

/* End of file paiements.php */
/* Location: ./modules/boutique/controllers/paiements.php */