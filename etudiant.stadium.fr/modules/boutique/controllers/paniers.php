<?php
require_once APPPATH.'controllers/front.php';

/**
 * Panier
 * Contrôleur permettant les actions sur le panier
 * 
 * @author 		Pauline MARTIN
 * @package 	boutique
 * @category 	Controllers
 * @version 	1.1
 */
class Paniers extends Front
{
	/**
	 * Paramètres du module
	 * 
	 * @var object
	 */
	private $_module;
	
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('cart');
		$this->load->config('catalogue/catalogue');
		$this->load->helper('catalogue/catalogue');
		$this->load->models(array(
			'module_boutique', 'catalogue/produit', 'catalogue/tarif', 
			'catalogue/module_catalogue', 'medias/media', 'panier', 'produits_panier', 'client', 'utilisateur'
		));

		$this->load->modules(array(
			'boutique/remises',
			'boutique/clients'
		));

		$this->_client = $this->clients->est_connecte();
		$this->_module = $this->module_boutique->get(1);
	}
	
	/**
	 * Liste des produits du panier
	 *
	 * @param	integer	$etape
	 * @return	VIEW
	 */
	public function afficher($etape)
	{
		$data = array(
				'module' => $this->_module,
				'panier' => $this->cart->contents()
		);
		return $this->load->view('boutique/panier/etape'.$etape, $data, TRUE);
	}
    
    /**
     * Liste des produits du panier sur le compte client des sauvegardés
     * 
     * @param	integer	$panier_id
     * @return	VIEW
     */
    public function afficher_sauvegarde($panier_id = NULL)
    {
		$this->lang->load('boutique');
    	$panier = new Commande();
    	$produits_panier = new Produits_commande();
    	
    	$data = array(
    		'module' 	=> $this->_module,
    	    'panier'	=> $panier->get_by_id($panier_id),
    		'produits' 	=> $produits_panier->get_by_commande_id($panier_id)
    	);
    	echo json_encode(array(
    		'vue'		=> $this->load->view('boutique/panier/afficher_sauvegarde', $data, TRUE)
    	));
    	return ;
    }
    
    /**
     * Supression d'un panier enregistré
     *  @param integer panier_id
     * */
    public function supprimer_enregistrement($panier_id = NULL)
    {
    	if (!is_null($panier_id))
    	{
    		$produits_panier = new Produits_panier();
    		$produits_panier->get_by_panier_id($panier_id)->delete_all();
    		
    		$panier = new Panier();
    		echo $panier->get_by_id($panier_id)->delete();
    		return ;
    		 
    	}
    }
    
    /**
     * Ajout au panier un panier enregistré
     * @param string $panier_id
     */
    public function commander_panier($panier_id = NULL)
    {
    	if (!is_null($panier_id))
    	{
    		$panier = new Panier();
    		$produits_panier= $panier->include_related('produits_panier')->get_by_id($panier_id);
    		foreach ($produits_panier as $produit_panier)
    		{
    			$produit = $this->produit->include_related('module_catalogue', array('gestion_stock'))->get_by_id($produit_panier->produits_panier_produit_id);
    			
    			
    			if ($produit->exists())
    			{
    				$quantite = $produit_panier->produits_panier_quantite;
    				//Stock
    				if ($produit->module_catalogue_gestion_stock && ($produit->stock < $quantite))
    				{
    					$quantite = $produit->stock;
    				}
    			
    				$client = $this->clients->est_connecte();
    				$tarif 	= $produit->tarif($client, $quantite);
    			
    				if (is_array($tarif) && (( ! $tarif['unique'] && isset($tarif['grille'])) or $tarif['unique']))
    				{
    					//Prix TTC si saisie en HT
    					if ( ! $tarif['taxe'])
    					{
    						$tarif['prix'] = $tarif['prix']*(1+$tarif['tva']/100);
    					}
    			
    					$cart = array(
    							'id' 		=> $produit->id,
    							'qty'		=> $quantite,
    							'price'		=> $tarif['prix'],
    							'name'		=> $produit->libelle,
    							'options'	=> $produit_panier->options,
    					);
    			
    					if ($rowid = $this->cart->insert($cart))
    					{
    						$json = array(
    								'erreur'	=> FALSE,
    								'libelle'	=> $produit->libelle,
    								'tarif'		=> $tarif['prix'],
    								'options'	=> $cart['options'],
    								'id'		=> $cart['id'],
    								'rowid'		=> $rowid
    						);
    					}
    					else
    					{
    						$json = array(
    								'erreur'	=> TRUE,
    								'message'	=> lang('erreur_ajout_panier')
    						);
    					}
    				}
    				else
    				{
    					$json = array(
    							'erreur'	=> TRUE,
    							'message'	=> lang('erreur_prix_produit')
    					);
    				}
    			}
    			else
    			{
    				$json = array(
    						'erreur'	=> TRUE,
    						'message'	=> lang('erreur_produit_inexistant')
    				);
    			}
    		}
    	}
    }
    
	/**
     * Ajouter un produit au panier
     * 
	 * @param	integer	$produit_id
     * @return	JSON
     */
    public function ajouter($produit_id = NULL)
    {
    	if ( ! is_null($produit_id))
		{
			$json = $this->_produit($produit_id);
		}
		elseif ($this->input->post('quantite') > 0)
    	{
	    	if ($produit_id = $this->input->post('produit_id'))
	    	{
	    		$json = $this->_produit($produit_id, $this->input->post('quantite'), $this->input->post('options'));
	    	}
	    	else
	    	{
	    		$json = array(
	    			'erreur'	=> TRUE,
	    			'message'	=> lang('erreur_produit_inexistant')
	    		);
	    	}
    	}
    	else
    	{
    		$json = array(
    			'erreur'	=> TRUE,
    			'message'	=> lang('erreur_quantite_nulle')
    		);
    	}
    	
		//Vérifier la remise
		$this->remises->verifier_validite(FALSE);
		
    	echo json_encode($json);
    }
    
	/**
     * Modifier la quantité d'un produit au panier
     * 
     * @param	array	$items
     * @return 	void
     */
    public function modifier($items)
    {
    	foreach ($items as $rowid => $quantite)
    	{
    		foreach ($this->cart->contents() as $item)
    		{
    			if ($item['rowid'] == $rowid)
    			{
    				$produit = $this->produit->include_related('module_catalogue', array('gestion_stock'), 'gestion_stock')->get_by_id($item['id']);
					
					if ( ! $produit->gestion_stock or ($produit->gestion_stock && ($quantite <= $produit->stock)))
					{
						$this->cart->update(array(
					   		'rowid' => $rowid,
							'qty'	=> $quantite
					   	));
					}
					
					//Vérifier la remise
					$this->remises->verifier_validite(0);
    			}
    			
    			if (isset($item['options']['pack_parent']) && ($item['options']['pack_parent'] == $rowid))
    			{
    			    $this->cart->update(array(
    			        'rowid' => $item['rowid'],
    			        'qty'	=> $quantite
    			    ));
    			    
    			    //Vérifier la remise
    			    modules::run('boutique/remises/verifier_validite');
    			}
    		}
    	}
    }
    
	/**
     * Supprimer un produit du panier
     * 
	 * @param	string	$rowid
     * @return	json
     */
    public function supprimer($rowid = NULL)
    {
    	$rowid = ( ! is_null($rowid)) ? $rowid : $this->input->get('rowid');
		
    	$update = $this->cart->update(array(
    		'rowid' => $rowid,
			'qty'	=> 0
    	));

    	foreach ($this->cart->contents() as $row_impose)
    	{
    	    if (isset($row_impose['options']['produit_impose']) && ($row_impose['options']['produit_impose']) == $rowid)
    	    {
    	        $this->cart->update(array(
    	            'rowid' => $row_impose['rowid'],
    	            'qty'	=> 0
    	        ));
    	    }
    	    //Produits composés (PACKS)
    	    if (isset($row_impose['options']['pack_parent']) && ($row_impose['options']['pack_parent']) == $rowid)
    	    {
    	        $this->cart->update(array(
    	            'rowid' => $row_impose['rowid'],
    	            'qty'	=> 0
    	        ));
    	    }
    	    
    	}
    	
    	//Vérifier la remise
		$this->remises->verifier_validite();
    	
    	if ($update !== FALSE)
    	{
    		echo json_encode(array(
    			'id' 	=> $rowid,
    			'erreur'=> FALSE
    		));
    	}
    	else
    	{
    		echo json_encode(array(
    			'erreur'	=> TRUE,
    			'message'	=> lang('erreur_suppression_produit')
    		));
    	}
    }
    
	/**
	 * Vide le panier
	 */
    public function vider()
    {
    	$this->order->delete_discount();
    	$this->cart->destroy();
    }
    
    /**
     * Retourne le tableau des articles et le nombre d'article
     * dans le panier
     * 
     * @return	JSON
     */
    public function actualiser()
    {
    	echo json_encode(array(
    		'nb_article'=> $this->cart->total_items(),
    	    'contenu'	=> $this->load->view('panier/widget', array('panier' => $this->cart->contents(), 'client' => $this->_client), TRUE)
    	));
    }
    
    /**
     * Retourne le nombre d'article
     * dans le panier
     *
     * @return	JSON
     */
    public function get_nb_articles()
    {
        echo $this->cart->total_items();
    }
	
    /**
     * Enregistre le panier
     * 
     */
    public function enregistrer($client = NULL)
    {
    	if (!is_null($client))
    	{
    		if ($this->cart->total_items() > 0)
    		{
    			//Calcul de la tarification TVA
    			$montants = modules::run('boutique/calcul_commande');
    			
    			//Données de la commande
    			$panier = new Panier();
    			//$panier->id					= $this->order->item('id');
    			$panier->utilisateur_id		= $this->_client->id;
    			$panier->reference			= $this->_module->prefixe_reference.date('ymd').'-'.time();
    			$panier->sous_total			= $montants['sous_total'];
    			$panier->montant			= $montants['sous_total'];
    			$panier->acompte			= $montants['acompte'];
    			$panier->hors_taxe			= $montants['hors_taxe'];
    			$panier->commentaire 		= $this->order->item('commentaire');
    			$panier->save();
    			
    				
    			if ($panier->save())
    			{
    				//Etat de la commande
    		
    				//Supprime les produits déja commandés
    				//$panier->produits()->delete_all();
    		
    				//Produits commandés
    				foreach ($this->cart->contents() as $item)
    				{
    					$produit = new Produit();
    					$produit->get_by_id($item['id']);
    					$tarif_produit = $produit->tarif($this->_client, $item['qty']);
    					
    					$this->produits_panier->clear();
    					$this->produits_panier->enregistrer(array(
    							'panier_id' 	=> $panier->id,
    							'produit_id'	=> $item['id'],
    							'libelle'		=> clean_chaine($produit->libelle, ' '),
    							'reference'		=> $produit->reference,
    							'prix'			=> $item['price'],
    							'prix_sans_remise' => $tarif_produit['prix_sans_remise'],
    							'tva'			=> $tarif_produit['tva'],
    							'quantite'		=> $item['qty'],
    							'options'		=> json_encode($this->cart->product_options($item['rowid']))
    					));
    				}
    		
    				echo json_encode(array(
    					'erreur' => 'false'
    				));
    				return;
    			}
    		}
    		echo json_encode(array(
    			'erreur' => 'true'
    		));
    		return ;
    	}
    }
    
    
	/**
	 * Retourne le rowid d'un produit dans le panier
	 * 
	 * @param	integer	$produit_id
	 * @return	string OR FALSE
	 */
	public function rowid($produit_id)
	{
		foreach ($this->cart->contents() as $rowid => $produit)
		{
			if ($produit['id'] == $produit_id)
			{
				return $rowid;
			}
		}
		return FALSE;
	}
	
	/**
	 * Retourne le rowid d'un produit dans le panier
	 *
	 * @param	integer	$produit_id
	 * @return	string OR FALSE
	 */
	public function infos_rowid($produit_id)
	{
		$cart = $this->session->userdata('cart_contents');
		if (is_array($cart))
		{
			foreach ($cart as $rowid => $produit)
			{
				if ($produit['id'] == $produit_id)
				{
					return array(
							'rowid'	=> $rowid,
							'produit'	=> $produit
					);
				}
			}
		}
		
		return FALSE;
	}
	
	/**
	 * Vérifie l'existence du produit et l'ajoute au panier
	 *
	 * @param 	integer	$produit_id
	 * @param 	integer $quantite
	 * @param	array	$options
	 * @return	JSON
	 */
	private function _produit($produit_id, $quantite = 1, $options = array())
	{
	    //Si le produit est déjà dans le panier, on mets juste à jour sa quantité (et composé/imposé si existe)
	    $row_id_infos = $this->infos_rowid($produit_id);
	    $produit_cumulable = TRUE;
	    
	    if ($option_verif = ($row_id_infos['produit']['options']))
	    {
	        if (isset($option_verif['gravure_complementaire']) or (isset($option_verif['associe_gravure'])))
	        {
	            $produit_cumulable = FALSE;
	        }
	    }

		//Si le produit est déjà dans le panier, on mets juste à jour sa quantité (et composé/imposé si existe)
	    if (($row_id_infos !== FALSE) && $produit_cumulable)
		{
			
			//Si gestion stock, tester si il reste assez de stock pour ajout
			$produit = $this->produit->include_related('module_catalogue', array('gestion_stock'))->get_by_id($produit_id);
			
			if ($produit->exists())
			{
				//Stock
				if ($produit->module_catalogue_gestion_stock && ($produit->stock < ($row_id_infos['produit']['qty'] + $quantite)))
				{
					return array(
							'erreur'	=> TRUE,
							'message'	=> lang('erreur_stock_insuffisant').' ('.$produit->stock.' en stock)'
					);
				}
				
				$this->cart->update(array(
						'rowid'	=> $row_id_infos['rowid'],
						'qty'	=> $row_id_infos['produit']['qty'] + $quantite
				));
				
				$json = array(
						'erreur'	=> FALSE,
						'libelle'	=> $row_id_infos['produit']['name'],
						'tarif'		=> html_price($row_id_infos['produit']['price']),
						'total'		=> html_price(($row_id_infos['produit']['price'])*($row_id_infos['produit']['qty'] + $quantite)),
						'options'	=> $row_id_infos['produit']['options'],
						'id'		=> $row_id_infos['produit']['id'],
						'rowid'		=> $row_id_infos['rowid']
				);
			}
			
			
		}
		else
		{
			$produit = $this->produit->include_related('module_catalogue', array('gestion_stock'))->get_by_id($produit_id);
			
			if ($produit->exists())
			{
				//Stock
				if ($produit->module_catalogue_gestion_stock && ($produit->stock < $quantite))
				{
					return array(
						'erreur'	=> TRUE,
						'message'	=> lang('erreur_stock_insuffisant').' ('.$produit->stock.' en stock)'
					);
				}
				
				
				
				$client = $this->clients->est_connecte();
				$tarif 	= $produit->tarif($client, $quantite);
				
				if (is_array($tarif) && (( ! $tarif['unique'] && isset($tarif['grille'])) or $tarif['unique']))
				{
					//Prix TTC si saisie en HT
					if ( ! $tarif['taxe'])
					{
						$tarif['prix'] = $tarif['prix']*(1+$tarif['tva']/100);
					}
					
					$cart = array(
						'id' 		=> $produit_id,
						'qty'		=> $quantite,
						'price'		=> $tarif['prix'],
						'name'		=> $produit->libelle,
						'options'	=> $options,
					);
					
					//Produits composés comme les collesctions, pack
					/*Test si contient au moins un article composé, on calcule son prix en fonction des composants*/
					$produits_composes = $this->db->query("
    					SELECT t_produits.id
    					FROM t_produits
    					INNER JOIN t_produits_produits ON t_produits.id = t_produits_produits.related_produit_id
    					WHERE t_produits_produits.compose = 1
    					AND t_produits_produits.produit_id = ".$produit_id."
    				");
					
					if ($produits_composes->num_rows() != 0)
					{
					    $cart['options']['is_pack'] = TRUE;
					}
					
					if ($rowid = $this->cart->insert($cart))
					{
					    //Vérifie si existence de produits imposés, et si c'est le cas, ajout des produits imposés
					    $produits_imposes = new Produit();
					    $produits_imposes->select('related_produit_id')->where_related('produits_produit', 'produit_id', $produit_id)->where('impose', TRUE)->get();
					    foreach ($produits_imposes as $impose)
					    {
					        $imp = new Produit();
					        $imp->get_by_id($impose->related_produit_id);
					        $tarif_impose = $imp->tarif($client, 1);
					        
					        $options = array_merge((array)$options, array('produit_impose' => $rowid));
					        //Si le parent a un prix a 0, alors ne pas afficher les prodiuts imposés dans le panier (seulement facture)
					        if ($tarif['prix'] == 0)
					        {
					            $options = array_merge((array)$options, array('enfant_multiTvaKit' => $rowid));
					        }
					        
					        $tarif_impose['prix'] = $tarif_impose['prix']*(1+$tarif_impose['tva']/100);
					        
					        $this->cart->insert(array(
					            'id' 		=> $imp->id,
					            'qty'		=> $quantite,
					            'price'		=> $tarif_impose['prix'],
					            //'name'		=> str_replace(array('"', "'",'?','!','=','&', '  ','>','<','°'), array("","","",""," ",'et', ' ', "", "",'o'), supprimeAccents($imp->libelle)),
					            'name'		=> url_rewrite($imp->libelle),
					            'options'	=> $options,
					        ));
					    }
					    
					    if ($produits_composes->num_rows() != 0)
					    {
					        $tarif['prix']	= 0;
					        $cart['options']['is_pack'] = TRUE;
					        foreach ($produits_composes->result() as $row)
					        {
					            $prod_compo = new Produit();
					            $prod_compo->get_by_id($row->id);
					            
					            $tarif_compo 	 = $prod_compo->tarif($client, $quantite);
					            $tarif_compo['prix'] = $tarif_compo['prix']*(1+$tarif_compo['tva']/100);
					            
					            $this->cart->insert(array(
					                'id' 		=> $prod_compo->id,
					                'qty'		=> $quantite,
					                'price'		=> $tarif_compo['prix'],
					                //'name'		=> str_replace(array('"', "'",'?','!','=','&', '  ','>','<','°'), array("","","",""," ",'et', ' ', "", "",'o'), supprimeAccents($imp->libelle)),
					                'name'		=> $prod_compo->libelle,
					                'options'	=> array('pack_parent' => $rowid)
					            ));
					            
					            $tarif['prix']	+= $tarif_compo['prix'];
					        }
					    }
					    
						$json = array(
							'erreur'	=> FALSE,
							'libelle'	=> $produit->libelle,
							'tarif'		=> $tarif['prix'],
	    					'total'		=> html_price($tarif['prix']*$quantite),
							'options'	=> $cart['options'],
							'id'		=> $cart['id'],
							'rowid'		=> $rowid
						);
					}
					else
					{
						$json = array(
							'erreur'	=> TRUE,
							'message'	=> lang('erreur_ajout_panier')
						);
					}
				}
				else
				{
					$json = array(
						'erreur'	=> TRUE,
						'message'	=> lang('erreur_prix_produit')
					);
				}
			}
			else
			{
				$json = array(
					'erreur'	=> TRUE,
					'message'	=> lang('erreur_produit_inexistant')
				);
			}
		}
		return $json;
	}
	
	/**
	 * Mets à jour les options, au niveau de l'étape 1 du panier, pour mettre à jour les gravures
	 */
	public function maj_options($row_id)
	{
	    if ($this->input->post('texte') || $this->input->post('fichier'))
	    {
	        /*echo '<pre>';
	         var_dump($this->session->all_userdata());
	         echo '</pre>';
	         die();*/
	        $data 	= $this->session->all_userdata()['cart_contents'];
	        $this->session->unset_userdata('cart_contents');
	        
	        
	        $data[$row_id]['options']['gravure_complementaire'] = $this->input->post('texte');
	        $data[$row_id]['options']['associe_gravure'] = $this->input->post('fichier');
	        $this->session->set_userdata('cart_contents', $data);
	        $json = array(
	            'erreur'	=> 'false'
	        );
	        /*if ($this->session->set_userdata('cart_contents', $data))
	         {
	         $json = array(
	         'erreur'	=> 'false'
	         );
	         }
	         else
	         {
	         $json = array(
	         'erreur'	=> 'true',
	         'message'	=> 'Une erreur est survenue lors de la mise à jour du produit'
	         );
	         }*/
	    }
	    else
	    {
	        $json = array(
	            'erreur'	=> 'true',
	            'message'	=> 'Veuillez saisir un texte ou importer un fichier à votre gravure'
	        );
	    }
	    
	    echo json_encode($json);
	}
	
	/**
	 * Ajout au panier un panier enregistré
	 * @param string $panier_id
	 */
	public function finalise_commande($commande_id = NULL)
	{
	    if (!is_null($commande_id))
	    {
	        //Si le client est connecté, on peut utiliser sont statut pour le tarif (ex : professionnel)
	        $client = $this->_client;
	        
	        $commande = new Commande();
	        $produits_commande= $commande->include_related('produits_commande')->get_by_id($commande_id);
	        foreach ($produits_commande as $produit_commande)
	        {
	            $produit = $this->produit->include_related('module_catalogue', array('gestion_stock'))->get_by_reference($produit_commande->produits_commande_reference);
	            
	            
	            if ($produit->exists())
	            {
	                $quantite = $produit_commande->produits_commande_quantite;
	                //Stock
	                if ($produit->module_catalogue_gestion_stock && ($produit->stock < $quantite))
	                {
	                    $quantite = $produit->stock;
	                }
	                
	                $tarif 	= $produit->tarif($client, $quantite);
	                
	                if (is_array($tarif) && (( ! $tarif['unique'] && isset($tarif['grille'])) or $tarif['unique']))
	                {
	                    //Prix TTC si saisie en HT
	                    if ( ! $tarif['taxe'])
	                    {
	                        $tarif['prix'] = $tarif['prix']*(1+$tarif['tva']/100);
	                    }
	                    
	                    $cart = array(
	                        'id' 		=> $produit->id,
	                        'qty'		=> $quantite,
	                        'price'		=> $tarif['prix'],
	                        'name'		=> $produit->libelle,
	                        'options'	=> json_decode($produit_commande->produits_commande_options, TRUE),
	                    );
	                    
	                    if ($rowid = $this->cart->insert($cart))
	                    {
	                        $json = array(
	                            'erreur'	=> FALSE,
	                            'libelle'	=> $produit->libelle,
	                            'tarif'		=> $tarif['prix'],
	                            'options'	=> $cart['options'],
	                            'id'		=> $cart['id'],
	                            'rowid'		=> $rowid
	                        );
	                    }
	                    else
	                    {
	                        $json = array(
	                            'erreur'	=> TRUE,
	                            'message'	=> lang('erreur_ajout_panier')
	                        );
	                    }
	                }
	                else
	                {
	                    $json = array(
	                        'erreur'	=> TRUE,
	                        'message'	=> lang('erreur_prix_produit')
	                    );
	                }
	            }
	            else
	            {
	                $json = array(
	                    'erreur'	=> TRUE,
	                    'message'	=> lang('erreur_produit_inexistant')
	                );
	            }
	        }
	    }
	    $commande->changer_etat(5);
	    redirect('/boutique/etape,1.html');
	}
}

/* End of file panier.php */
/* Location: ./modules/boutique/controllers/panier.php */