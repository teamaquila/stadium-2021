<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

require_once CMSFULLPATH.'modules/utilisateurs/controllers/utilisateurs.php';
require_once APPPATH.'controllers/front.php';

/**
 * Contrôleur des actions clients
 * 
 * @author 		Pauline MARTIN
 * @package 	boutique
 * @category 	Controllers
 * @version 	2.0
 */
class Clients extends Utilisateurs
{
	/**
	 * Groupe d'utilisateur
	 * @var integer
	 */
	const GROUPE = 2;
	
	/**
	 * Type page
	 * @var integer
	 */
	const TYPE_COMPTE 	= 1;
	const TYPE_BOUTIQUE = 2;
	const TYPE_WIDGET 	= 3;
	
	/**
	 * Liste des redirections en fonction du type
	 * @var array
	 */
	private $_redirection;
	
	/**
	 * Options du module
	 * @var object Datamapper
	 */
	private $_module;
	
	/**
	 * Contrôleur du module front-end
	 * @var object
	 */
	private $_front;
	
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct(self::GROUPE);
		
		$this->load->models(array('module_boutique', 'client', 'pays', 
				'catalogue/commentaire', 'catalogue/produit', 'catalogue/produits_produit', 'adresses_utilisateur'));
		$this->load->library('form_validation');
		$this->lang->load('client');
		
		$this->_module 	= $this->module_boutique->get(1);
		$this->_front 	= new Front();
		
		$this->_redirection = array(
			1 => '/boutique/clients/commandes',
			2 => '/boutique/etape,3.html',
			3 => current_url()
		);
		
		$this->etats = array(
			1 => lang('etat_enregistre'),
			2 => lang('etat_payee'),
			3 => lang('etat_preparee'),
			4 => lang('etat_expediee'),
			5 => lang('etat_annulee'),
			6 => lang('etat_retournee'),
			7 => lang('etat_attente_paiement'),
		    8 => lang('paiement_compte'),
		);
	}
	
	/**
	 * Inscription d'un nouveau client
	 * 
	 * @see		Utilisateurs::inscrire()
	 * @param	integer	$type
	 * @return	VIEW
	 */
	public function inscrire($type = self::TYPE_COMPTE)
	{
		$erreur = NULL;
		
		if (($post = $this->input->post()) && $this->input->post('submit-inscription'))
		{
			//Identifiant
		    $post['email'] = trim($post['email']);
			$post['identifiant'] = trim($post['email']);
            // récupération info checkbox
			$post['newsletter']  = isset($post['newsletter']);
			
			//Mot de passe
			$mot_de_passe_clair   = (isset($post['mot_de_passe'])) ? trim($post['mot_de_passe']) : random_string();

			
				$regles = array(
					array(
						'field' => 'nom',
						'label' => 'Nom',
						'rules' => 'trim|required'
					),
					array(
						'field' => 'prenom',
						'label' => 'Prénom',
						'rules' => 'trim|required'
					),
					array(
						'field' => 'email',
						'label' => 'Email',
						'rules' => 'trim|required|valid_email'
					),
					array(
						'field' => 'adresse',
						'label' => 'Adresse',
						'rules' => 'trim|required'
					),
					array(
						'field' => 'ville',
						'label' => 'Ville',
						'rules' => 'trim|required'
					),
					array(
						'field' => 'code_postal',
						'label' => 'Code postal',
						'rules' => 'trim|required|alpha_numeric'
					),
					array(
						'field' => 'telephone',
						'label' => 'Téléphone',
					    'rules' => 'trim|required'
					),
					array(
						'field' => 'email',
						'label' => 'E-mail',
						'rules' => 'trim|required'
					),
					array(
						'field' => 'mot_de_passe',
						'label' => 'Mot de passe',
						'rules' => 'trim|required'
					),
					array(
						'field' => 'mot_de_passe_confirm',
						'label' => 'Confirmation du mot de passe',
						'rules'	=> 'required|matches[mot_de_passe]'
					),
					
				);
				$this->form_validation->set_rules($regles);
					
				if ($this->form_validation->run($this) !== FALSE)
				{
					$utilisateur = $this->client->get_by_email(trim($post['email']));
			
					if ( ! $utilisateur->exists())
					{
					    //On supprime les soucis de caractères HTML, posant problèmes pour Sage
					    $post['adresse'] = html_entity_decode($post['adresse'], ENT_QUOTES);
					    $post['adresse'] = str_replace('<br />', ' ', $post['adresse']);
					    $post['adresse'] = str_replace('<br/>', ' ', $post['adresse']);
					    $post['adresse'] = str_replace('<BR />', ' ', $post['adresse']);
					    $post['adresse'] = str_replace('<BR/>', ' ', $post['adresse']);
					    $post['nom'] = html_entity_decode($post['nom'], ENT_QUOTES);
					    $post['prenom'] = html_entity_decode($post['prenom'], ENT_QUOTES);
					    $post['adresse2'] = html_entity_decode($post['adresse2'], ENT_QUOTES);
					    $post['societe'] = html_entity_decode($post['societe'], ENT_QUOTES);
					    $post['ville'] = html_entity_decode($post['ville'], ENT_QUOTES);
					    
					    $email_minuscule =  $post['email'];
					    unset($post['email']);
					    
					    $mdp_minuscule = NULL;
					    if (isset($post['mot_de_passe'])){
					        $mdp_minuscule =  $post['mot_de_passe'];
					        unset($post['mot_de_passe']);
					    }
					    
					    $post = array_map('supprimeAccents', $post);
					    $post = array_map('strtoupper', $post);
					    $post = array_map('stripslashes', $post);
					    $post['email'] = $email_minuscule;
					    $post['identifiant'] = $email_minuscule;
					    if (!is_null($mdp_minuscule)){
					        $post['mot_de_passe'] = $mdp_minuscule;
					    }
					    
						if (($retour_inscri = $this->utilisateur->enregistrer($post, $type)))
						{
							if ($this->authentification($retour_inscri->identifiant, $retour_inscri->mot_de_passe))
							{
								modules::run('boutique/emails/inscription', $retour_inscri, $mot_de_passe_clair);
								unset($mot_de_passe_clair);
								//Redirection
								redirect($this->_redirection[$type]);
							}
							else
							{
								$erreur = lang('erreur_authentification');
							}
						}
					}
					else
					{
						//Si l'utilisateur existe déjà , vérifier si il est déjà relié au groupe client (1), et créer le lien si besoin
						$test_liaison = $this->db->get_where('tl_groupes_utilisateurs', array(
								'groupe_id' => 2, 
								'utilisateur_id' => $utilisateur->id
						));
						if ($test_liaison->num_rows() == 0)
						{
							$this->db->insert('tl_groupes_utilisateurs', array(
									'groupe_id' => 2,
									'utilisateur_id' => $utilisateur->id
							));
						}
						if($utilisateur->actif)
						{
						    // Client déjà inscrit à la newsletter message pour se désincrire afin de se créer le compte
						    $url_desinscription_newsletter = " : <a class='btn btn-primary' href=".base_url()."boutique/clients/nouveau_mdp > Mot de passe oublié</a> ";
						    $erreur = "Un compte existe déjà avec cet identifiant, renouvelez votre mot de passe ".$url_desinscription_newsletter;
						}
						else
						{
						    // Client déjà inscrit à la newsletter message pour se désincrire afin de se créer le compte
						    $url_desinscription_newsletter = " : <a class='btn btn-primary' href=".base_url()."newsletters/desabonnement > Se désinscrire</a> ";
						    $erreur = lang('erreur_identifiant_existant_creation_compte').$url_desinscription_newsletter;
						}

					}
					
				}
				else 
				{
					$erreur = validation_errors('<span class="ligne-erreur">', '</span>');
				}
			
			
		}
		
		$data = array(
			'module'	=> $this->_module,
    	 	'post'		=> $this->input->post(),
    	 	'erreur'	=> $erreur,
			'type'		=> $type
		);
		
    	if ($type == 2)
    	{
    		return $this->load->view('boutique/client/formulaires', $data, TRUE);
    	}
    	else
    	{
    		$this->_front->_afficher('clients/page', array(
    				'module' => $this->load->view('boutique/client/formulaires', $data, TRUE)
    		));
    	}
    	
		
		//return $this->load->view('boutique/client/formulaires', $data, TRUE);
	}
	
    /**
     * Connexion client
     * 
     * @param	integer	$type
     * @return	VIEW or JSON
     */
    public function connexion($type = self::TYPE_COMPTE)
    {
    	if  ($this->input->is_ajax_request())
    	{
    		if ($this->authentification($this->input->post('identifiant'), $this->input->post('mot_de_passe')))
	    	{
	    		echo json_encode(array(
	    			'connecte' 		=> TRUE,
	    			'redirection'	=> $this->_redirection[$type]
	    		));
	    	}
	    	else
	    	{
	    		echo json_encode(array(
	    			'connecte' 	=> FALSE,
	    			'erreurs'	=>lang('erreur_authentification')
	    		));
	    	}
    	}
    	else
    	{
			if ( ! $this->est_connecte())
			{
		    	$erreur = NULL;
				
		    	if (($post = $this->input->post()) && $this->input->post('submit-connexion'))
		    	{
			    	$this->form_validation->set_rules(array(
				    	array(
				            'field' => 'identifiant',
				            'label' => 'lang:identifiant',
				            'rules' => 'trim|required|valid_email'
				        ),
				        array(
				            'field' => 'mot_de_passe',
				            'label' => 'lang:mot_de_passe',
				            'rules' => 'trim|required'
				        )
					));
		    		
		    		if ($this->form_validation->run($this) !== FALSE)
		    		{
		    			if ($this->authentification($post['identifiant'], $post['mot_de_passe']))
		    			{
		    				//Redirection
		    				redirect($this->_redirection[$type]);
		    			}
		    			else
		    			{
		    				$erreur = lang('erreur_authentification');
						}
		    		}
		    		else
		    		{
		    			$erreur = validation_errors();
					}
		    	}
		    	
		    	$data = array(
		    		'post'	=> $this->input->post(),
		    		'erreur'=> $erreur
		    	);
		    	
		    	return $this->load->view('client/connexion', $data, TRUE);
			}
    	}
    	return FALSE;
    }
    
    /**
     * Gestion des adresses de facturation/livraison
     * 
     * @return	VIEW
     */
    public function adresses()
    {
    	if (($client = $this->est_connecte()) !== FALSE)
    	{
    		$erreur = NULL;
    		
    		if ($post = $this->input->post())
    		{
    			$this->_valider_adresses();
    			
    			if ($this->form_validation->run($this) !== FALSE)
    			{
    				//Si relais et que le mobile est renseigné
    				if ($this->input->post('mobile_relais'))
    				{
    					if (preg_match("#^0[6-7]([-. ]?[0-9]{2}){4}$#", $this->input->post('mobile_relais')))
						{
							$post['livraison']['mobile_relais'] = $this->input->post('mobile_transporteur');
							
							//Libellé des pays
							$post['facturation']['pays'] = $this->pays->libelle($post['facturation']['pays_id']);
							$post['livraison']['pays'] = $this->pays->libelle($post['livraison']['pays_id']);
							
							$post['facturation']['email'] = $client->email;
							$post['livraison']['email'] = $client->email;
							
							//On  vérifie qu'il n'y a pas d'antislash dans les adresses, auquel cas on supprime
							$post['facturation'] = array_map('stripslashes', $post['facturation']);
							$post['livraison'] = array_map('stripslashes', $post['livraison']);
							
							$this->order->insert_delivery($post['livraison']);
							$this->order->insert_biling($post['facturation']);
							
							redirect('/boutique/etape,4.html');
						}
						else
						{
							$erreur = lang('mobile_relais_obligatoire');
						}
    				}
    				else
    				{
    					//Libellé des pays
    					$post['facturation']['pays'] = $this->pays->libelle($post['facturation']['pays_id']);
    					$post['livraison']['pays'] = $this->pays->libelle($post['livraison']['pays_id']);
    					
    					$post['facturation']['email'] = $client->email;
    					$post['livraison']['email'] = $client->email;
    					
    					//On  vérifie qu'il n'y a pas d'antislash dans les adresses, auquel cas on supprime
    					$post['facturation'] = array_map('stripslashes', $post['facturation']);
    					$post['livraison'] = array_map('stripslashes', $post['livraison']);
    					
    					$this->order->insert_delivery($post['livraison']);
    					$this->order->insert_biling($post['facturation']);
    					
    					redirect('/boutique/etape,4.html');
    				}
    				
    				
    			}
    			else
    			{
    				$erreur = validation_errors();
    			}
    		}
    		
    		$facturation = (($this->order->biling()) ? $this->order->biling() : $client);
    		$livraison 	 = (($this->order->delivery()) ? $this->order->delivery() : $client->adresses(TRUE));
    		
    		$data = array(
				'module' 		=> $this->_module,
    			'client'		=> $client,
    			'erreur' 		=> $erreur,
    			'facturation' 	=> $facturation,
    			'livraison' 	=> $livraison,
    			'zone_livraison'=> array(
    				'facturation' 	=> $this->pays->liste(),
    				'livraison'		=> modules::run('boutique/transporteurs/liste_zone_livraison', isset_value($livraison, 'pays_id'), 3)
    			),
    			'transporteur'	=> modules::run('boutique/transporteurs/livraison', 3)
    		);
    		
    		return $this->load->view('boutique/client/adresses', $data, TRUE);
    	}
    	else
    	{
    		redirect('/inscription-client.html');
    	}
    }
	
	/**
     * Liste des commandes payées et traitées
     * 
     * @return	VIEW
     */
    public function commandes()
    {
    	if (($client = $this->est_connecte()) !== FALSE)
    	{
    		$data = array(
    			'commandes' => $client->commandes(),
    			'etats'		=> $this->etats,
    		    'utilisateur'    => $client,
    		    'client'    => $client,
			);
			
			$this->_front->_afficher('clients/page', array(
				'module' => $this->load->view('boutique/client/commandes', $data, TRUE)
			));
    	}
    	else
		{
			redirect('/inscription-client.html');
		}
    }
    
    
    /**
     * Liste des paniers enregistrés
     *
     * @return	VIEW
     */
    public function paniers()
    {
    	if (($client = $this->est_connecte()) !== FALSE)
    	{
    		$data = array(
    			'paniers' => $client->paniers(),
    		    'client'    => $client,
    		);
    			
    		$this->_front->_afficher('clients/page', array(
    				'module' => $this->load->view('boutique/client/paniers', $data, TRUE)
    		));
    	}
    	else
    	{
    		redirect('/inscription-client.html');
    	}
    }
    
    /**
     * Liste des commandes du client en attente (à finaliser)
     *
     * @return	VIEW
     */
    public function lister_commandes_attente()
    {
        if (($client = $this->est_connecte()) !== FALSE)
        {
            $commandes = new Commande();

            if (empty($client->code))
            {
                $liste_commande = $commandes->where('utilisateur_id', $client->id)->where('etat', 7)->order_by('created', 'DESC')->get();
            }
            else
            {
                $liste_commande = $commandes->where('code', $client->code)->where('etat', 7)->order_by('created', 'DESC')->get();
            }

            $data = array(
                'commandes' => $liste_commande,
                'client'    => $client,
                'etats'		=> $this->etats
            );
            
            $this->_front->_afficher('clients/page', array(
                'module' => $this->load->view('boutique/client/lister_commandes_attente', $data, TRUE)
            ));
        }
        else
        {
            redirect('/');
        }
    }
    
	/**
     * Fiche commande
     * 
     * @param	integer	$commande_id
     * @return	VIEW
     */
    public function commande($commande_id)
    {
    	if (($client = $this->est_connecte()) !== FALSE)
    	{
    		$commande = new Commande();
    		$commande->where_in('etat', array(2, 3, 4, 7, 8))->get_by_id($commande_id);
			
    		if ($commande->exists() && ($commande->utilisateur_id == $client->id))
    		{
    			$data = array(
    				'module'		=> $this->_module,
    				'commande' 		=> $commande,
    				'paiement' 		=> $commande->paiement(),
    				'facturation' 	=> $commande->facturation(),
    				'livraison' 	=> $commande->livraison(),
    				'produits'		=> $commande->produits(),
    				'transporteur' 	=> $commande->transporteur(),
    			    'utilisateur'    => $client,
    				'remise'		=> $commande->remise()
    			);
    			
				$this->_front->_afficher('clients/page', array(
					'module' => $this->load->view('boutique/client/commande', $data, TRUE)
				));
    		}
			else
			{
				redirect('/boutique/clients/commandes');
			}
    	}
    	else
		{
			redirect('/inscription-client.html');
		}
    }
	
	/**
	 * Compte client
	 * 
	 * @see		Utilisateurs::compte()
	 * @return	VIEW
	 */
	public function compte()
	{
		if (($client = $this->est_connecte()) !== FALSE)
		{
			$message = NULL;
			$erreur = FALSE;
			
			if ($post = $this->input->post())
			{
				$this->_valider_adresses();
				
				if ($this->form_validation->run($this) !== FALSE)
				{
					if ($this->utilisateur->enregistrer($post['facturation'], TRUE))
					{
						$this->adresses_utilisateur->enregistrer($post['livraison'], TRUE);
						
						$message = lang('success_modification_compte');
						$client = $this->est_connecte();
					}
				}
				else
				{
					$message = validation_errors();
					$erreur = TRUE;
				}
			}
			
			$data = array(
				'module'	=> $this->_module,
				'client' 	=> $client,
				'message' 	=> $message,
				'erreur'	=> $erreur
			);
			
			$this->_front->_afficher('clients/page', array(
				'module' => $this->load->view('boutique/client/compte', $data, TRUE)
			));
		}
		else
		{
			redirect('/inscription-client.html');
		}
	}

	
	/**
	 * Changement de l'adresse pour la livraison à l'étape 3, mise en session de l'id de l'adresse pour remplacer
	 */
	public function change_adresse_etape3()
	{
		$this->session->set_userdata('livraison_choisie', $this->input->post('adresse_id'));
	}
	
	
	/**
	 * Page permettant l'inscription et la connexion
	 * */
	public function formulaires()
	{
		$erreur = NULL;
		$data = array(
			'module'	=> $this->_module,
    	 	'post'		=> $this->input->post(),
    	 	'erreur'	=> $erreur,
		);
		$this->_front->_afficher('clients/page', array(
				'module' => $this->load->view('boutique/client/formulaires', $data, TRUE)
		));
	}
	
	
	/**
	 * Déconnecter le compte client
	 * 
	 * @see 	Utilisateurs::deconnecter()
	 */
	public function deconnecter()
	{
		delete_cookie('utilisateur');
		
		//Efface les sessions
		modules::run('boutique/paniers/vider');
		redirect('/');
	}
	
	/**
	 * Récupérer les données client à partir du cookie
	 * 
	 * @see 	Utilisateurs::est_connecte()
	 * @return	mixed
	 */
	public function est_connecte()
	{
		if ($cookie = get_cookie('utilisateur'))
		{
			$data = json_decode($this->encrypt->decode($cookie));
			
			$compte_utilisateur = $this->client->verifier(array(
				'identifiant'	=> $data->identifiant,
				//'mot_de_passe'	=> $data->mot_de_passe,
				'groupe'		=> $data->groupe
			));
			
			if (($compte_utilisateur !== FALSE) && $compte_utilisateur->exists() && (self::GROUPE == $data->groupe))
			{
				return $compte_utilisateur;
			}
			else
			{
				delete_cookie('utilisateur');
			}
		}
		return FALSE;
	}
	
	/**
	 * Supprimer le compte client
	 * 
	 * @see Utilisateurs::supprimer()
	 */
	public function supprimer()
	{
		if (($client = $this->est_connecte()) !== FALSE)
		{
			$message = NULL;
			
			if ($post = $this->input->post())
			{
				if ($post['confirmation'] == TRUE)
				{
					$client->delete();
					redirect('/');
				}
				else
				{
					$message = lang();
				}
			}
			
			$data = array(
				'post'		=> $this->input->post(),
				'message'	=> $message
			);
			
			$this->_front->_afficher('clients/page', array(
				'module' => $this->load->view('boutique/client/supprimer', $data, TRUE)
			));
		}
		else
		{
			redirect('/');
		}
	}
	
	/**
	 * Générer un nouveau mot de passe
	 *
	 * @return	VIEW
	 */
	public function nouveau_mdp()
	{
		$message = NULL;
		
		if ($post = $this->input->post())
		{
			$this->form_validation->set_rules(array(
				array(
					'field' => 'identifiant',
					'label' => 'lang:identifiant',
					'rules' => 'trim|required|valid_email'
				)
			));
			
			if ($this->form_validation->run($this) !== FALSE)
			{
				if ($this->utilisateur->existant($post['identifiant']))
				{
					
					$nouveau_mdp = random_string('alnum', 8);
					$this->utilisateur->mot_de_passe = password_hash($nouveau_mdp, PASSWORD_BCRYPT, array("cost" => 10));
						
						
					if ($this->utilisateur->save())
					{
						$this->utilisateur->mot_de_passe = $nouveau_mdp;
						modules::run('boutique/emails/nouveau_mdp', $this->utilisateur);
	
						unset($nouveau_mdp);
	
						$message = lang('success_envoi_nouveau_mot_passe');
					}
				}
				else
				{
					$message = lang('error_authentification');
				}
			}
			else
			{
				$message = validation_errors() ;
			}
		}
	
		$data = array(
			'message' => $message
		);
	
		$this->_front->_afficher('clients/page', array(
			'module' => $this->load->view('boutique/client/nouveau_mdp', $data, TRUE)
		));
	}
	
	/**
	 * Formulaire pour changer ou ajouter une adresse de livraion dans le compte client
	 * */
	public function genere_form_livraison($enregistre = FALSE, $livraison_id = NULL)
	{

		$this->load->model('adresses_utilisateur');
		$livraison = NULL;
		if(!is_null($livraison_id))
		{
			$livraison = $this->adresses_utilisateur->get_by_id($livraison_id);
		}
		if($enregistre)
		{
			//Enregistrement de la nouvelle adresse
			$client_adresse = $this->adresses_utilisateur->enregistrer($this->input->post());
				
			//Si adresse par défaut, mettre toutes les autres adresses a 0.
			if ($this->input->post('defaut'))
			{
				$adresses = new Adresses_utilisateur();
				$adresses->where('id !=',$client_adresse->id)->where('utilisateur_id',$this->input->post('utilisateur_id'))->update('defaut',FALSE);
			}
	
			redirect('/boutique/clients/compte');
		}
		$data = array(
				'adresse'		=> $this->input->post(),
				'client'		=> $this->est_connecte(),
				'adresse' 		=> $livraison,
				'livraison_id'	=> $livraison_id
		);
		echo $this->load->view('boutique/client/form_livraison',$data, TRUE);
		return;
	}
	
	/**
	 * Formulaire pour supprimer une adresse de livraion dans le compte client
	 * */
	public function suppr_form_livraison()
	{
		$this->load->model('adresses_utilisateur');
		$livraison = NULL;
		if(($this->input->post('livraison_id')))
		{
			$livraison = $this->adresses_utilisateur->get_by_id($this->input->post('livraison_id'));
			$livraison->delete();

			redirect('/boutique/clients/compte');
			return;
		}
		
	}
	
	
	/**
	 * Liste des pays par zone de livraison
	 *
	 * @param 	integer $pays_id
	 * @param	string	$name_select
	 * @return	VIEW
	 */
	public function zone_livraison($pays_id, $name_select = '')
	{
		$this->load->model('boutique/zone_livraison');
	
		$zones = $this->zone_livraison->disponibles();
		 
		if ($zones->exists())
		{
			//Multi-zones
			if ($zones->result_count() > 1)
			{
				$zone_livraison = array();
	
				foreach ($zones as $zone)
				{
					$zone_livraison[$zone->libelle] = array();
	
					//Pays associé à la zone
					$pays = array();
						
					foreach ($this->pays->par_zone($zone->id) as $item)
					{
						$pays[$item->id] = $item->nom;
					}
					$zone_livraison[$zone->libelle] = $pays;
				}
			}
			//Une zone
			else
			{
				//Pays associé à la zone
				$pays = array();
				$liste_pays = $this->pays->par_zone($zones->id);
			  
				//plusieurs pays
				if ($liste_pays->result_count() > 1)
				{
					foreach ($liste_pays as $item)
					{
						$pays[$item->id] = $item->nom;
					}
				}
				//un pays
				else
				{
					$pays[$liste_pays->id] = $liste_pays->nom;
				}
				$zone_livraison[0] = $pays;
			}
			$data = array(
					'zones_livraison'	=> $zone_livraison,
					'selectionne'	 	=> $pays_id,
					'name_select'		=> $name_select
			);
			return $this->load->view('boutique/client/zones_livraison',$data, TRUE);
		}
		return FALSE;
	}
	
	
	/**
	 * Formulaire de modification de son mot de passe
	 */
	public function formulairemotdepasse()
	{
		$message = NULL;
		$erreur  = NULL; 
		$client = $this->est_connecte();
		if ($post = $this->input->post())
		{
			$this->form_validation->set_rules(array(
					array(
							'field' => 'ancien_mdp',
							'label' => 'lang:ancien_mdp',
							'rules' => 'trim|required'
					),
					array(
							'field' => 'nouveau_mdp',
							'label' => 'lang:nouveau_mdp',
							'rules' => 'trim|required'
					),
					array(
							'field' => 'verif_nouveau_mdp',
							'label' => 'lang:verif_nouveau_mdp',
							'rules' => 'trim|required'
					),
			));
				
			if ($this->form_validation->run($this) !== FALSE)
			{
				if ($this->input->post('nouveau_mdp') == $this->input->post('verif_nouveau_mdp'))
				{
					if ($client = $this->est_connecte())
					{
						if (password_verify($this->input->post('ancien_mdp'), $client->mot_de_passe))
						{
							$nouveau_mdp = $this->input->post('verif_nouveau_mdp');
							$client->mot_de_passe = password_hash($nouveau_mdp, PASSWORD_BCRYPT, array("cost" => 10));
								
								
							if ($client->save())
							{
								$client->mot_de_passe = $nouveau_mdp;
								modules::run('boutique/emails/nouveau_mdp', $client);
									
								unset($nouveau_mdp);
									
								$message = lang('success_envoi_nouveau_mot_passe');
								$erreur  = FALSE; 
							}
						}
						else
						{
							$message = lang('error_ancien_mdp_incorrect');
							$erreur  = TRUE; 
						}
					}
					else
					{
						$message = lang('error_authentification');
						$erreur  = TRUE; 
					}
				}
				else
				{
					$message = lang('error_correspondance_mdp');
					$erreur  = TRUE; 
				}
			}
			else
			{
				$message = validation_errors() ;
				$erreur  = TRUE; 
			}
		}
		
		$data = array(
			'message' 	=> $message,
		    'client'=> $client,
			'erreur'	=> $erreur
		);
		
		$this->_front->_afficher('clients/page', array(
				'module' => $this->load->view('boutique/client/changer_motdepasse', $data, TRUE)
		));
	}
	
	/**
	 * Règles de validation pour le formulaire des adresses
	 * 
	 * @return 	void
	 */
	private function _valider_adresses()
	{
		$regles = array(
	    	array(
	            'field' => 'facturation[nom]',
	            'label' => 'lang:nom',
	            'rules' => 'trim|required'
	        ),
	        array(
	            'field' => 'facturation[prenom]',
	            'label' => 'lang:prenom',
	            'rules' => 'trim|required'
	        ),
	        array(
	            'field' => 'facturation[adresse]',
	            'label' => 'lang:adresse',
	            'rules' => 'trim|required'
	        ),
	        array(
	            'field' => 'facturation[ville]',
	            'label' => 'lang:ville',
	            'rules' => 'trim|required'
	        ),
	        array(
	            'field' => 'facturation[code_postal]',
	            'label' => 'lang:code_postal',
	            'rules' => 'trim|required|alpha_numeric'
	        ),
	        array(
	            'field' => 'facturation[telephone]',
	            'label' => 'lang:telephone',
// 	            'rules' => 'trim|required|numeric|regex_match[/^0[1-68]([-. ]?[0-9]{2}){4}$/]'
	            'rules' => 'trim|required'
	        ),
		);
		
		//Champs pour la livraison
		if ($this->_module->option_livraison)
		{
			array_push($regles, array(
	            'field' => 'livraison[nom]',
	            'label' => 'Nom (livraison)',
	            'rules' => 'trim|required'
	        ));
			array_push($regles, array(
	            'field' => 'livraison[prenom]',
	            'label' => 'Prénom (livraison)',
	            'rules' => 'trim|required'
	        ));
			array_push($regles, array(
	           	'field' => 'livraison[adresse]',
				'label' => 'Adresse (livraison)',
				'rules' => 'trim|required'
	        ));
			array_push($regles, array(
	            'field' => 'livraison[ville]',
	            'label' => 'Ville (livraison)',
	            'rules' => 'trim|required'
	        ));
			array_push($regles, array(
	           	'field' => 'livraison[code_postal]',
	            'label' => 'Code postal (livraison)',
	            'rules' => 'trim|required|alpha_numeric'
	        ));
			array_push($regles, array(
					'field' => 'livraison[telephone]',
					'label' => 'Téléphone (livraison)',
			    'rules' => 'trim|required'
			));
		}
		$this->form_validation->set_rules($regles);
	}
	
	public function exporter($client_id = 0, $modifie = TRUE, $ref_commande = NULL)
	{
	    
	    //Paramétrage temps et mémoire allouée au script
	    set_time_limit(1500);
	    ini_set('memory_limit', '1024M');
	    
	    $this->load->helper('csv');
	    $this->load->helper('file');
	    $tab_clients = array();
	    
	    $client_unique = FALSE;
	    if(($client_id != 0))
	    {
	        $this->client->where('id',$client_id);
	        $client_unique = $client_id;
	    }
	    
	    
	    $clients = $this->client->get();
	    
	    foreach ($clients as $client)
	    {
	        
	        $this->pays->clear();
	        $pays = $this->pays->get_by_id($client->pays_id)->extension;
	        
	        $type_client = ($client->pro == 1) ? 'Y' : NULL;
	        $societe = (!empty($client->societe)) ? ($client->societe) : ($client->prenom.' '.$client->nom);
	        $tab_client[] = array(
	            $client_id,$client->code, $societe, preg_replace("#(?<!\r)\n#", '', str_replace("'", "\'", $client->adresse)), preg_replace("#(?<!\r)\n#", '', str_replace("'", "\'", $client->adresse2)), $client->code_postal, str_replace("'", "\'", $client->ville), $pays,
	            $client->telephone, $client->fax, $client->email, $type_client,
	            $ref_commande
	        );
	        
	        $client_adresses = new Adresses_utilisateur();
	        $client_adresses->get_by_utilisateur_id($client->id);
	        
	        foreach ($client_adresses as $adresse_client)
	        {
	            $this->pays->clear();
	            $pays = $this->pays->get_by_id($adresse_client->pays_id)->extension;
	            $tab_contact[] = array(
	                $client_id,$client->code,$adresse_client->nom,$adresse_client->prenom,preg_replace("#(?<!\r)\n#", '', str_replace("'", "\'", $adresse_client->adresse)),preg_replace("#(?<!\r)\n#", '', str_replace("'", "\'", $adresse_client->adresse2)),$adresse_client->code_postal,
	                str_replace("'", "\'", $adresse_client->ville),$pays, $adresse_client->telephone,$adresse_client->mobile,
	                $adresse_client->fax,
	                $adresse_client->email,$adresse_client->defaut, $adresse_client->id_sage,
	                $ref_commande, $client->id_sage
	            );
	            
	        }
	        
	        
	        $client->modifie = FALSE;
	        $client->save();
	    }
	    
	    $csv_client = array_to_csv($tab_client, "");
	    $csv_contact = array_to_csv($tab_contact, "");
	    if ($client_unique && $ref_commande)
	    {
	        write_file( GENERAL_ROOTPATH.'/echanges/export/clients/'.$ref_commande.'-'.$client_unique.'-client.csv', $csv_client, 'w+');
	        write_file( GENERAL_ROOTPATH.'/echanges/export/clients/'.$ref_commande.'-'.$client_unique.'-contact.csv', $csv_contact, 'w+');
	        write_file( GENERAL_ROOTPATH.'/echanges/sauvegarde_export/clients/'.$ref_commande.'-'.$client_unique.'-client.csv', $csv_client, 'w+');
	        write_file( GENERAL_ROOTPATH.'/echanges/sauvegarde_export/clients/'.$ref_commande.'-'.$client_unique.'-contact.csv', $csv_contact, 'w+');
	    }
	    else
	    {
	        write_file( GENERAL_ROOTPATH.'/echanges/export/clients/'.mdate('%Y-%m-%d').'-clients.csv', $csv, 'w+');
	        write_file( GENERAL_ROOTPATH.'/echanges/sauvegarde_export/clients/'.mdate('%Y-%m-%d').'-clients.csv', $csv, 'w+');
	    }
	    
	}
}

/* End of file clients.php */
/* Location: ./modules/boutique/controllers/clients.php */