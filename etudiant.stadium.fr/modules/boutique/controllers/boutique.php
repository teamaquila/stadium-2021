<?php
require_once APPPATH.'controllers/front.php';

/**
 * Boutique
 * Contrôleur permettant les actions sur la boutique
 * 
 * @author 		Pauline MARTIN
 * @package 	boutique
 * @category 	Controllers
 * @version 	1.1
 */
class Boutique extends Front
{
	const PAYS_ID_FRANCE = 66;
	
	/**
	 * Options du module
	 * 
	 * @var object
	 */
	private $_module;
	
	/**
	 * Taux de TVA pour livraison
	 * 
	 * @var decimal
	 */
	private $_tva_livraison;
	
	/**
	 * Numéro de l'étape en cours
	 * 
	 * @var integer
	 */
	private $_etape_id;
	
	/**
	 * Client
	 * 
	 * @var object
	 */
	private $_client;
	
	/**
	 * Liste des étapes
	 * 
	 * @var array
	 */
	private $_liste_etapes;
	
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('module_boutique');
		$this->load->libraries(array('cart', 'order'));
		$this->load->modules(array(
			'boutique/clients',
			'boutique/commandes',
			'boutique/remises',
			'boutique/paniers',
			'boutique/transporteurs'
		));
		
		$this->_module = $this->module_boutique->get(1);
		$this->_client = $this->clients->est_connecte();
		$this->_tva_livraison = 1 + ($this->_module->tva_livraison/100);
		$this->_liste_etapes = array(
			1 => lang('panier'),
			2 => lang('authentification'),
			3 => lang('coordonnees'),
			4 => lang('recapitulatif'),
			5 => lang('validation')
		);
		
		//Etapes
		$this->_etape_id = 0;
		if( $this->uri->rsegment(3) && ($this->uri->rsegment(1) == 'boutique'))
		{
			$this->_etape_id = $this->uri->rsegment(3);
		}
		
		$this->_securite();
	}

	/**
	 * Appel de l'étape
	 * Si n'existe pas, on redirige vers une 404
	 */
	public function index()
	{
		if ($this->_etape_id)
		{
			if (method_exists('Boutique', '_etape'.$this->_etape_id))
			{
				eval('$this->_etape'.$this->_etape_id.'();');
				return TRUE;
			}
		}
		show_404();
	}
	
	/**
     * Détail des montants de la commande
	 * - Sous-total produits
	 * - Remise
	 * - Frais de port
	 * - Acompte
     * 
     * @return	array
     */
    public function calcul_commande()
    {
    	$montants = array(
			'hors_taxe' => $this->_paiement_hors_taxe(),
			'sous_total' => 0,
			'sous_total_remise' => 0,
			'frais_port' => 0,
			'acompte' => 0,
			'total' => 0
		);
		
    	//Sous-total du panier
    	$total = $total = $this->remises->sous_total_remise();
		$montants['sous_total'] = $total;
		
		//Sous-total après remise
    	/*if ($this->_module->option_remise)
    	{
			$total = $this->remises->sous_total_remise();
			$montants['sous_total_remise'] = $total;
		}*/

		//Frais de port
		if ($this->_module->option_livraison)
    	{
    		if ($livraison = $this->order->transporter())
			{
	    		$frais_port = $this->transporteurs->frais_port($livraison['pays_id'], $livraison['transporteur_id']);
			}
			else
			{
			    $transporteur_def = new Transporteur();
			    $transporteur_def->defaut();
			    
			    if($transporteur_def->exists())
			    {
			        $frais_port = $this->transporteurs->frais_port(self::PAYS_ID_FRANCE, $transporteur_def->id);
			    }
			    else
			    {
			        $frais_port = $this->transporteurs->frais_port(self::PAYS_ID_FRANCE, 1);
			        
			    }
			}
			
			if ($frais_port !== FALSE)
			{
	    		$total = $total+$frais_port->tarif;
				$montants['frais_port'] = $frais_port->tarif;
			}
    	}

		//Total de la commande avant acompte
		$montants['total'] = $total;
		
		//Acompte
		if ($this->_module->option_acompte)
    	{
	    	$total = $total*($this->_module->taux_acompte/100);
			$montants['acompte'] = $total;
    	}

	    return $montants;
    }
    
    /**
     * Créer un bouton avec le formulaire
     * pour ajouter le produit au panier
     * 
     * @param 	object	$produit
     * @param 	array 	$options
     * @return	VIEW
     */
    public function bouton_ajouter($produit, $options = NULL, $formulaire = FALSE)
    {
    	$module = $produit->module_catalogue->get();
    	
    	if ($module->boutique)
    	{
    		$data = array(
	    		'module'	=> $module,
	    		'client'	=> $this->_client,
	    		'produit'	=> $produit,
    		    'formulaire_produit'	=> $formulaire,
	    		'options'	=> $options
	    	);
    		
    		if (($module->gestion_stock && ($produit->stock > 0)) or ! $module->gestion_stock)
    		{
		    	return $this->load->view('boutique/boutique/bouton_ajouter', $data, TRUE);
    		}
    		return $this->load->view('boutique/boutique/produit_indisponible', $data, TRUE);
		}
    }
    
    /**
     * Créer un bouton avec le formulaire
     * pour ajouter le produit au panier
     *
     * @param 	object	$produit
     * @param 	array 	$options
     * @return	VIEW
     */
    public function bouton_ajouter_opt($produit, $options = NULL)
    {
        $module = $produit->module_catalogue->get();
        
        if ($module->boutique)
        {
            $data = array(
                'module'	=> $module,
                'client'	=> $this->_client,
                'produit'	=> $produit,
                'options'	=> $options
            );
            
            if (($module->gestion_stock && ($produit->stock > 0)) or ! $module->gestion_stock)
            {
                return $this->load->view('boutique/boutique/bouton_ajouter_opt', $data, TRUE);
            }
            return $this->load->view('boutique/boutique/produit_indisponible', $data, TRUE);
        }
    }

    /**
     * Créer un bouton avec le formulaire
     * pour ajouter le produit au panier => pour une seule référence
     *
     * @param 	object	$produit
     * @param 	array 	$options
     * @return	VIEW
     */
    public function bouton_ajouter_enfant($produit, $enfant, $options = NULL)
    {
        $module = $produit->module_catalogue->get();
        
        if ($module->boutique)
        {
            $data = array(
                'module'	=> $module,
                'client'	=> $this->_client,
                'produit'	=> $enfant,
                'options'	=> $options
            );
            
            if (($module->gestion_stock && ($enfant->stock > 0)) or ! $module->gestion_stock)
            {
                return $this->load->view('boutique/boutique/bouton_ajouter_enfant', $data, TRUE);
            }
            return $this->load->view('boutique/boutique/produit_indisponible', $data, TRUE);
        }
    }
    
    
    /**
     * Créer un bouton avec le formulaire catalogue en mode liste
     * pour ajouter le produit au panier
     *
     * @param 	object	$produit
     * @param 	array 	$options
     * @return	VIEW
     */
    public function bouton_ajouter_liste($produit, $options = NULL)
    {
        $module = $produit->module_catalogue->get();
        
        if ($module->boutique)
        {
            $data = array(
                'module'	=> $module,
                'client'	=> $this->_client,
                'produit'	=> $produit,
                'options'	=> $options
            );
            
            if (($module->gestion_stock && ($produit->stock > 0)) or ! $module->gestion_stock)
            {
                return $this->load->view('boutique/boutique/bouton_ajouter_liste', $data, TRUE);
            }
            return $this->load->view('boutique/boutique/produit_indisponible', $data, TRUE);
        }
    }
    
    /**
     * Créer un bouton avec le formulaire
     * pour ajouter le produit DISPO au panier
     *
     * @param 	object	$produit
     * @param 	array 	$options
     * @return	VIEW
     */
    public function bouton_ajouter_dispo($produit, $options = NULL, $type = 'fiche')
    {
        /*CHOISIR MODULE DISPO*/
        $module = new Module_disponible();
        $module = $module->get_by_id($produit->module_disponible_id);
        
        if ($module->boutique)
        {
            $data = array(
                'module'	=> $module,
                'client'	=> $this->_client,
                'produit'	=> $produit,
                'options'	=> $options,
                'type'      => $type
            );
            
            if (($module->gestion_stock && ($produit->stock > 0)) or ! $module->gestion_stock)
            {
                return $this->load->view('boutique/boutique/bouton_ajouter_dispo', $data, TRUE);
            }
            return $this->load->view('boutique/boutique/produit_indisponible_dispo', $data, TRUE);
        }
    }
    
	/**
	 * AJAX
	 * Met à jour l'affichage et les sessions du panier à l'étape 1
	 * 
	 * @return	JSON
	 */
	public function recalculer_panier()
	{
		if ($this->input->is_ajax_request() && ($quantite = $this->input->post('quantite')))
		{
			//Enregistrement des nouvelles quantitées
			$this->paniers->modifier($quantite);
			
			//Cette fonction vide le tampon de sortie sans l'envoyer au navigateur, 
			//pour ne pas avoir le json encode des fonctions précédentes
			ob_clean();
			
			
			$data = array(
	    		'panier' 		=> $this->paniers->afficher(1),
	    		'montants'		=> $this->calcul_commande(),
				'commentaire' 	=> $this->order->item('commentaire'),
				'date_livraison'=> $this->order->item('date_livraison'),
	    		'livraison'		=> FALSE,
	    		'remise'		=> FALSE,
	    		'acompte'		=> FALSE,
    			'client'		=> $this->_client
	    	);
			
			//Remise
			if ($this->_module->option_remise)
			{
				$data['remise'] = $this->remises->afficher();
			}
			
			//Livraisons
	    	if ($this->_module->option_livraison)
	    	{
	    		$data['livraison'] = $this->transporteurs->livraison();
	    	}
			
			//Acompte
			if ($this->_module->option_acompte)
			{
				$data['acompte'] = $this->_acompte();
			}
			
    		exit(json_encode(array(
				'vue' => $this->load->view('boutique/boutique/etape1_ajax', $data, TRUE)
			)));
		}
	}
	
	
	/**
	 * AJAX
	 * Enregistrement du panier à l'étape 1 si connecté, sinon redirige vers formulaire insciption/connexion
	 * 
	 * @return	JSON
	 * */
	
	public function enregistrer_panier()
	{
		if ($this->_client)
    	{
    		/*Mise à jour du panier avant enregistrement */
    		if ($this->input->post())
    		{
    			foreach ($this->input->post('quantite') as $rowid => $quantite)
    			{
    				$this->cart->update(array(
    						'rowid' => $rowid,
    						'qty'	=> $quantite
    				));
    			}	
    		}
    		
    		
    		echo $this->paniers->enregistrer($this->_client);
    		return;
    		
    	}
    	else
    	{
    		echo json_encode(array(
    				'erreur' => 'true'
    		));
    		return ;
    	}
	}
	
	/**
	 * Retourne le prix en fonction de la quantité
	 *
	 * @return	boolean
	 */
	public function calculer_prix_produit()
	{
		if ($this->input->is_ajax_request())
		{
	
			$client = $this->_client;
			$quantite = 1;
	
			if ($this->input->post('quantite') && is_numeric($this->input->post('quantite')) && $this->input->post('produit_id'))
			{
				$prix_unitaire 	= 0;
				$produits_id	= $this->input->post('produit_id');
	
				if (is_array($produits_id))
				{
					foreach ($produits_id as $produit_id)
					{
						$this->produit->clear();
						$this->produit->get_by_id($produit_id);
						$tarif  = $this->produit->tarif($this->input->post('quantite'), $client);
						$prix_unitaire += $tarif['prix'];
					}
				}
				else
				{
					$this->produit->clear();
					$this->produit->get_by_id($produits_id);
					$tarif = $this->produit->tarif($client, $this->input->post('quantite'));
					$prix_unitaire += $tarif['prix'];
				}
	
				if ($prix_unitaire > 0)
				{
					$prix_total = $prix_unitaire * $this->input->post('quantite');
	
					$json = array(
							'erreur'=> FALSE,
							'unite'	=> $this->load->view('boutique/boutique/tarifs', array('tarifs' => $tarif), TRUE),
							'total'	=> html_price($prix_total).' '.lang('prix_taxe_'.$tarif['tva'])
					);
				}
				/*else
				{
					$json = array(
							'erreur'	=> TRUE,
							'message'	=> lang('err_prix_produit'),
							'unite'		=> '',
							'total'		=> ''
					);
				}*/
			}
			else
			{
				$json = array(
						'erreur'	=> TRUE,
						'message'	=> lang('err_produit_inexistant')
				);
			}
			echo json_encode($json);
		}
		else
		{
			redirect('/');
		}
	}
	
	/**
	 * Retourne le prix en fonction de la quantité sur le disponible
	 *
	 * @return	boolean
	 */
	public function calculer_prix_produit_dispo()
	{
	    if ($this->input->is_ajax_request())
	    {
	        
	        $client = $this->_client;
	        $quantite = 1;
	        
	        if ($this->input->post('quantite') && is_numeric($this->input->post('quantite')) && $this->input->post('produit_id'))
	        {
	            $prix_unitaire 	= 0;
	            $produits_id	= $this->input->post('produit_id');
	            
	            if (is_array($produits_id))
	            {
	                foreach ($produits_id as $produit_id)
	                {
	                    $this->produit->clear();
	                    $this->produit->get_by_id($produit_id);
	                    $tarif  = $this->produit->tarif($this->input->post('quantite'), $client);
	                    $prix_unitaire += $tarif['prix'];
	                }
	            }
	            else
	            {
	                $this->produit->clear();
	                $this->produit->get_by_id($produits_id);
	                $tarif = $this->produit->tarif($client, $this->input->post('quantite'));
	                $prix_unitaire += $tarif['prix'];
	            }
	            
	            if ($prix_unitaire > 0)
	            {
	                $prix_total = $prix_unitaire * $this->input->post('quantite');
	                
	                $json = array(
	                    'erreur'=> FALSE,
	                    'unite'	=> $this->load->view('boutique/boutique/tarifs', array('tarifs' => $tarif), TRUE),
	                    'total'	=> html_price($prix_total).' '.lang('prix_taxe_'.$tarif['tva']),
	                    'prix_unitaire'    => html_price($prix_unitaire),
	                    'prix_conseille'    => html_price($prix_unitaire*2),
	                );
	            }
	            else
	            {
	                $json = array(
	                    'erreur'	=> TRUE,
	                    'message'	=> lang('err_produit_inexistant'),
	                    'unite'		=> '',
	                    'total'		=> ''
	                );
	            }
	        }
	        else
	        {
	            
	            if(!$this->input->post('quantite') == FALSE)
	            {
	                $json = array(
	                    'erreur'	=> TRUE,
	                    'message'	=> lang('err_produit_inexistant')
	                );
	            }
	            else
	            {
	                $json = array(
	                    'erreur'	=> FALSE
	                );
	            }
	            
	        }
	        echo json_encode($json);
	    }
	    else
	    {
	        redirect('/');
	    }
	}
	
	/**
     * ::Etape 1:: Panier
     * 
     * Liste du panier
     * Choix du transporteur
     */
    private function _etape1()
    {
    	$data = array(
    		'guide_etapes' 	=> $this->_guide_etapes(),
    		'panier' 		=> $this->paniers->afficher(1),
    		'montants'		=> $this->calcul_commande(),
    		'commentaire' 	=> $this->order->item('commentaire'),
    		'date_livraison'=> $this->order->item('date_livraison'),
    		'livraison'		=> FALSE,
    		'remise'		=> FALSE,
    		'acompte'		=> FALSE,
    		'client'		=> $this->_client
    	);
		
		//Remise
		if ($this->_module->option_remise)
		{
			$data['remise'] = $this->remises->afficher();
		}
		
		//Livraison
    	if ($this->_module->option_livraison)
    	{
    		$data['livraison'] = $this->transporteurs->livraison();
    	}
		
		//Acompte
		if ($this->_module->option_acompte)
		{
			$data['acompte'] = $this->_acompte();
		}
    	
    	//Validation de l'étape
   		if ($this->input->post('submit-etape1'))
    	{
    		$this->paniers->modifier($this->input->post('quantite'));
    		$this->order->update(array(
    		    'commentaire' => $this->input->post('commentaire'),
    		    'date_livraison' => $this->input->post('date_livraison'),
    		));
			
	    	redirect('/boutique/etape,2.html');
    	}
		
		//Affichage par défaut
    	else
    	{
			parent::_afficher('boutique/page', array(
				'fil_ariane'=> NULL,
				'module' 	=> $this->load->view('boutique/boutique/etape1', $data, TRUE),
				'seo'		=> $this->_seo()
			));
    	}
    }
    
    /**
     * ::Etape 2:: Identification ou inscription
     * 
     * Formulaire d'inscription
     * Formulaire de connexion
     */
	private function _etape2()
	{
    	$data = array(
    		'guide_etapes' 	=> $this->_guide_etapes(),
    		'inscription' 	=> $this->clients->inscrire(2),
    		//'connexion'		=> $this->clients->connexion(2)
    	);
    	
		parent::_afficher('boutique/page', array(
			'fil_ariane'=> NULL,
			'module'	=> $this->load->view('boutique/boutique/etape2', $data, TRUE),
			'seo'		=> $this->_seo()
		));
    }
    
	/**
     * ::Etape 3:: Coordonnées client
     * 
     * Formulaire de facturation
     * Formulaire de livraison
     */
	private function _etape3()
	{
    	$data = array(
    		'guide_etapes'	=> $this->_guide_etapes(),
    		'adresses' 		=> $this->clients->adresses()
    	);
		
		parent::_afficher('boutique/page', array(
			'fil_ariane'=> NULL,
			'module'	=> $this->load->view('boutique/boutique/etape3', $data, TRUE),
			'seo'		=> $this->_seo()
		));
    }
    
	/**
     * ::Etape 4:: Paiement
     * 
     * Récapitulatif du paiement
     * Enregistrement commande avec etat enregistrée
     * API banque ou Paybox
     */
	private function _etape4()
	{
		$data = array(
    		'guide_etapes'	=> $this->_guide_etapes(),
    		'module'		=> $this->_module,
			'commande'		=> NULL,
			'panier'		=> NULL,
    		'paiement'		=> NULL,
    		'cgv'			=> NULL,
    		'cgv_non_valide'=> FALSE,
		    'est_pro'       =>  $this->_client->pro
		);

		if (($commande = $this->commandes->enregistrer()) !== FALSE)
		{
			$paiement = ($commande->montant > 0 or ($this->_module->option_acompte && ($commande->acompte > 0)));
			
			//Commande
			$data['commande'] = $commande;

			//Panier
			$data['panier'] = $this->load->view('boutique/panier/etape4', array(
				'module' 	=> $this->_module,
				'commande' 	=> $commande,
				'panier' 	=> $this->cart->contents()
			), TRUE);

			//Commande avec paiement
			if ($paiement)
			{
				$data['paiement'] = $this->load->view('boutique/boutique/choix_mode_paiement', NULL, TRUE);
			}
			
			//CGV
			if ($this->_module->option_cgv)
			{
				$data['cgv'] = $this->load->view('boutique/boutique/cgv', array('module' => $this->_module), TRUE);
			}
						
			//Validation de la commande et paiement
			if ($this->input->post('submit-etape4'))
			{
				if ($this->_module->option_cgv && ! $this->input->post('cgv_validee'))
				{
					$data['cgv'] = $this->load->view('boutique/boutique/cgv', array(
						'module' => $this->_module,
						'cgv_non_validee' => TRUE
					), TRUE);
					
					$data['cgv_non_valide'] = TRUE;
				}
				elseif ($paiement)
				{
					redirect('/boutique/paiements/index/'.$commande->id.'?type_carte='.$this->input->post('paiement'));
				}
				else
				{
					redirect('/boutique/etape,5.html');
				}
			}
			elseif($this->input->get('cgv_validee') === '0')
			{
			    $data['cgv'] = $this->load->view('boutique/boutique/cgv', array(
			        'module' => $this->_module,
			        'cgv_non_validee' => TRUE
			    ), TRUE);
			    
			    $data['cgv_non_valide'] = TRUE;
			}
		}

		parent::_afficher('boutique/page', array(
			'fil_ariane'=> NULL,
			'module'	=> $this->load->view('boutique/boutique/etape4', $data, TRUE),
			'seo'		=> $this->_seo()
		));
    }
    
	/**
     * ::Etape 5:: Validation de la commande
     * 
     * Retour banque
     * Message de confirmation
     * Suppression des sessions
     */
	private function _etape5()
	{ 
    	$data = array(
    		'guide_etapes'	=> $this->_guide_etapes(),
    		'commande'		=> $this->commandes->validation()
    	);

    	//Efface les sessions
    	$this->cart->destroy();
    	$this->order->reset();
    	
		parent::_afficher('boutique/page', array(
			'fil_ariane'=> NULL,
			'module'	=> $this->load->view('boutique/boutique/etape5', $data, TRUE),
			'seo'		=> $this->_seo()
		));
		
    }
	
	/**
	 * Ligne pour l'acompte qui remplace le total commande
	 * 
	 * @return	VIEW
	 */
	private function _acompte()
	{
		$data = array(
			'module' => $this->_module
		);
		
		return $this->load->view('boutique/boutique/acompte', $data, TRUE);
	}
    
    /**
     * Fil d'ariane de la boutique
	 * 
	 * @return	VIEW
     */
    private function _guide_etapes()
    {
		$data = array(
    		'etapes' 		=> $this->_liste_etapes,
    		'etape_en_cours'=> $this->_etape_id
    	);
    	return $this->load->view('boutique/boutique/guide_etapes', $data, TRUE);
    }
    
    /**
     * Vérifie les sessions, la connexion client et le panier par étape
     */
    private function _securite()
    {
    	switch ($this->_etape_id)
    	{
    		case 2:
    			if ($this->cart->total_items() == 0)
    			{
    				redirect('/boutique/etape,1.html');
    			}
    			if ($this->_client)
    			{
    				redirect('/boutique/etape,3.html');
    			}
    		break;
    		
    		case 3: 
    			if ($this->cart->total_items() == 0)
    			{
    				redirect('/boutique/etape,1.html');
    			}
    			if ( ! $this->_client)
    			{
    				redirect('/boutique/etape,2.html');
    			}
    		break;
    		
    		case 4: 
    			if ($this->cart->total_items() == 0)
    			{
    				redirect('/boutique/etape,1.html');
    			}
    			if ( ! $this->_client)
    			{
    				redirect('/boutique/etape,2.html');
    			}
    			if ( ! $this->order->biling())
    			{
    				redirect('/boutique/etape,3.html');
    			}
    		break;
    		
    		case 5:
    			if ($this->cart->total_items() == 0)
    			{
    				redirect('/boutique/etape,1.html');
    			}
    			if ( ! $this->_client)
    			{
    				redirect('/boutique/etape,2.html');
    			}
    			if ( ! $this->order->biling())
    			{
    				redirect('/boutique/etape,3.html');
    			}
    		break;
    	}
    }
    
	/**
	 * Référencement de la boutique
	 * 
	 * @return	array
	 */
	private function _seo()
	{
		return array(
			'title'			=> $this->_parametres->intitule.' - '.lang('etape').' '.$this->_etape_id.' : '.$this->_liste_etapes[$this->_etape_id],
			'keywords'		=> NULL,
			'description'	=> NULL,
			'robots'		=> FALSE
		);
	}
	
	/**
	 * Défini si le paiment est HT ou TTC en fonction 
	 * du type de client (pro) et hors France
	 * 
	 * @return	boolean
	 */
	private function _paiement_hors_taxe()
	{
		if ($this->_module->option_livraison && ($this->_client !== FALSE) && ($livraison = $this->order->delivery()))
		{
			return (($this->_client->type_client_id == 2) && ($livraison['pays_id'] != self::PAYS_ID_FRANCE));
		}
		
		return FALSE;
	}
	
	/**
	 * Retourne le sous-total panier
	 * 
	 * @return	float
	 */
	private function _sous_total_panier()
	{
		$cart = $this->session->userdata('cart_contents');
		
		foreach ($this->cart->contents() as $item)
		{
			$produit = $this->produit->get_by_id($item['id']);
			$tarif 	 = $produit->tarif($this->_client, $item['qty']);
			
			$item['reference'] = $produit->reference;
			$item['libelle'] = $produit->libelle;
			$item['price'] = $tarif['prix'];
			$item['price_without_discount'] = $tarif['prix_sans_remise'];
			$item['tva'] = $tarif['tva'];
			
			//Si produit TTC et paiement HT, on retire la TVA
			if ($tarif['taxe'] && $this->_paiement_hors_taxe())
			{
				$item['price'] = $tarif['prix']/(1+$tarif['tva']/100);
				$item['price_without_discount'] = $tarif['prix_sans_remise']/(1+$tarif['tva']/100);
			}
			
			$cart[$item['rowid']] = $item;
			
			$this->session->set_userdata('cart_contents', $cart);
		}
		
		return $this->cart->total();
	}
	

	
    
}

/* End of file boutique.php */
/* Location: ./modules/boutique/controllers/boutique.php */