<?php
/**
 * Modèle de la table "t_clients"
 * 
 * @author 		Pierre-Etienne RABY
 * @package 	boutique
 * @category 	Models
 * @version		2.0
 */
class Clients_produit extends DataMapper
{
	//Relations
	var $has_many	= array();
	var $has_one	= array('client', 'produit');
	
	/**
	 * Constructeur
	 */
    public function __construct()
    {
		parent::__construct();
    }
	
	/**
     * Enregistrer les données
     * 
     * @param	array	$data
     * @return	object OR FALSE
     */
	public function enregistrer($data, $compte=FALSE)
	{
		
	}
    
    
}

/* End of file client.php */
/* Location: ./modules/boutique/models/client.php */ 