<?php
$liste = NULL;

if (($arbre !== FALSE) && ! empty($visible))
{
	$sections_visible = (isset($visible[$root_id])) ? $visible[$root_id] : NULL;
	$current_depth = 0;
	$node_depth	= 0;
	$i = 0;
	
	
	foreach ($arbre as $node)
	{
		if (is_array($sections_visible) && in_array($node['__id'], $sections_visible))
		{

			$this->load->models(array('catalogue/module_catalogue', 'categories/categorie'));
			$section = $this->section->get_by_id($node['__id']);

			$node_depth	= $node['__level'];
			
			if (($node_depth == $current_depth) && ($i > 0))
			{
				$liste .= '</li>';
				//$liste .= '<li class="separateurs"></li>';
			}
			elseif ($node_depth > $current_depth)
			{
				if (($node_depth > 1))
				{
					$liste .= '<ul class="niveau-'.$node_depth.'" >';
				}
				else
				{
					if (($root_id != 1))
					{
						$liste .= '<ul class="niveau-'.$node_depth.'" >';
					}
					else
					{
						$liste .= '<ul class="niveau-'.$node_depth.' mobimenu">';	
						
					}
					
				}
				
				//Lien vers l'accueil au début du menu
				if (($node_depth == 1) && $parametres->accueil_menu && ($root_id == 1))
				{
					$class = (is_null($selection)) ? ' active' : NULL;
					$liste .= '<li class="accueil'.$class.'"><a href="/"><span class="glyphicon glyphicon-home"></span> <span>'.lang('section_accueil').'</span></a></li>';
				}
				
				
				
				$current_depth = $current_depth	+ ($node_depth - $current_depth);
			}
			elseif ($node_depth < $current_depth)
			{
				$liste .= str_repeat('</li></ul>', $current_depth - $node_depth ).'</li>';
				//$liste .= '<li class="separateurs"></li>';
				
				$current_depth = $current_depth - ($current_depth - $node_depth);
			}

			$dropdown_li = ($node_depth == 1 ) ? 'ouvre-sous-menu' : '';
			$liste .= '<li class="section-'.$node['__id'].(($selection == $node['__id']) ? ' active' : NULL).' '.$dropdown_li.' menu-section-type-'.$node['type_module_id'].'">';
			
			//Url de la section
			$attributs = array();
			$suffixe = NULL;
			
			if ($node['ouvrir_onglet'])
			{
				$attributs['target'] = '_blank';
			}
			
			/**
			 * Insérer des items hors section au début du sous-menu
			 * de la section parente
			 *
			 * ex : catégories du module catalogue
			 */
			$sous_menu = NULL; 
			if (($node_depth == 1) && ($node['type_module_id'] == 8))
			{
				$module  = $section->module_catalogue->get();
				if ($module->affiche_sous_menu)
				{
					$sous_menu = modules::run('categories/lister_pour_menu', $node['__id']);
				}
			
			}
			
			//Section parent avec sous-menu
			if ((($node['right_id'] - $node['left_id']) > 1) or (($node_depth == 1) && ($node['type_module_id'] == 8) && (!is_null($sous_menu))))
			{
			    //$suffixe = ' <b class="caret"></b>';
			    //$lien = NULL; //Pas de lien sur le menu si il y a un sous menu
			    $lien = $this->section->url($node['__id']);
				
			}
			else 
			{
				$lien = $this->section->url($node['__id']);
			}
			
			//Picto de la section
			$picto_section = NULL;
			$picto2_section = NULL;
			if (!is_null($section->picto_id) && ($section->picto_id != 0))
			{
				$media = new Media();
				$media->get_by_id($section->picto_id);
				if ($media->exists())
				{
					$picto_section = img(array(
							'src' 	=> $this->config->item('media_image_size').'25x25/'.$media->dossier.$media->fichier,
							'alt' 	=> $media->titre,
							'class' => 'picto_origine'
					), TRUE);
				}
				
			}
			
			if (!is_null($section->picto2_id) && ($section->picto2_id != 0))
			{
				$media = new Media();
				$media->get_by_id($section->picto2_id);
				if ($media->exists())
				{
					$picto2_section = img(array(
							'src' 	=> $this->config->item('media_image_size').'25x25/'.$media->dossier.$media->fichier,
							'alt' 	=> $media->titre,
							'class'	=> 'picto_hover'
					), TRUE);
				}
				
			}
				
			if (!is_null($lien))
			{
				$liste .= anchor($lien, $picto_section.$picto2_section.$node['libelle'].$suffixe, $attributs);
			}
			else 
			{
				//$liste .= '<span>'.$picto_section.$picto2_section.$node['libelle'].$suffixe.'</span>';
				$liste .= anchor($lien, $picto_section.$picto2_section.$node['libelle'].$suffixe, $attributs);
			}
			
			
			if (!is_null($sous_menu))
			{
				$liste .= $sous_menu;
			}
		}
		$i++;
	}
	
	$liste .= str_repeat('</li></ul>', $node_depth);
}

if ( ! is_null($liste))
{
	echo $liste;
	//echo '<div class="navbar-collapse collapse menus" id="menu-'.$root_id.'">'.$liste.'</div>';	
}