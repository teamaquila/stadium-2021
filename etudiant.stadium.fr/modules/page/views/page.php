<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

$obj_section = $this->section->get_by_id($section);


$data = array(
	'url' => array(
		'image'	=> base_url().'assets/img/'
	),
	'assets' => assets(array(
		'favicon.ico',
		'bootstrap/bootstrap.min.css',
	    'jquery.fileupload.css',
		'jquery/colorbox.css',
		'jquery/jquery.rating.css',
		'jquery/jquery.mmenu.all.css',
		'hamburgers.min.css',
		'site.css',
		'jquery/jquery-core.min.js',
	    'jquery/jquery-ui-latest.min.js',
		'bootstrap/bootstrap.min.js',
		//'jquery/simpleMobileMenu.js',
		'jquery/jquery.mmenu.all.min.js',
		'jquery/jquery.mmenu.debugger.js',
	    'jquery/jquery.bxslider.min.js',
		'jquery/jquery.formsubmit.min.js',
		'jquery/jquery.colorbox-min.js',
		'jquery/jquery.rating.min.js',
	    'jquery/jquery.placeholder.min.js',
	    'jquery/jquery.fileupload.js',
		'commun.js',
		'boutique.js',
		'catalogue.js',
		'holder.js',
		'site.js'
	)),
	'rss' => rss_feed(array(array(
		'titre' => 'Actualités',
		'url'	=> '/rss'
	))),
	'metas' 		=> $metas,
	'statistiques' 	=> $statistiques,
	'fil_ariane' 	=> $fil_ariane,
	'menus'			=> $menus,
	'module'		=> $module,
	'widgets'		=> $widgets,
	'type_section'	=> $obj_section->type_module_id
);



$this->dwootemplate->output(tpl_path('page/page.tpl'), $data);
