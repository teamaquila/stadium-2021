<?php
/**
 * COMMUN
 */
$lang['nom']				= "Nom";
$lang['prenom']				= "Prénom";
$lang['adresse']			= "Adresse";
$lang['code_postal']		= "Code postal";
$lang['ville']				= "Ville";
$lang['pays']				= "Pays";
$lang['telephone']			= "Téléphone";
$lang['societe']			= "Société";
$lang['fonction']			= "Fonction";
$lang['mobile']				= "Mobile";
$lang['email']				= "E-mail";
$lang['identifiant']		= "Identifiant";
$lang['mot_de_passe']		= "Mot de passe";
$lang['mot_de_passe_confirm']= "Confirmez votre mot de passe";
$lang['valider']			= "Valider";
$lang['retourner_accueil'] 	= "Retourner sur l'accueil";

$lang['recherche']	= "Rechercher";
/**
 * ACCUEIL
 */
$lang['section_accueil'] = "Accueil";

/**
 * ACTUALITE
 */
$lang['actualite_savoir_plus'] 			= "En savoir plus";
$lang['actualite_vide'] 				= "Aucune actualité pour le moment.";
$lang['actualite_abonner_newsletter']	= "S'abonner à la newsletter";
$lang['actualite_retour_liste']			= "Retourner à la liste";
$lang['actualite_voir_toutes']			= "Voir toutes les actualités";

/**
 * FORMULAIRE
 */
$lang['nous_trouver']					= "Nous trouver";
$lang['formulaire_envoyer']				= "Envoyer";
$lang['formulaire_message_existant'] 	= "Ce message a déjà été envoyé";
$lang['formulaire_confirmation_envoi']	= "Vos informations ont bien été transmises.";
$lang['formulaire_recopier_code']		= "Recopier le code";


/**
 * NEWSLETTER
 */
//Inscription
$lang['newsletter_abonnement']				= "Abonnement à la newsletter";
$lang['newsletter_sabonner']				= "S'abonner";
$lang['newsletter_confirmation_inscription']= "Votre inscription a bien été enregistrée.";
$lang['newsletter_erreur_inscription']		= "Une erreur est survenue lors de votre inscription.";
$lang['newsletter_erreur_abonne_existant'] 	= "Cette adresse e-mail est déjà présente dans notre base de données";
$lang['newsletter_erreur_mail_non_valide']	= "Le champ Adresse e-mail doit contenir une adresse email valide";
$lang['newsletter_erreur_mail_non_saisi']	= "Veuillez remplir le champ Adresse e-mail";
$lang['newsletter_confirmation_abonnement']	= "Confirmation de votre abonnement";
$lang['retour_accueil']	= "Retourner sur l'accueil";


//Désinscription
$lang['newsletter_se_desabonner']			= "Se désabonner";
$lang['newsletter_desabonnement']			= "Désabonnement à la newsletter";
$lang['newsletter_desinscription']			= "Désinscription à la newsletter du site";
$lang['newsletter_erreur_desinscription']	= "Erreur de désinscription";
$lang['newsletter_confirmation_desinscription']	= "Votre désinscription à la newsletter a bien été prise en compte";
$lang['newsletter_erreur_email_inexistant']	= "Ce compte n'est actuellement pas abonné à la newsletter";
$lang['newsletter_erreur_desinscription']	= "Une erreur est survenue, nous n'avons pas pu retirer votre email de la liste de diffusion. <br/> Merci de contacter le webmaster. ";
$lang['newsletter_desinscription_abonne']	= "Un abonné s'est désinscrit de la newsletter";

/**
 * CATALOGUE
 */
$lang['devise'] 				= '&euro;';
$lang['a_partir_de'] 			= '&Agrave; partir de ';
$lang['prix_taxe_0'] 			= 'HT';
$lang['prix_taxe_1'] 			= 'TTC';
$lang['catalogue_prix'] 		= 'Prix';
$lang['catalogue_gratuit'] 		= 'Gratuit';
$lang['catalogue_sans_remise']	= 'sans remise';
$lang['catalogue_reduction'] 	= 'Réduction';
$lang['catalogue_vide'] 		= 'Aucun produit ne correspond à votre recherche';
$lang['catalogue_retour_liste'] = 'Retourner sur le catalogue';
$lang['catalogue_nouveau'] 		= 'Nouveau';
$lang['catalogue_savoir_plus'] 	= 'En savoir plus';
$lang['catalogue_note_produit']	= 'Note du produit : ';
$lang['gratuit']				= 'Gratuit';
$lang['produits']				= 'produits';
$lang['voir_produits']			= 'Voir tous les produits';
$lang['retirer_les_criteres']	= "Retirer tous les critères (X)";

//Catégories
$lang['categories']				= "Catégories";
$lang['categorie_vide']			= "Aucune catégorie";
$lang['afficher_les']			= "Afficher les";
$lang['articles']				= "articles";

//Tri
$lang['trier_par']				= "Trier par";
$lang['nouveaute']				= "Nouveauté";
$lang['prix_croissant']			= "Prix croissant";
$lang['prix_decroissant']		= "Prix décroissant";
$lang['liste']					= "Liste";
$lang['vignette']				= "Vignette";
$lang['afficher_par']			= "Afficher par";

//Statut stock
$lang['stock_disponible'] = 'Disponible';
$lang['plus_que'] = 'Plus que';
$lang['articles_disponibles'] = 'articles disponibles';
$lang['stock_indisponible'] = 'Indisponible';

//Commentaires
/*$lang['commentaires'] = "Commentaires";
$lang['aucun_commentaire'] = "Soyez le premier à donner votre avis";
$lang['saisissez_votre_commentaire'] = "Saissez votre commentaire";*/

//Produits associés
$lang['produits_recommandes'] = "Nous vous recommandons les produits suivants";

/**
 * BOUTIQUE
 */
$lang['boutique_ajouter_commentaire_commande'] = "Ajouter un commentaire pour le vendeur sur la commande";
$lang['boutique_ajouter_panier'] = "Ajouter au panier";
$lang['boutique_erreur_quantite_livraison'] = "Nous ne pouvons pas expédier votre commande car la quantité est trop importante.";
$lang['boutique_cgv'] = "En cochant cette case et en cliquant sur le bouton \"Terminer ma commande\" , 
vous acceptez de vous conformer aux présentes {link}conditions générales de vente{/link} que vous 
reconnaissez avoir lues, comprises et acceptées dans leur intégralité. Si vous refusez d'accepter 
ces conditions d'utilisation, nous vous remercions de ne procéder à aucune commande.";
$lang['continuer_achats'] = "Continuer mes achats";
$lang['recalculer_panier'] = 'Recalculer';

/* Fil d'ariane boutique */
$lang['etape']				= "Etape";
$lang['panier']				= "Panier";
$lang['authentification']	= "Authentification";
$lang['coordonnees']		= "Coordonnées";
$lang['paiement']			= "Paiement";
$lang['recapitulatif']		= "Récapitulatif";
$lang['validation']			= "Validation";
$lang['ajouter']			= "Ajouter";

/* Panier */
$lang['panier_vide']		= "Votre panier est vide";
$lang['sous_total']			= "Sous-total";
$lang['sous_total_sans_remise']	= "Sous-total sans remise";
$lang['supprimer']			= "Supprimer";
$lang['description']		= "Description";
$lang['reference']			= "Référence";
$lang['prix_unitaire']		= "Prix unitaire";
$lang['prix_remise']		= "Prix remisé";
$lang['quantite']			= "Quantité";
$lang['remise']				= "Remise";
$lang['frais_port']			= "Frais de port";
$lang['qte']				= "Qté";
$lang['prix']				= "Prix";
$lang['stock']				= "Stock";
$lang['total']				= "Total";
$lang['commentaire']		= "Commentaire";
$lang['commentaire_produit']	= "Commentaire produit";
$lang['commentaire_commande']	= "Commentaire commande";
$lang['commentaire_enregistre']	= "Commentaire enregistré";
$lang['ajouter_commentaire']	= "Commentaire";
$lang['commander']				= "Commander";
$lang['sauvegarder_panier']		= "Enregistrer mon panier";
$lang['choisir_une_reference']	= "Choisissez un article";
$lang['continuer_achat']		= "Continuer vos achats";
$lang['vider_panier']			= "Vider mon panier";
$lang['produit_indisponible_vente'] = "Produit indisponible à la vente";

//Remise
$lang['annuler'] = "Annuler";
$lang['reduction_de'] = "Réduction de";

// Erreurs
$lang['erreur_suppression_produit']	= "Impossible de supprimer le produit";
$lang['erreur_produit_inexistant']	= "Veuillez sélectionner tous vos critères";
$lang['erreur_quantite_nulle']		= "La quantité est nulle";
$lang['erreur_ajout_panier']		= "Impossible d'ajouter le produit au panier";
$lang['erreur_prix_produit']		= "Le prix n'est pas renseigné pour ce produit";
$lang['erreur_stock_insuffisant']	= "Stock insuffisant";
$lang['erreur_transporteur_incompatible'] = "Ce transporteur n'est pas disponible pour le pays sélectionné";

/* Remise */
$lang['saisir_code_remise']			= "Saisissez votre code promo";
$lang['erreur_code_vide'] 			= "Veuillez saisir un code remise";
$lang['erreur_code_inexistant'] 	= "Ce code remise n'existe pas";
$lang['erreur_hors_periode'] 		= "Ce code remise n'est plus disponible";
$lang['erreur_quota_depasse'] 		= "Ce code remise n'est plus disponible";
$lang['code_promo_non_applicable']	= "Cette remise n'est pas applicable à votre panier actuel";
$lang['connexion_utilisation_code'] = "Veuillez vous connecter pour utiliser votre remise";
$lang['code_unique_deja_utilise'] 	= "Vous avez déjà utilisé ce code à usage unique";
$lang['montant_panier_insuffisant'] = "Le montant total du panier est insuffisant pour bénéficier de cette remise";

// Avertissement
$lang['aver_aucune_commande']	= 'Aucune commande en cours';

/* Transporteur */
$lang['selection_pays']		= "Pays";
$lang['frais_de_port']		= "Frais de port";
$lang['frais_de_port_gratuit'] = "Frais de port gratuit";
$lang['frais_de_port_offerts'] = "Frais de port offerts";

/* Etats de commande */
$lang['etat_erreur']	= "Erreur";
$lang['etat_enregistre']= "Enregistrée";
$lang['etat_payee']		= "Payée";
$lang['etat_preparee']	= "Préparée";
$lang['etat_expediee']	= "Expédiée";
$lang['etat_annulee']	= "Annulée";
$lang['etat_retournee']	= "Erroné";
$lang['etat_attente_paiement']	= "En attente de paiement";
$lang['paiement_compte']	= "Paiement compte";

/* Erreur commande */
$lang['erreur_enregistrement_commande'] = "Une erreur est survenue lors de l'enregistrement. Veuillez réessayer ultérieurement.";

/* Email */
$lang['mail_sujet_nvl_commande'] = "Nouvelle commande sur votre site";
$lang['contact_vendeur']		= "Contacter le vendeur";
$lang['confirmation_payee'] 	= "Nous avons le plaisir de vous confirmer le paiement de votre commande";
$lang['confirmation_expediee'] 	= "Nous avons le plaisir de vous confirmer l'expédition de votre commande";
$lang['confirmation_annulee'] 	= "Nous vous confirmons l'annulation de votre commande";
$lang['confirmation_preparee'] 	= "Nous vous confirmons la préparation de votre commande";
$lang['confirmation_retournee'] = "Nous vous confirmons le retour de votre commande";
$lang['confirmation_validee'] 	= "Nous avons le plaisir de vous confirmer la validation de votre commande";
$lang['commande_disponible']	= "Votre panier est toujours disponible, ne l'oubliez pas !";
$lang['commande']				= "commande";
$lang['adresse_livraison']		= "Adresse de livraison";
$lang['adresse_facturation']	= "Adresse de facturation";
$lang['recapitulatif_commande']	= "Récapitulatif de votre commande";
$lang['telecharge_facture_site']= "Vous pouvez télécharger dès à présent votre facture sur votre compte client.";
$lang['prevenir_envoi']			= "Vous serez prévenu(e) par e-mail dès que votre commande sera envoyée.";
$lang['remerciement']			= "Nous vous remercions de votre confiance. A très bientôt sur notre site";
$lang['equipe']					= "L’équipe";

//Connexion
$lang['mon_compte'] = 'Mon compte';
$lang['creer_mon_compte'] = 'Créer mon compte';
$lang['possede_un_compte'] = "Déjà client ? Connectez-vous";
$lang['mot_de_passe_oublie'] = 'Mot de passe oublié ?';
$lang['se_connecter'] = 'Se connecter';
$lang['se_deconnecter'] = 'Se déconnecter';
$lang['acceder_espace_client'] = 'Accéder à mon espace client';
$lang['acceder_commandes'] = 'Accéder à mes commandes';
$lang['acceder_paniers'] = 'Devis en attente';
$lang['acceder_coordonnees'] = 'Modifier mes coordonnées';

//Inscription
$lang['acceptation_newsletter'] = "En cochant cette case, j'accepte de recevoir la newsletter";
$lang['champs_obligatoire'] = 'Champs obligatoires';

//Oubli mot de passe
$lang['recevoir_nouveau_mot_passe'] = "Recevoir un nouveau mot de passe";
$lang['success_envoi_nouveau_mot_passe'] = "Votre nouveau mot de passe vient d'être envoyé à votre adresse e-mail";

//Alertes
$lang['alert_aucune_commande']	= "Vous n'avez aucune commande pour le moment";

//Validations
$lang['modifications_enregistres'] = "Vos modifications sont enregistrées";

//Erreurs connexion
$lang['erreur_enregistrement'] 		= "Impossible d'enregistrer vos données";
$lang['erreur_authentification'] 	= "Identifiant et/ou mot de passe incorrect";
$lang['erreur_identifiant_existant']= "Cet identifiant est déjà utilisé";
$lang['erreur_identifiant_existant_creation_compte']= "Cet identifiant est déjà utilisé. Veuillez vous désinscrire à la newsletter pour créer votre compte client";

/**
 * Inclusion du fichier de langue propre au site
 */
if (file_exists(ROOTPATH.'modules/page/language/francais/site_lang.php'))
{
	include_once ROOTPATH.'modules/page/language/francais/site_lang.php';
}