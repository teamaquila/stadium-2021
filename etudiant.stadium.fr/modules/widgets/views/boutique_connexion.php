<?php
$html = NULL;

$html .= '<div class="widget-panel deconnecte">';

$html .= form_open('/boutique/clients/connexion', array('id' => 'form-connexion'));

$html .= form_fieldset();

$html .= form_hidden('type', 3);

$html .= form_input(array(
	'name'	=> 'identifiant',
	'id'	=> 'identifiant',
	'class'	=> 'form-control',
    'placeholder' => lang('identifiant')
));

$html .= form_password(array(
	'name'	=> 'mot_de_passe',
	'id'	=> 'mot_de_passe',
	'class'	=> 'form-control',
	'placeholder' => lang('mot_de_passe')
));
$html .= anchor('/boutique/clients/nouveau_mdp', lang('mot_de_passe_oublie'), 'rel="nofollow" class="btn oubli-mdp"');

//$html .= '<br class="clear-both">';
$html .= form_button(array(
	'type'		=> 'submit',
	'name'		=> 'submit-connexion',
	'value'		=> TRUE,
	'class'		=> 'btn btn-primary btn-block',
	'content'	=> lang('se_connecter')
));

$html .= form_fieldset_close();
$html .= form_close();

$html .= anchor('/inscription-client.html', lang('creer_mon_compte'), 'rel="nofollow" class="btn btn-block nouveau-compte"');
$html .= '</div>';

echo $html;