<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'controllers/front.php';

/**
 * Page : 
 * Contrôleur gérant l'affichage des pages du site
 * 
 * @author 		Pauline MARTIN
 * @package 	application
 * @category 	Controllers
 * @version		2.0
 */
class Recherche extends Front
{
	
	const ITEM_PAR_PAGE = 10;
	
	private $_widget_recherche;
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->load->models(array('actualites/actualite', 'editeurs/module_editeur', 'catalogue/produit', 'catalogue/module_catalogue',
			'actualites/module_actualite', 'widgets/widget'));
		$this->load->library('pagination');
		$this->lang->load('recherche');
		$this->lang->load('catalogue/catalogue');
		
		$this->_widget_recherche = $this->widget->get_by_type('recherche');

		$this->load->modules(array(
				'boutique/clients'
		));
	}
	
	/**
	 * Affiche une page de résultats de recherche
	 * 
	 * @return 	vue
	 */
	public function lister()
	{
		if ($this->input->post('recherche'))
		{
			$data = array(
					'recherche'			=> $this->input->post('recherche'),
					
					'tab_actualites'	=> ($this->_widget_recherche->param_1) ? TRUE : FALSE,
					'tab_editeurs'		=> ($this->_widget_recherche->param_2) ? TRUE : FALSE,
					'tab_catalogue'		=> ($this->_widget_recherche->param_3) ? TRUE : FALSE,
					
					'nb_actualites'		=> ($this->_widget_recherche->param_1) ? ($this->count_actualites_recherche(htmlentities($this->input->post('recherche')))) : FALSE,
					'nb_editeurs'		=> ($this->_widget_recherche->param_2) ? ($this->count_editeurs_recherche(htmlentities($this->input->post('recherche')))) : FALSE,
			    'nb_catalogue'		=> ($this->_widget_recherche->param_3) ? ($this->count_catalogue_recherche(html_entity_decode(preg_replace('/&([a-zA-Z])(uml|acute|grave|circ|tilde|ring);/','$1',htmlentities($this->input->post('recherche'), ENT_COMPAT, "UTF-8"))))) : FALSE,
						
			
					'contenu_tab_actualites'	=> ($this->_widget_recherche->param_1) ? ($this->lister_actualites_recherche(htmlentities($this->input->post('recherche')))) : NULL,
					'contenu_tab_editeurs'		=> ($this->_widget_recherche->param_2) ? ($this->lister_editeurs_recherche(htmlentities($this->input->post('recherche')))) : NULL,
			    'contenu_tab_catalogue'		=> ($this->_widget_recherche->param_3) ? ($this->lister_catalogue_recherche(html_entity_decode(preg_replace('/&([a-zA-Z])(uml|acute|grave|circ|tilde|ring);/','$1',htmlentities($this->input->post('recherche'), ENT_COMPAT, "UTF-8"))))) : NULL,
			);
			parent::_afficher('page', array(
					'fil_ariane'=> NULL,
					'module' 	=> $this->load->view('recherches/liste', $data, TRUE),
					'seo'		=> $this->_seo()
			));
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function lister_actualites_recherche($recherche = NULL, $page = 0, $offset = self::ITEM_PAR_PAGE)
	{
		/*$this->actualite->group_start()->group_start()->where('debut_publication <= ', 'CURDATE()', FALSE)->where('fin_publication >= ', 'CURDATE()', FALSE)->group_end();
		$this->actualite->or_where('publication_illimitee', TRUE)->group_end();
			
		$actualites = $this->actualite->like('contenu', $this->input->post('recherche'))->or_like('titre', $this->input->post('recherche'))->get();
*/
		//Pagination
		$suffixe = (query_string()) ? '?'.query_string() : '';
		/*$this->pagination->initialize(array(
				'base_url'		=> '/recherches/recherche/lister_actualites_recherche',
				'first_url'		=> '/recherches/recherche/lister_actualites_recherche/'.$recherche.$suffixe,
				'total_rows'	=> $actualites->count(),
				'per_page'		=> $offset,
				'cur_page'		=> $page,
				'prefix'		=> 'page,',
				'suffix'		=> $suffixe,
				'uri_segment'	=> 0
		));*/
		$this->actualite->clear();
		$this->actualite->group_start()->group_start()->where('debut_publication <= ', 'CURDATE()', FALSE)->where('fin_publication >= ', 'CURDATE()', FALSE)->group_end();
		$this->actualite->or_where('publication_illimitee', TRUE)->group_end();
			
		$actualites = $this->actualite->like('contenu', $recherche)->or_like('titre', $recherche)->get();
		
		$data = array(
			'pagination'	=> $this->pagination->create_links(),
			'actualites'	=> $actualites
		);
		
		return $this->load->view('recherches/liste_actualites', $data, TRUE);
	}
	
	public function lister_editeurs_recherche($recherche = NULL, $item = 0, $offset = self::ITEM_PAR_PAGE)
	{
		$editeurs = $this->module_editeur->like('contenu', $recherche)->get();
		
		$data = array(
				'editeurs'	=> $editeurs
		);
		
		return $this->load->view('recherches/liste_editeurs', $data, TRUE);
	}
	
	
	public function lister_catalogue_recherche($recherche = NULL, $page = 0, $offset = self::ITEM_PAR_PAGE)
	{
		$conditions_produits = array(
		        'enfants'	=> FALSE,
				'recherche' => array(
						'libelle'		=> $recherche,
						'description'	=> $recherche
				),
		        'visible'   => TRUE,

		);

	//	$total_produit = $this->produit->compter($conditions_produits);
		
		$suffixe = (query_string()) ? '?'.query_string() : '';
		
		//Pagination
	/*	$this->pagination->initialize(array(
			'base_url'		=> '/recherches/recherche/lister_catalogue_recherche',
			'first_url'		=> '/recherches/recherche/lister_catalogue_recherche'.$suffixe,
			'total_rows'	=> $total_produit,
			'per_page'		=> $offset,
			'cur_page'		=> $page,
			'prefix'		=> 'page,',
			'suffix'		=> $suffixe,
			'uri_segment'	=> 0
		));*/
		
		$data = array(
			'pagination'	=>NULL,
			'produits'		=> $this->produit->lister($conditions_produits, $page, 100),
			//'total_produit'	=> $total_produit,
		);
		
		return $this->load->view('recherches/liste_produits', $data, TRUE);
	}
	
	public function count_actualites_recherche($recherche = NULL)
	{
		$this->actualite->group_start()->group_start()->where('debut_publication <= ', 'CURDATE()', FALSE)->where('fin_publication >= ', 'CURDATE()', FALSE)->group_end();
		$this->actualite->or_where('publication_illimitee', TRUE)->group_end();
			
		$actualites = $this->actualite->like('contenu', $recherche)->or_like('titre', $recherche)->get();
		return $actualites->result_count();
	}
	
	public function count_editeurs_recherche($recherche = NULL)
	{
		$editeurs = $this->module_editeur->like('contenu', $recherche)->get();
		return $editeurs->result_count();
	}
	public function count_catalogue_recherche($recherche = NULL)
	{
		$conditions_produits = array(
			'recherche' => array(
					'libelle'		=> $recherche,
					'description'	=> $recherche
			),
			'enfants'	=> 0,
	        'visible'   => TRUE
		);
	
		return $this->produit->compter($conditions_produits);
	
	}
	
	
	/**
	 * Référencement de la recherche
	 *
	 * @return	array
	 */
	private function _seo()
	{
		return array(
				'title'			=> 'Recherche',
				'keywords'		=> NULL,
				'description'	=> NULL,
				'robots'		=> FALSE
		);
	}
}
/* End of file page.php */
/* Location: ./application/controllers/page.php */