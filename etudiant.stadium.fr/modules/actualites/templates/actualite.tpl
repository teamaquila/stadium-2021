<div class="actualite module-actualite module-{$module_id} {$important}" id="actualite-{$id}">
	{$fil_ariane}

	<div class="bloc_titre_h1">
		<h1>{$titre}</h1>
	</div>

	<div class="inner_interne">
<!-- 		<span class="date">{$date}</span> -->
		
<!-- 		{if $categorie} -->
<!-- 			<span class="label label-primary">{$categorie}</span> -->
<!-- 		{/if} -->
		
<!-- 		{if $images} -->
<!-- 			<div class="diaporama">{$images}</div> -->
<!-- 		{/if} -->
		
		<div class="contenu">
			{$contenu}
		</div>
		<br />
		<a href="{$url.retour}" class="btn btn-retour">
			<span class="glyphicon glyphicon-chevron-left"></span>
			{lang('actualite_retour_liste')}
		</a>
	
	</div>
</div>