<?php
/**
 * Module
 */
$lang['images'] = "Images";
$lang['taille_image_liste'] = "Dimension de l'image dans la liste";
$lang['taille_image_fiche'] = "Dimension de l'image dans la fiche produit";
$lang['taille_image_important'] = "Dimension de l'image importante";
$lang['taille_image_accueil'] = "Dimension de l'image sur l'accueil";

$lang['catalogue'] = "Catalogue";
$lang['type_catalogue'] = "Type";
$lang['type_1'] = "Accès par catégories en arbre";
$lang['type_2'] = "Liste de produits (avec ou sans catégories)";
$lang['categorie_parent'] = "Catégorie racine";
$lang['criteres'] = "Critères";
$lang['commentaires'] = "Avis clients";
$lang['export']	= "Exporter les produits";
$lang['item_par_page']	= "Nombre de produits par page";
$lang['aucun_critere_renseigne'] = "Aucun critère renseigné";
$lang['aucune_categorie_renseigne'] = "Aucune catégorie renseignée";
$lang['affiche_sous_menu'] 	= "Afficher le sous menu catégories";
$lang['afficher_categories_liste'] 	= "Afficher les catégories dans la liste";
$lang['afficher_criteres_liste'] 	= "Afficher les critères dans la liste";

$lang['boutique'] 			= "Boutique";
$lang['activer_boutique'] 	= "Activer la boutique";
$lang['type_boutique'] 		= "Type de boutique";
$lang['type_boutique_1'] 	= "Pour les particuliers";
$lang['type_boutique_2'] 	= "Pour les professionnels";
$lang['type_boutique_3'] 	= "Pour les particuliers et professionnels";
$lang['type_tarif_degressif'] 		= "Type de tarif dégressif";
$lang['type_tarif_degressif_1'] 	= "Quantitatif";
$lang['type_tarif_degressif_2'] 	= "Pourcentage";
$lang['type_boutique_3'] 	= "Pour les particuliers et professionnels";
$lang['activer_stock'] 		= "Activer la gestion du stock";
$lang['seuil_alerte_stock'] = "Seuil pour le stock critique";
$lang['afficher_panier_liste']		= "Afficher le panier dans la liste des produits";
$lang['autoriser_achat_gratuit']	= "Autoriser l'achat gratuit";
$lang['activer_commentaires']	= "Activer les commentaires";

$lang['saisie_produit']		= "Saisie d'un produit";
$lang['afficher_resume']	= "Activer le résumé";
$lang['afficher_categorie']	= "Activer les catégories";
$lang['module_categorie_desactive']	= "Le module catégorie est désactivé";
$lang['afficher_criteres']	= "Activer les critères";
$lang['afficher_tarif']		= "Activer la tarification";
$lang['afficher_produits_associes']	= "Activer les produits associés";
$lang['afficher_references']	= "Activer les références";
$lang['afficher_indisponibles'] = "Afficher les produits indisponibles à la vente";
$lang['unite_poids']		= "Unité du poids";
$lang['afficher_commentaires'] = "Afficher les commentaires";
$lang['remise_illimitee']	= "Remise illimitée";
$lang['duree_remise']		= "Durée de la remise";

$lang['textes']	= "Textes";
$lang['longueur_texte_liste']	= "Longueur du texte dans la liste";
$lang['longueur_texte_accueil']	= "Longueur du texte sur l'accueil";

/**
 * Liste
 */
$lang['ajouter_produit'] 	= "Ajouter un produit";
$lang['aucun_produit'] 		= "Aucun produit";
$lang['rechercher_produit'] = "Rechercher un produit";

/**
 * Formulaire
 */
//Infos
$lang['libelle'] = 'Libellé';
$lang['libelle2'] = 'Libellé 2';
$lang['reference'] = "Référence";
$lang['description'] = "Description";
$lang['resume'] = "Résumé";
$lang['poids'] = "Poids";
$lang['conditionnement'] = "Conditionnement";
$lang['kg'] = "Kg";

//Publication
$lang['publication'] = "Publication";
$lang['accueil'] = "Accueil";
$lang['nouveaute'] = "Nouveau";
$lang['non_commandable'] = "Non commandable";
$lang['important'] = "Important";
$lang['visible'] = "Visible";
$lang['visibilite_accueil'] = "Visible sur l'accueil";
$lang['visibilite_important'] = "Important";
$lang['visibilite_nouveaute'] = "Nouveauté";
$lang['ordre_liste'] = "Ordre dans la liste";
$lang['tri_defaut_champ']	= "Champ de tri par défaut";
$lang['tri_defaut_ordre']	= "Ordre de tri par défaut";
$lang['tri_defaut_ordre_asc']	= "Odre croissant";
$lang['tri_defaut_ordre_desc']	= "Ordre décroissant";
$lang['tri_defaut_ordre_rand']	= "Ordre aléatoire";

//Tarif
$lang['tarification'] 	= "Tarification";
$lang['prix_vente'] 	= "Prix de vente";
$lang['taxe'] 			= "Tarif saisi en";
$lang['taux_tva'] 		= "Taux de TVA";
$lang['tarif_degressif']= "Tarif dégressif";
$lang['prix_ttc']		= "Prix TTC";
$lang['prix_ht']		= "Prix HT";
$lang['seuil_minimum']	= "Minimum";
$lang['seuil_maximum']	= "Maximum";
$lang['ajouter_ligne']	= "Ajouter une ligne";
$lang['remise']			= "Remise";
$lang['prix_remise']	= "Remise";
$lang['err_stock_insuffisant']		= "Stock insuffisant";
$lang['produit_remisable']	= "Remisable par code promo";

//Stock
$lang['gestion_stock'] 	= 'Gestion du stock';
$lang['stock_restant'] 	= 'Stock restant';
$lang['seuil_alerte']	= "Seuil d'alerte";

//Catégorie
$lang['categories'] = "Catégories";
$lang['selectionner_categories'] = "Sélectionner vos catégories";
$lang['selection_categories'] = "Sélection des catégories";
$lang['categories_disponibles'] = "Catégories disponibles";

$lang['filtres'] = "Filtres";
$lang['selectionner_filtres'] = "Sélectionner vos filtres";
$lang['selection_filtres'] = "Sélection des filtres";

//Critère
$lang['criteres'] = 'Critères';
$lang['type_critere'] = 'Type de critère';
$lang['type_critere_1'] = 'Valeur à saisir';
$lang['type_critere_2'] = 'Sélection';
$lang['valeurs_uniques'] = 'Valeurs uniques';
$lang['valeurs_saisie'] = 'Valeurs';

//Produits associés
$lang['produits_associes'] = "Produits associés";
$lang['selectionner_produits_associes'] = "Sélectionner vos produits associés";
$lang['selection_produits_associes'] = "Sélection des produits associés";
$lang['produits_disponibles'] = "Produits disponibles";

//Références
$lang['references'] = "Références";
$lang['selectionner_references'] = "Sélectionner vos références";
$lang['selection_references'] = "Sélection des références";

//Médias
$lang['medias_associes'] = "Médias associés";
$lang['selectionner_medias'] = "Sélectionner vos médias";
$lang['bibliotheque_medias'] = "Bibliothèque des médias";

/**
 * Formulaire de suppression 
 */
$lang['titre_suppression']  = "Supprimer les produits";
$lang['message_suppression'] = "Voulez-vous supprimer les produits sélectionnés ?";

$lang['titre_duplication'] 	= "Dupliquer les articles";
$lang['message_duplication'] = "Voulez-vous dupliquer les produits sélectionnés ?";

//Catalogue - log
$lang['log_ajout_produit'] 			= "Ajout du produit";
$lang['log_modification_produit'] 	= "Mise à jour du produit";
$lang['log_suppression_produit'] 	= "Suppression du produit";
$lang['log_configuration_catalogue']= "Configuration du module Catalogue";
$lang['log_import_catalogue'] 		= "Import du catalogue effectué";
$lang['log_duplication_produit'] 	= "Duplication du produit";

/**
 * Critères de produits
 */
$lang['ajouter_critere'] = 'Ajouter un critère';
$lang['aucun_critere'] = 'Aucun critère';

//Critere - log
$lang['log_ajout_critere'] 			= "Ajout du critère";
$lang['log_modification_critere'] 	= "Mise à jour du critère";
$lang['log_suppression_critere'] 	= "Suppression du critère";

/**
 * Commentaires
 */

$lang['commentaire'] = 'Avis clients';
$lang['avis_clients'] = 'Avis clients';
$lang['utilisateur'] = 'Client';
$lang['produit'] = 'Produit';
$lang['note'] = 'Note';
$lang['client'] = 'Client';
$lang['ajouter_commentaire'] = 'Donner mon avis';
$lang['aucun_commentaire'] = 'Aucun avis';
$lang['donner_avis'] = "Soyez le premier à donner votre avis";
$lang['rechercher_commentaire'] = 'Rechercher un avis';
$lang['lu'] = 'Lu';
$lang['message_suppression_commentaire'] = "Voulez-vous supprimer ce(s) commentaire(s) ?";
$lang['message_voir_commentaire'] = "Voulez-vous changer l'état de visiblité de ce(s) commentaire(s) ?";
$lang['message_visibilite_commentaire'] = "Voulez-vous afficher sur le site ce(s) commentaire(s) ?";
$lang['message_invisibilite_commentaire'] = "Voulez-vous rendre invisible sur le site ce(s) commentaire(s) ?";


$lang['rendre_visible'] = 'Afficher sur le site';
$lang['rendre_invisible'] = 'Retirer du site';

//Commentaire - log
$lang['log_ajout_commentaire'] 			= "Ajout du commentaire";
$lang['log_modification_commentaire'] 	= "Mise à jour du commentaire";
$lang['log_suppression_commentaire'] 	= "Suppression du commentaire";
