<?php
$html  = NULL;
$html .= '<script src="/tinymce/tinymce.min.js" type="text/javascript" ></script>';


$html .= assets(array(
    'bootstrap/bootstrap-multiselect.css',
));

$html .= assets(array(
    'tinymce/tinymce.min.js',
    'tinymce/jquery.tinymce.min.js',
    'modules/catalogue/js/tinymce.js',
    'modules/catalogue/js/catalogue.js',
    'modules/editeur/css/editeur.css'
), TRUE);



//Entête
if (isset($produit))
{
    $html .= form_open('/catalogue/admin/produits/modifier/'.$produit->id, array('class' => 'clearfix submit-ajax design'));
    $html .= form_hidden('id', $produit->id);
}
else
{
    $html .= form_open('/catalogue/admin/produits/ajouter/'.$module->id, array('class' => 'clearfix submit-ajax design'));
    $html .= form_hidden('module_catalogue_id', $module->id);
}

$html .= form_hidden('offset', $offset);

//Bouton enregistrer
$html .= form_button(array(
    'type' 	=> 'submit',
    'class'	=> 'btn btn-success pull-right enregistrer',
    'name'	=> 'submit',
    'value'	=> TRUE,
    'content' => '<span class="glyphicon glyphicon-floppy-save"></span> '.lang('enregistrer')
));

$html.= '<div class="clearfix"></div>';

//Infos générales
$html .= form_fieldset(lang('informations_generales'));

//Titre
$html .= '<p class="ligne-champ">'.form_label(lang('libelle').'*');
$html .= form_input(array(
    'type' 	=> 'text',
    'name'	=> 'libelle',
    'class'	=> 'titre',
    'value'	=> isset_value($produit, 'libelle')
)).'</p>';

$html .= '<p class="ligne-champ">'.form_label(lang('reference').'*');
$html .= form_input(array(
    'type' 	=> 'text',
    'name'	=> 'reference',
    'value'	=> isset_value($produit, 'reference')
)).'</p>';

$html .= '<p class="ligne-champ">'.form_label(lang('stock_restant'));
$html .= form_input(array(
    'type' 	=> 'text',
    'name'	=> 'stock',
    'class'	=> 'width-50 align-center',
    'value'	=> isset_value($produit, 'stock')
)).'</p>';

$html .= '<p class="ligne-champ">'.form_label(lang('poids').'');
$html .= form_input(array(
    'type' 	=> 'text',
    'class'	=> 'width-50',
    'name'	=> 'poids',
    'value'	=> isset_value($produit, 'poids')
)).nbs().$module->unite_poids.'</p>';

$html .= '<p class="ligne-champ">'.form_label(lang('conditionnement').'');
$html .= form_input(array(
    'type' 	=> 'text',
    'class'	=> 'width-50',
    'name'	=> 'conditionnement',
    'value'	=> (isset_value($produit, 'conditionnement')) ? (isset_value($produit, 'conditionnement')) : 1
)).'</p>';

//TVA
$html .= '<p class="ligne-champ">'.form_label(lang('taux_tva'), 'tva');
$html .= form_dropdown('tva_id', $this->config->item('taux_tva')[66], isset_value($produit, 'tva_id', 1)).' %</p>';

$html .= form_fieldset_close();

//Libellé 2
$html .= form_fieldset(lang('libelle2'));
$html .= form_textarea(array(
    'name'	=> 'libelle2',
    'class'	=> 'tinymce',
    'value'	=> isset_value($produit, 'libelle2')
));
$html .= form_fieldset_close();

//Résumé
$html .= form_fieldset(lang('resume'));
$html .= form_textarea(array(
    'name'	=> 'resume',
    'class'	=> 'tinymce',
    'value'	=> isset_value($produit, 'resume')
));
$html .= form_fieldset_close();

//Description
$html .= form_fieldset(lang('description'));
$html .= form_textarea(array(
    'id'	=> 'description',
    'name'	=> 'description',
    'class'	=> 'tinymce',
    'style'	=> 'width: 100%; height: 350px;',
    'value'	=> isset_value($produit, 'description')
));
$html .= form_fieldset_close();

//Tarif
/*if (isset($produit))
 {
 $html .= form_hidden('tarif_particulier_id', $produit->tarif_particulier_id);
 $html .= form_hidden('tarif_pro_id', $produit->tarif_pro_id);
 }*/
if ($module->afficher_tarif)
{
    $html .= $tarifs;
}


//Publication
$html .= form_fieldset(lang('publication'));
$html .= '<p class="ligne-champ">'.form_label(lang('visible'), 'visible');
$html .= form_checkbox(array(
    'id'	=> 'visible',
    'name'	=> 'visible',
    'checked' => isset_value($produit, 'visible', TRUE)
)).'</p>';

$html .= '<p class="ligne-champ">'.form_label(lang('visibilite_accueil'), 'accueil');
$html .= form_checkbox(array(
    'id'	=> 'accueil',
    'name'	=> 'accueil',
    'checked' => isset_value($produit, 'accueil')
)).'</p>';

$html .= '<p class="ligne-champ">'.form_label(lang('visibilite_nouveaute'), 'nouveau');
$html .= form_checkbox(array(
    'id'	=> 'nouveau',
    'name'	=> 'nouveau',
    'checked' => isset_value($produit, 'nouveau')
)).'</p>';

if ($boutique_active == 1)
{
    $html .= '<p class="ligne-champ">'.form_label(lang('non_commandable'), 'non_commandable');
    $html .= form_checkbox(array(
        'id'	=> 'non_commandable',
        'name'	=> 'non_commandable',
        'checked' => isset_value($produit, 'non_commandable')
    )).'</p>';
}

$html .= '<p class="ligne-champ">'.form_label(lang('visibilite_important'), 'important');
$html .= form_checkbox(array(
    'id'	=> 'important',
    'name'	=> 'important',
    'class'	=> 'toggle-action',
    'data-id' => 'option-important',
    'checked' => isset_value($produit, 'important')
)).'</p>';

$classe = (isset_value($produit, 'important')) ? '' : 'class="hidden"';

$html .= '<div id="option-important" '.$classe.'>';
$html .= '<p class="ligne-champ">'.form_label(lang('ordre_liste'), 'ordre');
$html .= form_input(array(
    'type' 	=> 'text',
    'name'	=> 'ordre',
    'class'	=> 'width-30 align-center',
    'value'	=> isset_value($produit, 'ordre')
)).'</p>';
$html .= '</div>';

$html .= form_fieldset_close();

$html .= '<div class="row">';

//Catégorie
if ($module->categorie_racine != 0)
{
    $html .= form_fieldset(lang('categories'), array('class' => 'col-md-6'));
    if (isset($produit))
    {
        $url_categories = '/catalogue/admin/produits/selection_categories/'.$produit->id;
    }
    else
    {
        $url_categories = '/catalogue/admin/produits/selection_categories/0/'.$module->id;
    }
    $html .= anchor($url_categories, lang('selectionner_categories'), array(
        'title' => lang('selection_categories'),
        'class' => 'btn btn-info open-dialogbox'
    ));
    $html .= br().'<div id="categories-associees" class="association">'.$categories_associees.'</div>';
    $html .= form_fieldset_close();
    
}

//Médias
$html .= form_fieldset(lang('medias_associes'), array('class' => (($module->categorie_racine == 0) ? 'col-md-12' : 'col-md-6')));
if (isset($produit))
{
    $url_medias = '/medias/admin/medias/widget_liste/type_insertion/3/type_liaison/'.Media::LIAISON_PRODUIT.'/id/'.$produit->id;
}
else
{
    $url_medias = '/medias/admin/medias/widget_liste/type_insertion/3';
}
$html .= anchor($url_medias, lang('selectionner_medias'), array(
    'title' => lang('bibliotheque_medias'),
    'class' => 'btn btn-info open-dialogbox'
));
$html .= br(2).$medias_associes;

$html .= form_fieldset_close().'</div>';

$html .= '<div class="row">';

//Produits associés
$html .= form_fieldset(lang('produits_associes'), array('class' => 'col-md-6'));

if (isset($produit))
{
    $url_produits = '/catalogue/admin/produits/selection_associes/0/0/0/'.$produit->id;
}
else
{
    $url_produits = '/catalogue/admin/produits/selection_associes';
}

$html .= anchor($url_produits, lang('selectionner_produits_associes'), array(
    'title' => lang('selection_produits_associes'),
    'class' => 'btn btn-info open-dialogbox'
));
$html .= br().'<div id="produits-associes" class="association">'.$produits_associes.'</div>';

$html .= form_fieldset_close();


//Produits composes

$html .= form_fieldset('Produits composés', array('class' => 'col-md-6'));

if (isset($produit))
{
    $url_produits = '/catalogue/admin/produits/selection_associes/0/0/1/'.$produit->id;
}
else
{
    $url_produits = '/catalogue/admin/produits/selection_associes/0/0/1';
}

$html .= anchor($url_produits, 'Sélectionnez les produits composés', array(
    'title' =>  'Sélectionnez les produits composés',
    'class' => 'btn btn-info open-dialogbox'
));
$html .= br().'<div id="produits-composes" class="association">'.$produits_composes.'</div>';

$html .= form_fieldset_close();

$html .= '</div>';
$html .= '<div class="row">';

//Produits complementaires
$html .= form_fieldset('Produits complémentaires', array('class' => 'col-md-6'));

if (isset($produit))
{
    $url_produits = '/catalogue/admin/produits/selection_associes/1/0/0/'.$produit->id;
}
else
{
    $url_produits = '/catalogue/admin/produits/selection_associes/1/0/0';
}

$html .= anchor($url_produits, 'Sélectionnez les produits complémentaires', array(
    'title' =>  'Sélectionnez les produits complémentaires',
    'class' => 'btn btn-info open-dialogbox'
));
$html .= br().'<div id="produits-complementaires" class="association">'.$produits_complementaires.'</div>';

$html .= form_fieldset_close();


//Produits imposés
$html .= form_fieldset('Produits Imposés', array('class' => 'col-md-6'));

if (isset($produit))
{
    $url_produits = '/catalogue/admin/produits/selection_associes/0/1/0/'.$produit->id;
}
else
{
    $url_produits = '/catalogue/admin/produits/selection_associes/0/1/0';
}


$html .= anchor($url_produits, 'Sélectionnez les produits imposés', array(
    'title' =>  'Sélectionnez les produits imposés',
    'class' => 'btn btn-info open-dialogbox'
));
$html .= br().'<div id="produits-imposes" class="association">'.$produits_imposes.'</div>';

$html .= form_fieldset_close();

$html .= '</div>';
$html .= '<div class="row">';

//Références
$html .= form_fieldset(lang('references'), array('class' => 'col-md-12'));

if (isset($produit))
{
    $url_produits = '/catalogue/admin/produits/selection_enfants/'.$produit->id;
}
else
{
    $url_produits = '/catalogue/admin/produits/selection_enfants';
}
$html .= anchor($url_produits, lang('selectionner_references'), array(
    'title' => lang('selection_references'),
    'class' => 'btn btn-info open-dialogbox'
));
$html .= br().'<div id="produits-enfants" class="association">'.$produits_enfants.'</div>';

$html .= form_fieldset_close();
$html .= '</div>';

//Critères
if (isset($criteres_associes) && ! empty($criteres_associes))
{
    $html .= form_fieldset(lang('criteres'));
    $html .= '<div id="criteres-associes" class="association">'.$criteres_associes.'</div>';
    $html .= form_fieldset_close();
}

$this->load->models(array('utilisateurs/groupe_administrateur'));
$groupe_user = new Groupe_administrateur();
$groupe_user->where('utilisateur_id', $admin->id)->get();

//Referencement
if (($groupe_user->option_referencement) or ($admin->id == 1))
{
    $html .= form_fieldset(lang('referencement'), array('style' => 'clear:both;'));
    
    $html .= '<p class="ligne-champ">'.form_label(lang('seo_titre'), 'seo_titre');
    $html .= form_input(array(
        'name'	=> 'seo_titre',
        'id'	=> 'seo_titre',
        'class'	=> 'width-300',
        'value'	=> isset_value($produit, 'seo_titre')
    )).'</p>';
    
    $html .= '<p class="ligne-champ">'.form_label(lang('seo_mots_cles'));
    $html .= form_input(array(
        'name'	=> 'seo_mots_cles',
        'id'	=> 'seo_mots_cles',
        'class'	=> 'width-300',
        'value'	=> isset_value($produit, 'seo_mots_cles')
    )).'</p>';
    
    $html .= '<p class="ligne-champ">'.form_label(lang('seo_description'), 'seo_description');
    $html .= form_textarea(array(
        'id' 	=> 'seo_description',
        'name'	=> 'seo_description',
        'class'	=> 'width-300',
        'cols'	=> 31,
        'rows'	=> 2,
        'value'	=> isset_value($produit, 'seo_description')
    )).'</p>';
    $html .= '<p class="ligne-champ">'.form_label(lang('seo_reecriture'));
    $html .= form_input(array(
        'name'	=> 'seo_reecriture',
        'id'	=> 'seo_reecriture',
        'class'	=> 'width-300',
        'value'	=> isset_value($produit, 'seo_reecriture')
    )).'</p>';
    $html .= form_fieldset_close();
}

//Bouton enregistrer
$html .= br().form_button(array(
    'type' 	=> 'submit',
    'class'	=> 'btn btn-success pull-right enregistrer',
    'name'	=> 'submit',
    'value'	=> TRUE,
    'content' => '<span class="glyphicon glyphicon-floppy-save"></span> '.lang('enregistrer')
));
$html .= form_close();



echo $html;