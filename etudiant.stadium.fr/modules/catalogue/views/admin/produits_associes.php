<?php
$liste = NULL;

if ( ! is_null($produits))
{
    foreach ($produits as $produit)
    {
        $liste .= '<li>'.form_hidden($type_assoc.'[]', $produit->id).'<span class="libelle">['.$produit->reference.'] '.$produit->libelle.'</span>';
        $liste .= anchor(NULL, '<span class="glyphicon glyphicon-trash"></span>', array('class' => 'pull-right action-supprimer')).'</li>';
    }
}

echo $liste;