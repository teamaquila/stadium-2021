<?php
//URL de retour
if ( ! is_null($categorie))
{
	$url_retour = construire_url(array('section' => $section->segment_url(), 'categorie' => $categorie->url()));
}
else
{
	$url_retour = construire_url(array('section' => $section->segment_url()));
}

//Note
$note_produit = NULL;
if ( ! is_null($note))
{
	$note_produit .= '<div id="note">';
	
	for ($i = 1; $i <= 5; $i++)
	{
		if ($i <= $note)
		{
			$note_produit .= '<span class="glyphicon glyphicon-star"></span>';
		}
		else
		{
			$note_produit .= '<span class="glyphicon glyphicon-star-empty"></span>';
		}
	}
	$note_produit .= '</div>';
}

$alt_images = 'Acheter '.$produit->libelle.' Stadium ';

if($produit->medias_associes()->exists())
{
    $index_diapo =0;
    $diaporama = '<ul class="diaporama_produit">';
    $thumbnail = '<div id="bx-pager">';
    foreach ($produit->medias_associes() as $media)
    {
        $diaporama .= '<li><img itemprop="image" src="/medias/images/size/'.$module->taille_image_fiche.'/'.$media->dossier.$media->fichier.'" alt="'.$alt_images.'"></li>';
        $thumbnail .= '<span><a href="javascript:void(0)" data-slide-index="'.$index_diapo.'"><img src="/medias/images/size/80x70/'.$media->dossier.$media->fichier.'"  alt="'.$alt_images.'"></a></span>';
        
        $index_diapo ++;
    }
    
    $thumbnail .= '</div>';
    $diaporama .= '</ul>';
}
else
{
    $thumbnail = '';
    $diaporama = '<img itemprop="image" src="/assets/img/img-non-disponible-fiche.jpg" alt="'.$alt_images.'">';
}



//Produit
$data = array(
	'id' 			=> $produit->id,
	'libelle'		=> $produit->libelle,
    'libelle2'		=> $produit->libelle2,
	'reference'		=> $produit->reference,
	'resume'		=> $produit->resume,
	'description' 	=> $produit->description,
	'non_commandable'=> $produit->non_commandable,
	'note'			=> $note_produit,
	'chiffre_note'	=> $note,
    'nb_note'       => $produit->nb_note(),
	'nouveau' 		=> $produit->nouveau,
	'important' 	=> ($produit->important) ? 'important' : NULL,
    'produits_associes' => $produits_associes,
    'produits_complement' => $produits_complement,
	'critere' 		=> NULL,
	'panier'		=> $panier,
    'images' 		=> $thumbnail.$diaporama,
	'commentaire' 	=> $commentaire,
	'visible'		=> ($visible && !$produit->non_commandable),
	'poids' 		=> array(
		'valeur' => $produit->poids,
		'unite'  => $module->unite_poids
	),
	'url' => array(
		'retour' => $url_retour
	),
	'image_section'	=> $image_section,
	'fil_ariane'	=> $fil_ariane,
    'seo'	=> $produit->resume,
);

// //Image
// $medias = $produit->medias_associes();
// if ($medias->exists())
// {
// 	$data['images'] = html_media($medias, array('taille' => $module->taille_image_fiche, 'libelle' => $produit->libelle));
// }
// else
// {
// 	$data['images'] = '<img src="/assets/img/img-non-disponible-fiche.jpg" alt="Photo indisponible '.$produit->libelle.'" />';
// }


//Critères
if ( ! is_null($criteres_associes) && ($criteres_associes->result_count() > 0))
{
	$liste_criteres = array();
	
	foreach ($criteres_associes as $critere_associe)
	{
		if ( ! empty($critere_associe->criteres_valeur_valeur))
		{
			$liste_criteres[$critere_associe->critere_id]['libelle'] = $critere_associe->critere_libelle;
			$liste_criteres[$critere_associe->critere_id]['valeurs'][] = array(
				'valeur' 	=> $critere_associe->criteres_valeur_valeur,
				'media'		=> html_media($critere_associe->criteres_valeur->media_associe(Media::IMAGE, $produit->id), array('taille' => '50x50'))
			);
		}
		else
		{
			$liste_criteres[$critere_associe->critere_id][] = array(
				'libelle' 	=> NULL,
				'valeur' 	=> NULL,
				'media'		=> NULL
			);
		}
	}
	$data['critere'] = $liste_criteres;
}

$this->dwootemplate->output(tpl_path('catalogue/fiche.tpl'), $data);