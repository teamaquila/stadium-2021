<?php
require_once APPPATH.'controllers/admin/back.php';

/**
 * Commentaire produits
 * 
 * @author 		Nathan Rivière
 * @package 	catalogue
 * @category 	Controllers
 * @version 	1.0
 */
class Commentaires extends Back
{
	const ITEM_PAR_PAGE = 15;
	
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->lang->load('catalogue');
		$this->load->models(array('module_catalogue', 'commentaire', 'medias/media'));
	}
	
	/**
	 * Lister les commentaires
	 * 
	 * @param	integer $module_id
	 * @param	integer $offset
	 * @return	VIEW
	 */
	public function lister($module_id, $offset = 0)
	{
		$this->_module	= $this->module_catalogue->get_by_id($module_id);
		$this->_offset 	= $offset;
		
		/*
		 * Conditions par défaut
		 */
		$conditions = array(
			'module_id'	=> $this->_module->id,
			'tri' => array(
				'champ' => 'created',
				'ordre'	=> 'ASC'
			)
		);
		
		if ($post = $this->input->post())
		{
			$conditions = array_merge($conditions, $post);
		}

		$total_item = $this->commentaire->lister($conditions);
		
		//Pagination
		$this->pagination->initialize(array(
			'base_url'		=> '/catalogue/admin/commentaires/lister/'.$this->_module->id,
			'first_url'		=> '/catalogue/admin/commentaires/lister/'.$this->_module->id.'?reload=true',
			'suffix'		=> '?reload=true',
			'total_rows'	=> $total_item,
			'per_page'		=> self::ITEM_PAR_PAGE,
			'cur_page'		=> $offset
		));
		
		$data = array(
			'commentaires'	=> $this->commentaire->lister($conditions, $offset, self::ITEM_PAR_PAGE),
			'pagination'	=> $this->pagination->create_links(),
			'module'		=> $this->_module,
			'offset'		=> $offset,
			'total_item'	=> $total_item
		);
		
		if ($this->input->get('reload'))
		{
			exit(json_encode(array(
				'liste'			=> $this->load->view('admin/commentaires/lister_ajax', $data, TRUE),
				'total_item' 	=> $total_item,
				'pagination'	=> $this->pagination->create_links()
			)));
		}
		else
		{
			$data['lister_ajax'] = $this->load->view('admin/commentaires/lister_ajax', $data, TRUE);
			$this->load->view('admin/commentaires/lister', $data);
		}
	}
	
	/**
	 * Supprimer les commentaires sélectionnés
	 * 
	 * @param 	number $offset
	 * @return void|boolean
	 */
	public function supprimer($offset = 0)
	{
		$post = $this->input->post();
		$ids = $_POST['commentaire'];
		if (empty($ids))
		{
			return;
		}
		
		if ($this->input->post('submit'))
		{
			$offset = $this->input->post('offset');
			if ($offset > 0)
			{
				$liste = $this->commentaire->lister($post, $offset, self::ITEM_PAR_PAGE)->result_count();
				$nombre = $liste - count($ids);
				if ($nombre == 0 || $nombre == null || !isset($nombre))
				{
					$offset = $offset-self::ITEM_PAR_PAGE;
				}
			}
			foreach($ids as $id)
			{
				$commentaire = $this->commentaire->get_by_id($id);
				$produit = new Produit();
				$module_id = $produit->get_by_reference($commentaire->produit_reference)->module_catalogue_id;
				if ($commentaire->supprimer())
				{
					$this->log->enregistrer(array(
						'administrateur_id' => $this->administrateur_connecte->id,
						'type'			=> LOG_SUPPRESSION,
						'description'	=> lang('log_suppression_commentaire'),
						'adresse_ip'	=> $this->input->ip_address(),
						'user_agent'	=> $this->input->user_agent()
					));
				}
				else
				{
					echo json_encode(array(
						'erreur'	=> TRUE,
						'message'	=> lang('erreur_supression')
					));
					return;
				}
			}
			echo json_encode(array(
				'erreur'	=> FALSE,
				'message'	=> lang('succes_suppression'),
				'url'		=> array(
					'liste' 	=> '/catalogue/admin/commentaires/lister/'.$module_id.'/'.$offset.'?reload=true&reset=true',
				)
			));
			return TRUE;
		}
		else
		{
			$data = array(
				'parametres'=> $this->_parametres,
				'ids'		=> $ids,
				'offset' => $offset
			);
			 
			echo json_encode(array(
				'title'	=> 'Supprimer les commentaires',
				'html'	=> $this->load->view('admin/commentaires/supprimerSelection', $data, TRUE)
			));
		}
	}
	
	/**
	 * Supprimer les commentaires sélectionnés
	 *
	 * @param 	number $offset
	 * @return void|boolean
	 */
	public function rendre_visible($offset = 0)
	{
		$post = $this->input->post();
		$ids = $_POST['commentaire'];
		if (empty($ids))
		{
			return;
		}
	
		if ($this->input->post('submit'))
		{
			$offset = $this->input->post('offset');
			if ($offset > 0)
			{
				$liste = $this->commentaire->lister($post, $offset, self::ITEM_PAR_PAGE)->result_count();
				$nombre = $liste - count($ids);
				if ($nombre == 0 || $nombre == null || !isset($nombre))
				{
					$offset = $offset-self::ITEM_PAR_PAGE;
				}
			}
			foreach($ids as $id)
			{
				$commentaire = $this->commentaire->get_by_id($id);
				$produit = new Produit();
				$module_id = $produit->get_by_reference($commentaire->produit_reference)->module_catalogue_id;
				if ($commentaire->rendre_visible())
				{
					$this->log->enregistrer(array(
							'administrateur_id' => $this->administrateur_connecte->id,
							'type'			=> LOG_MODIFICATION,
							'description'	=> lang('log_visible_commentaire'),
							'adresse_ip'	=> $this->input->ip_address(),
							'user_agent'	=> $this->input->user_agent()
					));
				}
				else
				{
					echo json_encode(array(
							'erreur'	=> TRUE,
							'message'	=> lang('erreur_supression')
					));
					return;
				}
			}
			echo json_encode(array(
					'erreur'	=> FALSE,
					'message'	=> lang('succes_modification'),
					'url'		=> array(
							'liste' 	=> '/catalogue/admin/commentaires/lister/'.$module_id.'/'.$offset.'?reload=true&reset=true',
					)
			));
			return TRUE;
		}
		else
		{
			$data = array(
					'parametres'=> $this->_parametres,
					'ids'		=> $ids,
					'offset' => $offset
			);
	
			echo json_encode(array(
					'title'	=> 'Rendre visible les commentaires',
					'html'	=> $this->load->view('admin/commentaires/rendrevisibleSelection', $data, TRUE)
			));
		}
	}
	/**
	 * Supprimer les commentaires sélectionnés
	 *
	 * @param 	number $offset
	 * @return void|boolean
	 */
	public function rendre_invisible($offset = 0)
	{
		$post = $this->input->post();
		$ids = $_POST['commentaire'];
		if (empty($ids))
		{
			return;
		}
	
		if ($this->input->post('submit'))
		{
			$offset = $this->input->post('offset');
			if ($offset > 0)
			{
				$liste = $this->commentaire->lister($post, $offset, self::ITEM_PAR_PAGE)->result_count();
				$nombre = $liste - count($ids);
				if ($nombre == 0 || $nombre == null || !isset($nombre))
				{
					$offset = $offset-self::ITEM_PAR_PAGE;
				}
			}
			foreach($ids as $id)
			{
				$commentaire = $this->commentaire->get_by_id($id);
				
				$produit = new Produit();
				$module_id = $produit->get_by_reference($commentaire->produit_reference)->module_catalogue_id;
				if ($commentaire->rendre_invisible())
				{
					$this->log->enregistrer(array(
							'administrateur_id' => $this->administrateur_connecte->id,
							'type'			=> LOG_MODIFICATION,
							'description'	=> lang('log_visible_commentaire'),
							'adresse_ip'	=> $this->input->ip_address(),
							'user_agent'	=> $this->input->user_agent()
					));
				}
				else
				{
					echo json_encode(array(
							'erreur'	=> TRUE,
							'message'	=> lang('erreur_supression')
					));
					return;
				}
			}
			echo json_encode(array(
					'erreur'	=> FALSE,
					'message'	=> lang('succes_modification'),
					'url'		=> array(
							'liste' 	=> '/catalogue/admin/commentaires/lister/'.$module_id.'/'.$offset.'?reload=true&reset=true',
					)
			));
			return TRUE;
		}
		else
		{
			$data = array(
					'parametres'=> $this->_parametres,
					'ids'		=> $ids,
					'offset' => $offset
			);
	
			echo json_encode(array(
					'title'	=> 'Rendre visible les commentaires',
					'html'	=> $this->load->view('admin/commentaires/rendreinvisibleSelection', $data, TRUE)
			));
		}
	}
	
	/**
	 * Afficher le commentaire
	 * 
	 * @param 	integer
	 * @return 	VIEW
	 */
	public function afficher($id)
	{
		$commentaire = $this->commentaire->get_by_id($id);
		$data = array(
			'commentaire'	=> $commentaire
		);
		$commentaire->lu = TRUE;
		$commentaire->save();
		$this->load->view('admin/commentaires/afficher', $data);
	}
	
	/**
	 * Réponse du vendeur au commentaire du client
	 * Le commentaire sera visible sur le site et un email informera le client que le vendeur lui a répondu
	 */
	public function reponse_client()
	{
		$this->load->models(array('boutique/module_boutique', 'boutique/client'));
		
		$module_boutique = new Module_boutique();
		$module_boutique->get_by_id(1);
		
		$parametres = $this->_parametres;
		
		if ($this->input->post('reponse'))
		{
			$commentaire = new Commentaire();
			$commentaire->get_by_id($this->input->post('commentaire_id'));
			
			if ($commentaire->exists())
			{
				$commentaire->reponse = $this->input->post('reponse');
				$commentaire->save();
				
				/**
				 * Envoi du mail au client
				 */
				$utilisateur = new Utilisateur();
				$utilisateur->get_by_id($commentaire->utilisateur_id);
				
				$produit = new Produit();
				$produit->get_by_reference($commentaire->produit_reference);

				$this->load->library('email');
				$data = array(
					'commentaire'	=> $commentaire,
					'client'		=> $utilisateur,
					'module'		=> $module_boutique,
					'produit'		=> $produit,
					'parametres'	=> $parametres
				);
				$template = $this->load->view('catalogue/admin/commentaires/email_reponse', $data, TRUE);
				
				$this->email->clear();
				$this->email->from($module_boutique->vendeur_email, $module_boutique->vendeur_societe);
				$this->email->to($utilisateur->email);
				$this->email->subject("Une réponse à votre commentaire sur le site ".$module_boutique->vendeur_societe);
				$this->email->message($template);
				
				$this->email->send();
				
				echo json_encode(array(
						'erreur'	=> FALSE,
						'message'	=> "Votre réponse a été envoyée au client et ajoutée à la fiche du produit."
				));
				return;
			}
		}
		echo json_encode(array(
			'erreur'	=> TRUE,
			'message'	=> "Aucune réponse saisie"
		));
		return;
	}
}

/* End of file commentaires.php */
/* Location: ./modules/catalogue/controllers/admin/commentaires.php */