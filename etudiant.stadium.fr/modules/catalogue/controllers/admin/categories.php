<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

require_once APPPATH.'controllers/admin/back.php';

/**
 * Catégories :
 * Contrôleur permettant la gestion des catégories.
 * 
 * @author 		Pauline MARTIN
 * @package		catalogue
 * @category 	Controllers
 * @version 	2.0
 */
class Categories extends Back
{
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->lang->load('categories/categories');
		$this->load->model('categories/categorie');
	}
	
	/**
	 * Retourne une liste des catégories
	 * 
	 * @param	integer	$racine
	 * @return	VIEW
	 */
	public function lister($racine)
	{
		
		//$racine = (is_null($racine)) ? 1 : $racine;
		
		$data = array(
				'racine' => $racine,
				'categories' => $this->categorie->arbre($racine, FALSE)
		);
		
		if ($this->input->get('reload'))
		{
			echo json_encode(array(
				'liste'	=> $this->load->view('admin/categories/lister_ajax', $data, TRUE),
				'page'	=> 0
			));
		}
		else
		{
			$data['liste_ajax'] = $this->load->view('admin/categories/lister_ajax', $data, TRUE);
			$this->load->view('admin/categories/lister', $data);
		}
	}
	
	/**
	 * Ajouter une catégorie
	 * 
	 * @param	integer	$root_id
	 * @param	integer	$parent_id
	 * @return	void
	 */
	public function ajouter($root_id = NULL, $parent_id = NULL)
	{
		if ($this->input->post('submit'))
		{
			$this->form_validation->set_rules(array(
				array(
					'field' => 'libelle',
					'label' => 'lang:libelle',
					'rules'	=> 'required'
				)
			));
			
			if ($this->form_validation->run($this) === FALSE)
			{ 
				echo json_encode(array(
					'erreur' => TRUE,
					'message'=> validation_errors()
				));
			}
			else
			{
				if ($this->categorie->enregistrer($this->input->post()) !== FALSE)
				{
					$this->log->enregistrer(array(
						'administrateur_id' => $this->administrateur_connecte->id,
						'type'			=> LOG_AJOUT,
						'description'	=> lang('log_ajout_categorie').' '.$this->categorie->libelle,
						'adresse_ip'	=> $this->input->ip_address(),
						'user_agent'	=> $this->input->user_agent()
					));
					
					echo json_encode(array(
						'erreur'	=> FALSE,
						'message'	=> lang('succes_ajout'),
						'url'		=> array(
							'liste' => '/catalogue/admin/categories/lister?reload=true',
						)
					));
				}
				else
				{
					echo json_encode(array(
						'erreur'	=> TRUE,
						'message'=> lang('erreur_ajout'),
					));
				}
			}
			return;
		}
		
		$data = array(
			'parametres'=> $this->_parametres,
			'categorie'	=> NULL,
			'medias' 	=> modules::run('medias/admin/medias/inserer'),
			'root_id'	=> (is_null($root_id)) ? $this->categorie->last_root_id()+1 : $root_id, 
			'parent_id'	=> $parent_id,
			'admin'		=> $this->administrateur_connecte
		);
		$this->load->view('admin/categories/formulaire', $data);
	}
	
	/**
	 * Modifier une catégorie
	 * 
	 * @param	integer	$root_id
	 * @param	integer	$id
	 * @return	TRUE
	 */
	public function modifier($root_id, $id)
	{
		$categorie = $this->categorie->get_by_id($id);
		
		if ($categorie->exists())
		{
			if ($this->input->post('submit'))
			{
				$this->form_validation->set_rules(array(
					array(
						'field' => 'libelle',
						'label' => 'lang:libelle',
						'rules'	=> 'required'
					)
				));
				
				if ($this->form_validation->run($this) === FALSE)
				{ 
					echo json_encode(array(
						'erreur' => TRUE,
						'message'=> validation_errors()
					));
				}
				else
				{
					if ($this->categorie->enregistrer($this->input->post()) !== FALSE)
					{
						$this->log->enregistrer(array(
							'administrateur_id' => $this->administrateur_connecte->id,
							'type'			=> LOG_MODIFICATION,
							'description'	=> lang('log_modification_categorie').' '.$this->categorie->libelle,
							'adresse_ip'	=> $this->input->ip_address(),
							'user_agent'	=> $this->input->user_agent()
						));
						
						echo json_encode(array(
							'erreur'	=> FALSE,
							'message'	=> lang('succes_modification'),
							'url'		=> array(
								'liste' => '/catalogue/admin/categories/lister/'.$this->categorie->root_id.'?reload=true'
							)
						));
					}
					else
					{
						echo json_encode(array(
							'erreur'	=> TRUE,
							'message'	=> lang('erreur_modification'),
						));
					}
				}
				return;
			}
			
			$data = array(
				'parametres'=> $this->_parametres,
				'categorie'	=> $categorie,
				'medias' 	=> modules::run('medias/admin/medias/inserer', $categorie->medias_associes(FALSE)),
				'root_id'	=> $root_id,
				'admin'		=> $this->administrateur_connecte
			);
			$this->load->view('admin/categories/formulaire', $data);
		}
	}
	
	/**
	 * Supprimer une catégorie
	 * 
	 * @param	integer	$id
	 * @return	VIEW
	 */
	public function supprimer($id)
	{
		$categorie = $this->categorie->get_by_id($id);
		
		if ($categorie->exists())
		{
			if ($this->input->post('submit'))
			{
				$libelle = $categorie->libelle;
				
				if ($categorie->supprimer() !== FALSE)
				{
				    $log = new Log();
				    $log->enregistrer(array(
				        'utilisateur_id' => $this->administrateur_connecte->id,
						'administrateur_id' => $this->administrateur_connecte->id,
						'type'			=> LOG_SUPPRESSION,
						'description'	=> lang('log_suppression_categorie').' '.$libelle,
						'adresse_ip'	=> $this->input->ip_address(),
						'user_agent'	=> $this->input->user_agent()
					));
				
					exit(json_encode(array(
						'erreur'	=> FALSE,
						'message'	=> lang('succes_suppression'),
						'url'		=> array(
							'liste' => '/catalogue/admin/categories/lister?reload=true',
						)
					)));
				}
			}
			
			$data = array(
				'parametres'=> $this->_parametres,
				'categorie'	=> $categorie
			);
			$this->load->view('admin/categories/supprimer', $data);
		}
	}
}

/* End of file categories.php */
/* Location: ./modules/catalogue/controllers/admin/categories.php */