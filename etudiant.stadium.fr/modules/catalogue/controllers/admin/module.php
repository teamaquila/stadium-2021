<?php
require_once APPPATH.'controllers/admin/back.php';

/**
 * Catalogue
 * 
 * @author 		Pauline MARTIN
 * @package 	catalogue
 * @category 	Controllers
 * @version 	2.0
 */
class Module extends Back
{
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->load->models(array('module_catalogue', 'type_module'));
	}
	
	/**
	 * Afficher le module avec les onglets
	 * 
	 * @param	integer	$id
	 * @return	VIEW
	 */
	public function index($id)
	{
		$module	 = $this->module_catalogue->get_by_id($id);
		$section = $module->section->get();
		
		$data = array(
			'tabs' => array(
				array(
					'url' 	  => '/catalogue/admin/produits/lister/'.$id,
					'libelle' => strip_tags_br($section->libelle)
				),
				array(
						'url' 	  => '/catalogue/admin/criteres/lister/'.$id,
						'libelle' => lang('criteres')
				)
			)
		);
		
		/*if ($module->categorie_racine != 0)
		{
			array_push($data['tabs'], array(
					'url' 	  => '/catalogue/admin/categories/lister/'.$module->id,
					'libelle' => lang('categories')
			));
		}*/
			
		if ($module->afficher_commentaires)
		{
			array_push($data['tabs'], array(
					'url' 	  => '/catalogue/admin/commentaires/lister/'.$id,
					'libelle' => lang('commentaires')
			));
		}
			
		
		//Super admin peut accéder aux options, aux commentaires et aux critères
		if ($this->administrateur_connecte->superadmin == 1)
		{
			
			array_push($data['tabs'], array(
				'url' 	  => '/catalogue/admin/module/options/'.$id,
				'libelle' => lang('options')
			));
		}
		
		$this->load->view('onglets', $data);
	}
	
	/**
	 * Formulaire pour gérer les options du module
	 * 
	 * @param 	integer $id
	 * @return	VIEW
	 */
	public function options($id)
	{
		$this->lang->load('catalogue');
		$this->load->model('categories/categorie');
		
		$module = $this->module_catalogue->get_by_id($id);
		
		if ($this->input->post())
		{
			if ($module->enregistrer($this->input->post()) !== FALSE)
			{
				$this->log->enregistrer(array(
					'administrateur_id' => $this->administrateur_connecte->id,
					'type'			=> LOG_CONFIGURATION,
					'description'	=> lang('log_configuration_catalogue').' #'.$module->id,
					'adresse_ip'	=> $this->input->ip_address(),
					'user_agent'	=> $this->input->user_agent()
				));
				
				echo json_encode(array(
					'erreur' => FALSE,
					'message'=> lang('succes_modification')
				));
			}
			else
			{
				echo json_encode(array(
					'erreur' => TRUE,
					'message'=> lang('erreur_modification')
				));
			}
			return;
		}
		
		$data = array(
			'module' 	=> $module,
			'categories_racine' => $this->categorie->racines(),
		    'criteres' 	=> $module->critere->where('code', '')->get(),
		    'critere_filtres' => $module->critere->filtres()->all_to_single_array('id')
		);
		$this->load->view('admin/options', $data);
	}
}

/* End of file module.php */
/* Location: ./modules/catalogue/controllers/admin/module.php */