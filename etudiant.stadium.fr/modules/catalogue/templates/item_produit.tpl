<div class="produits" id="produit-{$id}" data-id="{$id}">
		{if $nouveau}
			<span class="nouveau">{lang('catalogue_nouveau')}</span>
		{/if}
		
		<a href="{$url}" class="lien-image" title="{$libelle}">{$image}</a>
		
		<div class="zone-info-pdt">
			<h3><a href="{$url}" title="{$libelle}">{$libelle}</a></h3>	
			{if $commandable}
				<div class="tarification">
					{$panier}
				</div>
			
	
			{else}
				<div class="tarification">
					<span class="prix indispo">{lang('stock_indisponible')}</span>		
				</div>
			{/if}
			<span class="libelle-2">{$libelle2}</span>	
		
		</div>
		
	
</div>