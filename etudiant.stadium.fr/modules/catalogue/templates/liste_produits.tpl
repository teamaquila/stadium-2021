<div id="section-{$section_id}" class="module-catalogue">
	{$fil_ariane}
		<div class="bloc_titre_h1">
			<h1>
				{if $titre_seo != ''}
					{$titre_seo}
				{elseif ! is_null($categorie)}
					{$categorie.libelle}
				{else}
					{$titre_page}
				{/if}
			</h1>
			<span class="compteur-produit">
				{if $total_produit <= 1} <span>Nous vous présentons <span class='montant'>{$total_produit}</span></span> {singular('produit')} {/if}
				{if $total_produit > 1} <span>Nous vous présentons <span class='montant'>{$total_produit}</span></span> {plural('produit')} {/if}
			</span>
		</div>
	<div class="inner_interne">

		{if $afficher_criteres_liste}
			<div id="gestion-filtres">
				{if $afficher_criteres_liste}{$filtres.criteres}{/if}
			</div>{/if}<!-- debug 
	--><div id="liste-produits" class="liste">
			{$liste}
			<div class="clearfix"></div>
<!-- 			<div class="text-center">{$pagination}</div> -->
		</div>
<!-- 		<div class="text-center"> -->
<!-- 			{$pagination} -->
<!-- 		</div>	 -->

	</div>
	<div class="zone-pagination text-center">
		{$pagination}
	</div>
	
	{if $texte_seo != ''}
	<div class="texte-categorie">
		<div class="inner">
			<p>{$texte_seo}</p>
		</div>
	</div>
	{/if}
</div>