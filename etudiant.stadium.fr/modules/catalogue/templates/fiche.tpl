<div class="module-catalogue produit {$important}" id="produit-{$id}">
	{$fil_ariane}

	<div class="inner_interne">
		<div class="diaporama col-md-6">
			{if $nouveau}				
				<span class="nouveau">{lang('catalogue_nouveau')}</span>
				<div class="bloc_diaporama" >{$images}</div>				
			{else}	
				<div class="bloc_diaporama" >{$images}</div>
			{/if}			
		</div>
		<div class="col-md-6">
			<h1>{$libelle}</h1>
			<span class="libelle-2">{$libelle2}</span>	
			{$description}
			{if $visible}				
				{if $formulaire}
					<div class="avec_formulaire" id="bloc_commande">
						{$panier}
					</div>
				{else}
					<div class="sans_formulaire" id="bloc_commande">
						{$panier}
					</div>
				{/if}
			
			{else}
				<div class="indisponible liste-vide bg-warning text-warning">Ce produit n'est pas disponible pour le moment</div>
			{/if}	
			<div id="zone-reassurance">
				<div class="reassurance">
					<a href="/paiement-securise,57.html"><img src="/assets/img/ico-paiement.png" alt="" /></a>
					<a href="/paiement-securise,57.htmll">Paiement sécurisé</a>
				</div>
				<div class="reassurance">
					<a href="/faq,58.html"><img src="/assets/img/ico-livraison.png" alt="" /></a>
					<a href="/faq,58.html">Livraison soignée</a>
				</div>
				<div class="reassurance">
					<a href="/faq,58.html"><img src="/assets/img/ico-question.png" alt="" /></a>
					<a href="/faq,58.html">Une question ?</a>
				</div>
			</div>
		</div>

	</div>
	
	{if $produits_complement}
		<div>
			<h2>Produits complémentaires</h2>
			{$produits_complement}
		</div>
	{/if}

	{if $produits_associes}
		<div>
			<h2>Découvrez aussi</h2>
			{$produits_associes}
		</div>
	{/if}
	
	{if $id == 36 || $id == 136 || $id == 39 || $id == 246 || $id == 170 || $id == 388 || $id == 395 || $id == 366 || $id == 30 || $id == 258 || $id == 304 || $id == 60 || $id == 162 || $id == 189 || $id == 22 || $id == 294 || $id == 318 || $id == 55 || $id == 306}
		<div class="texte-categorie">
			<div class="inner">
				{$seo}
			</div>
		</div>
	{/if}
</div>