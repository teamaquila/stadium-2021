<?php
require_once APPPATH.'controllers/front.php';

/**
 * Import
 * 
 * @author 		Pauline Martin
 * @package		Import
 * @category	Controllers
 * @version 	1.0
 */
class Import extends Front
{
	/**
	 * Constructeur
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->lang->load('catalogue/catalogue');
		$this->load->library('csvreader');
		$this->load->library('email');
		$this->load->helpers(array('csv', 'file', 'directory'));
		$this->load->helper('file');
		$this->load->models(array(
			 'catalogue/produit', 'catalogue/produits_produit', 'catalogue/tarif', 'categories/categorie', 
		    'catalogue/critere', 'catalogue/criteres_valeur', 'catalogue/criteres_valeurs_produit', 'log', 'catalogue/module_catalogue',
		    'boutique/client', 'boutique/adresses_utilisateur', 'disponible/dispo_produit'
		));
	}
	
	/**
	 * Importation des produits
	 * 
	 * @param	boolean
	 * @param	integer
	 */
	public function articles($reset = FALSE, $module_catalogue_id = 1)
	{
	    set_time_limit(0); //20min
	    ini_set('memory_limit', '128M');

	    
	    $ip_autorise = array(
	        '81.252.124.194',
	        '46.18.195.156',
	        '127.0.0.1'
	    );
	    
	    if (in_array($_SERVER['REMOTE_ADDR'], $ip_autorise))
	    {

    	    $dossier_import = scandir(GENERAL_ROOTPATH.'echanges/import/');
    		$fichier_log = root_path().'logs/import_catalogue.log';
    		$fichier_produits = FALSE;
    		$fichier_tarifs = FALSE;
    		
    		$module = $this->module_catalogue->get(1);
    		
    		$fichier_produits 	= GENERAL_ROOTPATH.'echanges/import/ARTICLES.TXT';
    		
    		/*$content = file_get_contents($fichier_produits);
    		//if (mb_detect_encoding($content) == "UTF-16LE")
    		//{
    			$fp = fopen($fichier_produits, 'w');
    			fwrite($fp, iconv("UTF-16LE", "UTF-8", $content));
    			fclose($fp);
    		//}
    		*/
    		if ($this->test_fichier_exists($fichier_produits))
    		{			
    			$lignes_produits = $this->csvreader->parse_file($fichier_produits, FALSE);
    			
    			if (count($lignes_produits) > 0)
    			{
    				$i = 0;
    				$this->benchmark->mark('code_start');
    				
    				/*----------------------------*/
    				/*  Initialise les tables
    				 /*----------------------------*/
    				if ($reset)
    				{
    					//Sauvegarde des tables
    					$backup =& $this->dbutil->backup(array(
    							'tables' => array(
    									't_produits',
    									't_produits_produits', /*'t_criteres_produits',*/ 't_tarifs',
    									/*'t_medias_produits',*/ 'tl_produits_tarifs'
    							)
    					));
    					write_file(ROOTPATH.'import/backup.sql.gz', $backup);
    				
    					//Vide les tables
    					$this->db->truncate('t_produits');
    					//$this->db->truncate('tl_categories_produits');
    					$this->db->truncate('t_produits_produits');
    					//$this->db->truncate('t_criteres_produits');
    					$this->db->truncate('t_tarifs');
    					//$this->db->truncate('t_medias_produits');
    					$this->db->truncate('tl_produits_tarifs');
    				}
    				
    				
    				foreach ($lignes_produits as $index => $ligne)
    				{
    				    if ($ligne[2] == 1 && $ligne[15] == 1)
    					{
    						//Produit
    						$produit = array(
    							'module_catalogue_id' => $module->id,
    							'tva_id' 		=> 1,
    							'poids'			=> ($ligne[4]),
    							'reference' 	=> ($ligne[0]),
    							'reassort'		=> ($ligne[3]), //Permet de savoir si le produit est affiché ou pas
    							//'libelle' 		=> ($ligne[1])
    						);
    						
    						
    							
    						/**
    						 * Pour une mise à jour du produit avec comme clé la référence
    						*/
    						if ( ! empty($ligne[1]))
    						{
    							$produit_reference = $this->produit->get_by_reference($ligne[0]);
    						
    							if ($produit_reference->exists())
    							{
    								$produit['id'] = $produit_reference->id;
    								if (empty($produit_reference->libelle))
    								{
    									$produit['libelle'] = ($ligne[1]);
    								}
    							}
    							else 
    							{
    								$produit['libelle'] = ($ligne[1]);
    							}
    						}
    						
    						//Prix unique
    						$produit['type_client'] = array(1 => array(
    								'unique' => 1,
    								'remise' => 0,
    								'prix_remise' => NULL,
    								'taxe' 	 => 0, //HT
    								'prix_vente' => ($ligne[5])
    						));
    						$this->produit->clear();
    						
    						if ($produit_new = $this->produit->enregistrer($produit))
    						{
    							
    							if (!empty($ligne[11]) && (strlen($ligne[11]) > 2))
    							{
    								//Produits associes basiques/ produits de remplacement
    								$produits_associes_b = explode('/', $ligne[11]);
    							
    								foreach ($produits_associes_b as $index => $reference_associee_b)
    								{
    									$produit_ref_asso_b = new Produit();
    									$produit_ref_asso_b->get_by_reference($reference_associee_b);
    									if ($produit_ref_asso_b->exists())
    									{
    										$test_asso_exists_b = new Produits_produit();
    										$test_asso_exists_b->where('produit_id', $produit_new->id)->where('related_produit_id', $produit_ref_asso_b->id)->where('enfant', FALSE)->where('complement', FALSE)->where('impose', FALSE)->where('compose', FALSE)->get();
    										if (!$test_asso_exists_b->exists())
    										{
    											$produit_asso_b = new Produits_produit();
    											$produit_asso_b->produit_id = $produit_reference->id;
    											$produit_asso_b->related_produit_id = $produit_ref_asso_b->id;
    											$produit_asso_b->enfant = 0;
    							
    											$produit_asso_b->save();
    										}
    									}
    							
    							
    								}
    							}
    							
    							if (!empty($ligne[13]) && (strlen($ligne[13]) > 2))
    							{
    								//Produits complémentaires
    								$produits_associes = explode('/', $ligne[13]);
    						
    								foreach ($produits_associes as $index => $reference_associee)
    								{
    									$produit_ref_asso = new Produit();
    									$produit_ref_asso->get_by_reference($reference_associee);
    									if ($produit_ref_asso->exists())
    									{
    										$test_asso_exists = new Produits_produit();
    										$test_asso_exists->where('produit_id', $produit_new->id)->where('related_produit_id', $produit_ref_asso->id)->where('complement', TRUE)->get();
    										if (!$test_asso_exists->exists())
    										{
    											$produit_asso = new Produits_produit();
    											$produit_asso->produit_id = $produit_reference->id;
    											$produit_asso->related_produit_id = $produit_ref_asso->id;
    											$produit_asso->complement = TRUE;
    											$produit_asso->enfant = 0;
    												
    											$produit_asso->save();
    										}
    									}
    										
    										
    								}
    							}
    						
    						
    							//Code regroupement parent enfant
    							if (!empty($ligne[14]) && (strlen($ligne[14]) > 2))
    							{
    								$produit_parent = new Produit();
    								$produit_parent->get_by_reference($ligne[14]);
    								if (!$produit_parent->exists())
    								{
    									//Produit
    									$obj_produit_parent = array(
    											'module_catalogue_id' => $module->id,
    											'tva_id' 		=> 1,
    											'poids'			=> ($ligne[4]),
    											'reference' 	=> ($ligne[14]),
    											'visible' 		=> ($ligne[3] == 0) ? TRUE : FALSE
    									);
    										
    									//Prix unique
    									$obj_produit_parent['type_client'] = array(1 => array(
    											'unique' => 1,
    											'remise' => 0,
    											'prix_remise' => NULL,
    											'taxe' 	 => 0, //HT
    											'prix_vente' => ($ligne[5])
    									));
    									
    						
    									$this->produit->clear();
    						
    									$produit_parent = new Produit();
    									$produit_parent->enregistrer($obj_produit_parent);
    								}
    						
    								//Liaison de la référence au produit
    								$tab_relation =  array(
    										'produit_id' => $produit_parent->id,
    										'related_produit_id'=> $produit_new->id,
    										'enfant' => 1
    								);
    								$relation_produit_test = new Produits_produit();
    								$relation_produit_test->where($tab_relation)->get();
    								if (!$relation_produit_test->exists())
    								{
    									$this->db->insert('t_produits_produits',$tab_relation);
    								}
    								
    								
    								//test du tarif du parent, si moins cher que l'actuel, on le mets à jour
    								
    								$tarif_parent = new Tarif();
    								$tarif_parent->include_related('produit')->where('produit_id', $produit_parent->id)->get();
    								
    								if ($tarif_parent->exists())
    								{
    									if ((floatval($tarif_parent->prix_vente) == 0) || (floatval($tarif_parent->prix_vente) > floatval(($ligne[5]))))
    									{
    										$obj_taif = new Tarif();
    										$obj_taif->get_by_id($tarif_parent->id);
    										$obj_taif->prix_vente = floatval(($ligne[5]));
    										$obj_taif->save();
    									}
    									
    								}
    								else
    								{
    									$tarif = new Tarif();
    									$tarif->pro  = 0;
    									$tarif->remise 	        = 0;
    									$tarif->prix_remise     = NULL;
    									$tarif->taxe 	        = 0;
    									$tarif->unique = TRUE;
    									$tarif->prix_vente = floatval(($ligne[5]));
    									
    									$tarif->save();
    									
    									
    									//Liaison de la référence au produit
    									$tab_relation =  array(
    											'produit_id' => $produit_parent->id,
    											'tarif_id'=> $tarif->id
    									);
    									$relation_produit_test = new Produits_produit();
    									$relation_produit_test->where($tab_relation)->get();
    									if (!$relation_produit_test->exists())
    									{
    										$this->db->insert('t_produits_tarifs',$tab_relation);
    									}
    								}
    							}
    							
    							
    							/*Produits complementaire*/
    							//Code regroupement parent enfant
    							if (!empty($ligne[13]) && (strlen($ligne[13]) > 2))
    							{
    								$tab_produits_complementaires = explode('/', $ligne[13]);
    								foreach ($tab_produits_complementaires as $code_complement)
    								{
    									$produit_complement = new Produit();
    									$produit_complement->get_by_reference($code_complement);
    									if ($produit_complement->exists())
    									{
    										//Liaison de la référence au produit
    										$tab_relation =  array(
    												'produit_id' => $produit_new->id,
    												'related_produit_id'=> $produit_complement->id,
    												'enfant' => 0,
    												'complement'	=> 1
    										);
    										$relation_produit_test = new Produits_produit();
    										$relation_produit_test->where($tab_relation)->get();
    										if (!$relation_produit_test->exists())
    										{
    											$this->db->insert('t_produits_produits',$tab_relation);
    										}
    										
    										
    										//Association du complement aussi au parent pour affichage sur fiche produit
    										
    										$produit_parent_complement = $produit_new->produit_parent();
    										//Liaison de la référence au produit
    										$tab_relation =  array(
    												'produit_id' => $produit_parent_complement->id,
    												'related_produit_id'=> $produit_complement->id,
    												'enfant' => 0,
    												'complement'	=> 1
    										);
    										$relation_produit_test = new Produits_produit();
    										$relation_produit_test->where($tab_relation)->get();
    										if (!$relation_produit_test->exists())
    										{
    											$this->db->insert('t_produits_produits',$tab_relation);
    										}
    									}
    																	
    								}
    
    							}
    							$i++;
    						}
    					}
    					
    				}
    				
    				$this->import_articles_composes();
    				
    				$this->import_produits_clients();
    				
    				$this->check_infos_parents();
    				
    				$this->benchmark->mark('code_end');
    	    		$temps_traitement = $this->benchmark->elapsed_time('code_start', 'code_end');
    				
    				//Log OM
    				$this->log->enregistrer(array(
    					'utilisateur_id' => 1,
    					'type'			=> LOG_IMPORT,
    					'description'	=> $this->lang->line('log_import_catalogue'),
    					'adresse_ip'	=> $this->input->ip_address(),
    					'user_agent'	=> $this->input->user_agent()
    				));
    				
    				//Log fichier
    				$log_string = ' Stadium | '.mdate('%Y-%m-%d %H:%i:%s').' | '.$temps_traitement.'s | '.$i.' produits'."\n";
    			}
    			else
    			{
    				$log_string = ' Stadium | '.mdate('%Y-%m-%d %H:%i:%s').' | Aucun produit dans le fichier'."\n";
    			}
    			
    		}
    		else
    		{
    			$fichier_manquant = NULL;
    			if (!$this->test_fichier_exists($fichier_produits))
    			{
    				$fichier_manquant .= 'ARTICLES.TXT ';
    			}
    			$log_string = ' Site Transport | '.mdate('%Y-%m-%d %H:%i:%s').' | Fichier absent ou de taille nulle : '.$fichier_manquant."\n";
    		}
    
    		//rename(root_path().'echanges/import/ARTICLES.TXT', root_path().'echanges/import/archives/ARTICLES.TXT');
    		write_file($fichier_log, $log_string, FOPEN_WRITE_CREATE);
	    }
	}
	
	
	public function import_produits_clients()
	{
	    set_time_limit(0); //20min
	    ini_set('memory_limit', '128M');
	    
		$this->load->model('boutique/clients_produit');
		$dossier_import = scandir(GENERAL_ROOTPATH.'echanges/import/');
		$fichier_log = root_path().'logs/import_catalogue.log';
		$fichier_produits = FALSE;
		$fichier_tarifs = FALSE;
		
		$module = $this->module_catalogue->get(1);
		
		$fichier_produits 	= GENERAL_ROOTPATH.'echanges/import/TARIFS.TXT';
		
		/*$content = file_get_contents($fichier_produits);
		//if (mb_detect_encoding($content) == "UTF-16LE")
		//{
		$fp = fopen($fichier_produits, 'w');
		fwrite($fp, iconv("UTF-16LE", "UTF-8", $content));
		fclose($fp);*/
		//}
		
		if ($this->test_fichier_exists($fichier_produits))
		{
			$lignes_produits = $this->csvreader->parse_file($fichier_produits, FALSE);
			
			if (count($lignes_produits) > 0)
			{
			    
				$i = 0;
				$this->benchmark->mark('code_start');
				
				//$this->db->truncate('t_clients_produits');
				$sql = 'TRUNCATE TABLE t_clients_produits;';
				$query = $this->db->query($sql);
				
				if(!$query) {
				    var_dump($this->db->error());die();
				}

				foreach ($lignes_produits as $index => $ligne)
				{ 
					$this->client->clear();

					$clients = $this->client->get_by_code(($ligne[1]));

					$this->produit->clear();

					$produit = $this->produit->get_by_reference(($ligne[0]));
					
					if ($produit->exists() && $clients->exists())
					{
						foreach ($clients as $client)
						{
							//Produit
							$client_produit = new Clients_produit();
							$client_produit->client_id 			= $client->id;
							$client_produit->produit_id 		= $produit->id;
							$client_produit->prix_brut			= ($ligne[2]);
							$client_produit->remise 			= ($ligne[3]);
							$client_produit->prix_net			= ($ligne[4]);
							$client_produit->type_prix_net		= ($ligne[5]);
							$client_produit->save();
								
							//$client_produit->check_last_query();
						}
					}
					
				}					
			}
		}
		
		$this->benchmark->mark('code_end');
		$temps_traitement = $this->benchmark->elapsed_time('code_start', 'code_end');
		
		//Log OM
		$this->log->enregistrer(array(
				'utilisateur_id' => 1,
				'type'			=> LOG_IMPORT,
				'description'	=> $this->lang->line('log_import_catalogue'),
				'adresse_ip'	=> $this->input->ip_address(),
				'user_agent'	=> $this->input->user_agent()
		));
		
		//Log fichier
		$log_string = ' Stadium | '.mdate('%Y-%m-%d %H:%i:%s').' | '.$temps_traitement.'s | '.$i.' produits'."\n";
	}
	
	/**
	 *  Import des associations parents-enfants
	 * */
	public function import_articles_composes()
	{
		$dossier_import = scandir(GENERAL_ROOTPATH.'echanges/import/');
		$fichier_log = root_path().'logs/import_composes.log';
		$fichier_produits = FALSE;
		$fichier_tarifs = FALSE;
		
		$module = $this->module_catalogue->get(1);
		
		$fichier_produits 	= GENERAL_ROOTPATH.'echanges/import/COMPOSES.TXT';
		

		/*$content = file_get_contents($fichier_produits);
		if (mb_detect_encoding($content) == "UTF-16LE")
		{
			$fp = fopen($fichier_produits, 'w');
			fwrite($fp, iconv("UTF-16LE", "UTF-8", $content));
			fclose($fp);
		//}*/
		
		if ($this->test_fichier_exists($fichier_produits))
		{
			$lignes_produits = $this->csvreader->parse_file($fichier_produits, FALSE, FALSE);
				
			if (count($lignes_produits) > 0)
			{
				$i = 0;
				$this->benchmark->mark('code_start');
		
		
				foreach ($lignes_produits as $index => $ligne)
				{
					if (isset($ligne[0]) && isset($ligne[1]))
					{
						$produit_parent = new Produit();
						$produit_parent->get_by_reference(htmlspecialchars($ligne[0]));
						
						$produit_enfant = new Produit();
						$produit_enfant->get_by_reference(htmlspecialchars($ligne[1]));
						
						if ($produit_parent->exists() && $produit_enfant->exists())
						{
							$produit_liaison_test = new Produits_produit();
							$produit_liaison_test->where('compose', TRUE)->where('produit_id', $produit_parent->id)->where('related_produit_id', $produit_enfant->id)->get();

							if (!$produit_liaison_test->exists())
							{
								$produit_liaison = new Produits_produit();
								$produit_liaison->produit_id = $produit_parent->id;
								$produit_liaison->related_produit_id = $produit_enfant->id;
								$produit_liaison->compose = TRUE;
									
								$produit_liaison->save();
							}
						}
					}
				}
		
				//rename(root_path().'echanges/import/COMPOSES.TXT', root_path().'echanges/import/archives/COMPOSES.TXT');
		
				$this->benchmark->mark('code_end');
				$temps_traitement = $this->benchmark->elapsed_time('code_start', 'code_end');
		
				//Log OM
				$this->log->enregistrer(array(
						'utilisateur_id' => 1,
						'type'			=> LOG_IMPORT,
						'description'	=> $this->lang->line('log_import_catalogue'),
						'adresse_ip'	=> $this->input->ip_address(),
						'user_agent'	=> $this->input->user_agent()
				));
		
				//Log fichier
				$log_string = ' Stadium | '.mdate('%Y-%m-%d %H:%i:%s').' | '.$temps_traitement.'s | '.$i.' produits composes'."\n";
			}
			else
			{
				$log_string = ' Stadium | '.mdate('%Y-%m-%d %H:%i:%s').' | Aucun produit dans le fichier'."\n";
			}
				
		}
		else
		{
			$fichier_manquant = NULL;
			if (!$this->test_fichier_exists($fichier_produits))
			{
				$fichier_manquant .= 'ARTICLES.TXT ';
			}
			$log_string = ' Stadium | '.mdate('%Y-%m-%d %H:%i:%s').' | Fichier absent ou de taille nulle : '.$fichier_manquant."\n";
		}
		
		write_file($fichier_log, $log_string, FOPEN_WRITE_CREATE);
		
	}
	
	
	/**
	 * A la fin de l'import, remettre les prix des parents en fonction du moins cher des enfants
	 */
	public function check_infos_parents()
	{
		
		//On récupère les parents
	    $produits_parents_related = new Produits_produit();
	    $produits_parents_related->select('produit_id')->where('enfant', 1)->group_by('produit_id');

	    $produits_parents = new Produit();
    	$produits_parents->where_in_subquery('id', $produits_parents_related)->get();
    	
    	//Pour chaque parent on va récupérer les infos de tous ses enfants 
    	foreach ($produits_parents as $parent)
    	{
    		$prix_parent = 0;
    		$id_enfant_select = NULL;
    		$enfants = $parent->produits_enfants();
    		foreach ($enfants as $enfant)
    		{
    			//O0n récupère le tarif de l'enfant pour comparer aux autres, pour que le parent ai le tarif minimum
    			$tarif_enfant = $enfant->tarif();
    			
    			if (($prix_parent == 0) or ($tarif_enfant['prix'] < $prix_parent))
    			{
    				$prix_parent = $tarif_enfant['prix'];
    				$id_enfant_select = $enfant->id;
    				
    			}
    		}
    		
    		//On associe le tarif de l'enfant le moins cher au parent
    		if (!is_null($id_enfant_select) && ($prix_parent > 0))
    		{
    			$tarif_enfant_pour_parent = new Tarif();
    			$tarif_enfant_pour_parent->include_related('produit')->where('produit_id', $id_enfant_select)->get();
    			
    			$tarif_parent_actuel = new Tarif();
    			$tarif_parent_actuel->include_related('produit')->where('produit_id', $parent->id)->get();
    			
    			$tarif_parent_actuel->pro  	= 0;
    			$tarif_parent_actuel->remise 	        = 0;
    			$tarif_parent_actuel->prix_remise    	= NULL;
    			$tarif_parent_actuel->taxe 	        	= 0;
    			$tarif_parent_actuel->unique 			= TRUE;
    			$tarif_parent_actuel->prix_vente 		= $tarif_enfant_pour_parent->prix_vente;
    					
    			$tarif_parent_actuel->save();
    		}
    	}
	}
	
	
	
	/**
	 * Importation des produits
	 *
	 * @param	boolean
	 * @param	integer
	 */
	public function clients($reset = FALSE)
	{
	    $this->load->dbforge();
	    $this->load->dbutil();
	    $ip_autorise = array(
	        '81.252.124.194',
	        '46.18.195.156',
	        '127.0.0.1'
	    );
	    
	    if (in_array($_SERVER['REMOTE_ADDR'], $ip_autorise))
	    {
    	    //Désactive l'historique des requêtes
    	    $this->db->save_queries = FALSE;
    	    
    	    set_time_limit(1200); //20min
    	    ini_set('memory_limit', '1940M');
    	    
    	    
    	    $dossier_import = scandir(GENERAL_ROOTPATH.'echanges/import/');
    	    $fichier_log = root_path().'logs/import_clients.log';
    	    $fichier_log_clients_doublons = root_path().'logs/import_clients_doublons.log';
    	    $log_string_clients_doublons = NULL;
    	    $fichier_produits = FALSE;
    	    $fichier_tarifs = FALSE;
    	    
    	    $module = $this->module_catalogue->get(1);
    	    
    	    $fichier_clients 		= GENERAL_ROOTPATH.'echanges/import/CLIENTS.TXT';
    	    $fichier_contacts 		= GENERAL_ROOTPATH.'echanges/import/CONTACTS.TXT';
    	    $fichier_livraisons 	= GENERAL_ROOTPATH.'echanges/import/LIVRAISONS.TXT';
    	    
    	    
    	    /*$content = file_get_contents($fichier_clients);
    	     //if (mb_detect_encoding($content) == "UTF-16LE")
    	     //{
    	     $fp = fopen($fichier_clients, 'w');
    	     fwrite($fp, iconv("UTF-16LE", "UTF-8", $content));
    	     fclose($fp);
    	     //}
    	     
    	     $content = file_get_contents($fichier_contacts);
    	     //if (mb_detect_encoding($content) == "UTF-16LE")
    	     //{
    	     $fp = fopen($fichier_contacts, 'w');
    	     fwrite($fp, iconv("UTF-16LE", "UTF-8", $content));
    	     fclose($fp);
    	     //}
    	     
    	     $content = file_get_contents($fichier_livraisons);
    	     //if (mb_detect_encoding($content) == "UTF-16LE")
    	     //{
    	     $fp = fopen($fichier_livraisons, 'w');
    	     fwrite($fp, iconv("UTF-16LE", "UTF-8", $content));
    	     fclose($fp);
    	     //}*/
    	     
    	     if ($this->test_fichier_exists($fichier_clients) && $this->test_fichier_exists($fichier_contacts) && $this->test_fichier_exists($fichier_livraisons))
    	     {
    	         $lignes_clients = $this->parse_file_key($fichier_clients, FALSE);
    	         $lignes_contacts = $this->csvreader->parse_file($fichier_contacts, FALSE);
    	         $lignes_livraisons = $this->csvreader->parse_file($fichier_livraisons, FALSE);
    	         
    	         
    	         if (count($lignes_contacts) > 0)
    	         {
    	             $i = 0;
    	             $this->benchmark->mark('code_start');
    	             
    	             foreach ($lignes_contacts as $index => $ligne_contact)
    	             {
    	                 $abandon = FALSE;
    	                 $code = (trim($ligne_contact[0]));
    	                 
    	                 $id_sage = (trim ($ligne_contact[7]));
    	                 $id_web = (trim ($ligne_contact[8]));
    	                 
    	                 $client = array(
    	                     'pays_id'	=> 66,
    	                     'nom'		=> (trim ($ligne_contact[1])),
    	                     'prenom'	=> (trim ($ligne_contact[2])),
    	                     'identifiant'		=> (trim ($ligne_contact[5])),
    	                     'email'		=> (trim ($ligne_contact[5])),
    	                     'civilite'	=> (trim ($ligne_contact[6])),
    	                     'id_sage'	=> $id_sage
    	                 );
    	                 
    	                 //Récupère la ligne client associée au contact à partir de son code
    	                 if (isset($lignes_clients[$code]) && is_array($lignes_clients[$code]))
    	                 {
    	                     if ((trim ($lignes_clients[$code][18])) == '1')
    	                     {
    	                     
        	                     if ((trim ($lignes_clients[$code][11])) == 'Y') //Si extranet = Y, alors on peut importer le client
        	                     {
        	                         //Produit
        	                         $client['code']				= $code;
        	                         $client['societe']			= (trim ($lignes_clients[$code][1]));
        	                         $client['adresse']			= (trim ($lignes_clients[$code][2]));
        	                         $client['adresse2']			= (trim ($lignes_clients[$code][3]));
        	                         $client['code_postal']	 	= (trim ($lignes_clients[$code][5]));
        	                         $client['ville']	 		= (trim ($lignes_clients[$code][6]));
        	                         $client['telephone']	 	= (trim ($lignes_clients[$code][7]));
        	                         $client['fax']			 	= (trim ($lignes_clients[$code][8]));
        	                         //'email'				=> ($ligne_client[$key_client][9]),
        	                         $client['pro']	= ((trim ($lignes_clients[$code][12]) == 'Y')) ? 0 : 1;
        	                         $client['mail_representant'] = (trim ($lignes_clients[$code][17]));
        	                         
        	                         /**
        	                          * Pour une mise à jour du produit avec comme clé la référence
        	                          */
        	                         /*if (!empty($id_sage) && ($id_sage != 0))
        	                          {
        	                          $client_reference = $this->client->get_by_id_sage($id_sage);
        	                          
        	                          if ($client_reference->exists())
        	                          {
        	                          $client['id'] = $client_reference->id;
        	                          }
        	                          }
        	                          elseif ( ! empty($id_web) && ($id_web != 0))
        	                          {
        	                          $client_reference = $this->client->get_by_id($id_web);
        	                          
        	                          if ($client_reference->exists())
        	                          {
        	                          $client['id'] = $client_reference->id;
        	                          }
        	                          }*/
        	                         
        	                         //Identification par email seulement
        	                         $email = NULL;
        	                         $email = trim($ligne_contact[5]);
        	                         $new_liaison = TRUE;
        	                         //echo $email.br();
        	                         if (!empty($email))
        	                         {
        	                             $client_reference = new Client();
        	                             $client_reference->get_by_identifiant($email);
        	                             
        	                             if ($client_reference->exists())
        	                             {
        	                                 
        	                                 if ($client_reference->result_count() > 1)
        	                                 {
        	                                     $log_string_clients_doublons .= ' Stadium | '.mdate('%Y-%m-%d %H:%i:%s').' | email en doublon : '.$client_reference->result_count().' | '.(trim ($ligne_contact[5])).' '."\n";
        	                                     write_file($fichier_log_clients_doublons, $log_string_clients_doublons, FOPEN_WRITE_CREATE);
        	                                     $abandon = TRUE;
        	                                 }
        	                                 else {
        	                                     $client['id'] = $client_reference->id;
        	                                     $new_liaison = FALSE;
        	                                 }
        	                                 
        	                             }
        	                             
        	                             
        	                             $this->client->clear();
        	                             if (!$abandon)
        	                             {
        	                                 if ($this->client->enregistrer($client))
        	                                 {
        	                                     if($new_liaison)
        	                                     {
        	                                         $this->db->insert('tl_groupes_utilisateurs', array(
        	                                             'utilisateur_id' => $this->client->id,
        	                                             'groupe_id' => 2
        	                                         ));
        	                                     }
        	                                     
        	                                     //$this->client->check_last_query();
        	                                     $i++;
        	                                 }
        	                             }
        	                             
        	                         }
        	                         
        	                         
        	                         
        	                     }
    	                     }
    	                 }
    	             }
    	             
    	             //rename(root_path().'echanges/import/CLIENTS.TXT', root_path().'echanges/import/archives/CLIENTS.TXT');
    	             //rename(root_path().'echanges/import/CONTACTS.TXT', root_path().'echanges/import/archives/CONTACTS.TXT');
    	             
    	             
    	         }
    	         else
    	         {
    	             $log_string = ' Stadium | '.mdate('%Y-%m-%d %H:%i:%s').' | Aucun client dans le fichier'."\n";
    	         }
    	         
    	         if (count($lignes_livraisons) > 0)
    	         {
    	             foreach ($lignes_livraisons as $livraison)
    	             {
    	                 $client_livraison = new Client();
    	                 $client_livraison->where('code', (trim ($livraison[0])))->get();
    	                 
    	                 if ($client_livraison->exists())
    	                 {
    	                     foreach ($client_livraison as $liv)
    	                     {
    	                         $client_adresse = new Adresses_utilisateur();
    	                         
    	                         //Test adresse par dafaut
    	                         $adresse_defaut = new Adresses_utilisateur();
    	                         $adresse_defaut->where('defaut', 1)->where('utilisateur_id', $client_livraison->id)->get();
    	                         
    	                         if ($adresse_defaut->exists())
    	                         {
    	                             $client_adresse->defaut = 0;
    	                         }
    	                         else
    	                         {
    	                             $client_adresse->defaut = 1;
    	                         }
    	                         
    	                         
    	                         
    	                         /**
    	                          * Pour une mise à jour du produit avec comme clé la référence
    	                          */
    	                         if ( ! empty($livraison[1]))
    	                         {
    	                             $client_adresse_reference = new Adresses_utilisateur();
    	                             $client_adresse_reference->get_by_id_sage((trim ($livraison[1])));
    	                             
    	                             if ($client_adresse_reference->exists())
    	                             {
    	                                 $client_adresse->id = $client_adresse_reference->id;
    	                             }
    	                         }
    	                         
    	                         
    	                         $client_adresse->utilisateur_id = $liv->id;
    	                         $client_adresse->id_sage = (trim ($livraison[1]));
    	                         $client_adresse->societe = $liv->societe;
    	                         $client_adresse->nom = $liv->nom;
    	                         $client_adresse->prenom = $liv->prenom;
    	                         $client_adresse->adresse = (trim ($livraison[3]));
    	                         $client_adresse->adresse2 = (trim ($livraison[4]));
    	                         $client_adresse->code_postal = (trim ($livraison[5]));
    	                         $client_adresse->ville = (trim ($livraison[6]));
    	                         $client_adresse->telephone = (trim ($livraison[7]));
    	                         $client_adresse->pays_id = 66;
    	                         
    	                         $client_adresse->save();
    	                         //$client_adresse->check_last_query();
    	                     }
    	                 }
    	             }
    	             
    	         }
    	         
    	         $this->benchmark->mark('code_end');
    	         $temps_traitement = $this->benchmark->elapsed_time('code_start', 'code_end');
    	         
    	         //Log OM
    	         $this->log->enregistrer(array(
    	             'utilisateur_id' => 1,
    	             'type'			=> LOG_IMPORT,
    	             'description'	=> 'Import des clients',
    	             'adresse_ip'	=> $this->input->ip_address(),
    	             'user_agent'	=> $this->input->user_agent()
    	         ));
    	         
    	         //Log fichier
    	         $log_string = ' Stadium | '.mdate('%Y-%m-%d %H:%i:%s').' | '.$temps_traitement.'s | '.$i.' clients'."\n";
    	         //rename(root_path().'echanges/import/LIVRAISONS.TXT', root_path().'echanges/import/archives/LIVRAISONS.TXT');
    	     }
    	     else
    	     {
    	         $fichier_manquant = NULL;
    	         if (!$this->test_fichier_exists($fichier_clients))
    	         {
    	             $fichier_manquant .= 'CLIENTS.TXT ';
    	         }
    	         if (!$this->test_fichier_exists($fichier_contacts))
    	         {
    	             $fichier_manquant .= 'CONTACTS.TXT ';
    	         }
    	         $log_string = ' Site  | '.mdate('%Y-%m-%d %H:%i:%s').' | Fichier absent ou de taille nulle : '.$fichier_manquant."\n";
    	     }
    	     
    	     write_file($fichier_log, $log_string, FOPEN_WRITE_CREATE);
    	     
    	     
    	     /***
    	      * Requete suppression doublons client
    	      * SELECT t_clients.* FROM t_clients LEFT OUTER JOIN ( SELECT MIN(id) as id, email FROM t_clients GROUP BY email ) as t1 ON t_clients.id = t1.id WHERE t1.id IS NULL and t_clients.email IS NOT NULL and t_clients.email != ''
    	      */
	    }
	}
	
	/**
	 * Parse a file containing CSV formatted data.
	 *
	 * @param	string
	 * @param	boolean
	 * @param	boolean
	 * @param	boolean
	 * @return	array
	 */
	public function parse_file_key($p_Filepath, $p_NamedFields = TRUE, $skip_empty_lines = TRUE, $convert_utf8 = FALSE)
	{
		$fields;            /** columns names retrieved after parsing */
		$separator = ';';    /** separator used to explode each line */
		$enclosure = '"';    /** enclosure used to decorate each field */
		
		$max_row_size = 4096;    /** maximum row size to be used for decoding */
		$content = false;
		$file = fopen($p_Filepath, 'r');
	
		if ($p_NamedFields)
		{
			$fields = fgetcsv($file, $max_row_size, $separator, $enclosure);
		}
	
		while( ($row = fgetcsv($file, $max_row_size, $separator, $enclosure)) != false )
		{
			$i = 0;
			if (($row[0] == null) && $skip_empty_lines)
			{
				continue;
			}
			 
			if ( ! $content)
			{
				$content = array();
			}
			if ($p_NamedFields)
			{
				$items = array();
	
				// I prefer to fill the array with values of defined fields
				foreach ($fields as $id => $field)
				{
					if ($convert_utf8)
					{
						$field = utf8_encode($field);
	
						if (isset($row[$id]))
						{
							$items[$field] = utf8_encode($row[$id]);
						}
					}
					else
					{
						if (isset($row[$id]))
						{
							$items[$field] = $row[$id];
						}
					}
	
					if ($i == 0)
					{
						$key = $row[$id];
					}
					$i++;
				}
				$content[(trim($key))] = $items;
			}
			else
			{
				$content[(trim($row[0]))] = $row;
			}
		}
		fclose($file);
		return $content;
	}

	
	function array_searchRecursive( $needle, $haystack, $strict=false, $path=array() )
	{
	    if( !is_array($haystack) ) {
	        return false;
	    }
	 
	    foreach( $haystack as $key => $val ) {
	        if( is_array($val) && $subPath = $this->array_searchRecursive($needle, $val, $strict, $path) ) {
	            $path = array_merge($path, array($key), $subPath);
	            return $path;
	        } elseif( (!$strict && $val == $needle) || ($strict && $val === $needle) ) {
	            $path[] = $key;
	            return $path;
	        }
	    }
	    return false;
	}
	
	/**
	 * Teste si le fichier existe et que sa taille est > 0
	 * */
	private function test_fichier_exists($url_fichier = NULL)
	{
		if (file_exists($url_fichier) && (filesize($url_fichier) > 0))
		{
			return TRUE;
		}
		return FALSE;
	}
	
	/**
	 * Mise à jour des stocks
	 */
	public function stock()
	{
		$fichier_log 	= root_path().'logs/import.log';
		$fichier_stock 	= root_path().'import/catalogue/web-stock-abelia.xml';
		
		
		
		if (file_exists($fichier_stock) && (filesize($fichier_stock) > 0))
    	{
			//Paramétrage temps et mémoire allouée au script
			set_time_limit(20);
	    	ini_set('memory_limit', '128M');
	    	
	    	$nb_reference = 0;
			$nb_produit = 0;
			$produits_parent_id = array();
			
			$this->benchmark->mark('code_start');
			
			//Lecture et mise à jour des données
	        $oXml = simplexml_load_file($fichier_stock);
			
	    	foreach ($oXml->produits->produit as $produit_xml)
	    	{
	    		$this->produit->clear();
				$this->produit->get_by_id($produit_xml->ID_ARCODE);
				$this->produit->stock = ($produit_xml->STOCK_DISPO > 1) ? $produit_xml->STOCK_DISPO : 0;
				//$this->produit->stock = $produit_xml->STOCK_DISPO;
				
				if ($this->produit->exists() && $this->produit->save())
				{
					$nb_reference++;
					$produit_parent = $this->produit->produit_parent();
					
					//Stock le parent pour le mettre à jour
					if ($produit_parent->exists() && ! in_array($produit_parent->id, $produits_parent_id))
					{
						array_push($produits_parent_id, $produit_parent->id);
					}
				}
	    	}
			//Mise à jour du stock des produits
			if (count($produits_parent_id) > 0)
			{
				$produits = $this->produit->where_in('id', $produits_parent_id)->get();
				
				foreach ($produits as $produit)
				{
					$related = new Produits_produit();
		    		$related->select('related_produit_id')->where(array(
						'produit_id' => $produit->id,
						'enfant' => 1
					));
		    		
		    		$references = new Produit();
		    		$references->select('SUM(stock) AS total_stock')->where_in_subquery('id', $related)->get();
					
					$produit->stock = $references->total_stock;
					
					if ($produit->save())
					{
						$nb_produit++;
					}
				}
			}
			
			//Mise à jour des catégories avec les produits en stock
            $this->check_categories_import();
	    	
			//Archive le fichier
			rename($fichier_stock, root_path().'import/catalogue/archives/web-stock-abelia.xml');
            
			//Fin du traitement
	   	 	$this->benchmark->mark('code_end');
			$temps_traitement = $this->benchmark->elapsed_time('code_start', 'code_end');
			
			//Log OM
			$this->log->enregistrer(array(
				'utilisateur_id' => 1,
				'type'			=> LOG_IMPORT,
				'description'	=> lang('log_import_stock'),
				'adresse_ip'	=> $this->input->ip_address(),
				'user_agent'	=> $this->input->user_agent()
			));
			
			//Log fichier
			$log_string = mdate('%Y-%m-%d %H:%i:%s').' | stock | '.$temps_traitement.'s | '.$nb_produit.' produits | '.$nb_reference.' références'."\n";
			write_file($fichier_log, $log_string, FOPEN_WRITE_CREATE);
    	}
		else
		{
			//Log fichier
			$log_string = mdate('%Y-%m-%d %H:%i:%s').' | stock | fichier "web-stock-abelia.xml" indisponible'."\n";
			write_file($fichier_log, $log_string, FOPEN_WRITE_CREATE);
		}
	}
	
    /*
     * Vérification du stock des produits de chaque catégorie pour la mettre visible ou non
     * 
	 * @param	integer
	 * @return	void
     */
    private function check_categories_import($module_catalogue_id = 1)
    {
        $this->load->models(array('catalogue/module_catalogue', 'categories/categorie'));
        $this->load->helpers(array('catalogue/catalogue', 'categories/categorie'));
        
        $categories = $this->categorie->get();
		
        foreach ($categories as $categorie)
        {
            $stock_categorie = FALSE;
            
            $module = $this->module_catalogue->get_by_id($module_catalogue_id);
            $conditions = array(
                'visible'   => TRUE,
                'module_id' => $module->id,
                'categorie' => $categorie->id
            );
            
            foreach ($this->produit->lister($conditions) as $produit)
            {
                //if ($produit->stock > 1)
				if ($produit->stock > 0)
                {
                    $stock_categorie = TRUE;
                }
            }
            $categorie->visible = $stock_categorie;
            $categorie->save();
        }
    }
    
	/**
	 * Affiche les catégories dans le menu
	 * 
	 * @return	VIEW
	 */
	public function menu_catalogue()
	{
		$this->load->model('categories/categorie');
		$this->load->helpers(array('catalogue/catalogue', 'categories/categorie'));
		
		$data = array(
			'section'	=> $this->section->get_by_id(3), //Catalogue,
			'conditions'=> array(
				'visible' => TRUE,
				'stock' => TRUE,
				'enfant' => FALSE
			),
		);
		
		return $this->load->view('abelia/menu_catalogue', $data);
	}
	
	/**
	 * Choix de la référence sur la fiche produit
	 * 
	 * @param	integer
	 * @return	JSON
	 */
    public function reference($produit_id)
    {
        $this->load->helper('catalogue/catalogue');
        $this->load->model('catalogue/module_catalogue');
        $produit = $this->produit->get_by_id($produit_id);
        $module = $produit->module();
        
        if ($produit->exists())
        {
            $tarif = $produit->tarif();
           
            $quantite = NULL;
            if ($module->gestion_stock)
            {
                $tab_quantite = array();
                
                for ($i=1; $i <= $produit->stock ; $i++) { 
                    $tab_quantite[$i] = $i;
                }
                
                $quantite = form_dropdown('quantite', $tab_quantite);
            }
            echo json_encode(array(
                'libelle'   => $produit->libelle,
                'quantite'  => $quantite,
                'stock'     => statut_stock($produit->stock, $module->seuil_alerte_stock),
                'prix'      => array(
                    'vente' => prix($tarif['prix_sans_remise']).nbs().lang('devise'),
                    'achat' => prix($tarif['prix']).nbs().lang('devise')
                ),
                'poids'     => number_format($produit->poids, 3).$module->unite_poids
            ));
        }
    }

	/*
	 * Récupère la dernière actialité de chaque module d'actu se trouvant dans le sous menu de la team 4-20-5
	 * Widget pour la page d'accueil et pages internes
	 * 
	 * @param	integer
	 */
	public function widget_actualites_team($section_team_id=NULL)
	{
		if(!is_null($section_team_id))
		{
			$liste_widget = NULL;
			$this->load->model('actualites/actualite');
			$this->load->model('actualites/module_actualite');
			
			$section = $this->section->get_by_id($section_team_id);
			$sections_enfants = $section->select_root($section->root_id)->/*get_root()->*/dump_tree(array('libelle', 'contenu', 'root_id', 'version_id', 'typemodule_id', 'ouvrir_onglet', 'menu_enrichi', 'afficher_lien'), 'array', TRUE);

			foreach ($sections_enfants as $section_enfant) 
			{
				if ($section_enfant['id'] != $section_team_id)
				{
					if ($section_enfant['typemodule_id'] == 5) //id type module actualite
					{
						$this->section->clear();
						$obj_section_enfant = $this->section->get_by_id($section_enfant['id']);
						$module_section_id = $obj_section_enfant->module();
						
						$conditions = array(
							'publiee'	=> '1'
						);
						$actualite_module = $this->actualite->lister($module_section_id['id'], $conditions, 0, 1);
						foreach ($actualite_module as $actu) 
						{
							$data_actu = array(
								'actualite'	=> $actu,
								'module'	=> $this->module_actualite->get_by_id($module_section_id['id'])
							);
							$liste_widget .= $this->load->view('abelia/item_actualite_widget', $data_actu, TRUE);
						}
					}
				}
			}
		}
		return $liste_widget;
	}

    /**
	 * Lister les actualités d'une section 
	 */
    public function liste_actu_par_section($section_id)
    {
        $liste_widget = NULL;
        
        $section = $this->section->get_by_id($section_id);
        if ($section->typemodule_id == 5) //id type module actualite
        {
            $module_section_id = $section->module();
            
             $conditions = array(
                'publiee'   => '1'
            );
            $actualite_module = $this->actualite->lister($module_section_id['id'], $conditions, 0, 4);
            foreach ($actualite_module as $actu) 
            {
                $data_actu = array(
                    'actualite' => $actu,
                    'module'    => $this->module_actualite->get_by_id($module_section_id['id'])
                );
                $liste_widget .=  $this->load->view('abelia/item_actualite_team', $data_actu, TRUE);
            }
        }
        return $liste_widget;
    }
    
    /**
     * CATEGORIES
     * Afficher les catégories enfants d'une catégorie
     *
     * @param	string	$identifiant
     * @param   integer $categorie_parent_id
     * @param	integer	$section_id
     * @param   boolean $media
     * @return  VIEW
     */
    public function menu($identifiant = NULL, $categorie_parent_id = 1, $section_id = 3, $media = FALSE)
    {
    	$this->load->model('categories/categorie');
    	$data = array(
    			'categories'        => $this->categorie->enfants($categorie_parent_id),
    			'section'           => $this->section->get_by_id($section_id),
    			'afficher_media'    => $media,
    			'categorie_url'		=> $this->categorie->get_by_id($categorie_parent_id),
    			'categorie_parent_id' => $categorie_parent_id
    	);
    	$this->load->view('import/categories_enfants',$data);
    }
    
    
    /**
     * Mets à jour les données avec celles de l'ancien site
     * */
    public function maj_donnees_ancien_site()
    {
    	
    	$liste_criteres = array(
    			'variante' => 'Variante',
    	);
    		
    	
    	$this->load->models(array('medias/media', 'catalogue/medias_produit', 'catalogue/produits_produit'));
    	
    	$dossier_import = scandir(root_path().'echanges/import/ancien');
    	$fichier_log = root_path().'logs/import_catalogue_ancien.log';
    	$fichier_produits = FALSE;
    	$fichier_tarifs = FALSE;
    	
    	$module = $this->module_catalogue->get(1);
    	
    	$fichier_produits 	= root_path().'echanges/import/ancien/produits.csv';
    	
    	$this->db->truncate('tl_categories_produits');
    	
    	if ($this->test_fichier_exists($fichier_produits))
    	{
    		$lignes_produits = $this->csvreader->parse_file($fichier_produits, FALSE, FALSE);
    		
    		if (count($lignes_produits) > 0)
    		{
    			$i = 0;
    			$this->benchmark->mark('code_start');
    	
    	
    			foreach ($lignes_produits as $index => $ligne)
    			{
    				
    					
    				/**
    				 * Pour une mise à jour du produit avec comme clé la référence
    				*/
    				if ( ! empty($ligne[2]))
    				{
    					$this->produit->clear();
    					$produit_reference = $this->produit->get_by_reference(htmlspecialchars($ligne[2]));
    					if ($produit_reference->exists())
    					{
    						$k= 0;
    						
    						//Produit
    						//echo $ligne[3].' '.$ligne[5].' '.$ligne[6].br();
    						$produit_reference->description	= $ligne[15];
    						$produit_reference->libelle	= htmlspecialchars($ligne[3].' '.$ligne[5].' '.$ligne[6]);
    						$produit_reference->visible = 1;

    						
    						
    						if ($produit_reference->save())
    						{

    							//$produit_reference->check_last_query();
    							
    							//Critères
    							foreach ($liste_criteres as $critere_id => $critere_libelle)
    							{
    								//Critère
    								$critere = new Critere();
    								$critere->get_by_libelle($critere_libelle);
    							
    								$this->db->delete('t_criteres_produits', array('critere_id' => $critere->id, 'produit_id' => $produit_reference->id));
    								//echo $this->db->last_query();
    								if ( ! $critere->exists())
    								{
    									$critere->enregistrer(array(
    											'module_catalogue_id' => $module_catalogue_id,
    											'libelle' => $critere_libelle
    									));
    								}
    							
    							
    								$criteres_produit = new Criteres_produit();
    								$criteres_produit->where(array(
    										'critere_id'=> $critere->id,
    										'produit_id'=> $produit_reference->id,
    										'valeur'	=> $ligne[6],
    								))->get();
    							
    								if ( ! $criteres_produit->exists())
    								{
    									$criteres_produit->enregistrer(array(
    											'critere_id' => $critere->id,
    											'produit_id' => $produit_reference->id,
    											'valeur' => $ligne[6]
    									));
    								}
    							}
    							
    							//Catégorie
    							if ( ! empty($ligne[0]))
    							{
    								$categorie_famille = new Categorie();
    								//$libelle_famille = trim(strtolower(str_replace("\'", "'", $this->db->escape_str($ligne[0]))));
    							
    								$categorie_famille->where('libelle', str_replace("\'", "'", $this->db->escape_str($ligne[0])))->get();

    								if ($categorie_famille->exists())
    								{
    									$this->db->insert('tl_categories_produits', array(
    											'categorie_id' 	=> $categorie_famille->id,
    											'produit_id' 	=> $produit_reference->id
    									));
    								}
    							}
    							
    							//Sous-catégorie
    							if ( ! empty($ligne[1]))
    							{
    								$categories_sous_famille = new Categorie();
    								//$libelle_sous_famille = trim(strtolower(str_replace("\'", "'", $this->db->escape_str($ligne[1]))));
    							
    								$categories_sous_famille->where('libelle', str_replace("\'", "'", $this->db->escape_str($ligne[1])))->get();
    								
    								if ($categories_sous_famille->exists())
    								{
    									//Parcours les sous-catégories pour trouver celle avec le bon parent
    									foreach ($categories_sous_famille as $categorie)
    									{
    										$this->categorie->clear();
    										$this->categorie->select_root($categorie->root_id)->get_parent($categorie);
    							
    										if ($this->categorie->id == $categorie_famille->id)
    										{
    											$sous_categorie_id = $categorie->id;
    											$this->db->insert('tl_categories_produits', array(
    													'categorie_id' 	=> $categorie->id,
    													'produit_id' 	=> $produit_reference->id
    											));
    											break;
    										}
    									}
    								}
    							}
    							
    							//Sous-sous-catégorie
    							if ( ! empty($ligne[16]))
    							{
    								$categories_sous_sous_famille = new Categorie();
    								//$libelle_sous_famille = trim(strtolower(str_replace("\'", "'", $this->db->escape_str($ligne[1]))));
    									
    								$categories_sous_sous_famille->where('libelle', str_replace("\'", "'", $this->db->escape_str($ligne[16])))->get();
    							
    								if ($categories_sous_sous_famille->exists())
    								{
    									//Parcours les sous-catégories pour trouver celle avec le bon parent
    									foreach ($categories_sous_sous_famille as $categorie)
    									{
    										$this->categorie->clear();
    										$this->categorie->select_root($categorie->root_id)->get_parent($categorie);
    											
    										if ($this->categorie->id == $sous_categorie_id)
    										{
    											$this->db->insert('tl_categories_produits', array(
    													'categorie_id' 	=> $categorie->id,
    													'produit_id' 	=> $produit_reference->id
    											));
    											break;
    										}
    									}
    								}
    							}
    							
    							$i++;
    						}
    					}
    				}
    	
    				
    			}
    	
    			//rename(root_path().'echanges/import/ARTICLES.TXT', root_path().'echanges/import/archives/ARTICLES.TXT');
    	
    			$this->benchmark->mark('code_end');
    			$temps_traitement = $this->benchmark->elapsed_time('code_start', 'code_end');
    	
    			//Log OM
    			$this->log->enregistrer(array(
    					'utilisateur_id' => 1,
    					'type'			=> LOG_IMPORT,
    					'description'	=> $this->lang->line('log_import_catalogue'),
    					'adresse_ip'	=> $this->input->ip_address(),
    					'user_agent'	=> $this->input->user_agent()
    			));
    	
    			//Log fichier
    			$log_string = ' Stadium | '.mdate('%Y-%m-%d %H:%i:%s').' | '.$temps_traitement.'s | '.$i.' produits'."\n";
    		}
    		else
    		{
    			$log_string = ' Stadium | '.mdate('%Y-%m-%d %H:%i:%s').' | Aucun produit dans le fichier'."\n";
    		}
    			
    	}
    	else
    	{
    		$fichier_manquant = NULL;
    		if (!$this->test_fichier_exists($fichier_produits))
    		{
    			$fichier_manquant .= 'ARTICLES.TXT ';
    		}
    		$log_string = ' Site Transport | '.mdate('%Y-%m-%d %H:%i:%s').' | Fichier absent ou de taille nulle : '.$fichier_manquant."\n";
    	}
    	
    	write_file($fichier_log, $log_string, FOPEN_WRITE_CREATE);
    	
    }
    
    
    
    public function import_anciennes_commandes()
    {
        $ip_autorise = array(
            '81.252.124.194',
            '46.18.195.156',
            '127.0.0.1'
        );
        
        if (in_array($_SERVER['REMOTE_ADDR'], $ip_autorise))
        {
            $this->load->models(array('medias/media', 'catalogue/medias_produit', 'catalogue/produits_produit', 'boutique/commande', 'boutique/produits_commande'));
            
            $dossier_import = scandir(GENERAL_ROOTPATH.'echanges/import/ancien');
            $fichier_log = root_path().'logs/import_anciennes_factures.log';
            $fichier_produits = FALSE;
            $fichier_tarifs = FALSE;
            
            $module = $this->module_catalogue->get(1);
            
            $fichier_factures 	= GENERAL_ROOTPATH.'echanges/import/FACTURES.TXT';
            
            /*	$content = file_get_contents($fichier_factures);
             $fp = fopen($fichier_factures, 'w');
             fwrite($fp, iconv("UTF-16LE", "UTF-8", $content));
             fclose($fp);
             */
            
            if ($this->test_fichier_exists($fichier_factures))
            {
                $lignes_factures = $this->csvreader->parse_file($fichier_factures, FALSE, FALSE);
                
                if (count($lignes_factures) > 0)
                {
                    $i = 0;
                    $this->benchmark->mark('code_start');
                    
                    $ref_commande_actuelle = NULL;
                    foreach ($lignes_factures as $index => $ligne)
                    {
                        
                        $client = new Client();
                        $client->get_by_code($ligne[0]);
                        if ($client->exists())
                        {
                            if ($ligne[2] == $ref_commande_actuelle)
                            {
                                $commande = new Commande();
                                $commande->get_by_reference($ref_commande_actuelle);
                                
                                $commande->montant += $ligne[8]*(1+($ligne[9]/100)); //Tarif de la commande = somme des montants des produit
                                
                                $commande->save();
                            }
                            else
                            {
                                //Nouvelle commande
                                $commande = new Commande();
                                
                                //Test si la commande existe déjà sur le site
                                $commande_test = new Commande();
                                $commande_test->like('reference',  $ligne[2], 'before')->get();
                                if ($commande_test->exists())
                                {
                                    $commande->id = $commande_test->id;
                                }
                                
                                
                                $commande->utilisateur_id = $client->id;
                                $commande->created = $ligne[1];
                                $commande->reference = $ligne[2];
                                $commande->etat = 4;
                                $commande->montant = $ligne[8]*(1+($ligne[9]/100));
                                
                                $commande->save();
                                
                                $ref_commande_actuelle = $ligne[2];
                                
                            }
                            
                            //Insertion du produit commandé avec la ligne en cours
                            $produit = new Produit();
                            $produit->get_by_reference($ligne[3]);
                            //$produit->check_last_query();
                            
                            if ($produit->exists())
                            {
                                //Supprime les anciens produits commande de la commande
                                $produit_commande_suppr = new Produits_commande();
                                $produit_commande_suppr->where('produit_id',$produit->id)->get_by_commande_id($commande->id);
                                $produit_commande_suppr->delete();
                                
                                $produit_commande = new Produits_commande();
                                $produit_commande->commande_id = $commande->id;
                                $produit_commande->produit_id = $produit->id;
                                $produit_commande->libelle = $produit->libelle;
                                $produit_commande->reference = $ligne[3];
                                $produit_commande->prix = ($ligne[6] > 0) ? ((($ligne[5]) - (($ligne[5])*($ligne[6]/100)))*(1+($ligne[9]/100))) : ($ligne[5]*(1+($ligne[9]/100)));
                                $produit_commande->prix_sans_remise = $ligne[5]*(1+($ligne[9]/100));
                                $produit_commande->tva = $ligne[9];
                                $produit_commande->quantite = $ligne[7];
                                
                                $produit_commande->save();
                            }
                        }
                    }
                    
                    //rename(root_path().'echanges/import/ARTICLES.TXT', root_path().'echanges/import/archives/ARTICLES.TXT');
                    
                    $this->benchmark->mark('code_end');
                    $temps_traitement = $this->benchmark->elapsed_time('code_start', 'code_end');
                    
                    //Log OM
                    $this->log->enregistrer(array(
                        'utilisateur_id' => 1,
                        'type'			=> LOG_IMPORT,
                        'description'	=> $this->lang->line('log_import_catalogue'),
                        'adresse_ip'	=> $this->input->ip_address(),
                        'user_agent'	=> $this->input->user_agent()
                    ));
                    
                    //Log fichier
                    $log_string = ' Stadium | '.mdate('%Y-%m-%d %H:%i:%s').' | '.$temps_traitement.'s | '.$i.' commandes'."\n";
                }
                else
                {
                    $log_string = ' Stadium | '.mdate('%Y-%m-%d %H:%i:%s').' | Aucune commande dans le fichier'."\n";
                }
                
            }
            else
            {
                $fichier_manquant = NULL;
                if (!$this->test_fichier_exists($fichier_produits))
                {
                    $fichier_manquant .= 'FACTURES.TXT ';
                }
                $log_string = ' Stadium | '.mdate('%Y-%m-%d %H:%i:%s').' | Fichier absent ou de taille nulle : '.$fichier_manquant."\n";
            }
            
            write_file($fichier_log, $log_string, FOPEN_WRITE_CREATE);
        }
    }
    
    /**
     * Fonction corrective pour supprimer les t_produits_commandes en trop ! (MEM 04/07/2017)
     */
    public function corrige_commandes()
    {
    	// sélectionner l'ensemble des ensembles (tout sauf id!)
    	// pour chaque enregistrement ne garder qu'une seule ligne
    	// pour chaque ligne faire un select 
    	// lire le premier
    	// faire un delete des suivants
    	
    	//Désactive l'historique des requêtes
    	$this->db->save_queries = FALSE;
    	set_time_limit(1200); //20min
    	ini_set('memory_limit', '128M');
    	
    	//$this->load->models(array('medias/media', 'catalogue/medias_produit', 'catalogue/produits_produit', 'boutique/commande', 'boutique/produits_commande'));
    	//$query = new Produits_Commande();
    	$i = 0;
    	$this->load->dbutil();
    	$sql = "select t_pc.nb, t_pc.commande_id, t_pc.produit_id,t_pc.libelle, t_pc.quantite, t_pc.reference, t_pc.prix, t_pc.options from 
    			(	select count(*) as nb, t_produits_commandes.commande_id, t_produits_commandes.produit_id, t_produits_commandes.options, t_produits_commandes.libelle, t_produits_commandes.quantite, t_produits_commandes.reference, t_produits_commandes.prix 
					from t_produits_commandes 
					where 1 
					group by t_produits_commandes.commande_id, t_produits_commandes.produit_id, t_produits_commandes.options, t_produits_commandes.libelle, t_produits_commandes.quantite, t_produits_commandes.reference, t_produits_commandes.prix) as t_pc
				where t_pc.nb>1 ";
				//and t_pc.commande_id=6828";
    	$query = $this->db->query($sql);
    	$produits_commandes = $query->result();
    	//var_dump($produits_commandes);
    	foreach ($produits_commandes as $produit_commande)
    	{
    		//echo "Ligne ".$produit_commande->commande_id." - ".$produit_commande->produit_id." - ".$produit_commande->reference." ---- \n";
    		
    		
    		$sql2 = "select * from t_produits_commandes where commande_id=".$produit_commande->commande_id." and produit_id=".$produit_commande->produit_id. " and 
    				quantite=".$produit_commande->quantite." and prix=".$produit_commande->prix." and options='".$produit_commande->options."' and reference='".$produit_commande->reference."'";
    		//var_dump($sql2);
    		$query2 = $this->db->query($sql2);
    		//var_dump($query2);
    		$produits_commandes2 = $query2->result();
    		foreach ($produits_commandes2 as $produit_commande2)
    		{
    			$id_produit_commande = $produit_commande2->id;
    			// requête delete
    			$sql_delete = "delete from t_produits_commandes where id<>".$produit_commande2->id." and commande_id=".$produit_commande->commande_id." and 
    					produit_id=".$produit_commande->produit_id. " and quantite=".$produit_commande->quantite." and 
    					prix=".$produit_commande->prix." and options='".$produit_commande->options."' and reference='".$produit_commande->reference."'";
    			echo $sql_delete."\n";
    			
    			$query_delete = $this->db->query($sql_delete);
    			break;
    		}
    		
    		$i++;
    		if ($i==1000) {break;}
    	}
    	echo "Nombres d'enregistrement : ".$i;
    }
    
    /**
     *  Mise à jourt des métas pour le référencement
     */
    public function maj_metadonnees() 
    {
    	$produits = $this->produit->get();
    	foreach ($produits as $produit)
    	{
    		$categories  = $produit->categories_associees()->all_to_single_array('libelle');
    		$tab_categories_reset = array_values($categories);
    		
    		if (count($tab_categories_reset) > 2)
    		{
    			unset($tab_categories_reset[1]);
    		}
    		
    		$texte_categories = implode(', ', $tab_categories_reset);
    		
    		$produit->seo_titre = 'Acheter '.$produit->libelle.' Stadium '.$texte_categories;
    		$produit->seo_description = truncate($produit->description, 100, ' ').' '.$produit->libelle.' Stadium';
    		$produit->save();
    	}
    }
    
    public function fusion_criteres()
    {
        $criteres_valeurs = new Criteres_valeur();
        $criteres_valeurs->where('critere_id', 1)->group_by('valeur')->get();
        
        foreach($criteres_valeurs as $valeur)
        {
            $criteres_valeurs_supprimer = new Criteres_valeur();
            $criteres_valeurs_supprimer->where('critere_id', 1)->where('id !=', $valeur->id)->where('valeur', $valeur->valeur)->get();
            
            foreach($criteres_valeurs_supprimer as $valeur_supprimer)
            {
                $criteres_valeurs_produit = new Criteres_valeurs_produit();
                $criteres_valeurs_produit->where('criteres_valeur_id', $valeur_supprimer->id)->get();
                
                foreach($criteres_valeurs_produit as $produit)
                {
                    $produit->criteres_valeur_id = $valeur->id;
                    $produit->save();
                }
                $valeur_supprimer->delete();
            }
        }
    }
}
/* End of file abelia.php */
/* Location: ./modules/abelia/controllers/abelia.php */