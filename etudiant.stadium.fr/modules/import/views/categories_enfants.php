<?php
if ($categories !== FALSE)
{
	$html = NULL;
	foreach ($categories as $racine => $node)
	{
	    $html .= '<li>';
		$this->categorie->clear();
		$categorie = $this->categorie->get_by_id($node['__id']);
		
		$url =  $categorie->url($section);
		
        if ($afficher_media)
        {
            $media = html_media($categorie->medias_associes());
            $html .= anchor($url, $media, 'class="media"'); 
        }
		$html .= anchor($url, '<span class="glyphicon glyphicon-play jaune"></span> '.$categorie->libelle, 'class="libelle hvr-underline-from-left"');
        $html .= '</li>';
	}
}

echo $html;