<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//Etape 5 spécifique
$route['commande-compte.html']								= 'boutique/commandes/paiement_compte';
$route['commande-cheque.html']								= 'boutique/commandes/paiement_cheque';
$route['boutique/enregistrer-commande.html']				= 'boutique/commandes/enregistrer_devis';

$route['medaille-des-sapeurs-pompiers.html'] = 'page/afficher/boutique/section/3';
$route['faluches-insignes-calots.html'] = 'page/afficher/boutique/section/3';
$route['medaille-travail.html'] = 'page/afficher/boutique/section/3';
$route['medaille-travail-secteur-prive.html'] = 'page/afficher/boutique/section/3';
$route['armes-personnelles.html'] = 'page/afficher/boutique/section/3';
$route['faluches-calots.html'] = 'page/afficher/boutique/section/3';
$route['competence-personnelle.html'] = 'page/afficher/boutique/section/3';
$route['insignes-medailles-militaires.html'] = 'page/afficher/boutique/section/3';
$route['discipline.html'] = 'page/afficher/boutique/section/3';
$route['chiffres-lettres.html'] = 'page/afficher/boutique/section/3';
$route['fin-annee.html'] = 'page/afficher/boutique/section/3';
$route['accessoires.html'] = 'page/afficher/boutique/section/3';
$route['medaille-travail-secteur-prive-collection-monnaie-paris-argent-massif.html'] = 'page/afficher/boutique/section/3';
$route['medaille-regionale-departementale-communale-secteur-public.html'] = 'page/afficher/boutique/section/3';
$route['ecrin-pour-medaille.html'] = 'page/afficher/boutique/section/3';
$route['bacs.html'] = 'page/afficher/boutique/section/3';
$route['medaille-travail-secteur-prive-collection-stadium-fabrication-francaise-bronze.html'] = 'page/afficher/boutique/section/3';
$route['ecussons.html'] = 'page/afficher/boutique/section/3';
$route['tete-asso-corpo.html'] = 'page/afficher/boutique/section/3';
$route['collection-mairie.html'] = 'page/afficher/boutique/section/3';
$route['medailles-civiles.html'] = 'page/afficher/boutique/section/3';
$route['medaille-travail-secteur-prive-collection-stadium-bronze.html'] = 'page/afficher/boutique/section/3';
$route['collection-generale.html'] = 'page/afficher/boutique/section/3';
$route['medaille-des-sapeurs-pompiers.html'] = 'page/afficher/boutique/section/3';
$route['insignes.html'] = 'page/afficher/boutique/section/3';
$route['gastronomie.html'] = 'page/afficher/boutique/section/3';
$route['drapeaux-villes.html'] = 'page/afficher/boutique/section/3';
$route['medaille-regionale-departementale-communale-secteur-public-collection-stadium-bronze.html'] = 'page/afficher/boutique/section/3';
$route['potager.html'] = 'page/afficher/boutique/section/3';


/* End of file routes.php */
/* Location: ./application/config/routes.php */