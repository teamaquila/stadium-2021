$(document).ready(function(){
	if($("#liste-produits").length)
	{
		$( "#liste-produits .produits" ).each(function( index ) {
			var item = $(this);
			var id = item.data('id');
			/*var section = item.data('section');
			var categorie = item.data('categorie');*/
			$.ajax({
		        url: '/catalogue/catalogue/mise_en_place_enfants/'+id,
		        type: 'POST',
		        data: {},
		        success : function(retour){
		        	var html = jQuery.parseJSON(retour);
		        	item.children('div').find('.boutique').replaceWith(html.html);
		        },
		    });
		});
	}
});