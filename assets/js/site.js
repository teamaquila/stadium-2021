$(document).ready(function(){

	/**
	 * Début Menu
	 */
	var $menu = $("nav#mmenu").mmenu({
		"extensions": [
             "pagedim-black",
             "effect-menu-slide",
             "effect-panels-slide-0",
             "multiline",
            // "shadow-page",
        ],

         //columns: true, //Pour faire comme Oméga
         // slidingSubmenus : false //Si on veut que les sous menus s'affichent en dessous 
         /* A activer si on veut le menu qui sort de la droite 
          * "offCanvas": {
   	 		"position": "right"
        	}*/
	 }, {
	   clone: true,
	});
	resize_burger($menu);
	window.onresize = function(event){
		resize_burger($menu);
	};
	/**
	 * Fin Menu
	 */
	
	/*Taille Fichiers Formulaire*/
	$('input[type=file]').bind('change', function() {
		$(this).next('.taille-fichier').attr('data-size', this.files[0].size);
		$(this).next('.taille-fichier').text('Taille : '+returnFileSize(this.files[0].size));
		 
		var total=0;
		$(".taille-fichier").each(function( index ) {
			if($(this).attr('data-size')){
				total = total + parseInt($(this).attr('data-size'));
			}
		});
		 
		if(total>5242880){
			 $('.taille-envoyee').addClass('taille-depasser');
		}
		else{
			 $('.taille-envoyee').removeClass('taille-depasser');
		}
		$('.taille-envoyee').html('Taille totale : ' + returnFileSize(total) + '<span class="taille-limite"> sur 5 Mo</span>');
	
	});	 
	
	//Afficher Forumulaire de recherche
	$('#toggle_recherche').on('click', function(event){
		//$('#recherche').slideToggle();
		
			$(this).popover('destroy');
			$(this).popover({
				content: $('#widget_recherche').html(),
				html: true,
				placement: 'left'
			}).popover('show');
			
			//$('.popover-content form').toggle();
			$('.moteur-recherche.google_suggest').typeahead({
				hint: false,
				highlight: true,
				source: function(query, process){
					$.ajax({
						url: '/catalogue/rechercher',
						dataType: 'json',
						data: {
							requete: query
						},
						beforeSend: function(){
						},
						success: function(data){
							if (!data.erreur){
								process( data );	
							}
							
						}
					});
				},
				fitToElement : true,
				autoSelect : false,
				items: 'all',
				matcher : function(){
					return true;
				},
				updater : function(item){
					if(item.value != 'autres'){
						$('.moteur-recherche').val(item.name);
						$('#recherche').submit();
						return false;
					}
					else{
						$('#recherche').submit();
						return false;
					}
				},
				minLength: 2
			});
	});
		
	//Nouvel onglet
	$('a._blank').click(function(){
		window.open(this.href, '_blank');
		return false;
	});

	
	/**
	 * Choisir une référence 
	 */
	$(document).off('change', 'select[name=produit_id]');
	$(document).on('change', 'select[name=produit_id]', function(e){
		var _this = $(this);
		$.getJSON('/catalogue/catalogue/reference/'+$(this).val(), function(data){
			_this.parents('form.ajouter-panier').find('.prix-vente').html(data.prix.vente);
			_this.parents('form.ajouter-panier').find('.poids').html(data.poids);
			_this.parents('form.ajouter-panier').find('.stock').html(data.stock);
			_this.parents('form.ajouter-panier').find('input[name="quantite"]').val(1);
			
			//Modification de l'image avec celle de l'enfant
			if (data.images){
				$('.module-catalogue .bloc_diaporama').html(data.images);
				//Diaporama fiche produit
				$('.diaporama_produit').bxSlider({
					 pagerCustom: '#bx-pager',
					 controls : false,
				});
			}

			$('#resume_produit').remove();
			$('.description').empty();
			//Affichage d'infos sur le produit enfant dans le champ résumé
			if (data.resume)
			{
				$('.description').append('<div id="resume_produit">'+data.resume+'</div>');
			}
			
			if ($('#bloc_commande').length){
				if (data.formulaire){
					$('#bloc_commande').removeClass('sans_formulaire');
					$('#bloc_commande').addClass('avec_formulaire');
					
					if (!$('.bloc_formulaire').length){
						$('#bloc_commande .bloc_choix_produit').after('<div class="bloc_formulaire">'+
								'<div><label for="">Gravure :</label>'+
								'<input type="hidden" id="produit_referent_gravure" name="options[produit_referent_id]" value="'+_this.val()+'">'+
								'<textarea name="options[gravure_complementaire]" placeholder="Initiale du prénom + nom + promo "></textarea>'+
								'</div>'+
								
								'<div class="zone-btn-fichier"><label for="">Fichier :</label><div id="upload-fichier">'+
								'<input type="hidden" id="fichier_gravure" name="options[associe_gravure]" />'+
								'<span class="fileinput-button">'+
//									'<span>Ajouter votre fichier</span>'+
									'<input type="file" name="import_fichier" id="import_fichier">'+
								'</span><span class="info-format">(format .xls ou .xlsx)</span>'+
								'<span class="indicateur"></span><div id="bloc_import_fichier"></div></div>'+
						'<a class="btn btn-primary fichier-exemple" href="/medias/fichier-gravure/matrice.xlsx" target="_blank">Fichier d\'exemple</a><br class="clear-both"></div>');
						
						/**
						 * Ajouter un fichier à une gravure
						 */
						$('#import_fichier').fileupload({
						 	url: '/catalogue/envoyer_fichier',
						 	dataType: 'json',
						 	add: function(e, data){ data.submit(); },
						 	send: function(){
						 		$('#import_fichier').bind('fileuploadprogressall', function(e, data) {
									var progress_value = parseInt(data.loaded / data.total * 100, 10);
									$('.indictateur').html(progress_value + '%');
								});
						 	},
					        done: function(e, data){
					        	var data = data.result;
					        	if (!data.erreur) {
					        		$('.fileinput-button').fadeOut(400, function(){
					        			$('input#fichier_gravure').val(data.file_name);
					        			$('.info-format').remove();
					            		$('#bloc_import_fichier').html('<span class="nom_fichier">'+data.file_name+'</span><a href="'+data.delete_file+'" class="supprimer-fichier">Supprimer</a>');
					        		});
					        	}
					        	else {
					        		alert(data.erreur);
					        	}
					        }
					    });
					}
				}else{
					
					$('#bloc_commande').removeClass('avec_formulaire');
					$('#bloc_commande').addClass('sans_formulaire');
					$('#bloc_commande .bloc_formulaire').remove();
					/*
					var produit_referent_id = _this.val();
					if ($('.produits-associes .associe_gravure').length){
						$('.produits-associes .associe_gravure input.produit_referent_id').each(function(index){
							$(this).val(produit_referent_id);
						});
					}*/
				}
			}
			
		});
		e.preventDefault();
	});
	
	
	if($('.produit#produit-161').length)
	{
		$('#bloc_commande').removeClass('sans_formulaire');
		$('#bloc_commande').addClass('avec_formulaire');
		
		if (!$('.bloc_formulaire').length){
			$('#bloc_commande .bloc_choix_produit').after('<div class="bloc_formulaire">'+
					'<div><label for="">Gravure :</label>'+
					'<input type="hidden" id="produit_referent_gravure" name="options[produit_referent_id]" value="161">'+
					'<textarea name="options[gravure_complementaire]" placeholder="Initiale du prénom + nom + promo "></textarea>'+
					'</div>'+
					
					'<div class="zone-btn-fichier"><label for="">Fichier :</label><div id="upload-fichier">'+
					'<input type="hidden" id="fichier_gravure" name="options[associe_gravure]" />'+
					'<span class="fileinput-button">'+
	//					'<span>Ajouter votre fichier</span>'+
						'<input type="file" name="import_fichier" id="import_fichier">'+
					'</span><span class="info-format">(format .xls ou .xlsx)</span>'+
					'<span class="indicateur"></span><div id="bloc_import_fichier"></div></div>'+
			'<a class="btn btn-primary fichier-exemple" href="/medias/fichier-gravure/matrice.xlsx" target="_blank">Fichier d\'exemple</a><br class="clear-both"></div>');
			
			/**
			 * Ajouter un fichier à une gravure
			 */
			$('#import_fichier').fileupload({
			 	url: '/catalogue/envoyer_fichier',
			 	dataType: 'json',
			 	add: function(e, data){ data.submit(); },
			 	send: function(){
			 		$('#import_fichier').bind('fileuploadprogressall', function(e, data) {
						var progress_value = parseInt(data.loaded / data.total * 100, 10);
						$('.indictateur').html(progress_value + '%');
					});
			 	},
		        done: function(e, data){
		        	var data = data.result;
		        	if (!data.erreur) {
		        		$('.fileinput-button').fadeOut(400, function(){
		        			$('input#fichier_gravure').val(data.file_name);
		        			$('.info-format').remove();
		            		$('#bloc_import_fichier').html('<span class="nom_fichier">'+data.file_name+'</span><a href="'+data.delete_file+'" class="supprimer-fichier">Supprimer</a>');
		        		});
		        	}
		        	else {
		        		alert(data.erreur);
		        	}
		        }
		    });
		}
	}
	
	//Diaporama fiche produit
	if( $('.diaporama_produit').length){
		$('.diaporama_produit').bxSlider({
			 pagerCustom: '#bx-pager',
			 controls : false,
			 auto: ($(".diaporama_produit li").length > 1) ? true: false,
		});
	}
	
	//Modification de la gravure dans le panier etape 1, afficher ou non les champs
	$(document).on("click", '.modification_gravure a.toggle_modif', function(event){
		$(this).siblings('.zone-modifie-options').slideToggle();
		
		/**
		 * Ajouter un fichier à une gravure
		 */
		$('.import_fichier').fileupload({
		 	url: '/catalogue/envoyer_fichier',
		 	dataType: 'json',
		 	add: function(e, data){ data.submit(); },
		 	send: function(){
		 		$('.import_fichier').bind('fileuploadprogressall', function(e, data) {
					var progress_value = parseInt(data.loaded / data.total * 100, 10);
					$('.indictateur').html(progress_value + '%');
				});
		 	},
	        done: function(e, data){
	        	var data = data.result;
	        	if (!data.erreur) {
	        		$('.fileinput-button').fadeOut(400, function(){
	        			$('input.fichier_gravure').val(data.file_name);
	            		$('.bloc_import_fichier').html('<span class="nom_fichier">'+data.file_name+'</span><a href="'+data.delete_file+'" class="supprimer-fichier">Supprimer</a>');
	        		});
	        	}
	        	else {
	        		alert(data.erreur);
	        	}
	        }
	    });
	});
	
	//Valide les modifications des options dans l'étape 1 (gravure)
	$(document).on("click", '.save_modif_options', function(event){
		$.post('/boutique/paniers/maj_options/'+$(this).data('row_id'), {'texte' : $(this).parents('.modification_gravure').find('textarea').val(),  'fichier' : $(this).parents('.modification_gravure').find('input.fichier_gravure').val()}, function(data){
			var retour = $.parseJSON(data);
			
			if (retour.erreur == 'false'){
				window.location.reload();	
			}else{
				alert(retour.message);
			}
			
		});
	});
	
	/**Changement adresse formulaire étape 4 si paiement compte*/
	$('#choix_mode_paiement input').on('change', function(){

		$('#choix_mode_paiement label').addClass('non_active');
		$('#choix_mode_paiement label').removeClass('active');
		$(this).parents('td').find('label').removeClass('non_active');
		$(this).parents('div').find('label').addClass('active');
		
		$(this).parents('form').attr('action', $(this).parents('form').data('action_'+$(this).val()));
		
		if($(this).val() == "compte" || $(this).val() == "cheque"){
			$('div#choix-paiement').css('display', 'none');
		}else{
			$('div#choix-paiement').css('display', 'block');
		}
	});
	
	//Suppression d'un fichier associé à une gravure
	$(document).on('click', 'a.supprimer-fichier', function(event){
		var bouton = $(this);
		$.post($(this).attr('href'), function(data){
			$('.info-format').remove();
			bouton.parent('#bloc_import_fichier').replaceWith('<span class="fileinput-button">'
				+'<span>Ajouter votre fichier</span>'
				+'<input class="import_fichier" type="file" name="import_fichier">'
				+'</span><span class="info-format">(format .xls ou .xlsx)</span><br><br>');
			
			/**
			 * Ajouter un fichier à une gravure
			 */
			$('#import_fichier').fileupload({
			 	url: '/catalogue/envoyer_fichier',
			 	dataType: 'json',
			 	add: function(e, data){ data.submit(); },
			 	send: function(){
			 		$('#import_fichier').bind('fileuploadprogressall', function(e, data) {
						var progress_value = parseInt(data.loaded / data.total * 100, 10);
						$('.indictateur').html(progress_value + '%');
					});
			 	},
		        done: function(e, data){
		        	var data = data.result;
		        	if (!data.erreur) {
		        		$('.fileinput-button').fadeOut(400, function(){
		        			$('input#fichier_gravure').val(data.file_name);
		            		$('#bloc_import_fichier').html('<span class="nom_fichier">'+data.file_name+'</span><a href="'+data.delete_file+'" class="supprimer-fichier">Supprimer</a>');
		        		});
		        	}
		        	else {
		        		alert(data.erreur);
		        	}
		        }
		    });
		});
		return false;
		e.preventDefault();
	});
	
	
	//On rafraichit le nombre d'articles Panier toutes les 10 secondes
	rafraichir_panier();
	setInterval(rafraichir_panier, 10000);
});



function returnFileSize(number) {
	if(number < 1024) {
		return number + ' octets';
	} else if(number > 1024 && number < 1048576) {
		return (number/1024).toFixed(1) + ' Ko';
	} else if(number > 1048576) {
		return (number/1048576).toFixed(1) + ' Mo';
	}
}

/**
 * Gère le redimentionnement de la page pour afficher menu burger ou menu basique
 * @param $menu
 */
function resize_burger($menu)
{
	//Ouverture d'un sous menu
	if ($( window ).width() < 1054)
	{
		 var $icon = $("#ouvre-menu-burger");
		 var API = $menu.data( "mmenu" );

		 $icon.on( "click", function() {
		    API.open();
		    setTimeout(function() {
			       $icon.addClass( "is-active" );
			    }, 100);
		 });

		 API.bind( "closed", function() {
		    setTimeout(function() {
		       $icon.removeClass( "is-active" );
		    }, 100);
		 });
		 
		  
		 //Vu que le menu est en burger même en pc, il faut gérer le hover
		 //AJOUTER SI MENU TOUJOURS EN BURGER
		/*$('.ouvre-sous-menu').hover(function(event){
			$(this).parents('.mobimenu').find('li').removeClass('hover');
			$(this).addClass('hover');
		}, function(event){
			$(this).parents('.mobimenu').find('li').removeClass('hover');
			$(this).removeClass('hover');
		});*/
		
		 $(document).on('click', '.lib-filtre', function(e){
			 	if($(".lib-filtre").hasClass("opened"))
			 	{
			 		$(".lib-filtre").removeClass('opened');
			 	}
			 	else
			 	{
			 		$(".lib-filtre").addClass('opened');
			 	}
			});	
	}
	else
	{
		$('.mobimenu').find('ul.sous_categorie-3').remove();
		$('.ouvre-sous-menu').off();
		$('.ouvre-sous-menu').hover(function(event){
			$(this).parents('.mobimenu').find('li').removeClass('hover');
			$(this).addClass('hover');
			$(this).parents('.mobimenu').find('ul.niveau-2').hide();
			$(this).children('ul').slideToggle();
		}, function(event){
			$(this).parents('.mobimenu').find('li').removeClass('hover');
			$(this).parents('.mobimenu').find('ul.niveau-2').hide();

			$(this).removeClass('hover');
		});
	}
}

function rafraichir_panier()
{
	$.post('/boutique/paniers/get_nb_articles', function(data){
		$('#bt-panier').children('.nb-article').text(data);
	});
}