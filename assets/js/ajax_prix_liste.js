$(document).ready(function(){
	if($("#liste-produits").length)
	{
		$( "#liste-produits .produits" ).each(function( index ) {
			var item = $(this);
			var id = item.data('id');
			/*var section = item.data('section');
			var categorie = item.data('categorie');*/
			$.ajax({
		        url: '/catalogue/catalogue/mise_en_place_enfants_stadium/'+id,
		        type: 'POST',
		        data: {},
		        success : function(retour){
		        	var html = jQuery.parseJSON(retour);
		        	if(html.result.tarif.prix)
		        	{
		        		item.children('div').children('.tarification').find('.prix').html(html.result.tarif.prix);
		        	}
		        	else
		        	{
		        		item.children('div').children('.tarification').children('span').html('Prix : <span class="prix gratuit">Gratuit</span>');
		        	}
		        	
		        },
		    });
		});
	}
});